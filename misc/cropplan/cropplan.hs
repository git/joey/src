{- Crop rotation planner, core data structures and functions.
 -
 - Copyright 2012 Joey Hess <joey@kitenet.net>
 - 
 - Licensed under the GNU AGPL version 3 or higher.
 -}

import System.Environment
import Control.Applicative
import Control.Monad
import Data.List
import Data.Function
import qualified Data.Set as S
import Data.Set (Set)
import qualified Data.Map as M
import Data.Map (Map)

data Bed = Bed
	{ bedName :: String
	, bedSize :: SqFt
	, bedOptimal :: Set Tag
	, bedHistory :: HistoryMap
	}

type HistoryMap = Map Year (Set Crop)
type SqFt = Float
type Year = Int
type Crop = String
data Tag = Family String | Property String
type CropTags = Map Crop (Set Tag)

data Condition = WhenCondition Val Cmp Val | UnlessCondition Val Cmp Val
data Cmp = CmpEqual | CmpGreaterEqual | CmpLessEqual | CmpGreater | CmpLess
data Val = ValNum Int
	| YearsSinceCrop Crop | YearsSinceTag Tag
	| OptimalCrop Crop | OptimalTag Tag

type Score = Float
type ScoreAdjustment = Condition -> (Score -> Score)

type WarningCondition = Condition -> String
type WarningMsg = String

type Layout = [(Bed, Crop)]

data Input = Input
	{ beds :: [Bed]
	, want :: Map SqFt Crop
	, cropTags :: CropTags
	, scoreAdjustments :: [ScoreAdjustment]
	, warningConditions :: [WarningCondition]
	)

main = do
	input <- parseInput <$> readInput
	layouts <- calcLayouts input
	let l = reverse $ sortBy (compare `on` snd) $
		zip layouts (map (calcScore input) layouts)
	forM layouts $ \(score, l) -> do
		showLayout l
		putStrLn $ "score = " ++ show score
		putStrLn ""

showLayout :: Layout -> IO ()
showLayout l = forM l $ \(bed, crop) -> do
	putStrLn $ unwords [ crop, "in bed", bed ]
	mapM_ printWarningMsg $ calcWarnings input bed crop

printWarningMsg :: WarningMsg -> IO ()
printWarningMsg msg = putStrLn $ " warning: " ++ msg

calcLayouts :: Input -> [Layout]
calcLayouts input = undefined

calcWarnings :: Input -> Bed -> Crop -> [WarningMsg]
calcWarnings input bed crop = undefined

calcScore :: Input -> Layout -> Score
calcScore input layout = undefined

readInput :: IO String
readInput = getArgs >>= go
  where
	go [] = getContents
	go (file:_) = readFile file

parseInput :: String -> Input
parseInput s = undefined
