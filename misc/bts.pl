#!/usr/bin/perl -w
# Announce incoming bugs. To be run on master.

# Local configuration.
my $numfile=shift || '/var/lib/debbugs/spool/nextnumber';
my $server=shift || 'agony.evcom.net';
my @channels=('#debian-devel','#debian');
my $nick=shift || "bts";
my $ircname=shift || "Debian BTS Announcement bot";
my $highestbug=shift || 100000; # don't display any past here.

# Look up what the currently filed highest bug is.
my $highbug=get_current_bug();
if ($highbug >= $highestbug) {
	die "Exiting because $highestbug has been reached already.\n";
}

use strict;
use Net::IRC;

my $irc=Net::IRC->new;
my $conn=$irc->newconn(
	Nick => $nick,
	Server => $server,
	Ircname => $ircname,
) || die $!;
$conn->add_handler('376', \&on_connect); # 376 = end of MOTD: we're connected.
$conn->add_handler('kick', \&on_kick);
$conn->add_handler('msg', \&on_msg);

while (1) {
	$irc->do_one_loop;
	sleep(1);
	my $bug=get_current_bug();
	if ($bug > $highbug) {
		if ($bug >= $highestbug) {
			$bug=$highestbug; # race hack
		}
		$highbug=$bug;
		my $message=buginfo($bug);
		print STDERR "$message\n";
		foreach my $chan (@channels) {
			$conn->privmsg($chan,$message);
		}
		if ($highbug >= $highestbug) {
			$conn->quit("now somebody go fix it");
			print STDERR "high bg reached";
			while (1) {
				sleep(100);
			}
		}
	}
	elsif ($bug < $highbug) {
		warn "wtf? bug number just decreased!";
	}
}

# What to do when the bot successfully connects.
sub on_connect {
	my $self=shift;
	my $chan;
	
	foreach my $chan (@channels) {
		$self->join($chan);
	}
}

# Auto-join on kick
sub on_kick {
	my $self=shift;
	my $event=shift;
	my $from=$event->nick;
	my @stuff=$event->args;
	my $chan=$stuff[0];
	my $reason=$stuff[1];

	$self->join($chan);
}

sub on_msg {
        my $self=shift;
        my $event=shift;
        
        my $from=$event->nick;
        $self->privmsg($from,"current highest bug is: ".buginfo(get_current_bug()));
}

sub get_current_bug {
	open (IN, $numfile) || die "$numfile: $!";
	my $ret=<IN>;
	chomp $ret;
	close IN;
	return $ret - 1;
}

sub buginfo {
	my $bug=shift;
	for (1..10) {
		last if open (IN, "/var/lib/debbugs/spool/db/$bug.status");
		sleep(1);
	}
	my $submitter=<IN>;
	chomp $submitter;
	<IN>;
	my $subject=<IN>;
	chomp $subject;
	<IN>;
	my $package=<IN>;
	chomp $package;
	close IN;
	$subject=~s/^\s*\Q$package\E\s*:?//;
	return "[#$bug] $package: $subject";
}

# All this and I still doen't understand IRC. Scaaaarey! -- Joey
