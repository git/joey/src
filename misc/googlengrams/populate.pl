#!/usr/bin/perl
# Populates a xapian db with google ngram data read from stdin.
# Each ngram is split into a prefix and suffix.
# The suffix is then indexed by the prefix.

use warnings;
use strict;
use Search::Xapian;
use Search::Xapian::WritableDatabase;

my $length=shift;
if (! defined $length) {
	die "usage: populate.pl n < ngrams\n";
}

my $db=Search::Xapian::WritableDatabase->new("xapiandb",
	Search::Xapian::DB_CREATE_OR_OPEN());

my $c=0;
my $oldngram="";
while (<>) {
	my ($ngram)=/^([^\t]+)\t/;
	#print "bad line: $_" unless defined $ngram;
	next unless defined $ngram;
	next if $ngram eq $oldngram;
	$oldngram=$ngram;
		
	if ($c++ == 100000) {
		$c=0;
	}

	# Generate all possible prefixes of the ngram.
	foreach my $len (1..$length-1) {
		my ($prefix, $suffix) = $ngram =~ m/^((?:[^ ]+ ){$len})(.+)$/;
		next unless defined $prefix && defined $suffix;
		$prefix=~s/ $//;

		if ($c == 1) {
			print "  $prefix => $suffix\n";
		}
	
		my $doc=Search::Xapian::Document->new();
		$doc->add_term($prefix);
		$doc->set_data($suffix);
		$db->add_document($doc);
	}
}
