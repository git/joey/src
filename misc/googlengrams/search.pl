#!/usr/bin/perl

use warnings;
use strict;
use Search::Xapian;

my $db=Search::Xapian::Database->new("xapiandb");
my $enq = $db->enquire( shift );

my $c=0;
my @matches;
do {
	@matches = $enq->matches($c, $c+1000);
	print $_->get_document->get_data()."\n" foreach @matches;
	$c=$c+1000;
} while @matches;
