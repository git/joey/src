#!/usr/bin/perl

use warnings;
use strict;
use Search::Xapian;

my $ngram=shift;
$|=1;
print $ngram." ";

my $db=Search::Xapian::Database->new("xapiandb");

while (1) {
	my $enq = $db->enquire($ngram);

	my @matches;
	my $c=0;
	my @m;
	do {
		@m = $enq->matches($c, $c+100);
		$c=$c+100;
		push @matches, map { $_->get_document->get_data } @m;
	} while @m;
	last unless @matches;

	my $word=$matches[rand $#matches];
	print $word." ";

	$ngram=~s/[^ ]+ //;
	$ngram.=" ".$word;
}

print "\n";
