#!/usr/bin/perl

use warnings;
use strict;
use Search::Xapian;

my $max=5;
my @dbs;
my %stats;
initdbs();
$|=1;

if (@ARGV && $ARGV[0] eq '-i') {
	shift @ARGV;
	ircbot(@ARGV);
}
if (@ARGV) {
	# one-shot mode
	chainfrom(shift);
}
else {
	# listen mode
	while (<>) {
		chomp;
		eval { chainfrom($_, undef, 40) };
		if ($@) { initdbs() } # in case db is being written
		print "\n\n";
	}
}

sub chainfrom {
	my @ngram=split(' ', shift);
	my $handler=(shift || sub { print shift });
	my $limit=shift;

	$handler->("@ngram ");
	my $c=0;
	while (my @matches=findngrams(@ngram)) {
		my $m=$matches[rand $#matches]->get_document->get_data;
		$handler->($m." ");
		if (defined $limit && $c++ > $limit) {
			$handler->("...");
			last;
		}
	
		push @ngram, split(' ', $m);
		shift @ngram while @ngram >= $max;
	}
}

sub initdbs {
	@dbs=();
	foreach my $n (2..$max) {
		push @dbs, { 
			size => $n,
			object => Search::Xapian::Database->new($n."gram/xapiandb"),
       		};
	}
	@dbs=reverse @dbs;
}

sub findngrams {
	my @n=reverse @_;
	my @ret;
	foreach my $db (@dbs) {
		my $size=$db->{size};
		while ($size-- && $size > 0 && $size-1 <= @n) {
			my $ngram=join(" ", reverse(@n[0..$size-2]));
			next unless length $ngram;
			my $enq = $db->{object}->enquire($ngram);
			@ret=$enq->matches(0, 10000000);
			$stats{$db->{size}}++ if @ret;
			return @ret if @ret;
		}
	}
	return ();
}

sub ircbot {
	my $server=shift;
	my @channels=shift;
	my $nick="dadagoogoo";

	eval q{use Net::IRC};
	die $@ if $@;

	print "! IRC to $server on channels @channels\n";
	my $irc=Net::IRC->new;
	my $conn=$irc->newconn(
		Nick => $nick,
		Server => $server,
		Port => 6667,
		Ircname => $nick,
	);

	$conn->add_handler('376', sub {
		my $this=shift;
		print "! Connected!\n";
		foreach my $c (@channels) {
                	print "! Joining $c..\n";
	                $this->join($c);
		}
	});
	$conn->add_handler('kick', sub {
		my $this=shift;
		my $event=shift;
		my $from=$event->nick;
		my @stuff=$event->args;
		my $chan=$stuff[0];
		my $reason=$stuff[1];
                print "! Rejoining $chan..\n";
		$this->join($chan);
	});
	$conn->add_handler('public', sub {
		my $this=shift;
		my $event=shift;
		my $from=$event->nick;
		my $chan=$event->to;

		return if $from eq $nick;

		my $line=join(" ",$event->args);
		$line=~s/\./ \./g;
		$line=~s/\!/ \!/g;
		$line=~s/\?/ \?/g;

		my $collect="";
		my $collector=sub {
			if (! defined $collect) {
				$collect="";
			}
			else {
				$collect.=shift();
			}
		};
		eval { chainfrom($line, $collector, 10) };
		if ($@) { print "! db reload\n"; initdbs() } # in case db is being written
		$conn->privmsg($chan, $collect) if $collect ne $line." ";
		print "$line => $collect\n";
	});

	print "! Starting IRC bot...\n";
	$irc->start;
}
