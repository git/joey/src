#include <Wire.h>
#include <Adafruit_ADS1015.h>
#include <EEPROMex.h>

// for XBee
#include <SoftwareSerial.h>

SoftwareSerial XBee(2, 3); // Arduino RX, TX (XBee Dout, Din)

Adafruit_ADS1115 ads(0x48);

void setup(void) 
{
  Serial.begin(9600);  
  Serial.println("arduino booted");
  XBee.println("arduino booted"); 

  #ifdef _EEPROMEX_DEBUG
  EEPROM.setMaxAllowedWrites(200);
  #else
  #error "foo"
  #endif

  XBee.begin(9600); 

  // The ADC input range (or gain) can be changed via the following
  // functions, but be careful never to exceed VDD +0.3V max, or to
  // exceed the upper and lower limits if you adjust the input range!
  // Setting these values incorrectly may destroy your ADC!
  //                                                                ADS1015  ADS1115
  //                                                                -------  -------
  // ads.setGain(GAIN_TWOTHIRDS);  // 2/3x gain +/- 6.144V  1 bit = 3mV      0.1875mV (default)
  // ads.setGain(GAIN_ONE);        // 1x gain   +/- 4.096V  1 bit = 2mV      0.125mV
  // ads.setGain(GAIN_TWO);        // 2x gain   +/- 2.048V  1 bit = 1mV      0.0625mV
  // ads.setGain(GAIN_FOUR);       // 4x gain   +/- 1.024V  1 bit = 0.5mV    0.03125mV
  // ads.setGain(GAIN_EIGHT);      // 8x gain   +/- 0.512V  1 bit = 0.25mV   0.015625mV
  // ads.setGain(GAIN_SIXTEEN);    // 16x gain  +/- 0.256V  1 bit = 0.125mV  0.0078125mV
  //ads.setGain(GAIN_EIGHT);
  ads.begin();
  
  Serial.println("ADC initialized");
  XBee.println("ADC initialized");
}

void loop(void) 
{
  int16_t adc_diff, adc0, adc1, adc2, adc3;

  Serial.println("getting reading from ADC");
  XBee.println("getting reading from ADC");
  // adc_diff = ads.readADC_Differential_0_1();
  adc0 = ads.readADC_SingleEnded(0);
  // adc1 = ads.readADC_SingleEnded(1);
  // adc2 = ads.readADC_SingleEnded(2);
  // adc3 = ads.readADC_SingleEnded(3);
  
  // XBee.print("differential adc0 to adc1: "); 
  // XBee.print(adc_diff);
  Serial.print("\tadc0: "); 
  Serial.print(adc0);
  XBee.print("\tadc0: "); 
  XBee.print(adc0);
  // XBee.print("\tadc1: "); 
  // XBee.print(adc1);
  // XBee.print("\tadc2: ");
  // XBee.print(adc2);
  // XBee.print("\tadc3 ");
  // XBee.print(adc3);
  Serial.println("");
  XBee.println("");
  
  delay(1000);
}

