#!/usr/bin/perl
use strict;
use warnings;
no warnings 'utf8';

my $sol="http://storiesonline.net/";
my $login=$sol."login.php";
my $register="https://storiesonline.net/user/new.php";
my $activate="https://storiesonline.net/user/activate.php";
my $frontpage=$sol."home.php";
my $logout=$sol."sign_out.php";
my $meta=$sol."story/xmlget.php?&gi=1&cmd=showdetails&id=";

my $anonmail="no-spam.ws";

use Date::Parse;	
use LWP;
my $browser = LWP::UserAgent->new;
$browser->agent('Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1)');
$browser->cookie_jar({});
use LWP::ConnCache;
$browser->conn_cache(LWP::ConnCache->new());

$|=1;

my $action=shift;
my $num=shift || 10;
if (! defined $action) {
	print <<EOF;
Usage: solrip action

Actions:
	registerstart num	register num times
	registerend num		complete num registrations
	registertest		test all registered users and remove failing
	findauthors		find all authors
	findstories		find all stories (slow, needs findauthor 1st)
	newstories		find newly posted stories cheaply
				(may not find all)
	getstories num		download some stories that have not been
				downloaded yet, or that have been updated
				since download
				(uses up the quota of num accounts)
	getmeta			refresh metadata for all stories
				(also finds stories that have been updated)
	htmlize			create html index
EOF
	exit 1;
}
if ($action eq 'test') {
	register_start("yell4118\@$anonmail");
	register_complete("beepaaaaaa", "yell4118\@$anonmail");
	login("beepaaaaaa", "yell4118\@$anonmail");
	logout();
}
elsif ($action eq 'registerstart') {
	my @pending=load_pending_accounts();
	print "Signing up for $num new accounts";
	my $counter=0;
	for (1..$num) {
		my $seed="";
		for (1..(8+rand(4))) {
			my $l=chr(rand(26)+97);
			$seed.=$l;
		}
		my $password=$seed.$counter++;
		my $email=$seed.$counter++."@".$anonmail;
		eval {
			register_start($email);
		};
		if (! $@) {
			push @pending, [$password, $email];
			print ".";
		}
		else {
			print "!";
		}
	}
	print "done\n";
	save_pending_accounts(@pending);
}
elsif ($action eq 'registerend') {
	my @pending=load_pending_accounts();
	my @accounts=load_accounts();
	print "Completing registration for up to $num accounts";
	my @newpending;
	my $x=0;
	# reversing each time through, to burn the candle at both ends
	foreach my $account (reverse @pending) {
		$x++;
		if ($x <= $num) {
			eval {
				register_complete(@$account);
			};
			if (! $@) {
				push @$account, 0;
				push @accounts, $account;
				print ".";
			}
			else {
				push @newpending, $account;
				print "!";
			}
		}
		else {
			push @newpending, $account;
		}
	}
	print "done (".($#newpending+1)." still to finish)\n";
	save_pending_accounts(@newpending);
	save_accounts(@accounts);
}
elsif ($action eq 'findauthors') {
	my $account=find_account();
	die "out" unless defined $account;
	login($account->[0], $account->[1]);
	print "Updating author list";
	my @authors=load_authors();
	my %seen=map { $_->[0] => 1 } @authors;
	foreach my $let ('A'..'Z', '#') {
		my $response=$browser->get($sol."library/authors.php?let=$let");
		if (! $response->is_success) {
			print "!";
			next;
		}
		
		use HTML::LinkExtor;
		my $p = HTML::LinkExtor->new();
		$p->parse($response->decoded_content);
		my @links=grep { /\/a(?:uth)?\// } map { $sol.$_->[2] } $p->links;

		foreach my $l (@links) {
			my ($a)=$l=~/\/a(?:uth)?\/(.*)/;
			if (! $seen{$a}) {
				$seen{$a}=1;
				push @authors, [$a];
			}
		}

		print ".";
	}
	print "done\n";
	save_authors(@authors);
}
elsif ($action eq 'findstories') {
	# XXX should be resumable and use more than 1 account, but
	# it isn't used very often so I haven't bothered.
	my $account=find_account();
	die "out" unless defined $account;
	login($account->[0], $account->[1]);
	print "Finding stories\n";
	my @authors=load_authors();
	my @stories=load_stories();
	my %seen=map { $_->[0] => 1 } @stories;
	foreach my $author (map { $_->[0] } @authors) {
		print "\t$author";
		my $skip=1;
		while (1) {
			my $response=$browser->get($sol."a/$author/$skip");
			$skip=$skip+1;
			if (! $response->is_success) {
				print "!";
				last;
			}
		
			use HTML::LinkExtor;
			my $p = HTML::LinkExtor->new();
			$p->parse($response->decoded_content);
			my @links=grep { /\/s(?:tory)?\// } map { $sol.$_->[2] } $p->links;
			
			last unless @links; # no stories on a page probably means we're done
			
			print ":";
			foreach my $l (@links) {
				my ($s)=$l=~/\/s(?:tory)?\/(\d+)/;
				if (! $seen{$s}) {
					$seen{$s}=1;
					push @stories, [$s, $author, 0];
					print ".";
				}
			}
		}
		print "\n";
	}
	print "done\n";
	save_stories(@stories);
}
elsif ($action eq 'newstories') {
	my $account=find_account();
	die "out" unless defined $account;
	login($account->[0], $account->[1]);
	print "Finding new stories";
	my @stories=load_stories();
	my %seen=map { $_->[0] => 1 } @stories;
	my $notadded=0;
	my $added=0;
	my $p=1;
	while (1) {
		my $response=$browser->get($sol."library/new_stories.php?p=$p");
		$p++;
		if (! $response->is_success) {
			print "!".$response->message;
		}

		# it only allows going so deep
		if ($p > 7) {
			last;
		}
		
		use HTML::LinkExtor;
		my $p = HTML::LinkExtor->new();
		$p->parse($response->decoded_content);
		my @links=grep { /\/(s(?:tory)?|a(?:uth)?)\// } map { $sol.$_->[2] } $p->links;
			
		last unless @links; # no stories on a page probably means we're done
			
		print ":";
		while (@links) {
			my $l=shift @links;
			my ($s)=$l=~/\/s(?:tory)?\/(\d+)/;
			if (defined $s && length $s && ! $seen{$s}) {
				# next link should be the author
				$l=shift @links;
				my ($a)=$l=~/\/a(?:uth)?\/(.*)/;
				
				if (defined $a && length $a) {
					$seen{$s}=1;
					push @stories, [$s, $a, 0];
					print ".";
					$added++;
				}
				else {
					print "?";
				}
			}
			elsif (defined $s && length $s) {
				$notadded++;
				print "*";
			}
		}
	}
	save_stories(@stories);
	print "done\n";
	if ($added && ! $notadded) {
		print "warning: ALL stories were new, so this probably missed some older ones\n";
	}
}
elsif ($action eq 'getstories') {
	print "Downloading stories using up to $num accounts";
	mkdir("story");
	my @stories=load_stories();

	my @need_dl;
	foreach my $story (@stories) {
		my $dldate=$story->[2];
		next if $dldate > 0;
		push @need_dl, $story;
	}
	my $togo=@need_dl;
	my %seen;

ACC:	while ($num) {
		last ACC unless $togo;
		$num--;
		eval { logout() }; # fails if not logged in
		my $account=find_account();
		if (! defined $account) {
			save_stories(@stories);
			die "out";
		}
		eval { login($account->[0], $account->[1]) };
		if ($@) {
			print "login failure with ".$account->[0].": $@\n";
			next;
		}
		my $counter=0;
		print "\n\t";

STORY:		foreach my $story (@need_dl) {
			my $topsid=$story->[0];
			my @todo="story/".$topsid;
			next if $seen{"story/$topsid"};
			
			$counter++;
			print "$counter";
			next ACC if $counter > 15;
		
			while (@todo) {
				my $sid=shift @todo;
				next if $sid=~/:i$/; # index is a dup
				next if $seen{$sid};

				my $fn=$sid;
				$fn=~s/^\/+//g;
				$fn=~s/\.\.\///g;
				$fn=~s/^s\//story\//;
				$fn=~s/\//_/g;
				$fn=~s/^story_/story\//;
				if ($fn !~ /^story\//) {
					print "illegal story fn: $fn (for $topsid)";
					next STORY;
				}
				next if -e "$fn.html";

				my $response=$browser->get($sol.$sid);
				if (! $response->is_success) {
					if ($response->status_line =~ /not found/i) {
						$seen{$sid}=1;
						$story->[2]=time;
						$togo--;
						last ACC unless $togo;
					}
					print "!";
					next STORY;
				}
				else {
					print ".";
				}

				my $content=$response->decoded_content;
				
				if ($content=~/reached\s+your\s+daily\s+limit/) {
					print "overlimit";
					next ACC;
				}
	
				# look for subparts of the story (chapters,
				# pages, etc)
				use HTML::LinkExtor;
				my $p = HTML::LinkExtor->new();
				$p->parse($content);
				push @todo, grep { /^\/s(?:tory)?\/\Q$topsid\E/ } map { $_->[2] } $p->links;

				no warnings 'utf8';
				open(OUT, ">", "$fn.html") || die "$fn.html $!";

				# url hackup for local browsing
				# (partial only so far; only handles links
				# to story file and toplevel, but not css,
				# etc)
				$content=~s!/s(?:tory)?/([0-9]+([:;][0-9]+)*)(:i)?!./$1.html!g;
				$content=~s!(\.\./|/)*home\.php!index.html!g;

				print OUT $content;
				close OUT || die "story/$sid $!";

				$seen{$sid}=1;
			}
			
			my $meta=getmeta($story->[0]);
			if (defined $meta) {
				$story->[3]=$meta;
			}
			
			# mark story downloaded
			$story->[2]=time;
			$togo--;
			last ACC unless $togo;
		}
	}
	print "\ndone (".$togo." to go)\n";
	save_stories(@stories);
}
elsif ($action eq 'getmeta') {
	print "Downloading story metadata";
	my @stories=load_stories();
	
	my $account=find_account();
	die "out" unless defined $account;
	login($account->[0], $account->[1]);

	foreach my $story (@stories) {
		my $downloaddate=$story->[2];
		my $oldmeta=$story->[3];
		my $meta=getmeta($story->[0]);
		if (! defined $meta) {
			print "!";
			next;
		}

		$story->[3]=$meta;

		my ($oldupdated) = $oldmeta =~ m!<b>Updated:</b> (\d\d\d\d-\d\d\-\d\d)!;
		my ($newupdated) = $meta    =~ m!<b>Updated:</b> (\d\d\d\d-\d\d\-\d\d)!;
		
		if ((! defined $oldupdated && defined $newupdated) ||
		    (defined $oldupdated && ! defined $newupdated) ||
		    (defined $oldupdated && defined $newupdated &&
		     $newupdated ne $oldupdated) ||
		    (defined $newupdated && str2time($newupdated) > $downloaddate)) {
			# reset download date to force re-download
			# of updated story
			$story->[2]=0;
			print ":";
		}
		else {
			print ".";
		}
	}
	
	print "\ndone\n";
	save_stories(@stories);
}
elsif ($action eq 'htmlize') {
	open (I, ">index.html") || die $!;
	print I "<html><ul>\n";
	my @stories=load_stories();
	foreach my $story (sort { $a->[1] cmp $b->[1] } @stories) {
		my $sid=$story->[0];
		my $metadata=$story->[3];
		my $title;
		if (open(IN, "story/$sid.html")) {
			while (<IN>) {
				if (/<title>(.*)<\/title>/i) {
					$title=$1;
					$title=~s/\s*\(page 1\)//i;
					last;
				}
			}
			close(IN);
			print I "<li><a href=\"story/$sid.html\">$title</a> $metadata</i></b></li>\n";
		}
	}
	print I "</ul></html>\n";
	close I;
}
elsif ($action eq 'registertest') {
	print "Testing";
	my @accounts=load_accounts();
	my @accounts_ok;
	foreach my $account (@accounts) {
		eval { logout() }; # fails if not logged in
		eval { login($account->[0], $account->[1]) };
		if (! $@) {
			push @accounts_ok, $account;
			print ".";
		}
		else {
			print "!";
		}
	}
	print "\n";
	save_accounts(@accounts_ok);

}
else {
	die "unknown action $action";
}
	
# find an account that has not been used in at least the past day
sub find_account {
	my @accounts=load_accounts();
	my $account;
	foreach my $a (@accounts) {
		if (! defined $a->[3] || $a->[3] < time - 60*60*24) {
			$account=$a;
			last;
		}
	}
	if (! $account) {
		print STDERR "all accounts have been used today!\n";
		return undef;
	}

	$account->[3]=int time;
	save_accounts(reverse @accounts); # swap order to shake things up

	return $account;
}

# A list of sol accounts with registration started.
# Array contains arrays of password, email.
sub load_pending_accounts {
	my @ret;
	open(IN, "pending_accounts") || return;
	while (<IN>) {
		chomp;
		push @ret, [split(' ', $_)];
	}
	close IN;
	return @ret;
}
sub save_pending_accounts {
	open(OUT, ">", "pending_accounts");
	foreach (@_) {
		print OUT join(" ", @$_)."\n";
	}
	close OUT;
}

# A list of sol accounts with registration finished,
# Array contains password, email, date of last use.
sub load_accounts {
	my @ret;
	open(IN, "accounts") || return;
	while (<IN>) {
		chomp;
		push @ret, [split(' ', $_)];
	}
	close IN;
	return @ret;
}
sub save_accounts {
	open(OUT, ">", "accounts");
	foreach (@_) {
		print OUT join(" ", @$_)."\n";
	}
	close OUT;
}

# A list of sol author names
# Contains: name, last checked
sub load_authors {
	my @ret;
	open(IN, "authors") || return;
	while (<IN>) {
		chomp;
		push @ret, [split(' ', $_)];
	}
	close IN;
	return @ret;
}
sub save_authors {
	open(OUT, ">", "authors");
	foreach (@_) {
		print OUT join(" ", @$_)."\n";
	}
	close OUT;
}

# A list of stories
# Contains: id, author, download date, metadata
sub load_stories {
	my @ret;
	open(IN, "stories") || return;
	while (<IN>) {
		chomp;
		push @ret, [split(' ', $_,4)];
	}
	close IN;
	return @ret;
}
sub save_stories {
	open(OUT, ">", "stories");
	foreach (@_) {
		print OUT join(" ", @$_)."\n";
	}
	close OUT;
}

# Post form to start process.
sub register_start {
	my ($email)=@_;

	my $response=$browser->post($register,[
			cmd => "Submit Email",
			email1 => $email,
			email2 => $email,
		],
	);
	if ($response->is_success) {
		if ($response->decoded_content =~ /emailed/i) {
			return 1;
		}
	}

	die "registration failure: ".$response->message." ".$response->decoded_content;
}

# Pickup registration email and get activation code.
sub register_complete {
	my ($password, $email)=@_;

	my @links;
	if ($email=~/(.*)\@mailinator\.com/ || $email=~/(.*)\@streetwisemail.com/) {
		$email=$1;
		my $response=$browser->get("http://mailinator.com/inbox.jsp?to=$email");
		if (! $response->is_success) {
			die "registration pickup failure: ".$response->message;
		}

		use HTML::LinkExtor;
		my $p = HTML::LinkExtor->new();
		$p->parse($response->decoded_content);
		@links=grep { /showmail/ } map { "http://mailinator.com/".$_->[2] } $p->links;
	}
	elsif ($email=~/(.*)\@tempbox.org/) {
		# XXX doesn't quite work, sol's mail body is not shown

		$email=$1;
		my $response=$browser->get("http://www.tempbox.org/box/$email");
		if (! $response->is_success) {
			die "registration pickup failure: http://www.tempbox.org/box/$email ".$response->message;
		}

		use HTML::LinkExtor;
		my $p = HTML::LinkExtor->new();
		$p->parse($response->decoded_content);
		@links=grep { /message/ } map { $_->[2] } $p->links;
	}
	elsif ($email=~/(.*)\@mailcatch.com/) {
		$email=lc($1); # must be lc'd!
		my $response=$browser->get("http://mailcatch.com/en/temporary-inbox?box=$email");
		if (! $response->is_success) {
			die "registration pickup failure: tempbox ".$response->message;
		}

		use HTML::LinkExtor;
		my $p = HTML::LinkExtor->new();
		$p->parse($response->decoded_content);
		@links=grep { /show=/ } map { "http://mailcatch.com/en/temporary-inbox".$_->[2] } $p->links;
	}
	elsif ($email=~/(.*)\@makemetheking.com/) {
		$email=$1;

		# mail shown right on the user page, easy..
		@links=("http://makemetheking.com/?mailbox=$email");
	}
	elsif ($email=~/(.*)\@dispostable.com/) {
		$email=$1;

		my $response=$browser->get("http://www.dispostable.com/inbox/$email/");
		if (! $response->is_success) {
			die "registration pickup failure: tempbox ".$response->message;
		}

		use HTML::LinkExtor;
		my $p = HTML::LinkExtor->new();
		$p->parse($response->decoded_content);
		@links=map { "http://www.dispostable.com".$_->[2] } $p->links;
	}
	elsif ($email=~/(.*)\@no-spam.ws/) {
		$email=$1;

		my $response=$browser->get("http://no-spam.ws/?login=$email");
		if (! $response->is_success) {
			die "registration pickup failure: tempbox ".$response->message;
		}

		use HTML::LinkExtor;
		my $p = HTML::LinkExtor->new();
		$p->parse($response->decoded_content);
		@links=grep { /mail=/ } map { "http://no-spam.ws/".$_->[2] } $p->links;
	}
	else {
		die "unknown email service $email";
	}
	
	foreach my $link (@links) {
		my $response=$browser->get($link);
		if (! $response->is_success) {
			die "registration pickup failure $link: ".$response->message;
		}
		if ($response->decoded_content =~ /\Q$email\E/ &&
			$response->decoded_content =~ m!https://storiesonline.net/user/activate.php\?token=([^"<]+)</a>!) {
			return register_activate($email, $password, $1);
		}
		else {
			#die $response->decoded_content;
		}
	}

	die "failed to find registration code (try again later)";
}

# Post form to activate registration.
sub register_activate {
	my ($email, $password, $code)=@_;

	my $response=$browser->post($activate,[
			token => $code,
			upass1 => $password,
			upass2 => $password,
			cmd => "Create Account",
		],
	);
	if ($response->message =~/Found/) {
		return 1;
	}

	print STDERR $response->message." ".$response->decoded_content."\n";

	die "registration activation failure: ".$response->message;
}

# Log in to sol.
sub login {
	my ($password, $email)=@_;

	my $response=$browser->post($login,[
			theusername => $email,
			thepassword => $password,
		],
	);

	# sol probably redirects here, but if the login worked,
	# I should be able to go right to the page now and see my account
	# etc.

	$response=$browser->get($frontpage);
	if ($response->is_success) {
		if ($response->decoded_content =~ /My\s+Account/i) {
			return 1;
		}
		else {
		}
	}

	die "login failure ($email): ".$response->message;
}

# Log out of sol.
sub logout {
	my $response=$browser->get($logout);

	if ($response->is_success) {
		if ($response->decoded_content =~ /Come back soon/i) {
			$browser->cookie_jar({});
			return 1;
		}
	}

	die "logout failure: ".$response->message;
}

sub getmeta {
	my $sid=shift;

	my $response=$browser->get($meta.$sid);
	return unless $response->is_success;
	my $xml=$response->decoded_content;
	my ($data)=$xml=~m/<fdata><!\[CDATA\[(.*)\]\]><\/fdata>/s;
	$data=~s/\n/ /g if defined $data;
	return $data;
}
