/*
 * Tiny, specialized getty that runs a program w/o requiring you to log in.
 *
 * Syntax:
 * 
 * getty_run tty speed term_type program [program args...]
 *
 * Portions of this program were derived from the agetty program, by 
 * Wietse Venema <wietse@wzv.win.tue.nl>. The remainder was written by
 * Joey Hess <joey@kitenet.net>. Both portions fall under the GPL.
 *
 */

#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <termio.h>
#include <sys/stat.h>
#include <string.h>
#include <sys/ioctl.h>

void open_tty(char *tty,struct termio *tp) {
	
	/* close present stdio and error */
	close(0);
	close(1);
	close(2);
	
	/* open new stdin and stdout */
	chdir("/dev/");
	open(tty,0);
	open(tty,1);
	
	/* get termio */
	ioctl(0, TCGETA, tp);
}

/* Setup the terminal. Portions of this were swiped from agetty */
void termio_init(struct termio *tp,int speed) {
	tp->c_cflag = CS8 | HUPCL | CREAD | speed;
	tp->c_cflag |= CRTSCTS;
	tp->c_iflag = tp->c_lflag = tp->c_oflag = tp->c_line = 0;
	tp->c_cc[VMIN] = 1;
	tp->c_cc[VTIME] = 0;
	ioctl(0,TCSETA,tp);

#define CTL(x)    (x ^ 0100)  /* Assumes ASCII dialect */
#define CR    CTL('M')  /* carriage return */
#define NL    CTL('J')  /* line feed */  
#define BS    CTL('H')  /* back space */
#define DEL   CTL('?')  /* delete */
#define DEF_ERASE DEL   /* default erase character */
#define DEF_INTR  CTL('C')  /* default interrupt character */
#define DEF_QUIT  CTL('\\') /* default quit char */
#define DEF_KILL  CTL('U')  /* default kill char */
#define DEF_EOF   CTL('D')  /* default EOF char */
#define DEF_EOL   0
#define DEF_SWITCH  0   /* default switch char */

	tp->c_iflag |= IXON | IXOFF;    /* 2-way flow control */
	tp->c_lflag |= ICANON | ISIG | ECHO | ECHOE | ECHOK;
	tp->c_oflag |= OPOST;
	tp->c_cc[VINTR] = DEF_INTR;     /* default interrupt */
	tp->c_cc[VQUIT] = DEF_QUIT;     /* default quit */
	tp->c_cc[VEOF] = DEF_EOF;     /* default EOF character */
	tp->c_cc[VEOL] = DEF_EOL;
	tp->c_iflag |= ICRNL;     /* map CR in input to NL */
	tp->c_oflag |= ONLCR;     /* map NL in output to CR-NL */
	
	ioctl(0, TCSETA, tp);
}

/* Convert speed entered on command line to speed code. Swiped from agetty. */
int speed_conv(char *speed) {
	struct SpeedTab {
		long	speed;
		long	code;
	};
	
	static struct SpeedTab speedtab[] = {
		50, B50,
		75, B75,
		110, B110,
		134, B134,
		150, B150,
		200, B200,
		300, B300,
		600, B600,
		1200, B1200,
		1800, B1800,
		2400, B2400,
		4800, B4800,
		9600, B9600,
#ifdef  B19200
		19200, B19200,
#endif
#ifdef  B38400
		38400, B38400,
#endif
#ifdef  EXTA
		19200, EXTA,
#endif
#ifdef  EXTB 
		38400, EXTB,
#endif
		0, 0,
	};

	struct SpeedTab *sp;
	long s = atol(speed);
	for (sp = speedtab; sp->speed; sp++)
		if (sp->speed == s)
			return (sp->code);
	return 0;                      
}

void main (int argc,char **argv) {
	struct termio termio;

  	while (1) {
  
		/* Open and set up tty */
		open_tty(argv[1], &termio);
		termio_init(&termio,speed_conv(argv[2]));
	
		/* flush pending input */
		ioctl(0, TCFLSH, (struct termio *) 0);

		/* Set TERM= */
		setenv("TERM",argv[3],1);
		
    		execv(argv[4],argv+4);
	}
}
