#!/usr/bin/perl -w

my $src_server="irc.freenode.org";
my $dest_server="irc.debian.org";
my $channel=shift || "#debian-boot";
my $nick=shift || "watergate";
my $ircname=shift || "watergate";
my $port=shift || 6667;
my $gate=qw/^CIA-\d+$/;
my $verbose=1;

use strict;
use Net::IRC;

my $irc=Net::IRC->new;
my $src_conn=$irc->newconn(
	Nick => $nick,
	Server => $src_server,
	Port => $port,
	Ircname => $ircname,
);

$src_conn->add_handler('376', \&on_connect); # 376 = end of MOTD: we're connected.
$src_conn->add_handler('public', \&on_public);
$src_conn->add_handler('kick', \&on_kick);

my $dest_conn=$irc->newconn(
	Nick => $nick,
	Server => $dest_server,
	Port => $port,
	Ircname => $ircname,
);

$dest_conn->add_handler('376', \&on_connect); # 376 = end of MOTD: we're connected.
$dest_conn->add_handler('kick', \&on_kick);

$irc->start;

# What to do when the bot successfully connects.
sub on_connect {
	my $self=shift;
	my $chan;
	
	foreach my $chan (split(/:/, $channel)) {
		print "Joining $chan..\n" if $verbose;
		$self->join($chan);
	}
	
}

# Listen to dialog on the channel gateway it to the other one.
sub on_public {
	my $self=shift;
	my $event=shift;
	
	my $from=$event->nick;
	my $chan=$event->to;
	
	return unless $from=~/$gate/;
	
	$_=join("\n",$event->args)."\n";
	
	print "<$from> $_\n" if $verbose;
	$dest_conn->privmsg($chan, $_);
}

# Auto-join on kick
sub on_kick {
	my $self=shift;
	my $event=shift;
	my $from=$event->nick;
	my @stuff=$event->args;
	my $chan=$stuff[0];
	my $reason=$stuff[1];

	$self->join($chan);
}

# All this and I still doen't understand IRC. Scaaaarey! -- Joey
