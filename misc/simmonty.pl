#!/usr/bin/perl -s
# Copyright 2002 Joey Hess <joeyh@kitenet.net>
# Licensed under the DO WHATEVER THE FUCK YOU WANT WITH THIS license.

my $strategy = shift || "";
my $iterations = int(shift) || 1000;
my $numdoors = int(shift) || 3;

my $stratsub = (grep /^strat_\Q$strategy\E$/, keys %::)[0]; # bwahaha

if (! $stratsub) {
	print STDERR "Usage: simmonty.pl [-q] change|ignore|random [iterations] [numdoors]\n";
	print STDERR "Available strategies:\n";
	print STDERR "\tchange  change to a different door after Monty opens one\n";
	print STDERR "\tignore  ignore the shyster\n";
	print STDERR "\trandom  alternate between change and ignore at random\n";
	print STDERR "\nWarning: may not behave appropriatly with doors != 3\n";
	
	exit(1);
}

sub choosedoor {
	return int rand($numdoors) + 1;
}

sub vprint {
	print @_ unless $q;
}

my @prize = split /\n/, q{
a red convertible
a station wagon
an 11" color TV
a general electric washer/dryer set
silverware for 20
lawn darts
something vaguely achronistic
$$$
};
shift @prize;

my $wins = 0;
for (1..$iterations) {
	my $prizedoor = choosedoor();
	my $pickdoor = choosedoor();
	
	my $montydoor;
	do {
		# Ok, before someone nags me, I know this does the full
		# monty sometimes. Deal with it or write a better routine.
		$montydoor = choosedoor();
	} while ($montydoor == $prizedoor || $montydoor == $pickdoor);
	
	vprint "game ${_}:\n";
	vprint "\tYou pick door #$pickdoor. Monty opens door #$montydoor. Empty.\n";
	$pickdoor = $stratsub->($pickdoor, $montydoor);
	vprint "\tDoor #$pickdoor opens... ";
	if ($pickdoor == $prizedoor) {
		vprint "You win ".$prize[rand(@prize)]."!\n";
		$wins++;
	}
	else {
		vprint "You lose...\n";
	}
}

vprint "\n";
print "Summary: In $iterations games with $numdoors doors, strategy \"$strategy\",\n";
print "         you won $wins and lost ".($iterations - $wins).". ";
print "Success rate: ".($wins / $iterations * 100)."%\n";

sub strat_change {
	my ($oldpick, $montydoor) = @_;
	my $newpick;
	do {
		$newpick = choosedoor();
	} while ($newpick == $montydoor || $newpick == $oldpick);
	vprint "\tYou change your pick to door #$newpick\n";
	return $newpick;
}

sub strat_ignore {
	my ($pickdoor, $montydoor) = @_;
	vprint "\tYou do not change your pick.\n";
	return $pickdoor;
}

sub strat_random {
	if (rand > 0.5) {
		return strat_change(@_);
	}
	else {
		return strat_ignore(@_);
	}
}
