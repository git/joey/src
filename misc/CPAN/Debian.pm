#!/usr/bin/perl

=head1 NAME

CPAN::Debian - build Debian packages from CPAN

=head1 SYNOPSIS

Interactive mode:

  perl -MCPAN::Debian -e shell;

  makedeb Package

Batch mode:

  use CPAN::Debian;

  debianize, makedeb

=head1 DESCRIPTION

The CPAN::Debian bolts the ability to generate Debian packages onto CPAN.
Using this module is the same as using CPAN, except a few additional CPAN
shell commands and methods are available, as follows:

=over 2

=cut

use strict;
use warnings;
use CPAN;
use Carp;

# Bolt new code into CPAN. Is it possible to inherit from it instead?
# CPAN is OO, right? riiiight. :-P

package CPAN::Shell;

sub debianize { shift->rematein('debianize',@_) }
sub makedeb { shift->rematein('makedeb',@_) }

package CPAN::Module;

=item debianize

Creates debianized source tree from a perl module. The module will be 
downloaded from CPAN first, if necessary. The debianized source tree
is put in a subdirectory of $CPAN::Config->{build_dir}

=cut

sub debianize {
	my $this=shift;

	return if $this->{DEBIANIZED};

	$this->get;
	
	my $cpan_file = $this->cpan_file;
	my $pack = $CPAN::META->instance('CPAN::Distribution',$cpan_file);
	my $builddir=$pack->dir;
	my $maintainer=$this->username." <".$this->email.">";
	my $date=$this->date;
	my $package=$this->debname;
	my $version=$this->debversion;
	my $shortdesc=$this->debdesc;
	my $module=$this->id;

	chdir $builddir or Carp::croak("Couldn't chdir $builddir: $!");
	$this->debug("Changed directory to $builddir") if $CPAN::DEBUG;

	# Work out some docs and a changelog, if possible.
	my @files=<*>;
	my @docs=grep /^(readme|todo|bugs|news)$/i, @files;
	my @changelogs=grep /^(changes|changelog)$/i, @files;
	my $changelog=shift @changelogs || '';

	if (! -d 'debian') {
		mkdir 'debian' or Carp::croak("Couldn't make debian directory: $!");
	}

	open COPYRIGHT, ">debian/copyright" or Carp::croak("Couldn't write debian/copyright: $!");
	print COPYRIGHT << "EOF";
This package was downloaded from CPAN, http://www.cpan.org/, and was
debianized by the CPAN::Debian module.

Many packages in CPAN are licensed under the same terms as perl itself, but
you will have to look up the copyright of $module for yourself.
EOF
	close COPYRIGHT;

	open CHANGELOG, ">debian/changelog" or Carp::croak("Couldn't write debian/changelog: $!");
	print CHANGELOG << "EOF";
$package ($version-1) unstable; urgency=low

  * Debian package automatically created by CPAN::Debian.

 -- $maintainer  $date
EOF
	close CHANGELOG;

	open CONTROL, ">debian/control" or  Carp::croak("Couldn't write debian/control: $!");
	print CONTROL << "EOF";
Source: $package
Section: interpreters
Priority: optional
Build-Depends: debhelper (>= 3.0.5), perl (>= 5.6.0-17)
Maintainer: $maintainer

Package: $package
Architecture: any
Depends: \${perl:Depends}, \${shlibs:Depends}
Description: $shortdesc
 The $module perl module from CPAN.
 .
 This package was automatically created by CPAN::Debian.
EOF
	close CONTROL;

	open RULES, ">debian/rules" or Carp::croak("Couldn't write debian/rules: $!");
	print RULES << "EOF";
#!/usr/bin/make -f
#
# Generic perl module rules file.
# Copyright 2001 Joey Hess <joeyh\@debian.org>

# Uncomment this to turn on verbose mode.
#export DH_VERBOSE=1

# This is the debhelper compatability version to use.
export DH_COMPAT=3

PACKAGE=\$(shell dh_listpackages)
DESTDIR=..

build: build-stamp
build-stamp:
	dh_testdir
	perl Makefile.PL INSTALLDIRS=vendor
	\$(MAKE) OPTIMIZE="-O2 -g -Wall"
	\$(MAKE) test
	touch build-stamp

clean:
	dh_testdir
	dh_testroot
	rm -f build-stamp
	-\$(MAKE) realclean
	dh_clean

binary-indep: build
# I don't know if this is really a binary-indep package.

binary-arch: build
	dh_testdir
	dh_testroot
	dh_clean -k
	\$(MAKE) install PREFIX=\$(PWD)/debian/\$(PACKAGE)/usr
	dh_installdocs @docs
	dh_installchangelogs $changelog
	dh_strip
	dh_compress
	dh_fixperms
	dh_installdeb
	dh_perl
	dh_shlibdeps
	dh_gencontrol
# Remove any dangling comma, which will occur if \${shlibs:Depends}
# is empty. (That also means this is really an arch: all package,
# but I won't bother dealing with correcting that..)
	perl -i -pe 's/(Depends: .*), \$\$/\$\$1/' debian/\$(PACKAGE)/DEBIAN/control
	dh_md5sums
	dh_builddeb --destdir=\$(DESTDIR)

binary: binary-indep binary-arch
.PHONY: build clean binary-indep binary-arch binary
EOF
	close RULES;
	chmod 0755, "debian/rules";

	$this->{DEBIANIZED}=1;
}

=item makedeb

Builds a debian package from a CPAN module. If necessary it will be
downloaded and debianized first. The debian package is put in
$CPAN::Config->{deb_dest_dir}, or in the current directory if that
is not set.

dpkg-dev, fakeroot and debhelper are used in the build process, so all
three packages must be installed for this to work.

=cut


{

# Since CPAN has a nasty habit of changing the working directory
# (a habit I perpetuate in this module, mind you), preserve the directory
# this module is invoked in, to prevent unexpected suprises.
use Cwd;
my $origdir=getcwd;

sub makedeb {
	my $this=shift;
	$this->debianize;
	
	my $cpan_file = $this->cpan_file;
	my $pack = $CPAN::META->instance('CPAN::Distribution',$cpan_file);
        my $builddir=$pack->dir;
	chdir $builddir or Carp::croak("Couldn't chdir $builddir: $!");
	$this->debug("Changed directory to $builddir") if $CPAN::DEBUG;
	my $destdir=$CPAN::Config->{deb_dest_dir} || $origdir;
	system("fakeroot debian/rules binary DESTDIR=$destdir") == 0 or
		$CPAN::Frontend->mydie("fakeroot debian/rules binary failed");
}

}

# Try to cobble together a short description for the debian package.
sub debdesc {
	my $this=shift;

	return $this->{description} ||
	       "The ".$this->id." perl module from CPAN";
}

# Turn the package's version into a valid Debian version.
sub debversion {
	my $this=shift;

	my $version=$this->cpan_version;
	$version='' if $version eq 'undef';

	# "The <upstream-version> may contain only alphanumerics and the
	# characters `.'  `+' `-' `:' (full stop, plus, hyphen,
	# colon) and should start with a digit. [...] if there is no
	# <epoch> then colons are not allowed." -- Debian packaging manual
	$version=~s/[^-.+a-zA-Z0-9]+//g;
	$version="0$version" if $version !~ /^\d/;
	
	return $version;
}

# Convert a perl module name into a policy compliant Debian package name.
sub debname {
	my $this=shift;

	# Convert from module name into debian package name compliant
	# with debian perl policy.
	my $package=lc($this->id);
	$package=~s/::/-/g;
	$package="lib$package-perl";
	# Make sure it contains nothing policy frowns on.
	$package=~s/[^-.+a-zA-Z0-9]+//g;

	return $package;
}

# Returns the date, in rfc 822 format. Ripped from alien.
sub date {
	my $this=shift;

	my $date=`822-date`;
	chomp $date;
	if (!$date) {
		die "822-date did not return a valid result. You probably need to install the dpkg-dev debian package";
	}

	return $date;
}

# Work out a email address for control and changelog files.
# Ripped from alien.
sub email {
	my $this=shift;

	return $ENV{EMAIL} if exists $ENV{EMAIL};

	my $login = getlogin || (getpwuid($<))[0] || $ENV{USER};
	my $mailname='';
	if (open (MAILNAME,"</etc/mailname")) {
		$mailname=<MAILNAME>;
		chomp $mailname;
		close MAILNAME;
	}
	if (!$mailname) {
		$mailname=`hostname -f`;
		chomp $mailname;
	}
	return "$login\@$mailname";
}

# Returns the user name of the real uid. Ripped from alien.
sub username {
	my $this=shift;

	my $username;
	my $login = getlogin || (getpwuid($<))[0] || $ENV{USER};
	(undef, undef, undef, undef, undef, undef, $username) = getpwnam($login);

	# Remove GECOS fields from username.
	$username=~s/,.*//g;

	# The ultimate fallback.
	if ($username eq '') {
		$username=$login;
	}

	return $username;
}

=back 

=head1 BUGS

=over 2

=item *

makedeb does not honor inactivity_timeout.

=item *

Prerequisites are not dealt with.

=item *

CPAN shell help does not include the extra commands this module uses.

=item *

Bundles don't work.

=item *

The way this module is bolted onto the side of CPAN is very ugly.

=back

=head1 SEE ALSO

L<CPAN(8)>

=head1 AUTHOR

Joey Hess <joeyh@debian.org>

=head1 COPYRIGHT

Copyright 2001, Joey Hess.

This module is free software. It may be used, redistributed
and/or modified under the same terms as Perl itself; your choice
of the GPL or the Artistic Liscense.

=cut

1
