#!/usr/bin/perl
# Generates a graph showing how many votes were recieved for DPL on each
# day of the specified vote years.
#
# The input leaderyyyy-timing.dat files have the format "date runningtotal".

my $start=2004;
my $end=2008;

open (D, ">data");
open (D2, ">data-percent");

print D "day ";
print D2 "day ";

my @counts;
my @totals;
foreach my $year ($start..$end) {
	my $file="leader$year-timing.dat";
	my %seen;
	my $voteday=0;
	my $total=0;
	open (IN, $file) || die "$file: $!";
	while (<IN>) {
		my @w=split(" ", $_);
		my $newtotal=pop @w;
		my $date="@w[0..3]";
		if (! $seen{$date}) {
			$seen{$date}=1;
			$voteday++;
		}
		$counts[$voteday][$year]+=($newtotal - $total);
		$totals[$year]+=($newtotal - $total);
		$total=$newtotal;
	}
	close IN;

	print D "$year ";
	print D2 "$year ";
}

print D "\n";
print D2 "\n";

foreach my $voteday (1..$#counts) {
	print D "$voteday ";
	print D2 "$voteday ";
	foreach my $year ($start..$end) {
		my $num=$counts[$voteday][$year];
		print D defined $num ? $num : 0;
		print D " ";
		print D2 ($num / $totals[$year]) * 100;
		print D2 " ";
	}
	print D "\n";
	print D2 "\n";
}

open (G, "| gnuplot");
print G << "EOF";
set terminal png size 640,480
set output 'votes.png'
set ylabel 'number of votes recieved'
set xlabel 'day of vote'
set style data histogram
set style histogram cluster gap 1
set style fill solid border -1
EOF
print G "plot 'data' using 2:xtic(1) ti col";
my $x=1;
for ($start+1 .. $end) {
	$x++;
	print G ", '' u ".($x+1)." ti col";
}
print G "\n";
close G;

open (G, "| gnuplot");
print G << "EOF";
set terminal png size 640,480
set output 'votes-percent.png'
set ylabel 'percentage of total votes recieved'
set xlabel 'day of vote'
set style data histogram
set style histogram cluster gap 1
set style fill solid border -1
EOF
print G "plot 'data-percent' using 2:xtic(1) ti col";
$x=1;
for ($start+1 .. $end) {
	$x++;
	print G ", '' u ".($x+1)." ti col";
}
print G "\n";
close G;
