/*
 * Loadable module to replace the stock console beep routine
 *
 * Requires patches to .../linux/drivers/char/vt_kern.h and
 * .../linux/drivers/char/vt.c
 *
 * dave madden <dhm@proteon.com>
 *
 * Modified 10-May-96 by Joey Hess <jeh22@cornell.edu>, 
 *	now it works in 1.3.99 (and above?)
 *
 */


/* Kernel includes */

#include <linux/errno.h>
#include <linux/fs.h>
#include <linux/major.h>
#include <asm/segment.h>
#include <asm/io.h>
#include <linux/kernel.h>
#include <linux/signal.h>
#include <linux/module.h>
#include <linux/timer.h>
#include "sound_config.h"
#include "opl3.h"

/*
 * NB. we must include the kernel idenfication string in to install the module.
 */
#include <linux/version.h>
/* static char kernel_version[] = UTS_RELEASE; */


#ifdef __cplusplus
extern "C" {
#endif

extern int printk( const char* fmt, ... );

extern void	(*kd_nosound)( unsigned long ignored );
extern void	(*kd_mksound)( unsigned int count, unsigned int ticks );

static void	(*old_nosound)( unsigned long ignored );
static void	(*old_mksound)( unsigned int count, unsigned int ticks );

static void	 my_nosound( unsigned long ignored );
static void	 my_mksound( unsigned int count, unsigned int ticks );

static void	 opl3_command( int io_addr, unsigned int addr, unsigned int val );
static void	my_tenmicrosec( void );

int	 OPL3_PORT = 0x388;

int
init_module( void )
{
	printk("Replacing keyboard beeper.\n");

	kd_nosound( 0 );			/* make sure the beeper is turned off now */

	old_nosound = kd_nosound;
	old_mksound = kd_mksound;

	kd_nosound = my_nosound;
	kd_mksound = my_mksound;
	
	return 0;
}

void
cleanup_module( void )
{
  printk( "Restoring old keyboard beeper\n" );

  kd_nosound( 0 );

  kd_nosound = old_nosound;
  kd_mksound = old_mksound;
}

static void	 my_nosound( unsigned long ignored )
{
  opl3_command( OPL3_PORT, KSL_LEVEL + 3, 0xff );	/*
												 * Carrier
												 * volume to
												 * min
												 */
  opl3_command( OPL3_PORT, KSL_LEVEL + 0, 0xff);	/*
												 * Modulator
												 * volume to
												 */

  opl3_command( OPL3_PORT, KEYON_BLOCK + 0, 0x00 );	/*
												 * Note
												 * off
												 */
  
}

static void	 my_mksound( unsigned int count, unsigned int ticks )
{
	static struct timer_list sound_timer = { NULL, NULL, 0, 0, 0 };
	/*
	 * key off
	 */
	opl3_command( OPL3_PORT, KEYON_BLOCK + 0, 0x0d );

	/*
	 * Set Sound Characteristics
	 */
	opl3_command( OPL3_PORT, AM_VIB + 0, 0x03 );
	opl3_command( OPL3_PORT, AM_VIB + 3, 0x05 );

	/*
	 * Set Attack/Decay
	 */
	opl3_command( OPL3_PORT, ATTACK_DECAY + 0, 0xe5 );
	opl3_command( OPL3_PORT, ATTACK_DECAY + 3, 0xc6 );

	/*
	 * Set Sustain/Release
	 */
	opl3_command( OPL3_PORT, SUSTAIN_RELEASE + 0, 0xf3 );
	opl3_command( OPL3_PORT, SUSTAIN_RELEASE + 3, 0xf5 );

	/*
	 * Set Wave Select
	 */
	opl3_command( OPL3_PORT, WAVE_SELECT + 0, 0 );
	opl3_command( OPL3_PORT, WAVE_SELECT + 3, 0 );

	/*
	 * Set Feedback/Connection
	 */
	opl3_command (OPL3_PORT, FEEDBACK_CONNECTION + 0, 0x35 );

	opl3_command( OPL3_PORT, KSL_LEVEL + 0, 0 );
	opl3_command( OPL3_PORT, KSL_LEVEL + 3, 0 );

	opl3_command( OPL3_PORT, FNUM_LOW + 0, 0x56 );
	opl3_command( OPL3_PORT, KEYON_BLOCK + 0, 0x2d );

	sound_timer.expires = 75 + jiffies;
	sound_timer.function = kd_nosound;
	cli( );
	del_timer( &sound_timer );
	add_timer( &sound_timer );
	sti( );
}

static void
opl3_command (int io_addr, unsigned int addr, unsigned int val)
{
	outb( (unsigned char)(addr & 0xff), io_addr );	/*
													 * Select register
													 *
													 */

	my_tenmicrosec ();

	outb( (unsigned char) (val & 0xff), io_addr + 1 );	/*
														 * Write to register
														 *
														 */

	my_tenmicrosec ();
	my_tenmicrosec ();
	my_tenmicrosec ();

}

void my_tenmicrosec( void )
{
  int	 i;

  for (i = 0; i < 16; i++) inb( 0x80 );
}

#ifdef __cplusplus
}
#endif

