{- Horrible screen scraper to generate a RSS feed for a twitter search.
 - Provide the url to scrape 
 -   (such as 'http://twitter.com/search/realtime?q=git-annex')
 - RSS feed is output to stdout.
 -
 - This will break. When it does, send me a patch. Any other mail about
 - this will be ignored. <id@joeyh.name>
 -
 - Known bugs: Does not include a post date in the feed. 
 -
 - License: GPL-3+
 -}

import Text.HTML.TagSoup
import Network.HTTP.Conduit
import Text.RSS.Syntax
import Text.RSS.Export
import Text.XML.Light.Output
import System.Environment
import Control.Monad
import Data.String.Utils
import Data.ByteString.Lazy.UTF8 (toString, ByteString)

-- All the tag soup that makes up a tweet
type Tweet = [Tag String]

type HtmlString = String

main = mapM_ dumpFeed =<< getArgs

dumpFeed :: URLString -> IO ()
dumpFeed url = do
	page <- downloadUrl url
	putStr $ showElement $ xmlRSS $ genFeed url $ toString page

downloadUrl :: URLString -> IO ByteString
downloadUrl url = withManager $ \man -> do
	url' <- parseUrl url
	let req = url' { responseTimeout = Nothing }
	fmap responseBody $ httpLbs req man

genFeed :: URLString -> String -> RSS
genFeed url page = (nullRSS title url)
	{ rssChannel=(nullChannel title url) { rssItems = items } }
  where
	title = "twitter search"
	items = genItems page

tweetItem :: Tweet -> RSSItem
tweetItem tweet = (nullItem $ extractTweet tweet)
	{ rssItemLink = Just $ extractTweetUrl tweet
	, rssItemAuthor = Just $ extractTweetAuthor tweet
	}

genItems :: String -> [RSSItem]
genItems = map tweetItem . extractTweets . canonicalizeTags . parseTags

{- Each tweet in the page is currently inside a <div class=content>,
 - and look forward from there until a footer div to get enough tag soup. -}
extractTweets :: [Tag String] -> [Tweet]
extractTweets tags = map (takeWhile (~/= "<div class=stream-item-footer>")) $
	sections (~== "<div class=content>") tags

{- The text of a tweet is currently inside a particular <p>
 -
 - This boils it down to a plain string without any markup.
 -}
extractTweet :: Tweet -> String
extractTweet = innerText .
	takeWhile (~/= "</p>") .
	drop 1 .
	dropWhile (~/= "<p class=\"js-tweet-text tweet-text\">")

{- The url of a tweet is currently in a <a> inside a <small>,
 - and is currently relative. -}
extractTweetUrl :: Tweet -> URLString
extractTweetUrl tweet = "https://twitter.com" ++ fromAttrib "href" (links !! 0 !! 0)
  where
  	links = map (filter (~== "<a>")) $
		sections (~== "<small class=time>") tweet

{- The author of a tweet is in a <strong class=fullname> -}
extractTweetAuthor :: Tweet -> String
extractTweetAuthor = innerText .
	takeWhile (~/= "</strong>") .
	dropWhile (~/= "<strong class=\"fullname js-action-profile-name show-popup-with-id\">")

