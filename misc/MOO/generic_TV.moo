/*
 * This is a TV object that can be hooked up to multiple cameras and
 * display their signals on different channels.
 *
 * Made by Joey Hess Sun Jun  8 16:32:24 EDT 1997
 */

@create $state_machine named generic_TV:generic_TV
@describe generic_TV as "A television set."
@prop generic_TV.is_tv 1
@prop generic_TV.currentchannel 2
/* a list of lists of cameras */
@prop generic_TV.channels {}
@prop generic_TV.static_msg ""
@static generic_TV is "Nothing but static shows on the screen."
/* the range of channels this tv can use */
@prop generic_TV.minchannel 2
@prop generic_TV.maxchannel 12
/* clicks when change channels 0 = not */
@prop generic_TV.clicky 1
@on generic_TV is "The %d is turned %[dstate]"
@off generic_TV is "The %d is turned %[dstate]."
@prop generic_TV.show_channel_msg ""
/* Shown after the on_msg. %c is replaced by the channel */
@show_channel generic_TV is "to channel %c."
@prop generic_TV.change_channel_msg ""
@change_channel generic_TV is "You change the channel to %c."
@prop generic_TV.ochange_channel_msg ""
@ochange_channel generic_TV is "%N changes the channel on the %d to %c."
@prop generic_TV.connect_fail_msg ""
@connect_fail generic_TV is "You can't connect the %d to the %i."

/*
 * Look at the TV to tell if it's turned on, and if so, 
 * what channel it's on and to get a description of what's on that channel.
 */
@verb generic_TV:description this none this
@program generic_TV:description
	if (this.state == "on")
		return pass(@args) + " " + $string_utils:pronoun_sub(
			$string_utils:substitute(this.show_channel_msg,
				{{"%c",$string_utils:english_number(this.currentchannel)}}));
	else
		return pass(@args);
	endif
.

/* Change the channel. */
@verb generic_TV:"ch*annel ch*ange tu*rn sw*itch" this to any
@program generic_TV:channel
	number=toint(iobjstr);
	if (number < this.minchannel || number > this.maxchannel)
		player:tell("The ", this.name, " can only receive channels between ", 
			this.minchannel, " and ", this.maxchannel, ".");
	elseif (number == this.currentchannel)
		player:tell("The ", this.name, " is already on channel ", 
			$string_utils:english_number(number), ".");
	else
		english=$string_utils:english_number(number);
		if (this.clicky)
			click="";
			for x in [1..abs(this.currentchannel - number)]
				click=click + "click ";
			endfor
			/* Vary the clicking some. */
			if (random(2) ==1)
				click=$string_utils:substitute(click, {{"click click click", 
					"cliick"}});
			else
				click=$string_utils:substitute(click, {{"click click click click",
					"click ... click"}});
			endif
      player.location:announce_all(click);
    endif
    this.currentchannel=number;
    player:tell($string_utils:pronoun_sub(
			$string_utils:substitute(this.change_channel_msg,
				{{"%c",english}})));
    player.location:announce($string_utils:pronoun_sub(
			$string_utils:substitute(this.ochange_channel_msg,
				{{"%c",english}})));
  endif
.

/* Handle a video signal from a camera. */
@verb generic_TV:video_signal this none this
@program generic_TV:video_signal
if (this.state == "on");
	{signal_location, @rest}=args;
	text=$string_utils:from_list(rest," ");
	/*
   * No feedback allowed! Make sure we never pass on anything that's already
   * gone through a TV.
   */
  if (!index(text,"-- The",1))
		this.location:announce_all("-- The ", this.name, " shows ", 
			signal_location.name, " where ", @rest);
  endif
endif
.

/* 
 * This is here so you can connect tv to camera or camera to tv, without
 * having to remember which comes first.
 */
@verb generic_TV:"ho*okup co*nnect li*nk" this any any
@program generic_TV:hookup
	if (valid(iobj))
		if (this:test_camera(iobj))
			if (iobj:do_hookup(this))
				player:tell($string_utils:pronoun_sub(iobj.connect_msg));
				player.location:announce($string_utils:pronoun_sub(iobj.oconnect_msg));
			else
				player:tell($string_utils:pronoun_sub(iobj.already_connected_msg));
			endif
		else
			player:tell($string_utils:pronoun_sub(this.connect_fail_msg));
		endif
	else
		player:tell("There is no ", iobjstr, " here.");
	endif
.

/*
 * This gets called by a camera when this TV is connected to it.
 * Syntax: do_hookup(camera [, channel])
 * If channel isn't passed, prompt for one.
 * Returns 1 on success, 0 on failure.
 */
@verb generic_TV:do_hookup this none this
@program generic_TV:do_hookup
	{camera, ?channel = 0}=args;
	channel=toint(channel);
	interactive=0;
	if (channel == 0)
		interactive=1;
		/* have to prompt the user for the channel. */
		player:tell("What channel will the signal display on (", this.minchannel,
			"-", this.maxchannel, ")?");
		channel=toint(read());
	endif
	if (channel > this.maxchannel || channel < this.minchannel)
		if (interactive)
			player:tell("Sorry, channel out of range.");
		endif
		return 0;
	endif

	/* Don't bother hooking the same camera up to the same channel twice. */
  if (is_member(camera, this.channels[channel]))
		return 1; /* it's not an error, just don't do anything... */
	endif

	this.channels[channel]=$list_utils:append(this.channels[channel],{camera});
	return 1;
.

/*
 * This is here so you can disconnect tv from camera or camera from tv,
 * without having to remember which comes first. Note that the device we are
 * unhooking may not even exist anymore, or may not be a camera anymore!
 * Todo: and that case isn't handled yet.
 */
@verb generic_TV:"un*hook un*link dis*connect" this any any
@program generic_TV:unhook
  if (valid(iobj))
    if (iobj:do_unhook(this))
      player:tell("You disconnect the ", this.name, " from the ", iobj.name,
        ".");
      player.location:announce(player.name, " tinkers with the ", iobj.name,
        " and the ", this.name, ".");
    else
      player:tell("The ", this.name, " is not connected to that.");
    endif
  else
    player:tell("There is no ", iobjstr, " connected.");
  endif
.

/*
 * This is a little utility function that tests an object to see if it can
 * serve as a camera, ie, if it has a is_camera property.
 */
@verb generic_TV:test_camera this none this
@program generic_TV:test_camera
  try
    property_info(args[1], "is_camera");
  except v (ANY)
    return 0;
  endtry
  return 1;
.
