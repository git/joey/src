/*
 * This is an object you can place other objects on top of. Similar
 * to a container except it isn't opened or closed.
 *
 * Made by Joey Hess Sun Jun  8 16:32:24 EDT 1997
 */

@create $container named generic-shelf:generic-shelf
@prop generic_shelf."contents_msg" "On the %d:" rc
;;generic_shelf.("opaque") = 0
;;generic_shelf.("dark") = 0
;;generic_shelf.("put_msg") = "You put %d on %i."
;;generic_shelf.("opened") = 1
;;generic_shelf.("open_key") = 0
;;generic_shelf.("key") = 0
;;generic_shelf.("aliases") = {"generic-shelf"}
;;generic_shelf.("description") = "A flat surface, suitable for placing other 
objects on."
;;generic_shelf.("object_size") = {0, 0}

@verb generic_shelf:"tell_contents" this none this
@program generic_shelf:tell_contents
if (this.contents)
  player:tell($string_utils:pronoun_sub(this.contents_msg));
  for thing in (this:contents())
    player:tell("  ", thing:title());
  endfor
elseif (msg = this:empty_msg())
  player:tell(msg);
endif
.

@verb generic_shelf:"put" any any this
@program generic_shelf:put
pass(@args);
.

@verb generic_shelf:"close" this none none
@program generic_shelf:close
player:tell("You can't close this object.");
.

@verb generic_shelf:"open" this none none
@program generic_shelf:open
player:tell("You can't open this object.");
.
