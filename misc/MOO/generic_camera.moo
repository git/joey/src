/*
 * This is a camera object that can be hooked up to multiple TV's.
 * Everything that happens in the room with the camera is displayed 
 * on the TV's the camera is attached to.
 *
 * Made by Joey Hess Sun Jun  8 16:32:24 EDT 1997
 */

@create $state_machine named generic_camera:generic_camera
@prop generic_camera.is_camera 1
@prop generic_camera.devices {}
@describe generic_camera as "The video camera is mounted on the wall. A wire snakes from the camera up through the ceiling."
@prop generic_camera.active_msg ""
@active generic_camera is "A red light is flashing on the %d."
@prop generic_camera.connect_msg ""
@connect generic_camera is "You connect the %i to the %d."
@prop generic_camera.oconnect_msg ""
@oconnect generic_camera is "%N tinkers with the %d and the %i."
@prop generic_camera.already_connected_msg ""
@already_connected generic_camera is "The %d is already connected to the %i."
@prop generic_camera.connect_fail_msg ""
@connect_fail generic_camera is "You can't connect the %d to the %i."
@prop generic_camera.disconnect_msg ""
@disconnect generic_camera is "You disconnect the %i from the %d."
@prop generic_camera.odisconnect_msg ""
@odisconnect generic_camera is "%N disconnects the %i from the %d."
@prop generic_camera.disconnect_fail_msg ""
@disconnect_fail generic_camera is "That isn't connected to the %d."
@on generic_camera is ""

/*
 * Watch for events in the room, and send them to TV's.
 * This verb needs to be optimized as much as possible, because it is
 * called a _lot_.
 */
@verb generic_camera:tell this none this
@program generic_camera:tell
	if (this.state == "on")
		for device in (this.devices)
			try
			  if ((device.location != this.location) && (device.state == "on"))
			    device:video_signal(this.location,@args);
			  endif
			except v (ANY)
				endtry
		endfor
	endif
.

@verb generic_camera:description this none this
@program generic_camera:description
	active="";
	if (this.state == "on") 
		for device in (this.devices)
			if (this:test_tv(device) && device.state == "on")
				active=1;
				break;
			endif
		endfor
	endif
	if (active)
		return pass(@args) + $string_utils:pronoun_sub(this.active_msg);
	else
	  return pass(@args);
	endif
.

/*
 * Called by player to hook up another TV to the camera.
 * If called with no indirect object, will list current connections.
 */
@verb generic_camera:"ho*okups co*nnections li*nks" this any any
@program generic_camera:hookup
	if (valid(iobj)) 
		if (this:test_tv(iobj))
			if (this:do_hookup(iobj));
				player:tell($string_utils:pronoun_sub(this.connect_msg));
				player.location:announce($string_utils:pronoun_sub(this.oconnect_msg));
			else
				player:tell($string_utils:pronoun_sub(this.already_connected_msg));
			endif
		else
			player:tell($string_utils:pronoun_sub(this.connect_fail_msg));
		endif
	elseif (iobjstr == "")
		this:garbage_collect();
		if (this.devices == {})
			player:tell("Nothing is hooked up to the ", this.name, ".");
		else
			player:tell("The following devices are hooked up to the ", this.name, 
				":");
			for device in (this.devices)
				player:tell("  ",device.name);
			endfor
		endif
	else
		player:tell("There is no ", iobjstr, " here.");
	endif
	this:garbage_collect();
.

/* 
 * Actually handles the work of hooking up another TV to the camera.
 * Returns 1 on success, 0 on failure. Please make sure the arg is a real tv
 * before calling this function!
 */
@verb generic_camera:do_hookup this none this
@program generic_camera:do_hookup
	/* Make sure we never hook up duplicates. */
	if (!is_member(args[1],this.devices))
		this.devices=$list_utils:append(this.devices,{@args});
		return 1;
	else
		return 0;
	endif
.

/*
 * Unhook a device from the camera. Note that the device might not be
 * a TV. It might even not exist any longer! Thus, it's important we
 * garbage_collect first.
 */
@verb generic_camera:"un*hook un*link dis*connect" this any any
@program generic_camera:unhook
	this:garbage_collect();
	if (iobjstr == "")
		player:tell("You have to specify what to disconnect from the ", 
			this.name, ".");
	elseif (this:do_unhook(iobj))
		player:tell($string_utils:pronoun_sub(this.disconnect_msg));
		player.location:announce($string_utils:pronoun_sub(this.odisconnect_msg));
	else
		player:tell($string_utils:pronoun_sub(this.disconnect_fail_msg));
	endif
.

/*
 * Actually handles the work of unhooking a TV from the camera.
 * Returns 1 on success, 0 on failure.
 */
@verb generic_camera:do_unhook this none this
@program generic_camera:do_unhook
	device=args[1];
	listpos=is_member(device,this.devices);
	if (listpos !=0)
		this.devices=$list_utils:flatten({this.devices[1..listpos-1], 
			this.devices[listpos+1..$]});
		return 1;
	else
		return 0;
	endif
.

/*
 * Sometimes, devices that are hooked up to the camera get @recycled, but
 * are still on the camera's list. This is responsible for finding such
 * objects and getting rid of them. You don't have to call it often; I 
 * only call it when new items are connected/disconnected to the camera.
 * This also fixes objects that get linked up twice.
 */
@verb generic_camera:garbage_collect this none this
@program generic_camera:garbage_collect
	for device in (this.devices)
	  if (!this:test_tv(device))
			this:do_unhook(device);
		endif
	endfor
	this.devices=$list_utils:flatten($list_utils:sort(this.devices));
.

/*
 * This is a little utility function that tests an object to see if it can
 * serve as a TV, ie, if it has a is_tv property.
 */
@verb generic_camera:test_tv this none this
@program generic_camera:test_tv
	try
		property_info(args[1], "is_tv");
	except v (ANY)
		return 0;
	endtry
	return 1;
.
