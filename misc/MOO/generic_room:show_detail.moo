/* Display a detail message for this room. Detail messages can be set with the @detail command. */
/* This lets us add lots of detail to something w/o bothering with creating lots of objects. */

/* Right now, it only works on rooms, should be extended to show details of objects in the room.. */
/* Also, wild-cards in details, or multiple names for one detail, would be nice. */

/* generic_room:show_detail tnt */
/* Note that the index has to be odd to be the index we want. But I don't bother checking that. Bad programmer! */

index = dobjstr in this.detail;
if (index !=0)
	player:tell(this.detail[index + 1]);
	return 1;
endif
return 0;

/* generic_room:@detail any is any */
/* This adds a detail to the list for the room. Enter a null detail to erase a detail, or 
   you can replace an existing detail */

index = dobjstr in this.detail;
if (iobjstr == "") 
	if (index != 0)
		this.detail[index .. index + 1] = {};
	endif
	player:tell("Detail removed.");
elseif (index == 0)
	this.detail[$ + 1 .. $ + 2] = {dobjstr,iobjstr};
	player:tell("Detail set.");
else
	this.detail[index .. index +1] = {dobjstr,iobjstr};
	player:tell("Detail changed.");
endif

/* generic_room:look */
/* Modified this to add hook to call show_detail() if no object could be found. */

if (dobjstr == "" && !prepstr)
  this:look_self();
elseif (prepstr != "in" && prepstr != "on")
  if (!dobjstr && prepstr == "at")
    dobjstr = iobjstr;
    iobjstr = "";
  else
    dobjstr = dobjstr + (prepstr && (dobjstr && " ") + prepstr);
    dobjstr = dobjstr + (iobjstr && (dobjstr && " ") + iobjstr);
  endif
  dobj = this:match_object(dobjstr);
  if (dobj == $failed_match)
    if (!this:show_detail(dobjstr))
			$command_utils:object_match_failed(dobj, dobjstr);
		endif
  elseif (!$command_utils:object_match_failed(dobj, dobjstr))
    dobj:look_self();
  endif
elseif (!iobjstr)
  player:tell(verb, " ", prepstr, " what?");
else
  iobj = this:match_object(iobjstr);
  if (!$command_utils:object_match_failed(iobj, iobjstr))
    if (dobjstr == "")
      iobj:look_self();
    elseif ((thing = iobj:match(dobjstr)) == $failed_match)
      player:tell("I don't see any \"", dobjstr, "\" ", prepstr, " ", iobj.name, ".");
    elseif (thing == $ambiguous_match)
      player:tell("There are several things ", prepstr, " ", iobj.name," one might call \"", dobjstr, "\".");
    else
      thing:look_self();
    endif
  endif
endif
