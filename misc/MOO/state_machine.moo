/*
 * This is an object that can be turned on and off.
 * this.state will be either "on" or "off".
 *
 * Made by Joey Hess Mon Jun  9 21:03:04 EDT 1997
 *
 * Todo: 
 *   locking.
 */

@create $thing named state_machine
@describe state_machine as "A generic object that can be turned on and off."
@prop state_machine.state "on"
@property state_machine.change_state_msg ""
@change_state state_machine is "You turn %[dstate] the %d."
@property state_machine.ochange_state_msg ""
@ochange_state state_machine is "%N turns %[dstate] the %d."
@property state_machine.change_state_fail_msg ""
@change_state_fail state_machine is "The %d is already %[dstate]."
@prop state_machine.on_msg ""
@on state_machine is "The %d is %[dstate]."
@prop state_machine.off_msg ""
@off state_machine is "The %d is %[dstate]."

@corify state_machine as state_machine /* optional! */

/* Turn it on and off. */
@verb state_machine:"tu*rn sw*itch" this any none
@program state_machine:turn
if (prepstr == "on" || prepstr == "off")
	if (this.state == prepstr)
		player:tell($string_utils:pronoun_sub(this.change_state_fail_msg));
	else
		this.state = prepstr;
		player:tell($string_utils:pronoun_sub(this.change_state_msg));
		this.location:announce($string_utils:pronoun_sub(this.ochange_state_msg));
	endif
else
	player:tell("Turn the ", this.name, " on or off, not \"", prepstr, "\".");
endif
.

/* Description tells if it's off or on, by default. */
@verb state_machine:description this none this
@program state_machine:description
if (this.state == "on")
  return pass(@args) + " " + $string_utils:pronoun_sub(this.on_msg);
else
  return pass(@args) + " " + $string_utils:pronoun_sub(this.off_msg);
endif
.
