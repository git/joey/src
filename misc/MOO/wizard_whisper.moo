/* 
 * This is a modified version of whisper that can be heard by all wizards
 * in the current room.
 *
 * This is pretty sneaky, it should only be used where the wizard needs 
 * absolute control. It was written for use by the storyteller for online 
 * RPGing.
 */

this:tell(player.name, " whispers, \"", dobjstr, "\"");
player:tell("You whisper, \"", dobjstr, "\" to ", this.name, ".");
if (! $object_utils:isa(player,#2))
	for object in (player.location.contents)
		if ($object_utils:isa(object,#2) && $object_utils:connected(object))
			object:tell(player.name, " whispers to ", this.name, ": \"", 
				dobjstr, "\"");
		endif
	endfor
endif
