/*
 * This is a generic throwable thing that can be thrown back and forth between
 * players. It's also a useful parent for anything you want to throw.
 *
 * If you want to get fancy, make 3 frisbees, and try to juggle them, or
 * keep them all in the air at once. :-)
 *
 * Made by Joey Hess, Sat Jun  7 15:25:13 EDT 1997
 */

@create $thing named "throwable thing"
@describe "throwable thing" as "This is a generic throwable object. You'll want to change this description, and set @inair and @hangime to customize children to your liking."
@property "throwable thing".catcher 0
@property "throwable thing".inair_msg ""
@inair "throwable thing" is "It is flying through the air."
@property "throwable thing".hit_person_msg ""
@hit_person "throwable thing" is "Ouch!"

/*
 * The hangtime is how long it stays in the air, max, in seconds.
 * It also determined how likely you are to get hit if you miss catching
 * the object. (lower hangtime = more likely)
 */
@property "throwable thing".hangtime_msg ""
@hangtime "throwable thing" is 10

/* Give a different description if it's in the air. */
@verb "throwable thing":description this none this
@program "throwable thing":description
basic = pass(@args);
if (this.catcher != 0)
  return ($string_utils:from_list($list_utils:flatten({basic}), "  ") 
    + " ") + this.inair_msg;
else
	return basic;
endif
.

/* Throw it to a player or object, or just throw it. */
@verb "throwable thing":"to*ss th*row" this any any
@program "throwable thing":toss
if (this.location == player)
  if (valid(iobj))
		this.catcher = iobj;
		move(this, player.location);
		player:tell("You toss the ", this.name, " ", prepstr, " ", iobj.name, ".");
		this.location:announce(player.name, " tosses the ", this.name, " ", prepstr, " ", iobj.name, ".");
    if (is_player(iobj))
      iobj:tell("The ", this.name, " is flying toward you.");
		endif
    fork (random(toint(this.hangtime_msg)) + 2)
			/* Note that the catcher might have left the room. */
      if (this.catcher != 0 && (this.catcher.location == this.location))
        if (is_player(iobj))
					/* accidents happen */
					if (random(toint(this.hangtime_msg)) == 1)
						this.location:announce_all("The ", this.name, " hits ", iobj.name, ". ", this.hit_person_msg);
					else
	          this.location:announce_all(iobj.name, " misses the ", this.name, ".");
					endif
        endif
				this.location:announce_all("The ", this.name, " falls to the ground beside ", this.catcher.name, ".");
				this.catcher = 0;
			elseif (this.catcher != 0)
				this.location:announce_all("The ", this.name, " falls to the ground.");
				this.catcher=0;
			endif		
		endfork
  else
		if (iobjstr == "")
			this.catcher=this; /* tricky, we need a catcher that is not a player. */
			move(this, player.location);
			player:tell("You toss the ", this.name, ".");
			this.location:announce(player.name, " tosses the ", this.name, ".");
			fork (random(toint(this.hangtime_msg)) + 2)
				if (this.catcher != 0)
					this.location:announce_all("The ", this.name, " falls to the ground.");
					this.catcher = 0;
				endif
			endfork
		else
	    player:tell("There is no ", iobjstr, " here.");
		endif
  endif
else
  player:tell("You have to be holding the ", this.name, ".");
endif
.

/* A player catches the thing. */
@verb "throwable thing":"ca*tch gr*ab" this none none
@program "throwable thing":catch
if (this.catcher == 0)
  player:tell("The ", this.name, " is not in the air right now.");
else
  if (player == this.catcher || !is_player(this.catcher))
    player.location:announce(player.name, " catches the ", this.name, ".");
  else
    player.location:announce(player.name, " snags the ", this.name, " before ", this.catcher.name, " can catch it.");
  endif
  player:tell("You catch the ", this.name, ".");
  this.catcher = 0;
  move(this, player);
endif
.

/* Don't allow anyone to use "get" on this object while it is in the air. */
@verb "throwable thing":"g*et t*ake" this none none
@program "throwable thing":get
if (this.catcher == 0)
	pass(@args);
else
	player:tell("The ", this.name, " is flying through the air. You'll have to try to catch it.");
endif
.

