/* 
 * This is a custom program I wrote to clear a rectangle in the root window
 * to black. THe rectangle starts at (0,0), and extends all the way accross
 * the top of the screen, and is 160 pixels tall.
 * 
 * Joey Hess <joey@kitenet.net>
 */

#include <X11/Xlib.h>

Display *dpy;
int screen;

/* Probably is a better way to allocate a color. */
unsigned long GetColor () {
	XColor color;
	XAllocColorCells(dpy, DefaultColormap(dpy,screen),0,0,0,&color.pixel,1);
	return(color.pixel);
}

main() {
	Window root;
	
	dpy=XOpenDisplay(NULL);
	screen=DefaultScreen(dpy);
	root=RootWindow(dpy,screen);

	XSetWindowBackground(dpy,root,GetColor());
	XClearArea(dpy,root,0,0,10000,160,1); /* this should be configurable.. */
	XFlush(dpy);
}
