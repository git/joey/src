#!/usr/bin/perl
use warnings;
use strict;
use Term::Slang qw(:all);

# Game state variables.
my ($x, $y, $frame);
my $action="";
my $action_duration;
my @falling_blocks;
my $held_block;
my $dumping=0;
my $block_fallspeed=0.5;
my $block_freqtotal=0;

SLtt_get_terminfo();
SLang_init_tty(-1,0,1);
SLsig_block_signals();
SLsmg_init_smg;
SLsig_unblock_signals();
SLkp_init();
SLsmg_normal_video();
my ($s_rows, $s_cols) = SLtt_get_screen_size();

# Holds the available actions for the player.
# x: x displacement while action is in progress
# y: y displacement while action is in progress
# width: width of frame (needs to be the same for all currently)
# height: height of frame
# duration: time action will run if nothing else stops it
#           negative duration actions do not expire
# animation: a set of arrays of strings representing the ascii art
#         for each stage in the animation of the action
# 
# Note that in animations involving movement, the player's leading edge
# should be flush with that side of the frame whenever possible, as the 
# player's movement is stopped when that edge of the frame contacts an
# object.
my %actions = ( #{{{#
	stand => {
		x => 0,
		y => 0,
		width => 5,
		height => 4,
		duration => 60,
		animation => [
			[
				'  o  ',
				' <|> ',
				'  |  ',
				' _|_ ',
			],
		],
	},

	wave => {
		x => 0,
		y => 0,
		width => 5,
		height => 5,
		duration => 10,
		animation => [
			[
				'     ',
				'  o  ',
				' <|> ',
				'  |  ',
				' _|_ ',
			],
			[
				'     ',
				'  o  ',
				' <|\/',
				'  |  ',
				' _|_ ',
			],
			[
				'     ',
				'  o_)',
				' <|  ',
				'  |  ',
				' _|_ ',
			],
			[
				'   \ ',
				'  o/ ',
				' <|  ',
				'  |  ',
				' _|_ ',
			],
			[
				'    /',
				'  o/ ',
				' <|  ',
				'  |  ',
				' _|_ ',
			],
			[
				'   \ ',
				'  o/ ',
				' <|  ',
				'  |  ',
				' _|_ ',
			],
			[
				'    /',
				'  o/ ',
				' <|  ',
				'  |  ',
				' _|_ ',
			],
			[
				'   \ ',
				'  o/ ',
				' <|  ',
				'  |  ',
				' _|_ ',
			],
			[
				'     ',
				'  o_)',
				' <|  ',
				'  |  ',
				' _|_ ',
			],
			[
				'     ',
				'  o  ',
				' <|\/',
				'  |  ',
				' _|_ ',
			],
		],
	},

	stand_left => {
		x => 0,
		y => 0,
		width => 5,
		height => 4,
		duration => 7,
		animation => [
			[
				'  o  ',
				' /(\ ',
				' /|  ',
				' ||  ',
			],
		],
	},

	stand_right => {
		x => 0,
		y => 0,
		width => 5,
		height => 4,
		duration => 7,
		animation => [
			[
				'  o  ',
				' /)\ ',
				'  |\ ',
				'  || ',
			],
		],
	},

	tap => {
		x => 0,
		y => 0,
		width => 5,
		height => 4,
		duration => 8,
		animation => [
			[
				'  o  ',
				' <|> ',
				'  |  ',
				' _|/ ',
			],
			[
				'  o  ',
				' <|> ',
				'  |  ',
				' _|/ ',
			],
			[
				'  o  ',
				' <|> ',
				'  |  ',
				' _|_ ',
			],
			[
				'  o  ',
				' <|> ',
				'  |  ',
				' _|_ ',
			],
		],
	},
	
	walk_left => {
		x => -1,
		y => 0,
		width => 5,
		height => 4,
		duration => 3,
		animation => [
			[
				'  o  ',
				'  (\ ',
				' /|  ',
				'|  \ ',
			],
			[
				'  o  ',
				'  (  ',
				' /|  ',
				' ||  ',
			],
			[
				'  o  ',
				' /(  ',
				'  |  ',
				'  |  ',
			],
		],
	},
	
	walk_right => {
		x => +1,
		y => 0,
		width => 5,
		height => 4,
		duration => 3,
		animation => [
			[
				'  o  ',
				' /)  ',
				'  |\ ',
				' /  |',
			],
			[
				'  o  ',
				'  )  ',
				'  |\ ',
				'  || ',
			],
			[
				'  o  ',
				'  )\ ',
				'  |  ',
				'  |  ',
			],
		],
	},

	run_left => {
		x => -2,
		y => 0,
		width => 5,
		height => 4,
		duration => 2,
		animation => [
			[
				'  o  ',
				'\/(> ',
				' /|  ',
				' \ \ ',
			],
			[
				'(_o_ ',
				' _( )',
				'|  \ ',
				'    \\',
			],
			[
				'  o  ',
				'\/(> ',
				' / \_',
				' |   ',
			],
			[
				'  o  ',
				' _(  ',
				'  |_ ',
				'  |  ',
			],
			[
				'  o  ',
				'\/)> ',
				' /|  ',
				' \ \ ',
			],
			[
				'(_o_ ',
				' _) )',
				'|  \ ',
				'    \\',
			],
			[
				'  o  ',
				'\/)> ',
				' / \_',
				' |   ',
			],
			[
				'  o  ',
				' _)  ',
				'  |_ ',
				'  |  ',
			],
		],
	},

	run_right => {
		x => +2,
		y => 0,
		width => 5,
		height => 4,
		duration => 2,
		animation => [
			[
				'  o  ',
				' <)\/',
				'  |\ ',
				' / / ',
			],
			[
				' _o_)',
				'( )_ ',
				' /  |',
				'/    ',
			],
			[
				'  o  ',
				' <)\/',
				'_/ \ ',
				'    |',
			],
			[
				'  o  ',
				'  )_ ',
				' _|  ',
				'  |  ',
			],
			[
				'  o  ',
				' <(\/',
				'  |\  ',
				' / / ',
			],
			[
				' _o_)',
				'( (_ ',
				' /  |',
				'/    ',
			],
			[
				'  o  ',
				' <(\/',
				'_/ \ ',
				'    |',
			],
			[
				'  o  ',
				'  (_ ',
				' _|  ',
				'  |  ',
			],
		],
	},
	
	hands_up => {
		x => 0,
		y => 0,
		width => 5,
		height => 4,
		duration => 100,
		animation => [
			[
				'\_o_/',
				'  U  ',
				'  |  ',
				' _|_ ',
			],
		],
	},
	
	catch => {
		x => 0,
		y => 0,
		width => 5,
		height => 4,
		duration => 3,
		animation => [
			[
				'(_o_)',
				'  U  ',
				'  |  ',
				' _|_ ',
			],
			[
				'(_o_)',
				'  U  ',
				'  |  ',
				' _|_ ',
			],
			[
				'(_o_)',
				'  U  ',
				'  |  ',
				'_/ \_',
			],
			[
				'\_o_/',
				'  U  ',
				'  |  ',
				'_/ \_',
			],
		],
	},
	
	holding => {
		x => 0,
		y => 0,
		width => 5,
		height => 4,
		duration => -1,
		animation => [
			[
				'\_o_/',
				'  U  ',
				'  |  ',
				'_/ \_',
			],
		],
	},
	
	carry_left => {
		x => -1,
		y => 0,
		width => 5,
		height => 4,
		duration => 3,
		animation => [
			[
				'\_o_)',
				'  U  ',
				'  |  ',
				'_/ \_',
			],
			[
				'\_o_)',
				'  U  ',
				' .|  ',
				'_| \_',
			],
			[
				'\_o_)',
				'  U  ',
				'  |,  ',
				'_/ |_',
			],
			[
				'\_o_)',
				'  U  ',
				' .|  ',
				'_| \_',
			],
		],
	},
	
	carry_right => {
		x => +1,
		y => 0,
		width => 5,
		height => 4,
		duration => 3,
		animation => [
			[
				'(_o_/',
				'  U  ',
				'  |  ',
				'_/ \_',
			],
			[
				'(_o_/',
				'  U  ',
				'  |,  ',
				'_/ |_',
			],
			[
				'(_o_/',
				'  U  ',
				' .|  ',
				'_| \_',
			],
			[
				'(_o_/',
				'  U  ',
				'  |,  ',
				'_/ |_',
			],
		],
	},
); #}}}

# Special types of material. Indicated by character, so we have to be
# careful not to use these characters in any other ground-level 
# animations.
my %material = ( #{{{#
	'-' => 'girder',
	'[' => 'brick',
	']' => 'brick',
	
); #}}}

# A list of the blocks that can fall from above.
# frequency: how often this block occurs. Larger is more frequent.
# width: block width
# height: block height
# animation: How it looks. Special blocks can be animated, but
# regular ones are not, normally.
#
# Note: All regular blocks will be inverted half the time,
# so the mirror images need not be listed separately. 
# Other rotations should be though.
my @blocks = ( #{{{#
	{
		frequency => 10,
		width => 2,
		height => 2,
		animation => [
			[
				'OO',
				'OO',
			],
		],
	},
	{
		frequency => 10,
		width => 4,
		height => 1,
		animation => [
			[
				'OOOO',
			],
		],
	},
	{
		frequency => 10,
		width => 1,
		height => 4,
		animation => [
			[
				'O',
				'O',
				'O',
				'O',
			],
		],
	},
	{
		frequency => 7,
		width => 8,
		height => 1,
		animation => [
			[
				'========',
			],
		],
	},
	{
		frequency => 5,
		width => 3,
		height => 3,
		animation => [
			[
				'OOO',
				'OOO',
				'OOO',
			],
		],
	},
	{
		frequency => 7,
		width => 8,
		height => 1,
		animation => [
			[
				'OOOOOOOO',
			],
		],
	},
	{
		frequency => 5,
		width => 2,
		height => 4,
		animation => [
			[
				'OO',
				'OO',
				'OO',
				'OO',
			],
		],
	},
	{
		frequency => 5,
		width => 16,
		height => 1,
		animation => [
			[
				'================',
			],
		],
	},
	{
		frequency => 2,
		width => 23,
		height => 1,
		animation => [
			[
				'OOOO============OOOOOOO',
			],
		],
	},
	{
		frequency => 1,
		width => 11,
		height => 4,
		animation => [
			[
				'  OO       ',
				'OOOOOOOOOOO',
				'OOOOOO     ',
				'  OO       ',
			],
		],
	},
	{
		frequency => 1,
		width => 11,
		height => 4,
		animation => [
			[
				'   O   OO  ',
				'OOOOOOOOOOO',
				'   OOOOOOOO',
				'      OOO  ',
			],
		],
	},
	{
		frequency => 1,
		width => 11,
		height => 3,
		animation => [
			[
				'OOOOOOOOOOO',
				'OO       OO',
				'OO       OO',
			],
		],
	},
	{
		frequency => 7,
		width => 11,
		height => 2,
		animation => [
			[
				'[][][][][]',
				' [][][][][]',
			],
		],
	},
	{
		frequency => 6,
		width => 11,
		height => 3,
		animation => [
			[
				'[][][][][]',
				' [][][][][]',
				'[][][][][]',
			],
		],
	},
	{
		frequency => 5,
		width => 13,
		height => 3,
		animation => [
			[
				'[][][][][]   ',
				' [][][][][]  ',
				'  [][][][][] ',
				'   [][][][][]',
			],
		],
	},
	{
		frequency => 7,
		width => 5,
		height => 2,
		animation => [
			[
				' [][]',
				'[][] ',
			],
		],
	},
	{
		frequency => 5,
		width => 6,
		height => 2,
		animation => [
			[
				'[][][]',
				'   [] ',
			],
		],
	},
	{
		frequency => 5,
		width => 6,
		height => 2,
		animation => [
			[
				'   [] ',
				'[][][]',
			],
		],
	},
	{
		frequency => 1,
		width => 18,
		height => 5,
		animation => [
			[
				'           [][][] ',
				'           |[][][]',
				'           [][][] ',
				'[][][][][][][][]| ',
				' [][][][][][][][] ',
			],
		],
	},
	{
		frequency => 1000000,
		width => 18,
		height => 9,
		animation => [
			[
				'    [][][]        ',
				'   [][][]         ',
				'    [][][]        ',
				'[][][][][][][][]| ',
				' [][][][][][][][] ',
				'  [][][][]        ',
				'   [][][][]       ',
				'  [][][][]        ',
				'   [][][][]       ',
			],
		],
	},
); #}}}

# Checks to see if two areas, specified by position and width, overlap.
#      w1---p1
#         w2---p2
sub area_overlap ($$$$) {
	$_[0] - $_[1] < $_[2] && $_[0] > $_[2] - $_[3];
}

# Given two lines, checks to see if there are overlapping,
# non-whitespace characters.
# Note: runs at approximatly 34000/s on slow hw.
sub line_overlap ($$) {
	my $l1=shift;
	my $l2=shift;
	
	# Convert to numbers and sum, then any even numbers indicate
	# overlap.
	$l1=~y/ /1/c;
	$l2=~y/ /1/c;
	$l1=~y/ /0/;
	$l2=~y/ /0/;
	($l1+$l2)=~/2/;
}

# Checks if the two frames, at the specified positions
# on the screen, would have any non-space elements that overlap.
sub frame_overlap ($$$$$$$$$$) {
	my $y1=$_[0];
	my $x1=$_[1];
	my $frame1=$_[2];
	my $width1=$_[3];
	my $height1=$_[4];
	
	my $y2=$_[5];
	my $x2=$_[6];
	my $frame2=$_[7];
	my $width2=$_[8];
	my $height2=$_[9];

	# Inexpensive overlap test.
	return unless area_overlap($y1, $height1, $y2, $height2) &&
	              area_overlap($x1, $width1, $x2, $width2);

	# get absolute overlap coordinates                   
	my $overlap_start_x=
	my $overlap_start_y=
	my $overlap_end_x=
	my $overlap_end_y=
	
	my $overlap_width=$overlap_start_x - $overlap_end_x;
	my $overlap_height=$overlap_start_y - $overlap_end_y;
	my $yoff1= # TODO y offset to beginning of overlap for 1
	my $yoff2= # TODO y offset to beginning of overlap for 2
	my $xoff1= $x1 - $overlap_end_x;
	my $xoff2= # TODO x offset to beginning of overlap for 2
	
	for (my $y=0; $y < $overlap_height; $y++) {
		return 1 if line_overlap(
			substr($frame1[$yoff1 - $y], $xoff1, $overlap_width),
			substr($frame2[$yoff2 - $y], $xoff2, $overlap_width));
	}
	return 0;
}

# Draws a frame of the specified animation at the specified position.
# The first parameter is a reference to a counter that holds the last
# displayed frame. Note that frames are drawn with their lower-right corner
# starting at the given position.
sub drawanim ($$$$) {
	my $fref=shift;
	my $y=shift;
	my $x=shift;
	my $animation=shift;

	if (! defined $fref) {
		my $f=0;
		$fref=\$f;
	}
	
	for my $line (0..$animation->{height} - 1) {
		SLsmg_gotorc($y - $animation->{height} + $line, $x - $animation->{width});
		SLsmg_write_string($animation->{animation}->[$$fref]->[$line]);
	}
	$$fref++;
	if ($$fref > $#{$animation->{animation}}) {
		$$fref=0;
	}
}

# Undoes a drawanim.
sub clearanim ($$$) {
	my $y=shift;
	my $x=shift;
	my $animation=shift;

	my $space=(" " x $animation->{width});
	for my $line (0..$animation->{height} - 1) {
		SLsmg_gotorc($y - $animation->{height} + $line, $x - $animation->{width});
		SLsmg_write_string($space);
	}
}

# Mirrors an ascii line. Currently quite crude, doesn't support
# # switching / for \ etc, so only use it for basic blocks.
sub mirror_line ($) {
	$_=join "", reverse split //, shift; # every perl program is required
	s/\]\[/[]/g;                         # by law to have at least one
	$_;                                  # line like this one
}

# Mirrors an ascii art animation.
sub mirror_animation ($) {
	my $in=shift;
	my $out=[];
	foreach my $frame (@$in) {
		my $newframe=[];
		foreach my $line (@$frame) {
			push @$newframe, mirror_line($line);
		}
		push @$out, $newframe;
	}
	return $out;
}

# Generate a mirror image of a block.
sub mirror_block ($) {
	my $block=shift;
	return {
		frequency => $block->{frequency},
		width => $block->{width},
		height => $block->{height},
		animation => mirror_animation($block->{animation}),
	}
}

# Add mirror image blocks to the list and sum up the total of all the
# blocks' frequencies.
sub block_init () {
	my @mirror_blocks;
	foreach my $block (@blocks) {
		$block_freqtotal += $block->{frequency} * 2; # double for mirror
		push @mirror_blocks, mirror_block($block);
	}
	push @blocks, @mirror_blocks;
}

# Start a player action.
sub start_action ($) {
	$action=shift;
	$frame=0;
	$action_duration=$actions{$action}->{duration};
}
# Continue the ongoing action.
sub continue_action () {
	$action_duration=$actions{$action}->{duration};
}

# Select a block and start it falling.
sub start_block () {
	my $selection=int(rand($block_freqtotal));
	my $c=0;
	my $block;
	foreach (@blocks) {
		$c += $_->{frequency};
		if ($selection < $c) {
			$block=$_;
			last;
		}
	}
	if (! $block) {
		die "internal error choosing block ($selection)";
	}
	
	push @falling_blocks, {
		y => 0,
		x => int(rand($s_cols - $block->{width}) + $block->{width}),
		frame => 0,
		block => $block,
	};
}

# Stop a block from falling.
sub stop_block ($) {
	my $block=shift;
	# Asumes there are not many simulantaneous falling blocks.
	for (my $i=0; $i <= $#falling_blocks; $i++) {
		if ($block eq $falling_blocks[$i]) {
			splice(@falling_blocks, $i, 1);
			last;
		}
	}
}

# Catch a falling block.
sub catch_block ($) {
	my $block=shift;

	start_action("catch");
	stop_block($block);
	$held_block=$block;
	# The held block's position will be
	# relative to the player now.
	$held_block->{yoffset}=$y - $held_block->{y};
	$held_block->{xoffset}=$x - $held_block->{x};
	$dumping=0;
}

# Dump a held block in the specified direction.
sub dump_block ($) {
	my $direction=shift;
	
	# TODO: animation of block being dumped, including collision
	# detection with landscape and other falling blocks.
	
	# Update block position to be to left or right of player.
	$held_block->{y}=$y - $held_block->{yoffset};
	if ($direction == 1) {
		$held_block->{x} = $x + $held_block->{block}->{width};
	}
	else {
		$held_block->{x} = $x - $actions{$action}->{width};
	}
	
	start_action("stand");
	push @falling_blocks, $held_block;
	$held_block=undef;
	$dumping=0;
}

# Rotate a block overhead.
sub rotate_block () {
	# TODO: collision detection
	# Make xoffset be the xoffset measured from the opposite corner of
	# the person and block than the ones normally used.
	$held_block->{xoffset}=$actions{$action}->{width} - $held_block->{xoffset} - $held_block->{block}->{width};
	$held_block->{block}=mirror_block($held_block->{block});
}

block_init();
$x=int($s_cols / 2);
$y=$s_rows;
start_action("wave");

MAINLOOP:
while (1) {
	# Perform player movement due to action.
	$action_duration--;
	$y += $actions{$action}->{y};
	$x += $actions{$action}->{x};
	if ($x <= $actions{$action}->{width}) {
		$x=$actions{$action}->{width};
	}
	elsif ($x > $s_cols) {
		$x=$s_cols;
	}
	# TODO: falling block and landscpe collision detection!

	# Perform block movement and draw blocks.
	my @list=@falling_blocks;
	foreach my $block (@list) {
		clearanim($block->{y}, $block->{x}, $block->{block});
	}
	foreach my $block (@list) {
		$block->{y} += $block_fallspeed;
		drawanim(\$block->{frame}, $block->{y}, $block->{x}, 
			 $block->{block});
		if ($block->{y} >= $s_rows) {
			stop_block($block);
		}

		# TODO: Something must be done for the case where the
		# player is holding one block and catches another.
		# Probably just need to take into account the block a
		# player is holding when detecting a catch, and merge the
		# blocks together into one block.
		
		# Check if the block is right over the player,
		# if so they put their hands up to catch it.
		# Note that this comes _after_ the player's movement due
		# to the old action, but before the player is displayed.
		# This allows the player to still move while they have
		# their hands up, to better position under the block.
		if ($block->{y} > $y - $actions{$action}->{height} - $block->{block}->{height} - 1 &&
			area_overlap($block->{x}, $block->{block}->{width},
		            $x, $actions{$action}->{width})) {
	    		start_action("hands_up");

			# Now do the more expensive check to see if part
			# of the bottom of the block is actually touching
			# the player's hands, if so they start holding it.
			if (frame_overlap(
			          $y, $x, $actions{$action}->{animation}->[$frame],
				  $actions{$action}->{width},
				  $actions{$action}->{height},
			          $block->{y}, $block->{x},
				  $block->{block}->{anumation}->[$block->{frame}],
				  $block->{block}->{width},
				  $block->{block}->{height})) {
			  	catch_block($block);
			}
		}
	}
	if (@falling_blocks < 1) {
		start_block();
	}
	
	# Draw player and display screen.
	drawanim(\$frame, $y, $x, $actions{$action});
	drawanim(\$held_block->{frame}, $y - $held_block->{yoffset},
	         $x - $held_block->{xoffset}, $held_block->{block})
		if $held_block;
	SLsmg_gotorc(0,0);
	SLsmg_refresh();
	select undef, undef, undef, 0.075;
	
	# Clear player picture from screen. Do it here while the action
	# is the same in case the frame size changes.
	clearanim($y, $x, $actions{$action});
	clearanim($y - $held_block->{yoffset},
		  $x - $held_block->{xoffset}, $held_block->{block})
		if $held_block;
	
	# Handle input.
	while (SLang_input_pending(0)) {
		my $k = SLkp_getkey();
			
		if ($k eq SL_KEY_LEFT) {
			if ($held_block) {
				if ($dumping) {
					dump_block(-1);
				}
				elsif ($action eq "carry_left") {
					continue_action();
				}
				else {
					start_action("carry_left");
				}
			}
			elsif ($action eq "walk_left") {
				start_action("run_left");
			}
			elsif ($action ne "run_left" &&
			       $action ne "walk_left") {
				start_action("walk_left");
			}
			else {
				continue_action();
			}
		}
		elsif ($k eq SL_KEY_RIGHT) {
			if ($held_block) {
				if ($dumping) {
					dump_block(1);
				}
				elsif ($action eq "carry_right") {
					continue_action();
				}
				else {
					start_action("carry_right");
				}
			}
			elsif ($action eq "walk_right") {
				start_action("run_right");
			}
			elsif ($action ne "run_right" &&
			       $action ne "walk_left") {
				start_action("walk_right");
			}
			else {
				continue_action();
			}
		}
		elsif ($k eq SL_KEY_UP) {
			if ($held_block) {
				$dumping=!$dumping;
			}
		}
		elsif ($k eq ord(" ")) {
			if ($held_block) {
				rotate_block();
			}
		}
		elsif ($k eq ord('q')) {
			last MAINLOOP;
		}
	}

	# Check for expired actions.
	if ($action_duration < 1 && $action_duration > -1) {
		if ($action =~ /(walk|run)_left/) {
			start_action("stand_left");
		}
		elsif ($action =~ /(walk|run)_right/) {
			start_action("stand_right");
		}
		elsif ($action eq 'stand') {
			start_action("tap");
		}
		elsif ($action eq 'catch') {
			start_action("holding");
		}
		elsif ($held_block) {
			start_action("holding");
		}
		else {
			start_action("stand");
		}
	}
}

SLsmg_refresh();
SLang_reset_tty();
SLsmg_reset_smg();
