#!/usr/bin/perl
#
#Send-insults 2.0
#Copyright 1994, 1995 by Joey Hess <jeh22@cornell.edu>
#
#Sends insults to the insult-daemon mailing list.
#Reads ~/email-list, and sends to everyone in there, if it's the right time.
#This program is intended to be run once per day.
#
#~/email-list is of the format:
#address frequency counter
#Where address is the email address, frequency is how many days between 
#insults, and counter is how long till the next insult.
#If a person is desperate for 'em, they can have multiple entries in the 
#file...
#
#The send-insult.txt file is the template for the message, and 
#it can contain ~insult~, ~user~ and ~count~ variables.
#
#
#
#uses insult-functions.pl to get our insult.
push(@INC,"/home/joey/prog/insults/");
require "insult-functions.pl";

#slurp in the message
my $message='';
open (IN,"<$ENV{HOME}/send-insult.txt");
while (<IN>) {
	$message.=$_;
}
close IN;

#slurp in our database.
open (IN,"<$ENV{HOME}/email-list");
my @db=<IN>;
close IN;
$count=$#db+1;

#process database, and write it back to the file
#also send the mail.
open (OUT,">$ENV{HOME}/email-list");
foreach (@db) {
  s/\n$//;
  my ($address,$frequency,$counter)=split(/ /,$_,3);
  if ($address) { #otherwise, trim the blank line.
  	if (--$counter eq 0) {
  		$counter=$frequency; #reset it.
  		open (MAIL,"|mail -s \"Insult Mailing List\" $address");
			$user=&GetPreviousUser;
			$insult=&InsultString;
			$_=$message;
  		s/~insult~/$insult/ig;
  		s/~user~/$user/ig;
  		s/~count~/$count/ig; #the number of users at this time.
  		print MAIL $_;	
  		close MAIL;
  	}
  	print OUT "$address $frequency $counter\n";
  }
}
close OUT;
