#!/usr/bin/perl
#
# copyright Joey Hess <jeh22@cornell.edu>
# freely distributable under the terms of the gnu public license
# version 2.0 and up.
#
#Quick insult generator
#
#Reads from stdin, expands vars, prints to stdout.
#Possible vars:
#  ~insult~ --- place the insult here.
#  ~param1~ --- 1st parameter
#  ~param2~ --- 2nd parameter

require '/home/joey/prog/insults/insult-functions.pl';

$insult=&InsultString;

while (<>) {
  s/~insult~/$insult/gi; #do expansion
  s/~param1~/@ARGV[0]/gi;
  s/~param2~/@ARGV[1]/gi;
  print;
}
