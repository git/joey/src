#!/usr/bin/perl
#
#copyright Joey Hess <jeh22@cornell.edu>
#freely distributable under the terms of the gnu public license
#version 2.0 and up.
#
#A cgi script to insult a user on the WWW.
#
# set this to the location of the files that are required below
push(@INC,'/home/joey/prog/include/');
require 'cgi-lib.pl';
require 'insult-functions.pl';

&ReadParse;

$insult=&InsultString;
$last=&LastUser;
$name=$in{'name'};
if ($name eq '') { 
  $name='oh bashful one' 
} 
else {
  if ($name eq $last) { #get a new last name, so a person diesn't insult themselves.
    $last=&GetPreviousUser;
  }
  &SaveLog($name);
  &SaveLast($name);
} 

print &PrintHeader;
print "
<TITLE>Guess What?</TITLE>
$last said \"Thou art $insult, $name!\"<P>
[ <A HREF = \"$base/\">Kite</A> | <A HREF = \"$base/insult.html\">Insults</A> | <A HREF =\"$base/bymail.html\">Insults by Mail</A> | <A HREF=\"$base/insult-addict.html\">Are you an Insult Addict?</A> | <A HREF=\"$base/feedback.html\">Feedback</A> ]
<HR><I>insult-maker</I>
";
