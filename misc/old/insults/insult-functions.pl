#!/usr/bin/perl
#
#<jeh22@cornell.edu>
#
#Handly insult functions library
#

#Configuration: where is the storage subdirectory?
$storage='/home/www/cgi/storage/';
#Configuration: where is the insult subdirectory?
$insdir='/home/joey/prog/insults/';
#Configuration:set this to the base of urls on the server (ie, '/~joey')
$base='';


#Return a random line from a file.
#param: number of insult file to use - 1, 2, or 3.
#Note: to protect against repeating words, this never gives the same line of a
#file twice in the same session. It keeps a small 3 element array to ensure
#that.
sub OneWord {
  $n=shift;
  open(INFILE, '<'.$insdir.'insult.'.$n);
  $num=<INFILE>;
  $a='';
  while (($a eq '') || ($protect[$n] eq $a)) { $a = int(rand($num-1))+1 }
  $protect[$n]=$a;
  while ($a>0) {
    $a--;
    $n=<INFILE>;
  }
  chop $n;
  return $n;
}

#Save name to logfile (pass name as parameter)
sub SaveLog {
  open (LOG, '>>'.$storage.'insultees');
  print LOG "@_[0]\n";
  close LOG;
}

#Save name in insult.last (pass name as parameter)
sub SaveLast {
  open (LAST, '>'.$storage.'insult.last');
  print LAST "@_[0]\n";
  close LAST;
}

#return the name of the last user.
sub LastUser
{
  open (LAST, '<'.$storage.'insult.last');
  $r=<LAST>;
  close LAST;
  chop($r);
  return $r;
}

#return the name of a previous user.
#Reads the 'insultees' file, and picks one of the 1st 1000 names therin.
sub GetPreviousUser {
  open (IN, '<'.$storage.'insultees');
  $a = int(rand(1000))+1;
  while (($a>0) || ($n eq '')) {
    $a--;
    $n=<IN>;
    chop $n;
  }
  close IN;
  return $n;
}


#Returns the full insult string, including a leading "a" or "an".
#Insults can be of several formats, defined here:
@insultformats=("1 2 3","2 1 3","1 2 2 3");
#A or an is picked depending on the first letter of the first word of the 
#insult.
sub InsultString {
  $method=int(rand($#insultformats+1));
  @a=split(/ /,$insultformats[$method]);
  $_='';
  foreach $a (@a) {
    $_=$_.&OneWord($a).' ';
  }  
  s/ $//; #kill trailing space.
  if (/^[a,e,i,o,u]/) { $_='an '.$_ } else { $_='a '.$_ }
  return $_;
}
                    
srand($$ ^ time); #seed random number generator.
1