#!/usr/bin/perl
#
#Insult by mail generator. (cgi script)
#Uses a form with fields: name,name2,email,email2 - sends to email2
#
#copyright Joey Hess <jeh22@cornell.edu>
#freely distributable under the terms of the gnu public license
#version 2.0 and up.

# set this to the location of the files that are required below
@INC=('/home/joey/prog/include');
require 'cgi-lib.pl';
require 'insult-functions.pl';

#If you don't want people to send you insults, add your email addresse(s) here: 
$ignoreme{'jeh22\@cornell.edu'}='ignore';
$ignoreme{'joey@preferred.com'}='ignore';
$ignoreme{'root@preferred.com'}='ignore';

&ReadParse;
print &PrintHeader;

print "<TITLE>Insult By Mail</TITLE>";

#are all the items ok?
if (($in{'name'} eq '') || ($in{'name2'} eq '') || ($in{'email'} eq '') || ($in{'email2'} eq '')) {
  print 'Error - You did not fill in all the fields on the form. Please try again.<P>'; 
}
else {
  if (($ignoreme{$in{'email'}} eq 'ignore') || ($ignoreme{$in{'email2'}} eq 'ignore')) {
    print "<H1>Error<\/H1>\n Please - don't try to send me insults. I've had enough, already :-)<P>\n";
  }
  else {
    #test to see if this is a valid address by looking up its host
    #(thanks, Chris!)
    ($a,$hostname)=split(/\@/,$in{email2},2);
    if (`host $hostname` eq '') {
      print '<h1>Error</h1> Unable to lookup host "'.$hostname.'", so the mail was not sent.<P>';
    }
 		else {
	    $i=&InsultString;

	    #the command we will use to send the insult:
	    $mail='/usr/sbin/sendmail '.$in{'email2'};
	    open (OUT,"|$mail");

	    #write the message:
	    print OUT "From: ".$in{'email'}."\n";
	    print OUT "Reply-To: ".$in{'email'}."\n";
	    print OUT "Subject: Guess what?\n";
	    print OUT "X-Mailer: Insult-by-mail\n\n";
	    print OUT "At the request of ".$in{"name"}.", the Shakespearean Insult\n";
	    print OUT "Service (http://kite.ithaca.ny.us/insult.html)\n";
	    print OUT "has generated this semi-personalized, random insult\n";
	    print OUT "in the tone of the Bard, especially for you.\n\n";
	    print OUT '"Thou art '.$i.', '.$in{'name2'}."!\"\n\n"; 
	    print OUT $in{'message'};
	    close OUT; #and send off the message
	
  	  #finish up.
	    &SaveLog($in{'name'}); #save name to insult log
	    &SaveLast($in{'name'}); #save to last insulter..
	
	    #save email addresses and names to our own log
	    open (OUT,'>>'.$storage.'insult-by-mail.log');
	    print OUT $in{'name'}.' <'.$in{'email'}.'> - '.$in{'name2'}.' <'.$in{'email2'}.">\n";
	    close OUT;
	    print "
You shouldn't say such unkind things about $in{'name2'}!
\"$i\", indeed!<P>
I'm going to tell $in{'name2'} about this...<P>
";
		}
  }
}
print "\n[ <A HREF = \"$base/\">Kite</A> | <A HREF =\"$base/insult.html\">Insults</A> | <A HREF =\"$base/bymail.html\">Insults by Mail</A> | <A HREF=\"$base/feedback.html\">Feedback</A> ]\n";
print '<HR><I>insult-by-mail</I>';
