#!/usr/bin/perl
#
#Perlcurses demo.
#

push (@INC,'/home/joey/prog/include');

srand($$ ^ time);
require 'screen.pl';

&curses::init;
&screen::init;
&curses::hidecursor;
&curses::clear;
&curses::backcolor($BLUE);
&curses::forecolor($BLACK);
foreach (0..24) {
	&curses::pos($_,15);
	print "a";
}
#$|=1;
#&screen::horiz_line(10,20,10);   
#&screen::vert_line(10,10,10);
&screen::box(2,2,60,10,' ');
<>;
&curses::showcursor;
&curses::backcolor($BLACK);
&curses::forecolor($WHITE);
&curses::clear;
&screen::done;
