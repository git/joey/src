/* code for a generic machine. It can be on or off, and when on, it prints
   periodic messages. You can tell its on just by looking at it.

 set properties:
 @set x.time to "" -- time between messages
 @set x.on to "" -- on to start with? 1 or ""
 @startup x is "starts up, whatever..."
 @stop x is "stops, or whatever.."
 @set x.going_msg to {"hums quietly, or whatever"} <- must be a list.
 @set x.num_going_msgs to 1 <- number of elts in above list.
 @on x is "The machine is on" <- only displayed if on when machine is looked at.
 @off x is "The machine is off" <- only if looked at when off. 

 This defines the start*up verb, and a helper verb:
*/
if (!this.on)
  this.location:announce_all("The ",this.name," ",this.startup_msg);
  this.on = 1;
  this:it_keeps_going_and();
else
  player:tell("The ",this.name," is already turned on.");
endif

/* keep forking them out until turned off.. */

if (this.on)
  this.location:announce_all("The ",this.name," ",this.going_msg[random(this.num_going_msgs)]);
  fork tokill (this.time)
    this:it_keeps_going_and();
  endfork
  this.tokill=tokill;
endif

/* here we can turn it off with stop/off verb: 
   and kill the it_keeps_going_and task that is running. */
if (this.on)
  this.location:announce_all("The ",this.name," ",this.stop_msg);
  this.on = "";
  if (this.tokill > 0) 
    kill_task(this.tokill);
  endif
else
  player:tell("The ",this.name," is already turned off.");
endif

/* enhance the look command to show the description+additional text if it's
   turned on */
@program generic_machine:description
basic = pass(@args);
if (this.on)
  return basic + " " + this.on_msg;
else
  return basic + " " + this.off_msg;
endif
