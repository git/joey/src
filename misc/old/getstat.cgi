#!/usr/bin/perl

#Display amount of time a user has spent online.
#
#Written for Preferred Interent, Inc., by Joey Hess, <jeh22@cornell.edu>
#Copyright 1996 by Joey Hess.

#Version: 0.1

#-------------- user configurable information ------------------------------#

#The following line should point to the datafile we read to get accounting 
#information on users:

$acfile="/home/joey/totals"; #for testing.
#$acfile="/detail/totals";

#The following should be a html document that will be returned if this
#program is called and no name is entered. It should display a form that
#calls this program, with an input field named "name" where they can enter 
#their login name. Terminate the document with "eofform" of a line by
#itself.

$errorform=<<eofform;

<title>Preferred Internet Useage Meter</title>
<h1 align=center>How much time have you used?</h1>
<form method="post" action="http://preferred.com/~joey/getstat.cgi?">
Enter your name here: <input name="name"><br>
<input type=submit value="How much time have I used?">
</form>

eofform
# The above line terminates the error document.

#The following should be a html document that is returned when we find a 
#match to someone in the datafile. It has 2 "tags" that are replaced with 
#data from the datafile:
# ~name~ is changed to the user's login name.
# ~min~ is replaced with how many minutes they have used.
# ~sec~ is replaced with how many seconds they have used.
#Terminate the document with "eofform" on a line by itself.

$okform=<<eofform;

<title>Preferred Interent Useage Meter</title>
<h1 align=center>You have used ~min~ minutes</h1>
~name~ has used a total of ~min~ minutes and ~sec~ seconds on preferred.com.

eofform
# The above line teminates the document.

#The following document is displayed when they enter a name that doesn't show 
#up in our accounting info. It can have a ~name~ tag in it. End the document
#with "eofform" on a line by itself.

$badnameform=<<eofform;

<title>Preferred Interent Useage Meter</title>
<h1 align=center>Unknown login name</h1>
<p>You entered in ~name~, which is not a valid login name. Please enter in 
your proper login name below.
<hr>
<form method="post" action="http://preferred.com/~joey/getstat.cgi?">
Enter your name here: <input name="name"><br>
<input type=submit value="How much time have I used?">
</form>

eofform
# The above line teminates the document.

#---------------no user servicable parts below this point ------------------#

#Ok, so you could change this if you wanted to. Maybe it is a little user
#serviceable.
$help=<<eofhelp;

Run this CGI script via the web, not at the command line!

eofhelp

#Fix a string formatted so it'll work with CGI into the normal string
#format.
#
sub CgiFix { my $s=shift;
	#Convert plus's to spaces
	$s =~ s/\+/ /g;
	# Convert %XX from hex numbers to alphanumeric
	$s =~ s/%(..)/pack("c",hex($1))/ge;
  
	return $s;
}

#Read and parse httpd input, and save it to @in.
#Portions of this subroutine are adapted from cgi-bin.pl,
#Copyright 1994 Steven E. Brenner <S.E.Brenner@bioc.cam.ac.uk>.
#(about 11 lines remain unmodified..)
#
sub ReadParse {
  # Read in text from form.
  my($in);
  if ($ENV{'REQUEST_METHOD'} eq 'GET') {
    $in = $ENV{'QUERY_STRING'};
  }
  elsif ($ENV{'REQUEST_METHOD'} eq 'POST') {
    read(STDIN,$in,$ENV{'CONTENT_LENGTH'});
  }
  else { #what?! We're being run at the _command_ line!
    exit print $help; #display help and exit.
  }
  @in = split(/&/,$in);
  foreach $i (0 .. $#in) {
    # Split into key and value.
    my($key, $val) = split(/=/,$in[$i],2); # splits on the first =.
    # Convert %XX from hex numbers to alphanumeric
    $key=CgiFix($key);
    $val=CgiFix($val);
    # Create saveform variable out of it, store it in @in.
    $in{"$key"} .= "\0" if (defined($in{"$key"})); # \0 is the multiple separator.
    $in{"$key"} .= $val;                  
  }                                       
}                    

#Print Mime header.
sub PrintHeader { print "Content-type: text/html\n\n" }

&PrintHeader;
&ReadParse;

if ($in{name} ne '') {
	open (IN,"<$acfile") || exit print "Unable to read accounting info file. Please report this problem to the preferred.com sysadmins!\n";
	#Search for a match to the given name. We assume that all names are
	#case-insensitive, and contain no whitespace.
	$in{name}=lc($in{name});
	while (<IN>) {
		chomp; #remove \n.
		s/ \t/  /s; #remove extraneous whitespace.
		($name,$amount)=split(/ /,$_,2);
		$name=lc($name);
		if ($name eq $in{name}) {
			($min,$sec)=split(/\./,$amount,2);
			$okform=~s/~name~/$name/ig;
			$okform=~s/~min~/$min/ig;
			$okform=~s/~sec~/$sec/ig;
			print $okform;
			exit;
		}
	}
	close IN;
	
	#If we fall through to here, they didn't enter a proper name.
	$badnameform=~s/~name~/$in{name}/ig;
	print $badnameform;
}
else { 
	print $errorform;
}
