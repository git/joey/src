#!/bin/csh
#Try this out. It creates a pipe and waits until it is read from, then
#executes whatever you want.
mknod ~/.plan p

start:
cat ~/.real_plan > ~/.plan              # this now blocks until plan is read

/bin/prog.to.execute
goto start      
