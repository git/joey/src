#!/usr/bin/perl # #Library of functions used to work with the database. #
#Joey Hess <jeh22@cornell.edu>
#

#Set the $quiet varible to true to turn off all the print statements.
#if you don't set $quiet, you probably want to set $| to 1, so the
#output isn't buffered.

#Load database into memory.
#
#This creates an array named @database which is composed of <file,num> pairs. 
#And an array named size that lists the total amount of bytes of each file transfered.
#And an array named @keys composed of <file,num> pairs. 
#Yes, that's a touch redundant :-) --***fix*** ---fix req. perl 5.0!
#
sub LoadDB {
  print "Loading Database.." unless $quiet;
  open(DB,"<".$dbfile);
  $n=0;
  while(<DB>) {
    #break up database line into discrete elements.
    $_ =~ s/\n//iego;
    $_ =~ s/ \B//ego; #trim extra spaces.   
    $_ = "~".$_."~";  #prepare for next step..
    $_ =~ s/~\"//iego; #which is to trim leading and trailing " characters.
    $_ =~ s/\"~//iego; # """
    @fields = split(/\",\"/,$_); #break up at "," 
    if ($fields[0] ne "") {
      $n=$n+1;
      $fields[0]=$fields[0]."\",\"".$fields[1]; #tack the date and the filename together.
      $database{$fields[0]}=$fields[2];
      $keys[$n]=$fields[0];
      $size{$fields[0]}=$fields[3];
    }
  }  
  close DB;
  print "$n database entries loaded.\n" unless $quiet;
}

#This saves the database file from memory.
#Writes a comma - delimeted database of the form:
#"filename","numtransferred" (more items to be added later)
#
#You may optionally pass this a filename to save to, otherwise, it uses the
#value in $dbfile. A second parameter, of "y" will cause it to look up the 
#title of the page, and write it out as the last field. This is handy for reports you
#actually plan to display.
#
sub SaveDB {
	if ($_[1] eq "y") { $disp_title=1 }
  if (@_[0] ne "") { $savefile=@_[0] } else { $savefile=$dbfile }
  print "Saving Database to $savefile\n" unless $quiet;
  open (DB,">".$savefile);
  foreach $i (0 .. $#keys) {
    $_ = $keys[$i];
    if (($database{$keys[$i]} > 0) && ($keys[$i] ne "")) {
      print DB "\"".$keys[$i]."\",\"".
                    $database{$keys[$i]}."\",\"".
                    $size{$keys[$i]}."\"\n";
    }
  }  
  close DB;
}

#Processes a database according to a map file.
#The map file specifies which files can stay in the database, and 
#can create new files by combining groups of files in the database.
#Map file command formats:
#      for pattern change text to replacement
#      delete pattern
sub ProcessMap {
  open(MAP,"<".@_[0]);
  while (<MAP>) {
    ($command,$rest)=split(/ /,$_,2);
    $rest =~ s/\n//g;
    if (($command eq "change") || ($command eq "copy")) {
      ($pattern,$b,$text,$c,$replacement) = split(/ /,$rest,5);
      if (($replacement eq "") && ($c eq "")) {
        $replacement=$text;
        print $command."ing $pattern to $replacement\n" unless $quiet;
      }
      else {
        print $command."ing $pattern where $text is $replacement\n" unless $quiet;
      }
      if ($_ ne "") {
        foreach $i (0 .. $#keys) {
          if (&like($pattern,$keys[$i]) ne "") { #this line matches.
            if ($c ne "") { 
              $_=$keys[$i];
              s/$text/$replacement/ig;
            }
            else {
              $_=$replacement;
            }
            if ($_ ne $keys[$i]) {
              if (!($database{$_} > 0)) { #create a new entry.
                $keys[$#keys+1]=$_;
                $database{$_}=$database{$keys[$i]};
                $size{$_}=$size{$keys[$i]};
              }
              else { #add to an old entry..
                $database{$_}=$database{$_}+$database{$keys[$i]};
                $size{$_}=$size{$_}+$size{$keys[$i]};
              }
              if ($command ne "copy") { #delete old entry..
                $database{$keys[$i]}="";
                $size{$keys[$i]}="";
              }
            }
          }
        }
      }
    }
    elsif ($command eq "delete") {
      print "Deleting $rest\n" unless $quiet;
      foreach $i (0 .. $#keys) {
        if (($keep[$i] ne "y") && (&like($rest,$keys[$i]) ne "")) { 
          $database{$keys[$i]}="";
          $size{$keys[$i]}="";
        }
      }
    }
    elsif ($command eq "keep") {
      print "Keeping $rest from deletion.\n" unless $quiet;
      foreach $i (0 .. $#keys) {
        if (&like($rest,$keys[$i]) ne "") {
          $keep[$i]="y";
        }
      }
    }
  }
}

sub InitDB {
  #Init stuff - set a few handy variables users can use in date-matching:
  #Get today's date (messy..)
  open (OUT,">".$tempdir."temp");
  print OUT "temp\n";
  close OUT;
  ($dev,$ino,$mode,$nlink,$uid,$gid,$rdev,$size,$atime,$mtime,$ctime,$blksize,$blocks)= stat($tempdir."temp");
  ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime($ctime);
  @months=split(/ /,"Jan Feb Mar Apr May Jun Jul Aug Sept Oct Nov Dec");
  if($NT eq "y") { #NTperl has messed up months..fix that.
    $mon=$mon+1; 
  }
  if ($IgnoreCase eq "yes") { $months[$mon] =~ tr/A-Z/a-z/ } 
  if ($filetype eq "emwaclog") { 
    $date=" ".$months[$mon]." $mday "; 
  }
  else {
    $date="$mday\/".$months[$mon]."\/";   
  }
  if (length($mday) eq 1) { $mday="0$mday" } #add those zeros..
  $mon=$mon+1;
  if (length($mon) eq 1) { $mon="0$mon" }
  $year=~s/^19//;
  $year=~s/^20//; #safe 'till 3000 :-)
  $today="$mon\/$mday\/$year";
  $thismonth="$mon\/\*\/$year";
  $thisyear="\*\/\*\/$year";

  #quick moth-to-number lookups for reading log:
  foreach $i (1..12) {
    $months[$i -1 ]=~tr/A-Z/a-z/;
    $month_to_num{$months[$i - 1]}=$i;
  }

  if ($dbtype eq "address") { #change stuff so the address db is used:
    $dbfile=$dbaddress;
    $mapfile=$mapaddress;
    print "Processing addresses.\n" unless $quiet;
  }  
}
#return true.
1
