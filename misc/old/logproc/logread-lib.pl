#!/usr/bin/perl
#
#Library to read various types of HTTPD logs, and save in arrays.
#
#Joey Hess <jeh22@cornell.edu>
#

#Set the $quiet varible to true to turn off all the print statements.
#if you don't set $quiet, you probably want to set $| to 1, so the 
#output isn't buffered.

#Get's filesize for a file on a emwac server (since it's not logged)
#
sub emwacGetSize {
  #process filename, make it into a valid filename
  $fn=$serverroot;
  $f2=@_[0];
  $f2 =~ s:/:\\:g;
  $fn=$fn.$f2;
  return -s $fn; #return filesize   
}

#Parses 1 line of a log.
#
#Should fill the variables:
#$file -- file transfered (or, if $dbtype="address", the address sent to.
#$size -- number of bytes transferred.
#$date -- when the hit occurred. (mm/dd/yy format)
#
sub CutLog { 
  @parts=split(/ /,@_[0]);
  if ($filetype eq "emwaclog") {
    $d[0]=$parts[2];
    $d[1]=$parts[1];
    $d[2]=$parts[4];
    if ($dbtype ne "address") {
      $file=$parts[8];
    }
    else {
      $file=$parts[6];
    }
    $size=&emwacGetSize($parts[8]);
  }
  elsif ($filetype eq "httpdlog") {
    $d=$parts[3];
    $d=~s/\[//g;
    ($d,$a)=split(/:/,$d,2);
    @d=split(/\//,$d,3);
    if ($dbtype ne "address") {
      $file=$parts[6];
    }
    else {
      $file=$parts[0];
    }
    $size=$parts[$#parts]; #last element, in http 1.0 or 0.9
  }
  if ($old ne "$d[0]`$d[1]`$d[2]") {#save time by buffering the last date.
    $d[1]=~tr/A-Z/a-z/;
    $d[1]=$month_to_num{$d[1]};
    if (length($d[0]) eq 1) { $d[0]="0$d[0]" } #add those zeros..
    if (length($d[1]) eq 1) { $d[1]="0$d[1]" }
    $d[2]=~s/^19//;
    $d[2]=~s/^20//; #safe 'till 3000 :-)
  }
  $old="$d[0]`$d[1]`$d[2]";
  $date="$d[1]\/$d[0]\/$d[2]";
}

#Process a log file
#Pass the filename of the log to process (- for stdin).
#Simply read in each line, pull out the data we need, and increment 
#items in our database.
#
sub ProcessLog {
  $count="0";
  print "Processing ".@_[0]."...\n" unless $quiet;
  &Auto_NewLog(@_[0]);
  open(IN,"<".@_[0]);
  while (<IN>) {
    if (($_ ne "") && (&Auto_Ignore($_)) ne "1") {
      chop;
      if ($IgnoreCase eq "yes") { tr/A-Z/a-z/ } #make lower case
      &CutLog($_);
      $file=$file."\",\"".$date; #lump file and date together..
      $database{$file}=$database{$file}+1;
      $size{$file}=$size{$file}+$size;
      if ($database{$file} eq 1) { $keys[$#keys+1]=$file } #new key..
    }
  }
  close IN;
  if ($auto eq true) { &Auto_Dispose }
}


#Return true
1
