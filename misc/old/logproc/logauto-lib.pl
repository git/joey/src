#!/usr/bin/perl
#
#Logproc library used in automatic processing of logs
#
#Contains functions to let logproc automate the processing of logs, when 
#logproc is run with the "-auto" switch. 
#

#Pass this procedure a line of the log.   
#Returns true if this line should be ignored.
#
sub Auto_Ignore {
  if ($auto eq true) {
    $_=@_[0];
    if (m/$date/o eq "") { 
      if ($deletelogs eq false) { #save this line in the old-logs dir.
        print OLDLOG;
      }
      $autotossed=$autotossed+1;
      return "0";
    } 
    else { 
      print NEWLOG;
      return "1";
    }
  }
  else {
    $autotossed=$autotossed+1;
    return "0";
  }
}

#Disposes of the file. 
#
sub Auto_Dispose {
  if ($auto eq true) {
    if (deletelogs eq false) {
      close OLDLOG;
    }
    close NEWLOG;
  }
  if ($autotossed ne 0) { 
    print "moving: $mv $tempdir$autofn $autofln\n" unless $quiet;
    system "$mv $tempdir$autofn $autofln"; 
  }
 print "$autotossed lines processed.\n" unless $quiet;
}

#Pass this the name of the file we're starting to process.
#
sub Auto_NewLog {
  if ($auto eq true) {
    $autofln=@_[0];
    if ($NT eq "n") { @path=split(/\//,@_[0]) } else { @path=split(/\\/,@_[0]) }
    $autofn=$path[$#path];
    if ($deletelogs eq false) { open (OLDLOG,">>$oldlogdir$autofn") }
    open (NEWLOG,">$tempdir$autofn");
    $autotossed=0;
  }
}


#Initialize a few variables
#
sub Auto_Init {
  if ($oldlogdir ne "") { $deletelogs=false } else { $deletelogs=true }
  print "Automatic mode on - processing all entries earlier than $date.\n" unless $quiet; 
}

#Return true
1
