#!/usr/bin/perl
#
#Log processor.
#Processes log file, and saves data to comma-delimed database.
#
#Syntax:
#  logproc.pl [-files|-addresses] [-auto] [-oldlogdir dir] [-quiet]
#              [-conf file] [filespec] 
#    -files.....process and save data to the files database.
#    -addresses.process and savwe data to the addresses deatabase.
#    -conf......specify the location of our .conf file.
#    -auto......after processing a file, dispose of it, as determined by the 
#               conf file. Also causes all entries dated today to be 
#               ignored, and only that part of the log file which was 
#               entered earlier to be disposed of. If the whole file was 
#               created today, then it's ignored entirely. 
#    -quiet.....turn off progress display.
#    -filespec..files to process.
#
#Joey Hess <jeh22@cornell.edu>
#

#This has to point to the location of startup.pl.
require "/home/joey/prog/include/startup.pl";

$conf="logproc.conf";

#parse parameters..save filespecs in @files
$|=1;
foreach $i (0..$#ARGV) {
  if ($ARGV[$i] eq "-quiet") 
    { $quiet=true ; $|=0}
  elsif ($ARGV[$i] eq "-auto") 
    { $auto=true }
  elsif ($ARGV[$i] eq "-conf")
    { $conf=$ARGV[++$i] ; $ARGV[$i]="" }
  elsif ($ARGV[$i] eq "-files")
    { $dbtype="file" }
  elsif ($ARGV[$i] eq "-addresses")
    { $dbtype="address" } 
  else 
    { @files=(@files,$ARGV[$i]) }
}

require $conf;
&InitDB;

#Proces the files, and add to database.
if ($auto) { &Auto_Init }
&LoadDB;
foreach $i (0..$#files) { 
  if ($files[$i] ne "") {&ProcessLog($files[$i])} 
} 
&ProcessMap($mapfile);
&SaveDB;
