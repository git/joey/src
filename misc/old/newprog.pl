#!/usr/bin/perl
#
#Newprog
#Inform users of new programs or admin-type changes to the system.
#
#Bugs: /etc/newprog not formatted well. This program is a pretty poor job,
#but I'm tired and need to get it done NOW :-)
#

$p=shift;
$p=~tr/A-Z/a-z/;
if ($p eq 'add') {
  open (OUT,'>>/etc/newprog') || die "You aren't allowed to add new programs to the list!\n";
  print "Add new program\n";
  print "  So, you've installed something new, and you're dying to let everyone\n";
  print "  know about it, eh?\n";
  print "\nEnter the name of the program:\n";
  $name=<>;
  print "\nGive me a short 1 line description of the new program:\n";
  $desc=<>;
  print "\nNow give me a longer description (enter . on a line by itself when done):\n";
  $done='';
  while (!$done) {
    $_=<>;
    if ($_ eq ".\n") { 
      $done=1;
    }
    else {
      $longdesc=$longdesc.$_;
    }
  }
  print "\nSaving entry..\n";
  print OUT $name;
  print OUT $desc;
  print OUT $longdesc.'.';
  print OUT "\n"; #for future expansion.
  close OUT
}
elsif ($p eq 'new') {
  open (IN,'<'.$ENV{HOME}.'/.newprogrc');
  $last=<IN>;
  close IN;
  open (IN,'</etc/newprog');
  $c=0;
  $first=1;
  while (<IN>) {
    $name=$_;
    $c=$c+1;
    $desc=<IN>;
    $done='';
    $longdesc='';
    while (!$done) {
      $_=<IN>;
      if ($_ ne ".\n") { $longdesc=$longdesc.$_ } else { $done=1 }
    }
    $misc=<IN>;
    if (($c <=> $last) eq 1) {
      if ($first) {
        $first='';
        print "\nNew programs have been added since your last logon:\n";
      }
      print "#$c	$name";
      print "	$desc";
    }
  }  
  close IN;
  if (!$first) {
    print "For more details, type \"newprog n\", where n is the number of the program.\n";
    open (OUT,'>'.$ENV{HOME}.'/.newprogrc');
    print OUT $c;
    close OUT;
  }
}
elsif ($p <=> 0 ) {
  unshift @ARGV,$p;
  open (IN,'</etc/newprog');
  $c=0;
  while (<IN>) {
    $c=$c+1;
    $name=$_;
    $desc=<IN>;
    $done='';
    $longdesc='';
    while (!$done) {
      $_=<IN>;
      if ($_ ne ".\n") { $longdesc=$longdesc.$_ } else { $done=1 }
    }
    $misc=<IN>;
    foreach $a (@ARGV) {
      if ($a eq $c) {
        print "#$c	$name";
        print "	$desc";
        print $longdesc;
      }
    }
  }
  close IN;
}
else {
  if ($p eq '') {
    print "
Newprog syntax:
newprog n [n n n ..]
  Displays entries from the newprog log file, /etc/newprog, where 'n' is the
  number of the entry to display.
newprog new
  Displays decriptions of all new programs since last logon, updates
  ~/.newprogrc
newprog add
  Interactively add a new program description to the log file.";
}

}