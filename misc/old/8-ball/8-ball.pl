#!/usr/bin/perl
#
#Magic-8-ball cgi script.
#
#Expects to find a file, 8-ball.conf, in the same directory as it.
#That file gives all the configuration options needed by the program.
#
#Load the conf file
open (CONF,"<8-ball.conf") || die "Can't read 8-ball.conf file!"; 
while (<CONF>) {
  chomp;
  ($a,$b)=split(/=/,$_,2);
  if (($b ne "") && ($a ne "")) {
    $conf{$a}=$b;
  }
}

srand($$ ^ time); #seed random number generator, big time :-).

#Get our phrase.
open (IN,"<".$conf{"datafile"});
$_=<IN>;
$a = int(rand($_))+1; 
while ($a>0) {
  $a--;
  $n=<IN>;
}            
close IN;

print "Content-type: text/html\n\n";

#Process the template
open (IN,"<".$conf{"template"});
while (<IN>) {
  s/~phrase~/$n/eigo;
  print;
}
close IN;