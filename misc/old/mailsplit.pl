#!/usr/bin/perl
#
#Split a mail spool file in 2, at the given date. The date must be in the 
#format used by the standard dat program (something like 
#mailsplit.pl `-date -d "3 days ago"` filename 
#works nicely.)
#
$date=split;
open (IN, 