#!/usr/bin/perl
#
#Produces a hex/bin/dec dump of a binary file.

#the screen width -- how can this be found?
$width=80;

$help=<<EOFHELP;
hexdump Produces a dump of a binary file.
	Syntax: hexdump [-2] [-dec|-hex|-bin]
		-dec Display data in decimal format
		-hex Display in hexadecimal format (default)
		-bin Display in binary format
		-2 Display 2 columns of data. The 1st is in hex, dec, or bin
			format, while the second is the corresponding text.
    -raw Don't filter out escape characters, display the actual character.
    	(tab and newline are still filtered out)

EOFHELP

if (($ARGV[1] eq '--help') or ($ARGV[0] eq '-h')) {
  print $help;
}
else {
  $type='H';
  $charwidth=3;
	$raw='';
  
  $cols=1;
  #parse params
  foreach (@ARGV) {
    if ($_ eq '-hex') {
    	$type='H';
    	$charwidth=3;
    }
    elsif ($_ eq '-bin') {
    	$type='B';
    	$charwidth=9;
    }
    elsif ($_ eq '-dec') {
			$type='C';
			$charwidth=4;
    }
    elsif ($_ eq '-2') {
      $cols=2;
    }
		elsif ($_ eq '-raw') {
		  $raw=1;
		}
    else { @a=$_ }
  }
  @ARGV=@a;
  
  #the template to unpack with
  $template=$type.$charwidth;

  if ($type eq 'C') { $decfix=1 } #fix decimals to make them 3 letters long.

	if ($cols eq 2) { #since we are displaying both the hext dump and the text and a space to seperate them..
		$width=$width-1;	
	  $charwidth=$charwidth+1;
  }

  #figure out how many characters go on a line.
	$perline=int($width/$charwidth);

  #display dump of data.
	$count=-1;
	while (<>) {
	  @a=split(//,$_);
	  foreach (@a) {	
			$a.=$_;
  	  $b[++$count]=unpack($template,"$_");
   	 	if ($count eq $perline) { #end of a line.
        if ($decfix) { #fix decimals to 3 letters.
					foreach (0..$count) {
						$b=$b[$_];
						if (length($b) ne 3) {
						  $b='0'.$b;
						}
            if (length($b) ne 3) {
              $b='0'.$b;
            }
            $b[$_]=$b;
					}
        } 
				if (!$raw) {
   	      $a=~tr [\200-\377][\000-\177];
   	    }
 	      $a=~s/[\n|\t]/~/g;
				if ($cols eq 2) {print "@b $a\n"} else {print "@b\n"}
 	      $a='';
      	$count=-1;
  	  }
 	  }
	}
}
