#!/usr/bin/perl
#
#Make a config file from a java app.
#

if (!$ARGV[0]) {
  exit print "Syntax: makeconfig javasource\n"
}

$a=shift(@ARGV);
while ($a ne '') {

	($a,$java)=split(/\.java/,$a);

	open (IN,"<$a.java") || exit print "Can't open java source $a.java\n";
	open (OUT,">$a.config") || exit print "Cannot write to $a.config\n";
	print OUT "# Autoconfig file for $a.java.\n\n";
	while (<IN>) {
		if (m:\/\/\#: ne '') {
			#found a config line.
			($first,$comment)=split(/\/\/\#/,$_,2);
			$first=~s/ $//;
			($tag,$comment)=split(/=/,$comment,2);
			print OUT "#$comment";
			print OUT "$tag=$first\n\n";
		}
	}
	close OUT;
	close IN;
	$a=shift(@ARGV);
}