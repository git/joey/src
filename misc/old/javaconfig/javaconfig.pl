#!/usr/bin/perl
#
#Auto-config a java app before compilation.
#

$a=$ARGV[0];
if (!$a) {
	exit print "Syntax: javaconfig configfile\n"
}

open (CONFIG,"<$a.config") || exit print "Can't open config file $a.config\n";
while (<CONFIG>) {
	chomp;
	s/\t/ /g;
	tr/ / /s;
  s/= /=/g;
  s/ =/=/g;
	if (/^#/ eq '') { #not a comment.
		($key,$value)=split(/=/,$_,2);
		if (($key) && ($value)) {
			$vars{$key}=$value;
		}
	}
}
close CONFIG;

open (IN,"<$a.java") || exit print "Can't open java source $a.java\n";
@source=<IN>;
close IN;

open (OUT,">$a.java") || exit print "Cannot write to $a.java\n";
foreach $line (@source) {
	if ($line=~m:\/\/\#: ne '') { #config line.
		$_=$line;
		($first,$rest)=split(/\/\/\#/,$_,2);
		($tag,$a)=split(/=/,$rest,2);
		print OUT "$vars{$tag} //#$rest";
	}
	else { print OUT $line }
}
close OUT;
