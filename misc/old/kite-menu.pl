#!/usr/bin/perl -i.bak
#Perl5 only!
#
#Yeah, in the year of our lord 1995 anto domini, it came to pass that Joey,
#being stressed out for he had not studied enough for the final to come, 
#said,
#  "Let there be a perl5 script. And let this script TAKE MY MIND OFF MATH FOR 
#   AT LEAST 10 MINUTES. And, let there be in ths script, methods for the 
#   creation of a list of links, to adorn adorn each page on my fair server."
#And Joey commanded:
#  "Let each page begin with the ordained tags:
#   <HTML>
#   <!-- kite:page-type -->
#   <TITLE>....</TITLE>
#   <BODY>
#   And end in a pleasing manner:
#   </BODY>
#   </HTML>
#   (But, in my infinite wisdom and knowledge of //i, I will forgive errors of 
#   case, as Unix is evil)
#   And, there will be a menubar, and it will be good. And the menubar will
#   appear after the <BODY>. And in that menubar will be arrayed links,
#   in a manner that is pleasing for the given page type, as is laid down
#   in the kite-menu.conf. 
#   And, there will be a trailer, and the trailer shall be follwed by the
#   </BODY>. And the trialer will be choosen as is laid down in the 
#   kite-menu.conf"
#And Joey smote the disk-drive, and there appeared a kite-menu.conf, and it
#read:
#  "i=name1~
#   i=name2
#   i=name3
#   i=name4
#   ...
#   i=namen
#
#   +t=title1
#   c=name:NAME  #change item:to this
#   f=footer #overrides dafult footer
#   -t
#   t=title2....."
#
#And if there so appears "~date~" in the footer, it shall be replaced forthwith with the 
#time of change of the file.
#
#And so it came to pass...

#$topmessage="Under reenovation after moving...<HR>";

@months=split(/ /,"January Febuary March April May June July August September October November December");
      
$0=~s/.pl//igo;
open (CONF,"<".$0.".conf") || die "can't read $0!\n";
$defaults=1;
$default_menu="<P ALIGN=\"center\"><TT>[~]</TT><P>"; #seed of our default menu
while (<CONF>) {
  chop;
  if (m/$\#/ eq "") { #ignore comments
    ($_,$a)=split(/=/,$_,2);
    if ((/i/ ne "") && ($defaults eq 1)) { #a default item. Grow the default menu.
      $default_menu=~s/~/$a|~/ig;
    }
    elsif ((/f/ ne "") && ($defaults eq 1)) { #default footer
      $default_footer=$a."\n";
    }
    elsif (/\+t/ ne "") { #beginning of a page-type. As we go through the defini
      if ($defaults eq 1) { #end of defining defaults
         $defaults=0;                                 
         $default_menu=~s/\|\~//ig;
      }                                           
      $type=$a;                                             
      $menu=$default_menu;
      $footer=$default_footer;
    }
    elsif ((/c/ ne "") && ($defaults eq 0)) { #Change an item for this page
      ($a,$b)=split(/:/,$a,2);
      $menu=~s/$a/$b/ig;
    }
    elsif (/f/ ne "") { #Override default footer
      $footer=$a."\n";    
    }
    elsif (/-t/ ne "") { #end of a page-type - save the data
      $menu=~s/\|\~//ig;
      $header{$type}="$menu\n";
      $footer{$type}=$footer;    
    }
  }
}
close CONF;

$type="";
while (<>) {
#s/<HR><b>This Friday, <A HREF=\"\/moving.html\">Kite is moving<\/A><\/b>!<HR> <b>This Friday, <A HREF=\"\/moving.html\">Kite is moving<\/A><\/b>!//;
  if ($ARGV ne $oldARGV) {# update last-modified-date
    ($dev,$ino,$mode,$nlink,$uid,$gid,$rdev,$size,$atime,$mtime,$ctime,$blksize,$blocks)= stat($ARGV);
    ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime($ctime);
    $date=$mday." ".$months[$mon]." ".$year;
  }
  if (/<\!-- kite:/i ne "") { #set page-type
    ($a,$b)=split(/<\!-- kite:/,$_,2);
    ($type,$b)=split(/ -->/,$b,2);
  }
  elsif (($type ne "") &&
         (/<BODY>/i ne "")) { #add header now
    $_.="<!-- kh -->".$topmessage." ".$header{$type};
  }
  elsif (($type ne "") &&
         (/<\/BODY>/i ne "")) { #add footer now
    $_="<!-- kf -->".$footer{$type}.$_;
  }
  elsif ((/<\!-- kh -->/ ne "") || 
         (/<\!-- kf -->/ ne "")) { #delete old header or footer
   $_="";
  }
  s/~date~/$date/gi;
  print $_;
}