#!/usr/bin/perl
#
#dirsearch
#
#Scan directories for text, return in a html format.
#
#Preforms recursive searches, but does not follow symlinks.
#
#By Joey Hess, jeh22@cornell.edu
#

$startup="/httpd/htdocs/cgi/startup.pl";

#either buffer output so we can sort it, or print it now.
sub output { $_=shift;
  if ($sort eq 'y') { 
		$output_list[$#output_list+1]=$_;
  }
  else {
    print $_;
  }
}

#Search the passed file
sub SearchFile {
  $fn=shift;
  $title=$fn;
  open (IN,"<".$dirtosearch.'/'.$fn);
  while (<IN>) {
    if (/\<title\>/i ne "") { #got page title
      ($a,$b)=split(/\<title\>/i,$_,2);
      ($b,$a)=split(/\<\/title\>/i,$b,2);
      if ($b ne "") { $title=$b }
    }
    if (&like("*".$tofind."*",$_) ne "") { #got it!
      close IN;
      if ($hit ne 1) { #print top of page
        $hit=1;
				&output($toptext."\n");
      }
      &output($textbeforeurl."<A HREF=\"$url$fn\">$title</A>".$textafterurl."\n");
    }
  }
  close IN;
}

require $startup;
$path=&GetPath;                 #The path to this program, and our INI file
&INIproc($path.'dirsearch.ini');#Read our INI
$not_picky=1;
@ARGV=&ARGVproc(@ARGV);         #Read our parameters.

require $likelib;
require $cgilib;
&ReadParse;
print &PrintHeader;

foreach $a (@in) { #look for the 1st thing filled in on the form.
  ($a,$b)=split(/=/,$a,2);
  if (($b ne '') && ($tofind eq '')) {
    $tofind=$b
  }
}

#get a hash of valid extentions.
@a=split(/ /,$validextentions);
foreach $a (@a) {
  $ext{$a}=1;
}

$hit = '';
#This is the list of directories we havn't searched yet:
@togo=split(/ /,$dirtosearch);
#And the corresponding list of urls.
@urls=split(/ /,$url);

while (@togo) { #while still places to look..
  $dirtosearch=shift(@togo);
  $url=shift(@urls);
  opendir(DIR,$dirtosearch) || die "Can't read from directory $dirtosearch.\n";
  $_=readdir(DIR);
  while ($_) {
    ($a,$ext)=split(/\./,$_,2);
    if ((-d $dirtosearch.'/'.$_ eq '') #Is this a normal file and does it have an ok extention?
        && ($ext{".".$ext} eq 1)) {
      &SearchFile($_);
    }
    elsif ((-d $dirtosearch.'/'.$_ ne '') #Add this subdirectory to the list of places we still need to search.
        && ($_ ne ".")                    #Unless it is . or .. directory.
        && ($_ ne "..")) {
      $togo[$#togo+1]=$dirtosearch.'/'.$_;
      $urls[$#urls+1]=$url.$_.'/';
    }
    $_=readdir(DIR);
  }
}
closedir DIR;

if ($hit ne '') { #print trailer
  if ($sort eq 'y') { #print sorted list now.
    foreach (sort @output_list) { print $_ }
  }
  print $bottomtext."\n";
}
else {
  print $nomatches."\n";
}