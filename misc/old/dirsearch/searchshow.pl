#!/usr/bin/perl
#
#Display a html page, highlighting all matches to a search string, and
#creating an index at the top of links to internal anchor urls inserted
#at the matches.
#
#Takes parameters of:
#page*searchfor

#where is startup.pl located?
require '/httpd/htdocs/cgi/startup.pl';

$path=&GetPath;                   #The path to this program, and our INI file
&INIproc($path.'searchshow.ini'); #Read our INI

#fix it so we cut off a $wordcount words around the search term in the index.
#foreach $a (1..$wordcount) {
#  $s.='~ ';
#}
#$wordadd=$s;
#$wordadd=~s/~ /zX /g; #add this on both sides of the string to be sure we match words.
#$matchstring=$s;
$matchstring=~s/~ /\\S+ /g; #this is pattern to match..
##$matchstring=~s/ $//;

require $cgilib;

&ReadParse;
print &PrintHeader;

$run=$ENV{"PATH_INFO"}; #in NT, pass as the "path", since nothing else is left.
$run =~ s/\///;

$programurl=~s/\*/$run/;

($page,$searchfor)=split(/\*/,$run,2);
$searchfor=~s/_/ /g;

#can't handle pattern matches -- sorry.
#also, if we were searching for " ", or "", what's the pint of a TOC? :-)
if (($searchfor eq '') || ($searchfor eq ' ') || ($searchfor=~/\*/ ne '') || ($searchfor=~/\?/ ne '')) { $pattern=1 } else {$pattern=''}

open (IN,'<'.$page) || print "<h1>Error</h1>The page you selected cannot be found!\n";

$basedir=$page;
#all we need to do is get rid of the filename at the end of $basedir.
#so, first find that filename:
@junk=split(/\//,$basedir);
$a=$junk[$#junk];
(@junk)=split(/\\/,$a);
$a=$junk[$#junk];
#then, remove it from the end of $basedir, and return the result:
$basedir=~s/$a$//i;
$basedir=~s/^$base//;

$body_found='';


$anchorcount=0;
while (<IN>) {
  if ((/<title>/i eq '')) {
    s/</|</g;
    s/>/>|/g;
    @words=split(/\|/,$_);
		$line=$_;
		if (($body_found eq '') && (/<body/i ne '')) {$body_found=1}
    foreach $_ (@words) {
      if (/\<img src/i ne '') { #fix images with relative paths..
        $b=$_;
        s/= /=/g;
        s/ =/=/g;
				$c=$_;
        ($_)=m/\<IMG SRC=\"(.*)\"/i;
        #ok, so you were a bad html-er and didn't put in quotes?
        if ($_ eq '') {
					$_=$c;
          s/=/= /g; 
					s/\|/ |/g;
					s/>/ >/g;
          ($_)=m/\<IMG SRC= (\S+)/i;
					if (/\// eq '') { $b=~s/$_/\"$basedir$_\"/ } #fix it.
			  }
				else {
				  if (/\// eq '') { $b=~s/\"$_\"/\"$basedir$_\"/ }
        }
				$_=$b;
      }
      if (/\<A HREF/i ne '') { #fix files with relative paths
        $b=$_;
        s/= /=/g;
        s/ =/=/g;
        ($_)=m/\<A HREF=\"(.*)\"/i;
        if (/\// eq '') { $b=~s/\"$_\"/\"$basedir$_\"/ } 
				$_=$b;
      }
      if ($pattern eq '') {
        if (/$searchfor/i) {
          $line=~s/<.*?>//g; #fix up the line for use an an anchor..
					$line=~s/\|//g;    
					$line=~s/\n/ /g;   
					$line=~s/\t/ /g;
					$line=~s/ / ~/g; #this is a temporary change to keep spaces in the right places.
					
					#cut it down to $wordcount words on each side of the match.
					($a,$b)=split(/$searchfor/i,$line);
					($thissearch)=m/($searchfor)/i; #this preserves the case of the search term.
					reset 'X'; #have to reset arrays.
					@X=split(/ /,$a);
					@X2=split(/ /,$b);
					foreach $x (reverse(($#X-$wordcount+1)..$#X)) {
					  if (($x <=> -1) eq 1) {$X=$X[$x].$X}
					}
					foreach $x (0..$wordcount-1) {
					  $X2.=$X2[$x];
					}
					$line=$X.$thissearch.$X2;

					$line=~tr/~/ /s;
          $line=~s/^ //;
          $line=~s/ $//;
					if ($line ne '') {
    				$anchor{$line}="<A HREF=\"$programurl#$anchorcount\">".$line."</A>";
           	$lines[$#lines+1]="<A NAME=\"$anchorcount\"></A>";
						$anchorcount=$anchorcount+1
          }
       }
				#highlight term, but preserve case.
        if ((/>/ eq '') && (/</ eq '')) { s/($searchfor)/$matchfront$1$matchback/igo }
      }        
      $lines[$#lines+1]=$_; 
    }
  }
  else { $lines[$#lines+1]=$_ }
}
close IN;

$index_printed='';

#if instructed to, sort the index. Othewise, convert it to an array w/o sorting.
if ($sortindex eq 'y') {
  foreach $key (sort(keys %anchor)) {
    @index[$#index+1]=$anchor{$key}
  }
}
else {
  foreach $key (keys %anchor) {
    @index[$#index+1]=$anchor{$key}
  }
}                                                            
if (($body_found eq '') && ($#index ne -1)) { #no <body>, so print at top.
  print $toptext;
  foreach $a (@index) {print "...$a...<BR>\n"}
	print $bottomtext;
} 

foreach $a (@lines)  {
  print $a;
  if (($#index ne -1) && ($a=~/<body/i ne '') && ($index_printed eq '')) { #anchors here.
    $index_printed=1;
    print $toptext;
    foreach $an (@index) {print "...$an...<BR>\n"}
		print $bottomtext;
  }
}
