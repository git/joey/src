#!/usr/bin/perl

$to=shift;
open (M,"|mail -s \"Signature program mailing list\" $to");
print M '
I\'m writing to let you know about a mailing list I thought you might be 
interested in. This list is for the discussion and creation of .signature
programs. These are small programs that fit neatly into a signature. Perhaps
the most famous of these is the export-a-crypto-system sig. This and other 
examples can be found at http://wwwis.cs.utwente.nl:8080/~faase/Sign/

Anyway, the mailing list is sigprog-l@bbs.hkis.edu.hk. To subscribe,
send a message to listproc@bbs.hkis.edu.hk, with

	subscribe sigprog-l <your name>

in the body of the message. The list is just getting started, so don\'t
expect much mail on it yet. 

(Anyone care to help me whittle a few characters off this sig? :-)
-- 
#!/usr/bin/perl -pl-                                      ,,ep) ayf >|)nj,,
$_=reverse lc$_;s@"@\'\'@g;y/[]{A-R}<>()a-y1-9,!.?\`\'/][}        Joey Hess
{><)(eq)paj6y!fk7wuodbjsfn^mxhl5Eh29L86\`i\'%,/;s@k@>|@g#   jeh22@cornell.edu
';
close M;

#auj@aber.ac.uk