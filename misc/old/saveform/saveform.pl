#!/usr/bin/perl

$help='
                        S A V E F O R M
                          Version 3.0
         Copyright (C) 1995 Joey Hess <jeh22@cornell.edu>
                                                           
Usage: 
 Saveform is intended to be called as a cgi script, not ran at the command
 line. See saveform.html for details of its operation.

License:

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 1, or (at your option)
 any later version.
                    
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
                          
 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
';

#These are our global variables.
$header="Content-type: text/html";
$header_printed='';

#Other global vars include: @in, which holds the input from the form
#$params, which holds the parameters passed to the program as a query string,
#appropriately formatted. (also available as the ~ variable, ~params~),
#@Z, which holds any attached template or text for the command we're currently executing, 
#@cfg, which holds data from the config file, and
#$run, which holds the name of the runfile to process.

#Here are the commands directly supported by saveform, each with its own
#subroutine:
#----------------------------------------------------------------------------
#It's all so simple..

#Returning just means process the template.
sub return { my $template=shift;
	&printheader;
  &ProcessTemplate($template);
}

#Saving is just processing a template, and redirecting stdout.
sub save { my ($template,$to)=split(/ /,shift,2);
  open(SAVEOUT, ">&STDOUT"); #save standard output so we can restore it.
	open (STDOUT,">$to") || Error("Unable to write to file, \"$to\":$!");
	&ProcessTemplate($template);
	close STDOUT;
	open(STDOUT, ">&SAVEOUT");
}

#Appending is just saving to the filename with an extra >.
sub append { my ($template,$to)=split(/ /,shift,2);
	open(SAVEOUT, ">&STDOUT"); #save standard output so we can restore it.
	open (STDOUT,">>$to") || Error("Unable to append to file, \"$to\":$!");
  &ProcessTemplate($template);
  close STDOUT;
	open(STDOUT, ">&SAVEOUT");
}

#Do a simple little substitution of mail variables.
sub ExpandMailVars { my $s=shift; my $to=shift; my $from=shift; my $subject=shift;
  $s=~s/\%to/$to/ig;
  $s=~s/\%from/$from/ig;
  $s=~s/\%subject/$subject/ig;
	return $s;
}

#Mailing is just saving a template to the mailer, with correct params.
sub mail { my ($template,$rest)=split(/ /,shift,2);
	my ($to,$from,$subject)=split(/\:/,$rest,3);
	my $mail=&ExpandMailVars($cfg{mail},$to,$from,$subject);
	open(SAVEOUT, ">&STDOUT"); #save standard output so we can restore it.
	open (STDOUT,"|$mail") || Error("Couldn't run mail command, \"$cfg{mail}\"");
  if ($cfg{'mail headers'}) { #a forging we will go..a forging we will go..
		my $forgetext=&ExpandMailVars($cfg{'mail headers'},$to,$from,$subject);
		$forgetext=~s/\\n/\n/ig;
		print $forgetext;
	}
	&ProcessTemplate($template);
	close STDOUT;
	open(STDOUT, ">&SAVEOUT");
}

#Print the html header if we've not already done so.
#You can pass one to print, or it'll print the default.
#
sub printheader { my $a=shift;
  $a=$header if (!$a);
  if (!$header_printed) {
    $header_printed=1;
    print "$a\n\n";
  }
}

#Run a perl command -- handle inlined commands just as templates can be inlined
#No saveform variable/tag expansion is performed.
#
sub run { my $c=shift;
  if ($c=~/^*/) { #inlined run command.
		$c='';
		foreach (@Z) {$c.="$_\n"} #I should be able to use $c="@Z". I dunno why it doesn't work :-(
  }
	eval $c;
}

#----------------------------------------------------------------------------

#Display an error to the user. You can specify an optional header as the 2nd 
#parameter.
#
sub Error { my $message=shift; my $h=shift;
	$h='Saveform Error' if (!$h);
	&printheader;
  exit print "<h1>$h</h1>$message$cfg{trailer}";
}

#Process special conditional commands at the beginning of a line of text, 
#and return the line minus the conditionals if they evaluate to true, and ''
#if they evaluate to false.
#
#Use perl to evaluate the conditional, so I don't have to handle an if-type 
#statement on my own.
#
#This is high on the list to be re-done!!
#
sub ProcessConditional { my $line=shift;
	my ($cond,$line)=split(/\| /,$line,2);
	if ($line) {
		#save the " and ' characters we already have, and restore them in a while
		$cond=~s/\"/\$quOtE1\$/g; #MoNd0! ;-)
		$cond=~s/\'/\$quOtE2\$/g;
		$cond=&ProcessTemplate_Line($cond); #expand any tags in the conditional
		$cond=~s/\"/ /g;
		$cond=~s/\'/ /g;
    $cond=~s/\$quOtE1\$/\"/g;
    $cond=~s/\$quOtE2\$/\'/g;

		$cond=~tr/A-Z/a-z/;
		my $a="\$a=($cond)"; 
		eval $a;
		if ($a) {return $line} else {return ''}
	}
	else {
		return $cond;
	}
}

#Process one line of a template, expanding ~ variables as we go, and return 
#the result.
#
sub ProcessTemplate_Line { my $line=shift;
	$line=~s/~(.*?)~/$in{$1}/igo;
  return $line;
}

#Load in a template, expand all ~ variables, and output the whole mess
#to stdout
#
sub ProcessTemplate { my $filename=shift;
	if ($filename=~/\*/) { #add the attached template instead.
		foreach (@Z) {
			print STDOUT &ProcessTemplate_Line(&ProcessConditional($_));
		}
	}
	else {
		open (IN,"<$filename") || &Error("Couldn't open template: \"$filename\":$!");
		while (<IN>) {
		  print STDOUT &ProcessTemplate_Line(&ProcessConditional($_));
		}
		close IN;
	}
}

#Process a command. This command can be a standard perl command, or a special
#saveform command.
#
sub RunCommand { my $command=shift;
	$command=ProcessConditional($command); #process the conditional.
	if ($command) {
  	$command=ProcessTemplate_Line($command); #expand any variables.
		#in reality, all we do is dress it up into a normal perl command, and run that.
		#there are subroutines for all the saveform commands.
		my ($command,$rest)=split(/ /,$command,2);
		$rest=~s/\n$//;
		$command='&'.$command;
		$command=~tr/A-Z/a-z/;
    if (defined(@Z)) { $rest="* $rest" }
		$command.='($rest);';
		eval $command;
		if ($@) {
			&printheader;
			print "<P><b>Error in runfile: $@ when trying to execute \"$command\"</b><P>\n";
		}
	}
}

#Process an entire runfile.
#
sub RunRunFile { my $fn=shift;
	$fn=$cfg{mydir}.$fn;
	open (RUN,"<$fn") || Error("Unable to open runfile, \"$fn\":$!");
	while (<RUN>) {
	  s/\n$//;
	  if (($_) and (!/^#/)) {
			if (/\[$/) { #this command has an attached template. 
				#Processing these is the ugliest part of the program, mostly because
				#it's also the newest..
				s/\[$//;
				my $command=$_;
				$ok=1;
				while ($ok) {
					$_=<RUN>;
					s/^\t//; #eliminate leading tab, if any.
					if (!/^\]/) { @Z[$#Z+1]=$_ } else { $ok='' }
					if (eof(RUN)) { $ok='' }
				}
				&RunCommand($command);
			}
			else {
				&RunCommand($_);
			}
	  }
		undef @Z;
	}
	close RUN;
}

#Fix a string formatted so it'll work with CGI into the normal string format.
#
sub CgiFix { my $s=shift;
	#Convert plus's to spaces
  $s =~ s/\+/ /g;
  # Convert %XX from hex numbers to alphanumeric
  $s =~ s/%(..)/pack("c",hex($1))/ge;
	
	return $s;
}

#Read and parse httpd input, and save it to @in. 
#Portions of this subroutine are adapted from cgi-bin.pl, 
#Copyright 1994 Steven E. Brenner <S.E.Brenner@bioc.cam.ac.uk>.
#(about 11 lines remain unmodified..)
#
sub ReadParse {
  # Read in text from form.
	my($in);
  if ($ENV{'REQUEST_METHOD'} eq 'GET') {
		$in = $ENV{'QUERY_STRING'};
  } 
  elsif ($ENV{'REQUEST_METHOD'} eq 'POST') {
    read(STDIN,$in,$ENV{'CONTENT_LENGTH'});
  } 
  else { #what?! We're being run at the _command_ line!
		exit print $help; #display help and exit.  
  }
  
  @in = split(/&/,$in);
  
  foreach $i (0 .. $#in) {
    # Split into key and value.  
    my($key, $val) = split(/=/,$in[$i],2); # splits on the first =.
          
    # Convert %XX from hex numbers to alphanumeric
    $key=CgiFix($key);
    $val=CgiFix($val);
                                
    # Create saveform variable out of it, store it in @in.
    $in{"$key"} .= "\0" if (defined($in{"$key"})); # \0 is the multiple separator
    $in{"$key"} .= $val;
  }
}

#Parameters to the program are passed via the query string, the part of the
#url after the '?'.
#
sub ProcessParams {
	if (!$cfg{dos}) {
	  $run=$ENV{"QUERY_STRING"}; #the right place to pass parameters.
  }
  else {
    $run=$ENV{"PATH_INFO"}; #blah, gletch...
    $run =~ s/\///;
  }

  $run=$ENV{"QUERY_STRING"};
  ($run, $params)=split(/\+/,$run,2);
  $params=CgiFix($params);
  $in{"~$params~"}=$params;
}

#Reads a config file of the standard unix format -- lines, and # is a comment.
#by default, it's assumed to be in the same directory as the program, unless
#you include a '/' in the filename.
#Stores the values in @cfg.
#
sub ReadCfg { my $fn=shift;
  if (!($fn=~m#/#)) { 
    #figure out where this program is, and look there..specify absolute 
    #path, cause it's probably a good idea ;-)
		my @path=split(m#/#,$0); #there should be a regexp to do all this..
		$path=$0;
		$path=~s/@path[$#path]//; 
		$fn=$path.$fn;	
  }
	
	open (CFG,"<$fn") or Error("Unable to load config file, \"$fn\":$!"); 
	while (<CFG>) {
	  s/\n$//;
	  if (($_) and (!/^#/)) {
		  my ($key,$val)=split(/=/,$_,2);
		  $key=~tr/A-Z/a-z/;
		  $cfg{$key}=$val;
	  }
	}
	close CFG;
}

#set up a couple more variables, for date and time, etc.
#
sub SetExtraVars {
	my $a=localtime;
	$a=~tr/ / /s;
	my(@timedat)=split(/ /,$a);  # e.g. "Thu Oct 13 04:54:34 1994"
	$in{date}="$timedat[1] $timedat[2], $timedat[4]";
	$in{time}=$timedat[3];
	$in{http_from}=$ENV{'HTTP_FROM'}; #this is the email address of the user.
	$in{http_user_agent}=$ENV{'HTTP_USER_AGENT'}; #the browser name.
	$in{referrer_url}=$ENV{'REFERER_URL'}; #the url that brought them here.
	if ($ENV{'REMOTE_ADDR'}) {
		$in{remote_host}=$ENV{'REMOTE_HOST'}; #the remote address (if none, the IP is substituted.)
	}
	else {
		$in{remote_host}=$ENV{'REMOTE_ADDR'};
	}
	$in{remote_addr}=$ENV{'REMOTE_ADDR'};
}

#&printheader; #uncomment this if you aregetting weird problems and 
#need to debug it.

&ReadParse;
&SetExtraVars;
&ReadCfg('saveform.cfg'); 
&ProcessParams;
&RunRunFile($run);

&printheader;
print $cfg{trailer};
