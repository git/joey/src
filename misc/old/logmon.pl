#!/usr/bin/perl
#
#logmon 2.0, by Joey Hess
#
#This program is under the Gnu public license, any version.
#

#Have to set this 1st, or perl won't run setuid.
#This should be the directory that contains the logmon-tail program
#And the directory that contains 'ps', seperated by colons.
#(If you are dealing with ttysnoop, put in the directory 'who' is in as well.)
$ENV{'PATH'}='/usr/bin/:/bin/';

require '/home/joey/prog/include/startup.pl';

sub AmIRunning { #1 if already running, "" if not.
  open (TEST,"ps -x|grep logmon-tail|wc -l|"); 
  $_=<TEST>; #get the number of processes with "logmon-tail" in their name
  close TEST;    
  chomp;
  s/ //g;
  if ($_ gt 2) { return 1 } else { return "" }
}

sub Stop {
  system "killall -KILL logmon-tail";
  print "Logmon turned off.\n";
}

sub Go {
  &INIproc("/etc/logmon.conf");
	if ($ttysnoop) { #ttysnoop messes too many things up. We have to
  	#figure out what pseudo-tty it allocates, and use that instead.
    #so, we look at the output of 'who', and find the first use of ttypx that's
    #not coming from :0.0 or anywhere else. That's where ttysnoop is.
    my @w=`who`;
    print $a;
    foreach $_ (@w) {
			tr/ / /s;
      my ($user,$dev,$mon,$day,$time,$from)=split(/ /,$_,6);
      if (($dev=~/^ttyp/) and (!$from)) {
        $output="/dev/$dev";
      }
    }
  }
  if ($output) {$output=">$output"}
  if ($clearscreen eq "yes") { system "clear" }
  system "logmon-tail -f $input $output &";
  print "Logmon turned on.\n";
}

$not_picky=1;
@ARGV=&ARGVproc(@ARGV);
$a=shift(ARGV);

#we may need to wait here.
if ($wait gt 0) { select(undef, undef, undef, $wait) }

if (($a eq "help") || ($a eq "h")) {
  print "logmon 2.0, by Joey Hess

Usage: logmon [on|off|toggle|refresh]
on		Turn log on, unless it already is on.
off		Log off.
toggle		Toggle log between on and off (Default if nothing is
		specified.)
refresh		Restart log if there is a log going, if not, do nothing.

Note: Logmon reads /etc/logmon.conf for configurable information. A symlink
to the tail program, called logmon-tail must exist for this program to work.
Also note: you may add additional parameters in the form tag=value, to over-
ride similar settings in the /etc/logmon.conf file.\n";
}
elsif ($a eq "on") { 
  $a=&AmIRunning;
  if ($a eq "") { &Go }
}
elsif ($a eq "off") { 
  &Stop;
}
elsif (($a eq "refresh") || ($a eq "restart")) {
  $a=&AmIRunning;
  if ($a ne "") { 
    &Stop;
    select(undef, undef, undef,1); #1 second delay needed.
    &Go;
  }
}
elsif (($a eq "") || ($a eq "toggle")) {
  $a=&AmIRunning;
  if ($a ne "") { &Stop } else { &Go }
}
else {
  die "Bad command syntax,\"$a\". Run logmon -help for assistance.\n";
}