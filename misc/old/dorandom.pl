#!/usr/bin/perl
#
#Syntax:
#  dorandom <command> <selections>
#
#Will select a random item from the command line, substitute all occurences 
#in of "~here~" in the command with the chosen file, and run the command.
#
#Of course, you can use a filespec as <selections> to picka file at random..
#
#Handly for playing random sounds, etc.

srand(time);            
$a=int(rand($#ARGV))+1;
$do=$ARGV[0]; 
$do =~ s/~here~/$ARGV[$a]/ig;
system $do;
