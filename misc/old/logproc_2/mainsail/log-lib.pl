#Display a line to the screen, unless in quiet mode
sub log {
  $_=shift;
  $l=shift;
  if ($loglevel ge $l) {print $_}
}

#Load in a file defining the database formats to use..
#Creates:
#@dbfiles, listing all the filenames to save data to
#@dbfields, listing all the fields of each database
#@dbstruct, listing how to process each field of each db
sub LoadHeader {
  $dbcount=$dbcount+1;
  local $fn=shift;
  open (IN,'<'.$fn) || die "Unable to load header:$fn\n";
  $_=<IN>;
  s/\n$//;
  s/"//g;
  @{$dbfields[$dbcount-1]}=split(/,/,$_); #an array of arrays
  $_=<IN>;
  s/\n$//;
  s/"//g;
  @{$dbstruct[$dbcount-1]}=split(/,/,$_); #another. Reference like this: $dbfields[x][y]     
  close IN;
}

#Load all databases pointed to in the @dbfiles array, and put all of their 
#data in @db, an array of arrays of arrays..
#Pass any number of filenames to load.
sub LoadDBs {
  &log('Loading databases...',4);
  &log("\n",5);
  $x=0;
  while ($fn=shift) {
    &LoadHeader($fn.$header);
    $y=0;
    open (IN,'<'.$fn);
    &log("	$fn ",5);
    while (<IN>) {
      s/"//g;
      s/\n$//;
      if ($_ ne '') {
        @{$db[$x][$y]}=split(/,/,$_);
        #create the index for this line:
        $index='';
        $z=0;
        foreach $a (@{$dbstruct[$x]}) {
          if ($a eq 'index') {
            $index=$index.'||'.$db[$x][$y][$z]
          }
          $z=$z+1;
        }
        $dbindex[$x]{$index}=$y;
        $y=$y+1;
      }
    }
    close IN;
    $dbsize[$x]=$y-1;
    if ($y ne '') { &log("($y lines)\n",5) } else { &log("\n",5) }
    $x=$x+1;
  }
  &log("..done.\n",4);
}

#Save all databases
#Doesn't seve database entries whose 1st field starts with "-|-"
sub SaveDBs {
	&log('Saving databases...',4);
  $x=0;
	while ($fn=shift) {
	  open (OUT,'>'.$fn);
	  $y=0;
	  while (($y <=> $dbsize[$x]+1) eq -1) {
	    $_=$db[$x][$y][0];
      if (/-\|-/ eq '') {
	      $_='"';
	      @a=@{$db[$x][$y]};
	      foreach $a (@a) {
	        $_=$_.$a.'","';
	      } 
	      s/,"$//;
			  print OUT "$_\n";
			}
			$y=$y+1;
	  }
	  $x=$x+1;
	  close OUT;
	}  
  &log("..done.\n",4);	    
}

1