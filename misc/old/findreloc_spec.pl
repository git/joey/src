#!/usr/bin/perl
#
# Find candidates for relocatable rpms. Pass it a list of spec files, 
# it'll output a list of those you might want to make relocatable.
# (being those that have all files under the same directory)
#
# BUGS: None known, but this is just a quick hack. (Maybe it should handle 
# spec files with subpackages better?)
#
# Take this program's output only as an advisory, read the spec file to make your
# own descision.
#
# By Joey Hess <jeh22@cornell.edu>
#

foreach $spec (@ARGV) {

	#get filelist from spec file.
	open (S,"<$spec");
	undef @files;
	$file_sec=undef;
	while (<S>) {
		chomp;
		if (/^%files/ ne undef) { 
			$file_sec = 1 
		}
		elsif ((/^%/ ne undef) && (/^%doc / eq undef) && (/^%dir / eq undef) && (/^%config / eq undef)) { 
			$file_sec = undef
		}
		if (($_) && ($file_sec) && (/^%/ eq undef)) {
			$files[$#files+1]=$_;
		} 
		elsif ((/^%dir (.*)/ ne undef) || (/^%config (.*)/ ne undef)) {
			$files[$#files+1]=$1;
		}
	}
	close S;

	# Find the longest string that starts each filename.
	$longest=$files[0];
	if ($#files eq 0) { # only one file, get its basename.
		while (chop($longest) ne '/') {} # cut off a directory.
	}
	else {
		foreach(@files) {
			while (/^$longest/ eq undef) { # longest string no longer fits..
				while (chop($longest) ne '/') {} # cut off a directory.
			}
		}	
	}
	$longest=~s/\/$//; #remove trailing /.
	if ($longest) {
		print "$spec: $longest\n";
	}
}
