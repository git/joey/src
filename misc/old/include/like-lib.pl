# Adds a "like" function to perl.
# Syntax:
# like($a,$b)
# If $a is like $b, returns 1, else returns "".
# $a can include * and ? patterns.
sub like {
  $a=@_[0];
  $_=@_[1];
  $n="~";
  $n2=".*";
  $a =~ s;\*;$n;eg; #change * to .*, the perl equivialent
  $a =~ s;$n;$n2;eg;
  $n2=".?";
  $a =~ s;\?;$n;eg; #change ? to .?, the perl equivilant
  $a =~ s;$n;$n2;eg;
  $n = m/^$a/i;
  return $n;
}

1 #return true
