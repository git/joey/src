#This package implements a procedure to download a page from the web.
#
#Public: GetURL(hostname,port,file) returns list of lines on page.
#        ParseURL(url) returns a list containing (hostname, port, file)
#
#Copyright 1996 by Joey Hess <jeh22@cornell.edu>

{
package GetURL;

#Initialization
sub import {
	#Set these only once, so GetURL can be called multiple times with 
	#optimal speed.
	$AF_INET = 2;
	$SOCK_STREAM = 1;
	$sockaddr = 'S n a4 x8';
	($name,$aliases,$proto) = getprotobyname('tcp');
	chop($hostname = `hostname`);
	($name,$aliases,$type,$len,$thisaddr) = gethostbyname($hostname);
	$this = pack($sockaddr, $AF_INET, 0, $thisaddr);
}

sub dokill { #Private
  kill 9,$child if $child;
}

#Note: adapted from "wwwis" program.
sub GetURL { #Public
	local($them,$port,$url) = @_;
	$port = 80 unless $port;
	$them = 'localhost' unless $them;

	$SIG{'INT'} = 'dokill';
	($name,$aliases,$port) = getservbyname($port,'tcp') 
	unless $port =~ /^\d+$/;;
	($name,$aliases,$type,$len,$thataddr) = gethostbyname($them);
	$that = pack($sockaddr, $AF_INET, $port, $thataddr);

	if(!((socket(S, $AF_INET, $SOCK_STREAM, $proto)) && #Make the socket filehandle.
		(bind(S, $this)) && #Give the socket an address.
		(connect(S,$that)) )){ #Call up the server.
			#there was a problem
			print "ERROR: ".$!."\n";
			$size="";
	} 
	else {
		#Set socket to be command buffered.
		select(S); $| = 1; select(STDOUT);
		print S "GET /$url\n";
		return <S>;
	}
}

sub ParseURL { #Public
	my ($host,$port,$file);
	$_=shift;
	($host,$file)=m#http://(.*?)/(.*?)$#i;
	if (!$host) { 
		($host)=m#http://(.*?)$#i;
		$file='/';
	}
	($host,$port)=split(/:/,$host,2);
	if (($file=~m/\.html?$/i eq '') && ($file=~m/\/$/ eq '')) {
		$file.='/'; #append / to file if it's a directory.		
	}
	if ($file=~m/^\// ne '') { $file="/$file" }

	return ($host,$port,$file);
}

} #End of package.

1 #Return true.
