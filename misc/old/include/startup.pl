#!/usr/bin/perl
#
#Handy startup functions - process an INI file, and process parameters in a 
#consistent way.

#Process a file that has a format similar to a Windows (TM) ini file. 
#Ignores lines starting with "#" or having the format "[header]".
#For each tag in the file (part before the equals sign), a variable is created,
#filled with the value of the tag. (Spaces in the tag are removed, and the tag
#is made lower-case.)
sub INIproc {
  $fn=shift;
  open (INI,"<$fn") || die "Can't load $fn\n";
  while (<INI>) {
    chop;
    if (($_ ne "") && (/^#/ eq "") && (/^\[.*\]/ eq "")) { #good line
      &MakeVar($_);
    }
  }
  close INI;
}

#Intended to process the paramters in @ARGV, but can actually be used on any 
#array.
#Should be run after INIproc, so values in the INI/conf file can be overridden.
#Processes the array, breaking up tag=value pairs, and creating variables with 
#values just as is done by INIProc.
#If there are parameters that don't have an = in them, then they are returned
#in an array.
#NOTE: this procedure has a hidden danger: sometimes, ie with filenames, you 
#don't want to break items up at the =. If such cases are likely, this procedure
#shouldn't be called.
#NOTE: set $not_picky to 1 before calling and dashes will be removed from parameters
#that don't have a = in them, and those parameters will be lowercased.
#That's handy, if you want -help, -HELP, --Help, and help all to be treated the
#same..
sub ARGVproc {
  $_=shift;
  #local (
  $inc=0;
  #local(
  $retArray[0]="";
  while ($_ ne "") {
    if (/=/) { #good tag 
      &MakeVar($_);
    }
    else {
      if ($not_picky eq 1) {
        s/^-//;            #why be picky about a dash or 2
        s/^-//;
        tr/A-Z/a-z/;       #or case? :-)
      }
      $retArray[$inc++]=$_;
    }
    $_=shift;
  } 
  return @retArray;
}

#This takes a tag=value string that is known to be in the correct format, and 
#actually makes the variable from it.
sub MakeVar {
  $_=shift;
  my ($a,$b)=split(/=/,$_,2);

  $a=~tr/A-Z/a-z/;
  $a=~s/ //g;
	
	$b=~s/\@/\\@/ig;
	$b=~s/\"/\\"/ig;

  my $c="\$$a=\"$b\"";
  eval "$c"; #create a new global variable here.
}

#This returns a string that gives the path to get to the directory the program
#is in. It handles both dos and unix-style paths.
sub GetPath {
  #all we need to do is get rid of the filename at the end of $0.
  #so, first find that filename:
  #local (
  @junk=split(/\//,$0);
  #local 
  $a=$junk[$#junk];
  (@junk)=split(/\\/,$a);
  $a=$junk[$#junk];
  #then, remove it from the end of $0, and return the result:
  $b=$0;
  $b=~s/$a$//i;
  return $b
}
1; 