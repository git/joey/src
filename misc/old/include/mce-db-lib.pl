#!/usr/bin/perl
#
#Escher Database lookup library.
#
#Find entries in the database, and return them.

sub InitMCEDB {
  open(MCEDB,"<".$dbloc) || die "Can't open database!\n";
  while (<MCEDB>) {
    s/\n$//;
    $mcedb[$count]=$_;
    $count=$count+1;
  }
  close MCEDB;
}

#Find and return an entry from the DB;
#
#Returns line number of entry if an entry is found, '' if nothing found.
#Makes @DBhash contain a found entry.
sub Find {
  $_=shift;
  ($tofind{'fname'},
   $tofind{'name'},
   $tofind{'other_names'}, 
   $tofind{'date'},
   $tofind{'group'},
   $tofind{'xref'},
   $tofind{'medium'},
   $tofind{'comments'},
   $tofind{'any'})=split(/\;/,$_);
  $ct=0;
  $_=&FindNext();
  return $_;
}

#Find and return an entry from the DB. Pass no params, 
#instead call after Find, and I'll look for the next one.
sub FindNext {
  $ok='';
  while (($ok eq '') && ($endofDB eq '')) {
    $_=$mcedb[$ct];
    $ct=$ct+1;
    if ($ct eq $#mcedb) { $endofDB=1 }
    ($DBhash{'fname'},
     $DBhash{'name'},
     $DBhash{'other_names'},
     $DBhash{'date'},
     $DBhash{'sdate'},
     $DBhash{'group'},
     $DBhash{'xref'},
     $DBhash{'medium'},
     $DBhash{'comments'}) = split(/;/,$_,9);
    $b=$tofind{'any'};
    if ($b ne '') { #match on any field
      if (/$b/i ne '') {
        $ok=1;
      }
    }
    else
    {
      $ok=1;
      foreach $a ('fname','name','other_names','date','group','xref','medium','comments') {
        $b=$tofind{$a};
        if ($b ne '') {
          $_=$DBhash{$a};
          if (/$b/i eq '') { $ok='' } #This is an and search, not an or search.
        }
      }
    }
  }
  if ($ok eq 1) {return $ct} else {return $ok}    
}

#Get an item of the database, store data in @db
#Pass the filename of the item to get.
#Return 1 if found, '' if not.
sub GetItem { 
  $a=shift;
  $b='';
  foreach $_ (@mcedb) {
    if (/^$a/i) { $b=$_; }
  }
  if ($b ne '') {
    ($db{'fname'},
     $db{'name'},
     $db{'other_names'},
     $db{'date'},
     $db{'sdate'},
     $db{'group'},
     $db{'xref'},
     $db{'medium'},
     $db{'comments'}) = split(/;/,$b,9);
     return 1;
  }
  else {
    return '';
  }
}

1