#!/usr/bin/perl 
#
#Fuzzy pattern matching. $A and $B are the 2 strings to test, $ftune
#is how accurate the match need be (0.80 works well)
#

sub case_fmatch {
	local($A,$B,$ftune)=@_;
	$A=~tr/A-Z/a-z/;
	$B=~tr/A-Z/a-z/;
	return fmatch($A,$B,$ftune);
}

sub fmatch {
	local($A, $B,$ftune) = @_;
	local($l) = (length($A) < length($B)) ? length($A) : length($B);
	local($m) = 0;
	local($w) = 2;
	local($k);
		
	$A eq $B && return(1.0);
	$l > $w || return(0.0);

	for $k(0..$l-$w) { 
	  local($s) = substr($A, $k, $w);
	  #---escape r.e. characters in string A---
  	$s =~ s/([()[\]*|?.{}\\])/\\$1/;
	  $B =~ $s && $m++;
  }

	($m/($l-$w) > $ftune) ? 1 : 0;
}

1
