#!/usr/bin/perl
#
#Perl screen output library. Handles drawing lines, boxes, etc on the screen.
#Requires the perl cureses library
#

require 'curses.pl';

package screen;

#Startup stuff
#
sub init {
	#we need several terminfo capabilities:
	&curses::makesub('cup','pos');	#create a pos(y,x) subroutine to move the cursor.
	&curses::makesub('clear');	#create a clear() sub to clear the screen.
}

#Call before quitting.
#
sub done {
	&curses::altcharset;	#make sure the character set is back to normal.
}

#Draw a horizontal line
#
#Syntax: horiz_line(x,y,length,char);
#(perhaps I should define a point datatype?)
#
#If char is not passed, it uses line drawing characters, suitable for the
#line you are drawing.
#
#Note: if you make a really long line, yes, it will wrap :-)
#
sub horiz_line { my $x=shift; my $y=shift; my $length=shift; my $char=shift;
	$char=$HLINE if !$char;
	&curses::altcharset(1);	#make sure alternate character set is on
	&curses::pos($y,$x);
	foreach (1..$length) { print $char } #gotta be a better way..
	&curses::altcharset;  #make sure the character set is back to normal.
}

#Draw a vertical line
#
#Syntax: vert_line(x,y,height,char);
#
#if char is not passed, it uses a suitable line drawing character.
#
#(Note, could be optimized to set pos(), and then just go down 1 line and
#back 1 character, maybe)
#
sub vert_line { my $x=shift; my $y=shift; my $height=shift; my $char=shift;
	$char=$VLINE if !$char;
	&curses::altcharset(1);
	for ($c=0; $c<$height; $c++) {
		&curses::pos($y+$c,$x);
		print $char;		
	}
	&curses::altcharset;
}

#Draw a box. This is optimized, so it doesn't use the line-drawing subs above.
#
#Syntax: box(x1,y1,width,height,filled);
#
#If filled is any character, the box is filled with that character. 
#Otherwise, it is just a frame.
#
sub box { my $x=shift; my $y=shift; my $width=shift; my $height=shift; my $filled=shift;
	&curses::altcharset(1);
	&curses::pos($y,$x);
	print $ULCORNER;
	foreach (1..($width-1)) { print $HLINE }
	print $URCORNER;
	for ($c=1; $c<$height; $c++) {
		&curses::pos($y+$c,$x);
		if ($filled) {
  		print $VLINE;
  		foreach (1..($width-1)) { print $filled }
			print $VLINE;
		}
		else {
			print $VLINE;
			&curses::pos($y+$c,$x+$width);
			print $VLINE;
		}
	}
	&curses::pos($y+$height,$x);
	print $LLCORNER;
	foreach (1..($width-1)) { print $HLINE }
	print $LRCORNER;
	&curses::altcharset;
}

#Line drawing characters:
$VLINE='x';
$HLINE='q';
$ULCORNER='l';
$LLCORNER='m';
$URCORNER='k';
$LRCORNER='j';

1