#!/usr/bin/perl
#
#A "curses" library for perl, lets you move around, etc.
#
#By Joey Hess, jeh22@cornell.edu
#

#This is horrendouls buggy. Proof of concept only..

#Read in the termcap entry, parse it, and set up tons of little
#entries in $curses{xx}, which correspond to termcap capability strings.
#
#This is noticebly slow if you are using a weird terminal way down in the
#/etc/termcap file. This should use terminfo, I guess, but I don't understand
#that.
#
sub perlcurses_init {
	my $term=$ENV{TERM};
	if ($term eq '') { exit print "TERM environment variable not set.\n" }

	#get the termcap entry. THIS IS INCOMPLETE -- doesn't support
	#backreferencing another entry.
	open (TERMCAP,"grep -A 500 $term /etc/termcap|") || exit print "Read /etc/termcap failed: $!\n";
	my $found='';
	while (<TERMCAP>) {
		if ((!$found) && (/$term/i ne '')) {
			s/[\||:]/~/g;
			$_="~$_";
			if (/~$term~/i ne '') { #is $term defined here?
				$found=1;
				my $entry='';
			}
		}
		elsif ($found) {
			s/\n$//;
			s/\t//g;
			tr/ / /s;
			s/^ //;
			s/ $//;
			if (/\\$/ ne '') {
				s/\\$//;
				$entry.=$_;
			}
			else {
				close TERMCAP;
			}
		}
	}	
	if (!$found) {
		close TERMCAP;
		exit print "Couldn't find \"$term\" in termcap.\n";
	}	

	#massage the entry into something perl likes..
	$entry=~s/\\E/\e/g;
	foreach (split(/:/,$entry)) {
		my ($a,$b)=split(/=/,$_,2);
		if ($b) {
			$curses{$a}=$b;
		}
		elsif ($a=~m/co#/ ne '') {
			($term_cols)=($a=~m/co#(.*?)/);	#get number of columns on the screen
		}
		elsif ($a=~m/li#/ ne '') {
			($term_lines)=($a=~m/li#(.*?)/); #get number of lines on the screen
		}
		else {
			$curses{$a}=1;
		}
	}	
}

#Move to a given (absolute) position on the screen.
#performs range checking, and if the position is bad, returns a false value,
#and does nothing.
#
sub pos { my $x=shift; my $y=shift;
print $term_lines;
#	if (($y<0) or ($y>$term_lines) or ($x < 0) or ($x>$term_cols)) { return '' }
	$_=$curses{cm};	#"cursor move"
	s/%i//;
	s/%d/$y/;
	s/%d/$x/;
	print $_;	
	return 1		
}
	
1