#! /usr/bin/perl
#
#Mail processor library
#
#This is intende to be a library that interfaces perl scripts and the deliver
#command, enabling you to easily write perl scripts as .deliver files.
#
#It is oriented toward giving commands via mail, ie, as in a mailing list.
#
#Joey Hess <jeh22@cornell.edu>

#Call to initialize the mail processor.
#
#Reads stdin for message, and saves the headers and body.
sub MailprocInit {
  $headerfile=$ENV{"HEADER"};
  $bodyfile=$ENV{"BODY"};
  if ($headerfile.$bodyfile eq "") { die "Woah! Problems talking to Deliver.\n"}  
  open (HEADER, "<".$headerfile);
  while (<HEADER>) {
    ($a,$b)=split(/: /,$_,2);
    $a =~ tr/A-Z/a-z/; #make lower case.
    $b =~ s/\n//iego;  #remove enters at EOLs
    $headers{$a}=$b;
  }
  close HEADER;
}

#Returns true if there is a header that matches the pattern (give headers 
#in lower case)
#
#1st param: header name
#2nd param: value the header should match (a pattern)
sub IsHeader {
  $_=$headers{@_[0]};
  $pattern=@_[1];
  return /$pattern/i
}

#Processes the body of the message. (Ignore blank lines)
#
#1st param: command to use to process each line. (Ie, "&procline")
#You write this command yourself. It should parse @_[0] as a command from 
#the body of the message (changed to lowercase), and return a string to 
#send to the user, indicating what it did. 
#
#At the end of the message is appended the contents of $trailer (normally 
#
#Prints $sep between each line.
#
sub ProcessBody {
  open (BODY,"<".$bodyfile);
  open (OUT,">/tmp/mailproc-temp.$time");
  $done=false;
  while (<BODY>) {
		print OUT ">$_";
    $_ =~ s/\n//iego;  #remove enters at EOLs
    $_ =~ tr/A-Z/a-z/; #make lower case
    if ($_ ne "") {
      $a=eval @_[0]."(\$_)"; #run the 1st parameter on this line.
      print OUT $a; 
      if ($a ne "") {print OUT $sep}
    }
  }
  close BODY;
  print OUT $trailer;
  close OUT;
  $e=&GetEmail;
  system "mail -s \"Re: ".$headers{"subject"}."\" -c joey $e </tmp/mailproc-temp.$time"; 
}

#Tells deliver that we don't need to save this message - it's been 
#processed and can be trashed now.
sub DropMessage {
  print "DROP\n";
}

#Does its best to return the actual name of the user.
#
#Parses stuff like: "name <address@host>"
#At the worst case, it'll fall through to the email address, but that 
#shouldn't happen unless the name just can't be found anywhere. 
sub GetName {
  if ($getnamesave ne "") {
    return $getnamesave #save time this way..
  }
  else {
    ($name,$address)=split(/</,$headers{"from"},2);
    if ($name ne "") { #looks like we got us a name :-)
      $name =~ s/ \B//ego; #trim extra spaces.
    }
    else { #guess we'll have to return the address.. :-(
      $name=$address;
      $name =~ s/>//; #strip off the trailing ">"
    }
    $getnamesave=$name;
    return $name;
  }
}

#Return the email address minus junk
#
sub GetEmail {
  ($name,$address)=split(/</,$headers{"from"},2);  
  if ($address ne "") { 
    $address =~ s/>//ego;
  }
  else {
    $address=$name;
  }
  return $address;
}

$time=time;
1; #return true

