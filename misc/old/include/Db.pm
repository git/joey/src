#!/usr/bin/perl
#
#General db package.
#(Designed for unix systems)

{
package Db;

#Load in a database. Pass the filename of the db.
#
#Pass it a second parameter, and the database will be stored in an array 
#with the name of the passed parameter.
#
#@db_fields stores the fields of the databse (db=second parameter).
#
sub load { my $fn=shift; my $isother=shift;
	local (*db)=$isother if $isother;
	local (*db_fields)=$isother.'_fields' if $isother;
	lock($fn) if ($lock_ok);
  open (DB,"<$fn") || exit print "Can't read $fn:$!\n";
  #top line of the database specifies the fields.
	my $fields=<DB>;
	my $b;
	chomp $fields;
	@db_fields=split(/$db_delim/,$fields);
	#Load all the records of the db.
	while (<DB>) {
		chomp;
		my ($id,@a)=split(/$db_delim/,$_);
		foreach $x (0..$#a) {
			$b=$a[$x];
			$b=~s/\\n/\n/g;
			$b=~s/\\delim/$db_delim/g;
			$db{$id}{$db_fields[$x+1]}=$b;
		}
	}
	close DB;
	unlock($fn) if ($lock_ok);
}

#Save a database. Pass the filename to save it to.
#
#If you pass a second parameter, the db info from the array with that name
#is saved, rather than what's in @db. 
#
sub save { my $fn=shift; my $isother=shift;
	local (*db)=$isother if $isother;
	local (*db_fields)=$isother.'_fields' if $isother;
	lock($fn) if ($lock_ok);
  open (DB,">$fn") || exit print "Write failed to $fn:$!";
	$"=$db_delim; #Set the list seperator to our database delimeter
	print DB "@db_fields\n"; #print the fields of the db on the 1st line.
	$"=' '; #Reset the list seperator back to default.
	my $b;       
	#Print out all the records. My @db structure is easy to use, but a pain
	#to print out..
	foreach $key (keys(%db)) { #for each record
		my $a=$key; #add id to output string
		foreach $key2 (@db_fields) {  #for each field in the record
			$b=$db{$key}{$key2};	#some characters need to be escaped out..
			$b=~s/\n/\\n/g;
			$b=~s/\r//g;
			$b=~s/$db_delim/\\delim/g;
			$a.=$b.$db_delim; #add field to output string
		}
		$a=~s/$db_delim$//; #remove a trailing delimeter
		print DB "$a\n";  #output the record
	}
	close DB;
	unlock($fn) if ($lock_ok);
}

#Lock a file, prior to editing it.
#
#Pass in the name of the file to lock
#
#Note: calling Lock($a);Lock($a);Unlock($a) will lock $a, but you'll have to
#call Unlock($a) one more time to unlock $a.
#
sub lock { my $fn=shift;
	$LockList{$fn}=$LockList{$fn}+1;
	if ($LockList{$fn} eq 1) { #locking for the 1st time, so we really have to do something..
		open (LOCK,">$fn");
		flock(LOCK,$LOCK_EX);		
		close LOCK;
	}
}

#Unlock a file. (pass filename of file to unlock)
sub unlock { my $fn=shift;
	$LockList{$fn}=$LockList{$fn}-1;
	if (($LockList{$fn} <=> 1) eq -1) { #actaually unlock
    open (LOCK,">$fn");
    flock(LOCK,$LOCK_UN);
    close LOCK;
	}
}

#Initialization
sub import {
	#This is the delimeter for fields in the databases:
	$db_delim=';';

	#Locking constants
	$LOCK_EX = 2;
	$LOCK_UN = 8;

	$lock_ok='';
}

#Call this to enable locking of database files.
#If you will never need to write to the database, might as well leave it off.
sub EnableLocking {
	$lock_ok=1;
}

} #End of package

1
