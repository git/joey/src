#!/usr/bin/perl
#
#Basic perl curses package. Handles only the most primative 
#things, including loading the terminfo, and creating subroutines 
#to handle terminal capabilities.
#

#Doesn't handle <n> delays yet, or many of the % operators.

#Normal color definititions:
$BLACK=0;
$RED=1;
$GREEN=2;
$BROWN=3;
$BLUE=4;
$MAGENTA=5;
$CYAN=6;
$WHITE=7;

package curses;

#Returns the terminfo string for the current terminal.
#
sub getinfo {
	open(INFO,'infocmp|') || exit print "Unable to run infocmp.\n";
	<INFO>;	#1st line is unneeded
	<INFO>; #as is second
	my $info='';
	while (<INFO>) { $info.=$_;	}
	$info=~s/[\n|\t|]/ /g;	#remove newlines, tabs replace with spaces
	$info=~tr/ / /s;	#make multiple space into 1 space.
	$info=~s/\\E/\e/ig;	#insert real escape characters for \e
	$info=~s/\^(.{1})/\c[$1/g;	#insert real control-chars
	$info=~s/\\l/linefeed/g;	#fix!
	$info=~s/\\b/backspace/g;	#fix!
	$info=~s/\\f/formfeed/g;	#fix!
	$info=~s/\\^/\^/g;
	$info=~s/\\\\/\\/g;
	$info=~s/\\,/\,/g;	
	$info=~s/\\s/ /g;
	$info=~s/\\:/\:/g;
	eval "\$info=\"$info\"";	#expand some other things that perl directly supports.
	close INFO;
	return $info;
}

#Process a terminfo string, set global $curses{} hash 
#
sub processinfo { my $info=shift;
	foreach (split(/, /,$info)) {
		if (/=/ ne '') {	#capability strings
			my ($a,$b)=split(/=/,$_,2);
			$curses{$a}=$b;
		}
		elsif (/#/ ne '') {	#things like lines#xx, cols#xx
			my ($a,$b)=split(/#/,$_,2);
			$curses{$a}=$b;
		}
		else {	#booleans
			$curses{$_}=1;
		}
	}
}

#Parameter code
#Does not include optimizations, yet.
$param_length{'%'}=1;	$find{'%'}='\%\%';	$change{'%'}='print \'%\'; ';
$param_length{d}=1;		$find{d}='d';				$change{d}='print pop(\@stack); ';
$param_length{i}=1;		$find{i}='i';				$change{i}='\$a[1]++; \$a[2]++; ';
$param_length{p}=2;		$find{p}='p(.*)';		$change{p}='push(\@stack,\$a[$1]); ';

#This creates a subroutine to handle one terminfo sequence.
#
sub makesub { my $name=shift; my $subname=shift;
	if (!$subname) { $subname=$name }
	my $sequence=$curses{$name};
	my $sub="sub $subname { my \@a=\@_; unshift(\@a,''); ";
	my $collect='';
	for ($x=0; $x<length($sequence); $x++) {
		$char=substr($sequence,$x,1);
		if ($char eq '%') {	
			#pull out a parameter
			$char2=substr($sequence,$x+1,1);
			$param=substr($sequence,$x+1,$param_length{$char2});
			$x=$x+$param_length{$char2};
			#generate the perl code to do it
			if ($collect) { $sub.="print '$collect'; " }
			$collect='';		
			eval "\$param=~s/$find{substr($param,0,1)}/$change{substr($param,0,1)}/\n";
			$sub.=$param;
		}
		else { $collect.=$char }
	}
	if ($collect) { $sub.="print '$collect' " }
	$sub.='}';
	eval $sub;
}

#Turn alternate character set on and off
#
sub altcharset { my $val=shift;
	if ($altcharset ne $val) {
		if ($val ne '') { &smacs } else { &rmacs }
		$altcharset=$val;
	}
}

#Startup stuff
#
sub init {
	&processinfo(&getinfo);
		#Set number of lines and cols by reading environment vars.
	makesub('smacs'); #start alternate char-set
	makesub('rmacs'); #end alternate char-set
	makesub('acsc'); #setup alternate character set
	makesub('cnorm','showcursor');	#cursor normal
	makesub('civis','hidecursor'); #cursor invisible
	makesub('setaf','forecolor');
	makesub('setab','backcolor');
	&acsc;
}

1

