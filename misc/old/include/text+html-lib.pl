#!/usr/bin/perl
#
#Text <=> html translation functions

#Translate html to text.
#Currently, only the following tags are supported (all others are stripped):
#<LI> (only 1 level, changed to *, with possibly a \n before it)
#<P> (changed to hard return)
#<Hn> (\n comes just before it)
#<\Hn> (\n comes just after it)
#<title> and </title> (same as Hn)
#<hr> --becomes a bunch of dashes, equal to the width you set.)
#
#null space changed to spaces, and spaces condenced to 1 space.
#
#As an optional second parameter (1st is string), pass the width to 
#word-wrap it to (default=75).
#
#TO ADD: <IMG ALT="..."> should show up!
#
#Ideally, this should be called on a whole page of text, *NOT* on each line
#individually.
sub HtmlToText {
  $_=shift;
  my $width=shift;
  if ($width eq '') {$width=75} 
  my $hrstring='';
  foreach (1..$width-2) { $hrstring.='-' }
  $hrstring="\n $hrstring \n";
  s/&lt;/</gi;
  s/&gt;/>/gi;
  s/\n/ /g;
  s/\t/ /g;
  s/<LI>/\n * /ig;
  s/<P>/\n/ig;
  s/<hr>/$hrstring/ig;
  s/<H.{1}>/\n/ig;
  s/<\/H.{1}>/\n/ig;
  s/<title>/\n/ig;
  s/<\/title>/\n/ig;
  s/<.+?>//igs;
  s/^\n//;
  tr/ / /s; #condense spaces.
  #now, we get to word-wrap it. Joy..
  my $all='';
  @lines=split(/\n/,$_);
  #now, go through and build up the return string, wrapping long lines.
  foreach $l (@lines) {
    if ((length($l) <=> $width) eq 1) { #have to word-wrap this line.
      my $b='';
      @words=split(/ /,$l);
      foreach $w (@words) {
        if (($width <=> length($w)+length($b)+1) eq 1) {
          $b.=$w.' ';
        }
        elsif (($width <=> length($w)+length($b)) eq 1) {
          $all.=$b.$w."\n"; #here's a line..
					$b='';          
        }
        else {
          $all.=$b."\n"; #here's a line..
          $b=$w.' ';
        }  
      }
      $all.=$b."\n" #last line from this line..
    }
    else {
      $all.=$l."\n"
    }
  }
  return $all;
}

1;