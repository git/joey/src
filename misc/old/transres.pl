#!/usr/bin/perl
# Translate semi-freeform word-processed file into a comma-delimited
# database format.

# Written for Guiamaca catholic church

my $delim="	";

# Spanish months.
my %months = (
	enero => 1,
	febrero => 2,
	marzo => 3,
	abril => 4,
	mayo => 5,
	junio => 6,
	julio => 7,
	agosto => 8, # for a typo
	augusto => 8,
	septiembre => 9,
	octubre => 10,
	noviembre => 11,
	diciembre => 12
);

# The regexps try to match on the fields in the file.
my %fields=(
	'No. del libro' => qr/No[.:] Del Libro del? Bautismos?/i,
	'folio' => qr/Folio/i, 
	'numero' => qr/Numero/i, 
	'Fecha de nacimiento' => qr/Fecha de Nacimiento/i, 
	'Nacidio' => qr/Nacido\s*(?:\(a\))?/i, 
	'Nombre del ni�o' => qr/Nombr[e�] del Ni�o(?:\s*\(a\))?/i, 
	'Nombre del padre' => qr/Nombr[e�] (?:del? )?Padre/i,
	'Nombre de la madre' => qr/Nombr[e�] (?:de )?la mad?re/i, 
	'Nombre del padrino' => qr/Nombr[e�] del? padrino/i, 
	'Nombre de la madrina' => qr/Nombr[e�] del? (?:la )?madrina/i,
	'Fecha' => qr/(?:La )?Fecha\.?/i, 
	'Lugar' => qr/Lugar/i, 
	'Parroquia' => qr/Parroquia/i, 
	'Reverendo' => qr/Rev\.?/i,
);

my @headerfields=sort keys %fields;

# Header.
print join($delim, @headerfields)."\r\n";

my $lineno=0;
my $inheader=1;
my %keys;

LINE:
while (<>) {
	$lineno++;
	chomp;
	
	y/ 	/ /s;
	s/^ //;
	s/ $//;
	
	# Skip header of file.
	if ($inheader) {
		if (/^No/) {
			$inheader=0;
		}
		else {
			next;
		}
	}
	
	# Various typo cleanups:
	s/^Parroquial/Parroquia:/;
	s/^//;
	s/La fecha93/La fecha: 93/;
	
	foreach my $field (keys %fields) {
		if (/^$fields{$field}\s*[-;: ](.*)/i) {
			my $value=$1;
			$value=~s/^ //;

			# This starts most records.
			if ($field eq 'No. del libro' && keys %keys) {
				checkprintrecord(%keys);
				%keys=();
			}
			
			if (exists $keys{$field}) {
				print STDERR "warning: duplicate field $field on line $lineno\n";
			}
			
			$keys{$field}=$value;

			# This ends most records.
			if ($field eq 'Nombre de la madrina') {
				checkprintrecord(%keys);
				%keys=();
			}
			
			next LINE;
		}
	}
	if (/[a-z]/i) {
		print STDERR "warning: skipping line $lineno: $_\n";
	}
}

# Last record.
checkprintrecord(%keys);

my $record=0;
sub checkprintrecord {
	my %keys=@_;

	$record++;

	if (! length $keys{'Nombre del ni�o'}) {
		print STDERR "warning: record $record has no Nombre del ni�o, skipping\n";
		return;
	}
	
	foreach my $date ('Fecha de nacimiento', 'Fecha') {
		$keys{$date} = convertdate($keys{$date});
	}
	
	foreach my $field (@headerfields) {
		if (! length $keys{$field}) {
			print STDERR "warning: missing field $field in record $record (Nombre del ni�o: $keys{'Nombre del ni�o'})\n";
		}
	}
	
	print join($delim,
		map { $keys{$_} } @headerfields)."\r\n";
}

# Converts dates to dd/mm/yyyy form from long-from spanish.
sub convertdate {
	my $input=shift;
	$input=~s/no tiene mes/1/;
	my ($mday, $month, $year) = $input =~ 
		/(\d+)\.?\s*(?:de)?\s*(\w+)\s*(?:de)?\s*([0-9, ]+)/i;
	$mday=~s/^0//;
	$month=~s/^0//;
	$year=~s/[, ]//g;
	if (int($year) ne $year) {
		print STDERR "bad year: $year ($input)\n";
		return;
	}
	if (int($mday) ne $mday) {
		print STDERR "bad mday: $mday\n";
		return;
	}
	if (int($month) ne $month) {
		if (exists ($months{lc($month)})) {
			$month = $months{lc($month)};
		}
		else {
			print STDERR "bad month: $month\n";
			return;
		}
	}
	return "$mday/$month/$year";
}
