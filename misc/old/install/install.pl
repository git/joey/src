#!/usr/bin/perl
#
#Quick install program.
#
#Commands go in install.dat. 
#Supported commands:
#print text
#get variable
#install from to
#mode [fileconvert]
#default text

#This returns a string that gives the path to get to the directory the program
#is in. It handles both dos and unix-style paths.
sub GetPath {
  #all we need to do is get rid of the filename at the end of $0.
  #so, first find that filename:
  #local (
  @junk=split(/\//,$0);
  #local 
  $a=$junk[$#junk];
  (@junk)=split(/\\/,$a);
  $a=$junk[$#junk];
  #then, remove it from the end of $0, and return the result:
  $b=$0;
  $b=~s/$a$//i;
  return $b
}

#Install a file to a location, expanding --*-- variables as we go.
#(The variables are even expanded in the parameters)
sub InstallCopy { $from=shift; $to=shift;
  $from=&ExpandVars($from);
  $to=&ExpandVars($to);
  print "Installing $from --> $to\n";
  open (IN,'<'.$from) || die "Couldn't open $from!\n";
  open (OUT,'>'.$to) || die "Couldn't write to $to!\n";
  while (<IN>) { 
    $_=&ExpandVars($_);
    print OUT 
  }
  close IN;
  close OUT;
}

#Expand all --*-- variables in the passed text
sub ExpandVars {
  $_=shift;
  foreach $a (@var) {
    ($var,$val)=split(/=/,$a,2);
    s/--$var--/$val/gi;
  }
  return $_;
}

if ($#ARGV eq -1) {
  #Read in install.dat, from our current subdirectory, process it.
  $path=&GetPath;
  open (DAT,'<'.$path.'install.dat');                        
}
else {
  $_=shift;
  open (DAT,'<'.$_);
}
while (<DAT>) {
  ($c,$r)=split(/ /,$_,2);
  if ($c eq 'print') {
    $r=&ExpandVars($r);
    print $r;
  }
  elsif ($c eq 'get') {
    $r=~s/\n$//;
    if ($default ne '') { print "[$default]\n" }
    print '=>';
    $a=<>;
    chop $a;
    if ($a eq '') {$a=$default}
    if (($mode eq 'filefix') || ($mode eq 'dirfix')) {
      $_=$a;
      s/c://io;
      s#\\#/#og;
      if (($mode eq 'dirfix') && (m#/$# eq '')) {$_=$_.'/'}
      $a=$_;
    }
    $var[$#var+1]="$r=$a";
    $mode='';
    $default=''
  }
  elsif ($c eq 'install') {
    $r=~s/\n$//;
    ($r,$r2)=split(/ /,$r,2);
    &InstallCopy($r,$r2);
  }
  elsif ($c eq 'default') {
    $r=~s/\n$//;
    $r=&ExpandVars($r);
    $default=$r;
  }
  elsif ($c eq 'mode') {
    $r=~s/\n//;
    $r=&ExpandVars($r);
    $mode=$r;
  }
  else { #run as a perl command
    eval $c.' '.$r;
  }
}
close DAT;