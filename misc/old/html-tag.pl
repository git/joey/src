#!/usr/bin/perl -i.bak
#
#html-tag
#By Joey Hess, jeh22@cornell.edu
#

#This is a self-configuring program. Don't touch the next line!
$startup="/home/joey/prog/include/startup.pl";

while (-e $startup ne 1) { #oops -- can't find a required file -- ask where it is, and update this program!
  print "Unable to find a required file: startup.pl.\n";
  print "Currently, it should be in $startup, but is not.\n";
  print "Entering self-config mode..\n";
  print "Enter the location of this file, and press return.\n";
  print "(You should probably enter an absolute path to it.)\n";
  print "(Be sure to write any \"\\\" characters as \"\\\\\")\n";
  $startup=<>;
  chomp $startup;
  print "Updating myself so I don't have to ask you again..\n";
  $^I=".bak"; #run this line if in-place-editing is not already on.
  open (IN,"<$0");
  open (OUT,">html-tag.bak");
  while (<IN>) { print OUT }
  close IN;
  close OUT;
  open (IN,"<html-tag.bak");
  open (OUT,">$0");
  while (<IN>) {
    if (/^\$startup=/i ne '') { 
      $_='$startup="'.$startup.'";'."\n";
    }
    print OUT $_;
  }
  close IN;
  close OUT;
  unlink 'html-tag.bak';
}

#where is startup.pl located?
require $startup;

$help=<<eofhelp;
Html-tag.pl
Quick HTML page updating program.

Synatx;
html-tag.pl [config-file] filespec
config-file...configuration file containing tags and their associated text
              (Should be in same directory as program)
filespec......files to process (wildcards and lists of files are OK.)

By Joey Hess, jeh22\@cornell.edu, 1995

eofhelp

$conf=shift;
$path=&GetPath;                 #The path to this program, and our INI file
#&INIproc($path.$ini); #Read our INI
$not_picky=1;
@ARGV=&ARGVproc(@ARGV);         #Read our parameters.

if ($conf eq "") { 
  print $help;
}
else {
  #Read conf file
  open (CONF,"<".$path.$conf);

  $mode=0;
  #modes:
  #0:getting next tag name
  #1:getting opening {
  #2:saving tag text, waiting for closing }
  while (<CONF>) {
    if ((/^#/ ne "") && ($mode ne 2)) { #omit comments
      $_="";
    }
    if (($mode eq 0) && ($_ ne "")) { #new tag
      $tn=$_;
      chomp($tn);
      $mode=1;
      $collect="";
    }
    elsif (($mode eq 1) && (/^\{/ ne "")) { #found {
      $mode=2;
    }
    elsif ($mode eq 2) { #wait for }
      if (/^\}/ ne "") {
        $tags{$tn}=$collect;
        if ($debug eq "y") {print "tag:--$tn-- is:\n".$tags{$tn}}
        $mode=0;
      }
      else {
        $collect=$collect.$_;
      }
    }
  }
  close CONF;

  $mode=0;
  #modes:
  #0=hunting
  #1=found tag top, looking for bottom.
  while (<>) {
    if (/\<\!-- tag:/i ne "") { #found tag
      chomp;
      $tn=$_;
      $tn=~s/\<\!-- tag://i; #pull out the tag name from this mess..
      $tn=~s/--\>//i;        #...
      $tn=~s/ //g;           #...
      $mode=1;
    }
    if (/\<\!-- endtag --\>/i ne "") { #end of tag
      $mode=0;
      $_= "<!-- tag:$tn -->\n".$tags{$tn}."\n<!-- endtag -->\n";
    }
    if ($mode ne 1) { print $_ } 
  }
}