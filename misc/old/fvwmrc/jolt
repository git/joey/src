#From: c1dmurph@CompApp.DCU.IE (David Murphy CA1)
#
#####################################################################################
# Dave's <94096350@dcu.ie> .fvwm2rc with Virtual Jolt Extensions 

# Make The XLock button look like an X

ButtonStyle 7 13 26x29@1 34x21@1 50x35@1 70x21@1 79x29@1 63x48@0 79x65@1 70x75@0 50x61@0 34x75@0 26x65@0 44x48@1 26x29@0

# Paths
IconPath /usr/include/X11/bitmaps
PixmapPath /usr/include/X11/pixmaps:/usr/local/lib/pixmaps
ModulePath /usr/X11R6/lib/X11/fvwm2/

# Fonts
WindowFont           "-*-helvetica-bold-r-*-*-12-*-*-*-*-*-*-*"

# Misc Config Settings
OpaqueMoveSize 5
XORvalue 5

###############################################################################
#
# Set up the virtual desktop and pager
#

#set the desk top size in units of physical screen size
DeskTopSize 3x3

# flip by whole pages on the edge of the screen.
EdgeScroll 0 0
EdgeResistance 0 0

##############################################################################
#
# Styles
##############################################################################
# Menu Style ....
#
HilightColor gold darkred
MenuStyle gold darkred white "-*-times-medium-r-*-*-*-120-*-*-*-*-*-*" MWM

###############################################################################
#
# Fvwm main style declarations
#
# All clients 
#
Style "*"		BorderWidth 4, HandleWidth 4, IconBox 20 -40 -140 -5 
Style "*"		MWMButtons, MWMFunctions, MWMDecor, HintOverride
Style "*"		ForeColor black, BackColor grey60
Style "*"		SmartPlacement, ActivePlacement

#
# Fvwm program styles
#
Style "xclock"		Sticky, NoTitle, NoHandles, StaysOnTop
Style "About"		Sticky, NoTitle, NoHandles, StaysOnTop
Style "*Form"		Sticky, NoTitle, NoHandles
Style "*Verify"		Sticky, NoTitle, NoHandles
Style "xpostit"		Sticky
Style "xload"		Sticky, NoTitle
Style "xeyes"		NoTitle
Style "oclock"		NoTitle
Style "xbiff"		NoTitle, Sticky
Style "Fvwm Pager"	NoTitle, Sticky
Style "FvwmPrompt"	Notitle, NoHandles, Sticky, StaysOnTop
Style "*Buttons"	NoTitle, Sticky, NoHandles
Style "FvwmIconBox"	Sticky

# Icons 
Style "*xterm*"		Icon xterm.xpm
Style "rxvt"            Icon xterm.xpm
Style "xcalc"		Icon xcalc.xpm
Style "xbiff"		Icon mail1.xpm
Style "xman"		Icon xman.xpm	
Style "*mac*"		Icon gnu-animal.xpm
Style "xcon*"		Icon nose.xpm
Style "xpaint"		Icon daisy.xpm
Style "emacs:"		Icon word_processor.xpm
Style "ghostview"	Icon ghostview.xpm

###############################################################################
#
# Thats all for styles ... refer to the man page for more info as well as the
# /usr/lib/X11/fvwm/sample.fvwmrc or .fvwmrc file ...
#
###############################################################################

###############################################################################
#
# Stuff to do at start-up
#

AddToFunc InitFunction
+       "I"      Module FvwmButtons

AddToFunc RestartFunction
+       "I"      Module FvwmButtons

###############################################################################
#
# Now define the menus - defer bindings until later

AddToMenu Net "Net Clients"	Title
+	"JoltNews"		Exec exec rxvt -geometry 166x56+0+0 -fg gold -bg black -T JoltNews -n News -e sh -c "tin -r"
+	"Netscape"		Exec exec netscape

AddToMenu Games "Games"		Title
+	"XBill"			Exec exec xbill
+	"XBoing"		Exec exec xboing
+	"XChomp"		Exec exec xchomp
+	"XGalaga"		Exec exec xgal

AddToMenu Toys	"Toys"		Title
+	"Neko"			Exec exec oneko
+	"Neko in a Window"	Exec exec xneko
+	"SunClock"		Exec exec sunclock
+	"XMountains"		Exec exec xmountains
+	"XEarth"		Exec exec xearth
+	"XSnow"			Exec exec snow

AddToMenu Modules "Modules"	Title
+	"FvwmConfig"		FvwmConfig
+	"FvwmButtons"		FvwmButtons
+	"FvwmTalk"		FvwmTalk
+	"FvwmPrompt"		FvwmPrompt
+	"FvwmBanner"		FvwmBanner
+	"FvwmIdent"		FvwmIdent
+	"FvwmIconBox"		FvwmIconBox
+	"SaveDesktop"  		FvwmSaveDesk
+	"FvwmSave"  		FvwmSave
+	"FvwmWinList"   	FvwmWinList
+	"FvwmScroll"		FvwmScroll

AddToMenu WM-Change "Switch to?" Title
+	"Bowman"		Restart bowman
+	"Fvwm 1.23b"		Restart fvwm
+	"Olvwm"			Restart olvwm
+	"Twm"			Restart twm
+	"Mwm"			Restart mwm
+	""			Nop
+	"Stay with Fvwm2"	Nop

AddToMenu Utilities "Utilities" Title
+	"Xterm"			Exec exec /usr/X11R6/bin/color_xterm -sb -sl 500 -j -ls -fg gold -bg black -geometry 100x28+0+0
+	"Custom Xterm"		Module FvwmForm XtermForm
+	"XLock"			Exec exec /usr/local/bin/xlock -mode random -fg gold -bg DarkRed -install
+	"XV"			Exec exec xv
+ 	"XCalc"			Exec exec xcalc
+	"System Monitor"	Exec exec rxvt -geometry 166x56+0+0 -fg gold -bg black -T "System Monitor" -n "Monitor" -e top
+	"Telnet..."		Module FvwmForm TelnetForm
+	""			Nop
+	"Net"			Popup Net
+	""			Nop
+	"Games"			Popup Games
+	""			Nop
+	"Toys"			Popup Toys
+	""			Nop
+	"Modules"		Popup Modules
+	""			Nop
+	"Other WM"		Popup WM-Change
+	""			Nop
+	"About..."		Module FvwmForm About
+	"Restart"		Module FvwmForm RVerify
+	"Exit"			Module FvwmForm QVerify

AddToMenu Window-Ops "Window Ops" Title
+	"Move" 			Function Move-or-Raise
+	"Resize" 		Function Resize-or-Raise
+	"Raise"			Raise
+	"Lower"			Lower
+	"(De)Iconify" 		Iconify
+	"(Un)Stick"		Stick
+	"(Un)Maximize" 		Maximize 100 100
+	""			Nop
+	"Refresh Screen"	Refresh
+	""			Nop
+	"Destroy"		Destroy
+	"Delete"		Delete

####################################################################################
# Complex Functions
AddToFunc Move-or-Raise		"I" Raise
+				"M" Move
+				"D" Lower

AddToFunc Move-or-Lower		"I" Lower
+				"M" Move
+				"D" RaiseLower

AddToFunc Move-or-Iconify	"M" Move
+				"D" Iconify

AddToFunc Resize-or-Raise	"I" Raise
+				"M" Resize
+				"D" RaiseLower

##############################################################################
# This defines the mouse bindings

# First, for the mouse in the root window
# Button 1 gives the Utilities menu
# Button 2 gives the Window Ops menu
# Button 3 gives the WindowList (like TwmWindows)
# I use the AnyModifier (A) option for the modifier field, so you can hold down
# any shift-control-whatever combination you want!

#     Button	Context Modifi 	Function
Mouse 1		R   	A       PopUp Utilities
Mouse 2		R    	A      	PopUp Window-Ops
Mouse 3		R    	A      	WindowList 2

# Now the title bar buttons
# Any button in the left title-bar button gives the window ops menu
# Any button in the right title-bar button Iconifies the window
# Any button in the rightmost title-bar button maximizes
# Note the use of "Mouse 0" for AnyButton.

#     Button	Context Modifi 	Function
Mouse 0		1    	A      	Popup Window-Ops
Mouse 0		2    	A     	Maximize 100 100
Mouse 0		3    	A      	Popup Utilities
Mouse 0		4    	A     	Maximize 50 100
Mouse 0		5	A	WindowList 2
Mouse 0		6	A	Iconify
Mouse 0		7    	A     	Exec exec /usr/local/bin/xlock -mode random -fg gold -bg DarkRed -install

# Now the rest of the frame
# Here I invoke my complex functions for Move-or-lower, Move-or-raise,
# and Resize-or-Raise. 
# Button 1 in the corner pieces, with any modifiers, gives resize or raise
Mouse 1		F	A	Function "Resize-or-Raise"
# Button 1 in the title, sides, or icon, w/ any modifiers, gives move or raise
Mouse 1		TS	A	Function "Move-or-Raise"

# Button 1 in an icons gives move for a drag, de-iconify for a double-click,
# nothing for a single click
# Button 2 in an icon, w/ any modifiers, gives de-iconify

Mouse 1		I	A	Function "Move-or-Iconify"
Mouse 2		I	A	Iconify

# Button 2 in the corners, sides, or title-bar gives the window ops menu
Mouse 2		FST	A	Module "fvwmident" FvwmIdent
# Button 3 anywhere in the decoration (except the title-bar buttons)
# does a raise-lower
Mouse 3		TSIF	A	RaiseLower

# Button 3 in the window, with the Modifier-1 key (usually alt or diamond)
# gives Raise-Lower. Used to use control here, but that interferes with xterm
Mouse 3         W       M       RaiseLower

############################################################################
# Now some keyboard shortcuts.

# Arrow Keys
# press arrow + control anywhere, and scroll by 1 page
Key Left	A	C	Scroll -100 0
Key Right	A	C	Scroll +100 +0
Key Up		A	C	Scroll +0   -100
Key Down	A	C	Scroll +0   +100

# press shift arrow + control anywhere, and move the pointer by 1% of a page
Key Left	A	SC	CursorMove -1 0
Key Right	A	SC	CursorMove +1 +0
Key Up		A	SC	CursorMove +0   -1
Key Down	A	SC	CursorMove +0   +1

# press shift arrow + meta key, and move the pointer by 1/10 of a page
Key Left	A	SM	CursorMove -10 +0
Key Right	A	SM	CursorMove +10 +0
Key Up		A	SM	CursorMove +0   -10
Key Down	A	SM	CursorMove +0   +10

# Keyboard accelerators
Key F1		A	M	Popup Utilities
Key F1		A	C	Desk 0 0
Key F2		A	M	Popup Window-Ops
Key F2		A	C	Desk 0 1
Key F3		A	M	WindowList
Key F4		A	M	Iconify
Key F5		A	M	Move
Key F6		A	M	Resize

###################FvwmButtons#################################################
#
*FvwmButtonsFore gold
*FvwmButtonsBack DarkRed
*FvwmButtonsGeometry -1-1
*FvwmButtonsRows 1
*FvwmButtons 		-	whatever		Swallow	"xbiff"
*FvwmButtons 		-	whatever		Swallow	"xload"	exec nice -16 xload -nolabel -fg gold -bg DarkRed -update 1 -geometry -1500-1500
*FvwmButtons 		XLock	lock.xpm		Exec	""	exec /usr/local/bin/xlock -mode random -fg gold -bg DarkRed -install
*FvwmButtons 		Telnet	rterm.xpm 		Module	FvwmForm TelnetForm
*FvwmButtons 		XTerm	rterm.xpm 		Exec	""	/usr/X11R6/bin/color_xterm -sb -sl 500 -j -ls -fg gold -bg black -geometry 100x28+0+0
*FvwmButtons (2x1) 	-	whatever		Swallow	Module "Fvwm Pager" FvwmPager 0 1

##############################################################################
# FvwmPager
##############################################################################
#
# Module FvwmPager 0 0
*FvwmPagerBack DarkRed
*FvwmPagerFore gold
*FvwmPagerHilight DarkRed
*FvwmPagerLabel 0 One
*FvwmPagerLabel 1 Two
*FvwmPagerLabel 2 Three
*FvwmPagerSmallFont 5x8

###############################################################################
# The Identify Module...
###############################################################################
#
*FvwmIdentBack  DarkRed
*FvwmIdentFore  gold
*FvwmIdentFont  -times-medium-r-*-*-*-160-*-*-*-*-*-*

###############################################################################
# The Window List Module
###############################################################################
#
*FvwmWinListBack        #908090
*FvwmWinListFore        Black
*FvwmWinListFont        -bitstream-charter-medium-r-*-*-*-160-*-*-*-*-*-*
*FvwmWinListAction Click1 Iconify -1,Raise
*FvwmWinListAction Click2 Iconify 1
*FvwmWinListAction Click3 Module "FvwmIdent" FvwmIdent
*FvwmWinListUseSkipList
*FvwmWinListGeometry +0-1

###############################################################################
# FvwmPrompt
###############################################################################
*FvwmPromptFore		gold
*FvwmPromptBack		DarkRed


###############################################################################
# The IconBox
###############################################################################
#
# Module FvwmIconBox
*FvwmIconBoxSortIcons
*FvwmIconBoxPlacement Top Left
*FvwmIconBoxGeometry +1-1

###############################################################################
# Telnet Form
###############################################################################
#
# General Appearance
*TelnetFormWarpPointer
*TelnetFormFore 	gold
*TelnetFormBack 	black
*TelnetFormItemFore 	black
*TelnetFormItemBack	grey60
*TelnetFormFont         *helvetica*m*o*n*12*
*TelnetFormButtonFont   *helvetica*m*o*n*12*
*TelnetFormInputFont    *helvetica*m*o*n*12*

# Gadgets
*TelnetFormLine		expand
*TelnetFormSelection	CommonTarget	single
*TelnetFormChoice	SamsonButton	samson		off	"Samson"
*TelnetFormChoice	VaxButton	vax		off	"Vax"
*TelnetFormChoice	AIXButton	aixserver	off	"AIXServer"

*TelnetFormLine		center
*TelnetFormText		"Or somewhere else:"
*TelnetFormInput	CustomTarget		20	""

*TelnetFormLine		expand
*TelnetFormButton	quit		"Go"	^M

# Action
*TelnetFormCommand	Exec exec rxvt -fg gold -bg black -T $(CommonTarget) $(CustomTarget)  -e telnet $(CommonTarget) $(CustomTarget)

*TelnetFormButton	restart		"Clear"
*TelnetFormButton	quit		"Cancel"	^[
*TelnetFormCommand	Nop

###############################################################################
# Xterm Form
###############################################################################
#
# General Appearance
*XtermFormWarpPointer
*XtermFormFore 	gold
*XtermFormBack 	black
*XtermFormItemFore 	black
*XtermFormItemBack	grey60

# Gadgets
*XtermFormLine		center
*XtermFormText		"Foreground"
*XtermFormInput		Fore		20	"gold"

*XtermFormLine		center
*XtermFormText		"Background"
*XtermFormInput		Back		20	"black"

*XtermFormLine		center
*XtermFormText		"Title Bar"
*XtermFormInput		Title		20	"JoltTerm"

*XtermFormLine		expand
*XtermFormButton	quit		"Go"	^M

# Action
*XtermFormCommand	Exec exec xterm -fg $(Fore) -bg $(Back) -T $(Title)

*XtermFormButton	restart		"Clear"
*XtermFormButton	quit		"Cancel"	^[
*XtermFormCommand	Nop


###############################################################################
# About Box
###############################################################################
#
# General Appearance
*AboutWarpPointer
*AboutFore		gold
*AboutBack		DarkRed
*AboutFont		*helvetica*m*r*n*12*

# Gadgets

*AboutLine		center
*AboutText		"Fvwm version 2.0.38"

*AboutLine		center
*AboutText		"Virtual Jolt Extensions version 1.0"

*AboutLine		expand
*AboutButton		quit	"Dismiss"	^M

###############################################################################
# Quit Verification
###############################################################################
#
# General Appearance
*QVerifyGrabServer
*QVerifyWarpPointer
*QVerifyFore		gold
*QVerifyBack		DarkRed
*QVerifyFont		*helvetica*m*r*n*12*

*QVerifyLine		center
*QVerifyText		"Quitting X..."

*QVerifyLine		expand

*QVerifyButton		quit	"Quit X"	^M
*QVerifyCommand		Quit

*QVerifyButton		quit	"Don't Quit"	^[
*QVerifyCommand		Nop

###############################################################################
# Restart Verification
###############################################################################
#
# General Appearance
*RVerifyGrabServer
*RVerifyWarpPointer
*RVerifyFore		gold
*RVerifyBack		DarkRed
*RVerifyFont		*helvetica*m*r*n*12*

*RVerifyLine		center
*RVerifyText		"Are you sure?"

*RVerifyLine		expand

*RVerifyButton		quit	"Restart"	^M
*RVerifyCommand		Restart fvwm2

*RVerifyButton		quit	"Don't"	^[
*RVerifyCommand		Nop

