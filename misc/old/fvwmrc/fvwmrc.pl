#!/usr/bin/perl
#

#The valid section names, with a space after each.
#Order is important, this is the order things end up being loaded!
$sectionlist='menus functions init desktop windows buttons icons keysetup ';

#Name of the subdirectoy in $HOME where personal fvwm configuration goes.
$homedir="/.fvwm/";

#Location of the global config directory.
#$systemdir="/etc/X11/fvwm/";
$systemdir="/home/joey/prog/fvwmrc/fvwm/";

#Display usage text.
sub usage {
		print "usage: fvwmrc [options]
Options are processed in the order they appear in the command line.
options:
\t-help\t\t\tPrint this message
\t+verbose\t\tIncrease verbosity of output (default=1)
\t-verbose\t\tDecrease verbosity of output
\t-system\t\t\tModify the system fvwmrc file
\t-own\t\t\tModify your own .fvwmrc file (default)
\t-packages\t\tList all available packages
\t-query\t\t\tList the current makeup of the fvwmrc file
\t-all <pkg>\t\tLoad all sections from <pkg>
\t-load <pkg> <section>\tLoad specified <section> from <pkg>
<section> can be one of: buttons, desktop, functions, icons
                         init, keysetup, menus, windows
";
}

#Exit with an error.
sub gripe { 
		print STDERR @_[0]."\n";
		exit 1;
}

#Display output if it is important enough.
sub chat { 
		my ($level,$line)=@_;
		if ($verbose>=$level) {
				print "$line\n";
		}	
}

#Get the next parameter out of ARGV and return it.
sub getNextParam { 
		my $param=shift(@ARGV);
		if (!$param) {
				my $expected=shift;
				gripe "Missing parameter: $expected";
		}
		return $param;
}

#Copy a file, with optional textual substituitions.
sub scopy { 
		my ($source, $dest, $find, $replace)=@_;
		chat (3,"Copying $source to $dest.");
		open (S_IN,"<$source") || gripe "Unable to read $source: $!";
		open (S_OUT,">$dest") || gripe "Unable to write $dest: $!";
		while (<S_IN>) {
				if ($find) {
						s/$find/$replace/g;
				}
				print S_OUT $_;
		}
		close S_IN;
		close S_OUT;
}

#Return 1 if the passed packagename is valid, undef if not.
sub validPackage {
		$pkg=shift;
		for $dir ($owndir,$systemdir) {
				opendir (DIR,$dir);
				while ($f=readdir DIR) {
						if (($f eq $pkg) && ($f ne '.') && ($f ne '..') && (-d $dir.$f)) {
								return 1;
						}
				}
		}
		return undef;
}

#Returns 1 if the check is passed, undef if not. 
#This can be called in one of 2 ways:
#  validSection($section); -- is $section an ok sectionname, in general?
#  validSection($section,$pkg); -- is $section a section of $pkg?
sub validSection {
		my ($section,$pkg)=@_;
		if ($pkg) {
				return 1;
		}
		else {
				return ($sectionlist=~m/$section / ne '');
		}
}

#Display what packages are active in the fvwmrc file right now.
sub query {
		my $blank='                            ';
		chat (1,"section name  from package");
		chat (1,"------------  ------------");
		foreach $section (split(/ /,$sectionlist)) {
				$to=readlink($dir.$section);
				($to)=$to=~m#^(.*?)/#;
				if (!$to) {
						$to="[not available]";
				}
				chat (0,$section.substr($blank,0,14-length($section)).$to);
		}
}

#List all available packages.
#This is accomplished by listing all directories in $owndir or $systemdir
sub listPackages {
		$count=0;
		
		opendir (DIR,$dir);
		while ($f=readdir DIR) {
				if (($f ne '.') && ($f ne '..') && (-d $dir.$f)) {
						chat (0,$f);
						$count++;
				}
		}
		closedir DIR;

		if ($count eq 0) {
				chat (1,"No packages are available in $dir!");
		}
}

#It all comes down to this function...
#Install a section of a package, intelligently constructing things on the fly.
sub loadSection {
		my ($section, $package)=@_;
		my $fn=$dir.$section;

		#Remove old symlink, if any.
		if (-e $fn) {
				if (-l $fn eq '') { #unless it's not a symlink
						gripe "Unable to install $section from $package: $fn\nis not a symlink. Remove it manually and then rerun this command.";
				}
		}
		unlink $fn;

		#Set up the $dir directory if it doesn't exist, so the $HOME/.fvwm/ 
		#directory is made transparently from the user's viewpoint.
		if (-e $dir eq '') {
				chat (3,"Making $dir directory.");
				#Create the directory, and copy some basic files into it.
				my $dir2=$dir;
				$dir2=~s#/$##;
				mkdir $dir2,0777;
				scopy("$systemdir/fvwmrc","$dir2/fvwmrc",$systemdir,$dir);
				scopy("$systemdir/mods","$dir2/mods",$systemdir,$dir);
		}

		#Set up the package's directory if it doesn't already exist.
		if (-e "$dir$package" eq '') {
				chat (3,"Making $dir$package directory.");
				mkdir $dir.$package,0777;
		}

		#Import in the given section of the package if it doesn't exist.
		if (-e "$dir$package/$section" eq '') {
				scopy("$systemdir$package/$section","$dir$package/$section");
		}

		#Create new symlink. (note: it's relative for portability's sake)
		my $link_ok=symlink("$package/$section",$fn);
		if (!$link_ok) { 
				gripe "Unable to symlink $fn to $dir$package/$section: $!"
		}
}


#Initialization...
if ($#ARGV eq -1) { 
		usage();
}
$owndir=$ENV{HOME}.$homedir;
$dir=$owndir;
$verbose=1;

#Loop on arguments, preforming actions.
#We can't use a for loop here, because @ARGV is shifted elsewhere as well.
while ($_=shift @ARGV) {
		tr/A-Z/a-z/;

		if ($_ eq '-help') { 
				usage();
		}
		elsif ($_ eq '-query') {
				chat(2,"Querying fvwmrc file.");
				query();
		}
		elsif ($_ eq '-packages') {
				chat(2,"Listing available packages.");
				listPackages();
		}
		elsif ($_ eq '-system') {
				$dir=$systemdir;
				chat(2,"Using SYSTEM fvwmrc.");
		}
		elsif ($_ eq '-own') {
				$dir=$owndir;
				chat(2,"Using own .fvwmrc.");
		}
		elsif ($_ eq '+verbose') {
				$verbose++;
				chat(2,"Increasing verbosity of output, now at level $verbose.");
		}
		elsif ($_ eq '-verbose') {
				if ($verbose >0) {
						$verbose--;
						chat (2,"Decreasing verbosity of output, now at level $verbose.");
				}
		}
		elsif ($_ eq '-all') {
				my $pkg=getNextParam('<pkg>');
				if (validPackage($pkg)) {
						chat (1,"Loading all sections from $pkg");
						foreach $section (split(/ /,$sectionlist)) {
								if (!validSection($section,$pkg)) {
										chat(2,"$pkg doesn't contain a section named $section. Skipping..\n");
							}
								else {
										chat(2,"\t$section");
										loadSection($section,$pkg);
								}
						}
				}
				else {
						gripe "$pkg is not a valid package.";
				}
		}
		elsif ($_ eq '-load') {
				my $pkg=getNextParam('<pkg>');
				if (!validPackage($pkg)) {
						gripe "$pkg is not a valid package.";
				}
				my $section=getNextParam('<section>');
				if (!validSection($section)) {
						gripe "$section is not a valid section of the fvwmrc.";
				}
				if (!validSection($section,$pkg)) {
						gripe "$pkg doesn't contain a section named $section.";
				}
				chat(1,"Loading $section from $pkg package.");
				loadSection($section,$pkg);
		}
		else {
				gripe "Unrecognized command line option: $_";
		}
}



