#!/usr/bin/perl
#
#Let people know kite has moved. (Part 1)
#


#Here's how I used this:
#
#perl kite-moved.pl < ~ftp/usage/linkback.html > /tmp/allurls.dat
#(collects all the contents of the urls listed on that page into 1 huge file)
#perl kite-moved2.pl </tmp/allurls.dat > /tmp/allurls2.dat
#(pulls email addresses out of all pages that had an out-of-date link
#to kite)
#<editing to get rid of empty email addresses, whatever else>
#perl kite-moved3.pl </tmp/allurls22.dat >/tmp/allurls3.dat
#(concentrates email addresses)
#perl kite-moved4.pl </tmp/allurls3.dat 
#(finally, mail it to them)
#Check bounces, maunally look up email addresses, and send again.

BEGIN {
	push(@INC,'/home/joey/prog/include');
}

use GetURL;

while (<>) {
	($url)=m#<A HREF=\"(.*?)\"#;
	if ($url) {
		system "(kite-get.pl $url &);sleep 5;killall -KILL kite-get.pl";
	}
}
