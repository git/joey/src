#!/usr/bin/perl
#
#Foremenu for perl 5.0
#
#Joey Hess, <jeh22@cornell.edu>
#
#"It just keeps getting better and better.."
#

#this has to point to startup.pl
require "/home/joey/prog/include/startup.pl";

#Like many unix programs, this program can have config files in a variety of
#locations.
sub Init {
  $home=$ENV{'HOME'}.'/';
  $menudefs="$home.foremenu-menus";
  INIproc("$home.foremenurcpl");  #load user's personal configuration.
  @ARGV=ARGVproc(@ARGV);     #process command line (can override .foremenurc)
  $menudefs=~s/~\//$home/igo;
  ReadMenus($menudefs);      #load in the menus we need.
  
}

#Read in a menu definitions file and create menus. Menu def files look just 
#like those that make the root window in FVWM's rc files. You can also 
#optionally add additional info to them, though to control color, font, etc.
#(This sub can actually read a .fvwmrc and pick the menus out of it. You 
#probably will want a custom menu file just for foremenu, but it can give you
#a good idea of foremenu's capabilities quickly to use .fvwmrc.)
sub ReadMenus {
  my($fn)=shift;
  open (IN,"<$fn");
  $onmenu='';
  while (<IN>) {
    chomp;
    tr/\t/ /;
    tr/ / /s;
    #is this a non-commented line?
    if (/^#/ eq '') {
      ($_,$rest)=split(/ /,$_,2);
      #look for "Popup" keyword.
      if (/^popup/i ne '') { #new menu.
        if ($onmenu) { die "Nested menus at $_ $rest!\n" }
        $onmenu=1;
        $menutitles[$#menutitles+1]=$rest;
      }
      elsif (/^endpopup$/i ne '') { #end of menu
        if (!$onmenu) { die "EndPopup with no Popup!\n" }
        $onmenu='';
      }
      elsif ($onmenu) { #store menu items
        $menulength[$#menutitles]++;
        $menus{$menutitles[$#menutitles]}[$menulength[$#menutitles]]=$rest;
        print $rest."\n";
      }
    }
  }
  close IN;
}

Init; #Start the ball rolling..