#!/usr/bin/perl
#
#Play mods with an attractive interface, interactively.
#Currently, this program is hardwired to require s3mod to play the mods.
#
#I'm to lazy/dumb to do perl curses IO, so I use slackware.
#It's ugly but it works.
#

$looping='OFF';

#build the string that lists all the files to pick from.
#trim off path data from the filenames, to make the list work better (hotkeys)
foreach (@ARGV) {
  @a=split(/\//,$_);
  $list.="\"$a[$#a]\" \" \" ";
}

#set menu heights
if (($#ARGV+1 <=> 23) eq 1) { #max height
	$height=22;
}
else { #menu and dialog to fit number of entries in list.
	$height=$#ARGV+9;
}
$mheight=$height-7;

$a=0;
$tmp='/tmp/.sound-'.time;
@ret="";
while ($a eq 0) {
  $loop='"Looping" "'.$looping.'" ';
	$a=system "dialog --title \"Play MODS\" --menu \"Select the MOD file to play:\" $height 40 $mheight $loop$list 2> $tmp";
	open (IN,"<$tmp");
  $ret=<IN>;
	close IN;
  if ($a eq 0) {
		if ($ret eq 'Looping') {
			if ($looping eq 'ON') {$looping='OFF'} else {$looping='ON'}
		}
		else {
			#get the actual filename back.
			foreach (@ARGV) {
			  if (/\/$ret$/ ne '') {
			  	$fn=$_;
			  }
			}		
			if ($looping eq 'OFF') { $n='-n' } else { $n='' }
	    system "s3mod $n $fn>$tmp &";
			sleep 2;
	    system "dialog --title \"Playing MOD\" --textbox $tmp 22 76 ; killall -KILL s3mod 2>/dev/null";
		}
  }
}

unlink $tmp; 		#clean up disk.
system 'clear';	#clean up screen.