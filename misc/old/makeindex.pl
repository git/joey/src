#!/usr/bin/perl
#
#Creates an index of pages. The pages should be listed on a html page,
#this program will extract all urls on that page, and go get them, saving 
#them to an index directory that can be quickly searched via grep.
#
#Note:
#	This program will not currently handle relative urls on a page. All urls 
#	must be absolute (start with http://).
#
#Syntax:
#		makeindex.pl [filespec]
#Where filespec is the file containing the list of urls to get.
#
#Written for Preferred Interent, Inc., by Joey Hess, <jeh22@cornell.edu>
#Copyright 1996, by Joey Hess <jeh22@cornell.edu>

#Global variables to set:
BEGIN {

	#Where is the GetURL.pm library located?
  push (@INC,'/home/joey/prog/include/');

	#Output directory, where we will wrote the index files:
	$outputdir='/tmp/index/';

	#Set to 1 if you want debugging output, '' if not.
	$debug=1;
}

#-----------------No user servicable parts below this line-------------------#
use GetURL;

#Sanity checking:
if (-e $outputdir eq '') {
	exit print "No such output directory, \"$outputdir\". Program aborted!\n";
}

#Clear index directory of current contents.
system "rm $outputdir/*.index &>/dev/null";

undef %indexed;

$index=0;
$dup=0;

while (<>) {
	foreach (split(/ /)) { #helps catch multpile urls on 1 line.
		#Looking for urls of the style: http://host.here.there/somwhere/file
		($url)=m#(http://[\w+|:|\.|-|/|\\|~|]*)#i;
		if ($url) { #Index this url.
			($host,$file)=$url=~m#^http://(.*?)/(.*?)$#i;
			if (!$host) { ($host)=$url=~m#^http://(.*?)$#i }
			($host,$port)=split(/:/,$host,2);
			#Make sure we don't hit anything twice.
			if (!$indexed{$url}) {
				$indexed{$url}=1;
				$index++;
				print "$url: $host,$port,$file ($index)\n" if $debug;
				open (OUT,">$outputdir/$index.index");
				print OUT "$url\n";
				foreach (GetURL::GetURL($host,$port,$file)) { print OUT }
				close OUT;
			}
			else {
				print "$url: already indexed\n" if $debug;
				$dup++;
			}
		}
	}
}

print "$index urls indexed\n";
print "$dup duplicate urls ignored\n";
