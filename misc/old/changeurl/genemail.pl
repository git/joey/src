#!/usr/bin/perl
#
# Passed the output of getit, outputs a nicely formatted list of emails and
# urls. Also does guessing of ~user/ directory to email address mappings where
# necessary.

sub AddUrl { my ($emails,$url)=@_;
	s/%7E/~/g;
	if ($url=~m#://(.*?)/~(.*?)/# ne undef) {
		$emails="$2\@$1";
	}
	$emails=~tr/ / /s;
	$emails=~s/^ //;
	$emails=~s/ $//;
	$emails=~s/(\@.*)\:.*/$1/g; # fix :port that snuck into email address.
	$qmurl=quotemeta($url);
	foreach $email (split(/ /,$emails)) {
		if ($emailurl{$email}=~m/$qmurl/i eq undef && $email=~m/whitehouse\.gov/i eq undef) {
			$emailurl{$email}.="\t$url\n";
		}
	}
}

while (<>) {
	if (/(dejanews|search|debian|excite\.com|altavista\.|metacrawler\.|infoseek\.|hotbot\.|search\.com|lycos\.com|yahoo\.com|webcrawler\.com)/ ne undef) {
		# these were messing me up..
	}
	elsif (/^Bad (.*?) unknown/ ne undef) {
		AddUrl('',$1);
	}
	elsif (/^Bad (.*?) (.*?\@.*)\r\n$/ ne undef) {
		AddUrl($2,$1);
	}
}

foreach $email (keys(%emailurl)) {
	print "$email\n$emailurl{$email}\n";
}
