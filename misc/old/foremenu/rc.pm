#!/usr/bin/perl
#
#This package handles rc file reading.
#
{
package rc;

#I'm a little sloppy in here with global variables, but that's one reason 
#why the whole mess is wrapped up in this package..

#Get rid of comments, and extra white space.
#
sub FixRcLine { $_=shift;
	if (/^#/ ne '') { return '' } #comment
	chomp;
	tr/\t/ /;
	tr/ / /s;
	s/^ //;
	s/ $//;
	return $_;
}

#Display an error in the file format.
#
sub RcError { my ($message)=@_;
	print "$fn at $linenum: $message\n";
}

#Read in the named rc file, and process it
#
#You'll also need to pass this a subroutine, to actually process the 
#each line once we've whittled it down to a 'tag=value' format.
#This should take 2 parameters: the tag, and the value.
#It should return notihing unless it needs to return an error, then,
#it should just return the error message to display.
#
#If the $extended parameter is true, it'll look for "~/.$fn" and "/etc/$fn" and "$fn"
#and parse the 1st file it finds.
#
sub ReadRc { ($fn,$ExternalProcessorRoutine,$extended)=@_;
	$linenum=0;
	if ($extended) {
		print "Processing extened stuff\n";
		open(RC,"$ENV{HOME}/.$fn") || open(RC,"etc/$fn") || open(RC,$fn) || exit print "Can't open any $fn!\n";
	}
	else {
		open (RC,$fn) || exit print "Can't open $fn!";
	}
	while (<RC>) {
		$linenum++;
		$_=FixRcLine($_);
		if ($_) {
			ProcessCommand($_);
		}
	}
	close RC;
}

#Process a command of the rc file (they are always of the form "item=value" 
#or just 'item')
#
sub ProcessCommand { my $item=shift;
	my ($tag,$value)=split(/=/,$item,2);
	$tag=~tr/A-Z/a-z/;
	my $a=&$ExternalProcessorRoutine($tag,$value);	#not in this package, it's program-specific..
	if ($a) { RcError($a) }
}

} #end of the rc package

1