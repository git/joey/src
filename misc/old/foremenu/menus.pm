#!/usr/bin/perl
#
#This package makes menus on the fly.
#
{
package menus;

#Makes menu items, like menubars, menus, cascades, commands, 
#separators, etc.
#
sub ProcessRcLine { my($item,$params)=@_;
	my $newblock='';
	if ($params=~/ \[$/ ne '') { #this line is the beginning of a block.
		$params=substr($params,0,-2);
		$newblock=1; #need to make this item, then push it on..
		$newitem='';
	}
	elsif ($item=~/ \[$/ ne '') { #this list begins a block.
		$item=substr($item,0,-2);
		$newblock=1; #need to make this item, then push it on..
		$newitem='';		
	}

	($text,$params)=split(/:/,$params,2);

	if ($text=~/_/ ne '') { #handle an underlined letter
		my ($a,$b)=split(/_/,$text,2);
		$underline=length($a);
		$text=~s/_//;
	}
	else {
		$underline=-1; #no underline
	}

	$parent=$block_list[$#block_list]; #parent item is the one currently at the top of the stack..
	
	if ($item) {
		if ($item eq ']') {	#not really an item, this ends a block
			pop(@block_list);
		}
		elsif ($item eq 'menu') {
			$newitem=$parent->Menubutton(-text=>$text,-underline=>$underline);
			$newitem->pack(-side=>'left');
		}
		elsif ($item eq 'command') {
			if ($params=~/^&/ ne '') { #starts with &, means it's a perl command
				$params=~s/^&//;
				$parent->command(-underline=>$underline,-label=>$text,-command=>[sub {menus::docommand($params)}]);
			}
			else {	#shell command.
				$parent->command(-underline=>$underline,-label=>$text,-command=>[sub {menus::doshell($params)}]);
			}
		}
		elsif ($item eq 'menubar') {
			$newitem=$main::w->Frame(-relief => 'raised',-bd => 2); #hardwired parent for this is the last window created..
			$newitem->pack(-side=>'left',-fill=>'x');
		}
		elsif ($item eq 'separator') {
			$parent->separator;
		}
		elsif ($item eq 'cascade') {
			#make a cascade, and make a new menu to be pulled up by it.
			$parent->cascade(-label =>$text, -underline =>$underline);
			my $cm = $parent->cget(-menu);
			$newitem = $cm->Menu;
			$parent->entryconfigure($text, -menu => $newitem);
		}
		else {
			return "\"$item $text:$params\" is an undefined item.";
		}
		
		if (($newblock) and ($newitem)) { #push the item we just made onto the block stack.
			push(@block_list,$newitem);
		}
	}
	return;
}

sub doshell {	system shift }
sub docommand { eval shift }

sub import {
	print "Unsing the menus.pm module to create menus on the fly..\n";
}

} #end of the MenuMaker package

1
