#!/usr/bin/perl
#
#This foremenu package handles making the main foremenu window,
#and setting any foremenu parameters from the rc file. (These should probably be seperated.. foremenu.pm and window.pm)
#
#Note that if you call it twice from the rc file, yes, you will get 2 windows!
#And, they are pretty much independent, you can close one on the fly if you wish.
#
#Note: this modifies the main:: package namespace, and adds a '$w' for the created window!
#
{
package foremenu;

#Doesn't do anything at this point; foremenu doesn't need any parameters.
#
sub ProcessRcLine { my($item,$params)=@_;
	eval '$main::w->'.$item.'($params)';
}

#Create the window.
#
sub import {
	print "Making new main window.\n";
	$main::w=MainWindow->new;
	$main::w->title('foremenu');
	$main::w->iconname('foremenu');
}

} #end of package

1
