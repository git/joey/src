#!/usr/bin/perl
#
#Minture command line module for foremenu
#
{
package minicommandline;

#Doesn't do anything at this point; minicommand line doesn't need any parameters.
#
sub ProcessRcLine { my($item,$params)=@_;
}

#Create the minicommand line item in the window.
#
sub import {
	my $minicommand=$main::w->Entry(-relief=>'sunken',-textvariable=>\$minicommand_text);
	$minicommand->pack(-side=>'bottom',-expand=>'y',-fill=>'x',-pady=>'2m');
}

} #end of package

1

