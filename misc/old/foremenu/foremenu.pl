#!/usr/bin/perl
#
#Foremenu! 
#
#Notes on the philospohy of this program:
#Foremenu is designed to load other packages on the fly. Therefore, 
#*everything* in foremenu is a package, to eliminate naming conflicts.
#

use Tk;

BEGIN { #edit the line below to specify where all the .pm's go.
	push (@INC,'/home/joey/prog/foremenu/');
}

use rc;

#Processes a rc line.
#This looks for headers of the form [header], and when it finds one, it
#loads up a package to process every rc line until the next header.
#
#So, to add functionality to foremenu, make a package that contains a 
#ProcessRcLine subroutine that takes the same params as this one, and modify
#the rc file so it processes part of it. There you are!
#
sub ProcessRcLine { my ($command,$params)=@_;
	if ($command=~/^\[.*\]$/ ne '') { #new header
		($rcpackage)=($command=~m/^\[(.*)\]$/);
		eval "use $rcpackage";
		return;
	}
	else {
		#pass it along to whatever package is handling the rc right now..
		eval 'return &'.$rcpackage.'::ProcessRcLine($command,$params)';
	}
}	#<sigh> ... too many evals in there :-(

#Read the rc file, and build the menus.
rc::ReadRc('foremenurc',\&ProcessRcLine,1);

MainLoop;	#Process X events.
