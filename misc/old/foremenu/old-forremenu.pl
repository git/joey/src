#!/usr/bin/perl
#
#Foremenu! 
#
#Notes on the philospohy of this program:
#Foremenu is designed to load other packages on the fly. Therefore, 
#*everything* in foremenu is a package, to eliminate naming conflicts.
#
#Of course, some of these packages may be useful for other stuff..

#This package handles rc file reading.
{
package rc;

#I'm a little sloppy in here with global variables, but that's one reason 
#why the whole mess is wrapped up in this package..

#Get rid of comments, and extra white space.
#
sub FixRcLine { $_=shift;
	if (/^#/ ne '') { return '' } #comment
	chomp;
	tr/\t/ /;
	tr/ / /s;
	s/^ //;
	s/ $//;
	return $_;
}

#Display an error in the file format.
#
sub RcError { my ($message)=@_;
	print "$fn at $linenum: $message\n";
}

#Preocess 1 line of the file, handling blocks.
#
sub ProcessRcLine { my ($line)=@_;
	if (/ \[$/ ne '') { #this line is the beginning of a block.
		$rcblock=1;
		&AddToBlock(substr($_,0,-2));
	}
	elsif (/^\]$/ ne '') { #this line is the end of a block.
		if ($rcblock) {
			$rcblock='';
			&ProcessBlock;
		}
		else {
			RcError('End of block detected, but not in block!');
		}
	}
	elsif ($rcblock) {
		&AddToBlock($_);
	}
	else {
		&ProcessCommand($_);
	}
}

#Read in the named rc file, and process it
#
#You'll also need to pass this a subroutine, to actually process the 
#each line once we've whittled it down to a 'tag=value' format.
#This should take 2 parameters: the tag, and the value.
#It should return notihing unless it needs to return an error, then,
#it should just return the error message to display.
#
sub ReadRc { ($fn,$ExternalProcessorRoutine)=@_;
	$linenum=0;
	open (RC,"<$fn");
	while (<RC>) {
		$linenum++;
		$_=FixRcLine($_);
		if ($_) {
			ProcessRcLine($_);
		}
	}
	close RC;
	if ($rcblock) { 
		RcError('eof','Last block not terminated!');
		&ProcessBlock;
	}
}

#Add a rc command to a block
#
sub AddToBlock {
	$block[$#block+1]=shift;
}

#Process a block of commands.
#
#The point here is that all the commands inside the block are processed, and
#then the actual outer block command is processed last.
#
#(This isn't totally portable, what if we don't wanna process the inner 
#commands at all, they are just options? Oh, well..)
#
sub ProcessBlock {
#	my $topcmd=shift(@block);
	foreach (@block) {
		ProcessCommand($_);
	}
#	ProcessCommand($topcmd);
	$rcblock='';
	undef @block;
}

#Process a command of the rc file (they are always of the form "item=value" 
#or just 'item')
#
sub ProcessCommand { my $item=shift;
	my ($tag,$value)=split(/=/,$item,2);
	my $a=&$ExternalProcessorRoutine($tag,$value);	#not in this package, it's program-specific..
	if ($a) { RcError($a) }
}

} #end of the rc package


#This package makes menus on the fly.
{
package MenuMaker;

#Called by the rc package to make individual menu items, like menubars, 
#menus, cascades, commands, separators, etc.
#
sub MakeItem { my($item,$params)=@_;
	$item=~tr/A-Z/a-z/;
	if ($item eq 'menu') {
		$menus[$#menus+1]=$menubar->Menubutton(-text=>$params,-underline=>0);
		$menus[$#menus]->pack(-side=>'left');
	}
	elsif ($item eq 'command') {
		$menus[$#menus]->command(-label=>$params,-command=>[sub {exit}]);
	}
	elsif ($item eq 'menubar') {
		$menubar=$main::w->Frame(-relief => 'raised',-bd => 2);
		$menubar->pack(-side=>'top',-fill=>'x');
	}
	elsif ($item eq 'separator') {
		$menus[$#menus]->separator;
	}
	else {
		return "\"$item $params\" is a bad item.";
	}
}

} #end of the MenuMaker package

use Tk;

#Make our main window
$w=MainWindow->new;
$w->title('foremenu');
$w->iconname('foremenu');

#Now, read the rc file, and build the menus.
rc::ReadRc('foremenurc',\&MenuMaker::MakeItem);

MainLoop;	#Process X events.

#Make the window
#my $w=MainWindow->new;
#$w->title('foremenu');
#$w->iconname('foremenu');
#
##Make the menu bar.
#my $menubar=$w->Frame(-relief => 'raised', -bd => 2);
#$menubar->pack(-side=>'top',-fill=>'x');
#
##Make a sample menu.
#my $menu1=$menubar->Menubutton(-text=>'Control',-underline=>0);
#my $w=MainWindow->new;
#$w->title('foremenu');
#$w->iconname('foremenu');
#$menu1->pack(-side=>'left');
#$menu1->command(-label=>'Exit1',-command=>[sub {exit}]);
#
##Read in the config file, and make the menus.
#my $underline;
#open (IN,'<foremenurc');
#while (<IN>) {
#	if (/^#/ eq '') {	#not a comment
#		tr/\t/ /;	#general text cleanup..
#		tr/ / /s;
#		s/^ //g;
#		chomp;
#		
#		my ($w,$o)=split(/=/,$_,2);
#		$w=~tr/A-Z/a-z/;
#
#		my ($r,@options)=split(/,/,$o);
#	
#		if ($r=~/_/ ne '') {	#_x means underline that character. 
#			my ($a,$b)=split(/_/,$r,2);
#			$underline=length($a);
#			$r=~s/_//;
#		}
#	   else {
#	   	$underline=-1;	#no underline
#		}
#
##		$options='';
##		foreach (@options) {
##			s/=/=>/;
##			$options=$options.','.$_;
##		}
##
#		if ($w eq 'menu') { #create a new menu entry here
#			$menus[$#menus+1]=$menubar->Menubutton(-text=>$r,-underline=>$underline);
#			$menus[$#menus]->pack(-side=>'left');
#			foreach (@options) {
##				($a,$b)=split(/=/,$_,2);
#				$menus[$#menus]->entryconfigure($menus[$#menus],-tearoff=>0);
#			}
#		}
###		elsif ($w eq 'command') {
#			$menus[$#menus]->command(-label=>$r,-underline=>$underline,-command=>[sub {exit}]);
#		}
#		elsif ($w eq 'separator') {
#			$menus[$#menus]->separator;
##		}
##		else {
#			print "Bad command: $w=$r\n";
#		}
###	}
#}
#
#close IN;
#
#MainLoop;
