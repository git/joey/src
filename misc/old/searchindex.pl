#!/usr/bin/perl
#
#Searches an index of html docuemtns created with makeindex.pl.
#Run this as a cgi script.
#
#Written for Preferred Interent, Inc., by Joey Hess, <jeh22@cornell.edu>
#Copyright 1996 by Joey Hess <jeh22@cornell.edu>

#Customize the following things to your liking:

#What directory holds the index files?
$indexdir="/tmp/index";

#This HTML form is returned if the script is called with no parameters.
#It should display a form with a "search" field.
$defaultform=<<eof;

<h1 align=center>Search Businesses</h1>
This will search all pages listed in the 
<A HREF="http://preferred.com/pis/clients.html">business list</a>.<P>
<form method=post action="/~joey/searchindex.cgi">
Enter the text you wish to search for here: <input name=search><br>
<input type=submit value="Start Search">
</form>

eof

#This HTML form will be returned if at least one match is found.
#On it, a tag that looks like ~list~ will be replaced by the 
#full list of matches.
$listform=<<eof;

<h1 align=center>Results of search</h1>
Here are the results of your search
<ul>
~list~
</ul>
<hr>
Fill out the form below to search again:<P>
<form method=post action="/~joey/searchindex.cgi">
Enter the text you wish to search for here: <input name=search><br>
<input type=submit value="Search Again">
</form>

eof

#This form is filled out for each match. (And all of them combined end up
#replacing ~list~ in the form above.)
#On it, you can use the following tags:
#	~url~      Url to the page.
#	~title~    Title of the page.
#	~context~  Some words from the page, to put the match in context.
$matchform=<<eof;

<li>
	<b><A HREF="~url~">~title~</A></b><br>
	<pre>~context~</pre>

eof

#Just how many words of context do you want?
$context_words=20;

#This form will be returned if there are no matches for a search.
$noform=<<eof;
<h1 align=center>No Matches</h1>
There were no matches. Fill out the form below to try again.<P>
<form method=post action="/~joey/searchindex.cgi">
Enter the text you wish to search for here: <input name=search><br>
<input type=submit value="Search Again">
</form>

eof

#---------------no user servicable parts below this point ------------------#

#Ok, so you could change this if you wanted to. Maybe it is a little user
#serviceable.
$help=<<eofhelp;

Run this CGI script via the web, not at the command line!

eofhelp

#Fix a string formatted so it'll work with CGI into the normal string
#format.
#
sub CgiFix { my $s=shift;
	#Convert plus's to spaces
	$s =~ s/\+/ /g;
	# Convert %XX from hex numbers to alphanumeric
	$s =~ s/%(..)/pack("c",hex($1))/ge;
  
	return $s;
}

#Read and parse httpd input, and save it to @in.
#Portions of this subroutine are adapted from cgi-bin.pl,
#Copyright 1994 Steven E. Brenner <S.E.Brenner@bioc.cam.ac.uk>.
#(about 11 lines remain unmodified..)
#
sub ReadParse {
  # Read in text from form.
  my($in);
  if ($ENV{'REQUEST_METHOD'} eq 'GET') {
    $in = $ENV{'QUERY_STRING'};
  }
  elsif ($ENV{'REQUEST_METHOD'} eq 'POST') {
    read(STDIN,$in,$ENV{'CONTENT_LENGTH'});
  }
  else { #what?! We're being run at the _command_ line!
    exit print $help; #display help and exit.
  }
  @in = split(/&/,$in);
  foreach $i (0 .. $#in) {
    # Split into key and value.
    my($key, $val) = split(/=/,$in[$i],2); # splits on the first =.
    # Convert %XX from hex numbers to alphanumeric
    $key=CgiFix($key);
    $val=CgiFix($val);
    # Create saveform variable out of it, store it in @in.
    $in{"$key"} .= "\0" if (defined($in{"$key"})); # \0 is the multiple separator.
    $in{"$key"} .= $val;                  
  }                                       
}                    

#Print Mime header.
sub PrintHeader { print "Content-type: text/html\n\n" }

&PrintHeader;
&ReadParse;

$matches=0;
$listtext='';

if ($in{search}) {
	$search=quotemeta($in{search});
	opendir(DIR,$indexdir) || exit $!;
	while ($fn=readdir(DIR)) {
		open (IN,"<$indexdir/$fn");
		chomp($url=<IN>);
		$glob='';
		while (<IN>) { $glob.=$_ }
		$glob2=$glob;
		$glob2=~tr/\n /  /s;
		$glob2=~s/<.*?>//g; #remove html
		if ($glob2=~/$search/i ne '') {
				$matches++;
				
				($title)=$glob=~m#<title>(.*?)</title>#i;
				(@words)=split(/ /,$glob2);
				$context='';
				for (1..$context_words) { $context.="$words[$_] " }
				$context=~s/($search)/<b>$1<\/b>/ig;
	
				$_=$matchform;
				s/~url~/$url/g;
				s/~title~/$title/g;
				s/~context~/$context/g;
				$listtext.=$_;
		}
		close IN;
	}
	closedir DIR;
	
	if ($matches eq 0) { 
		print $noform;
	}
	else {
		$listform=~s/~list~/$listtext/;
		print $listform;
	}
}
else { 
	print $defaultform;
}
