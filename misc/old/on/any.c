#include <sys/types.h>
#include <sys/socket.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <malloc.h>
#include <unistd.h>
#include <signal.h>

extern char **environ;
int sig=0;

/* Signal handler. */
void handler(int s) {
	sig = s;
}

int main (int argc, char **argv) {
  	int s, shmid;
  	char *shm_p;
  	struct sockaddr addr;
  	char buf[16384];
	fd_set rfds, efds;
	int i, len, want_line=0, want_tag=1, want_colon=0;
  	char tag;
	char *p=NULL;
  	char ret[4];
	char sep[2];
  	sep[0]='\1';
  	sep[1]='\0';
  
  	/* Connect to server via unix domain socket. */
  	s=socket(AF_UNIX, SOCK_STREAM, 0);
  	if (s == -1) {
	  	perror("socket");
	  	exit(1);
	}
  	addr.sa_family = AF_UNIX;
	strncpy(addr.sa_data, "/tmp/sock", sizeof(addr.sa_data));
  	if (connect(s, &addr, sizeof(addr.sa_family)+strlen(addr.sa_data)) == -1) {
	  	perror("connect");
	  	exit(1);
	}
  
  	/* Set up signal handlers. */
  	for (i=1;i<32;i++) {
	  	signal(i,handler);
	}
  
  	/* Assemble information, send to server. */
	/* Working directory. */
 	p=getcwd(p,1024);
  	sprintf(buf,"D:%s\n",p);
  	send(s,buf,strlen(buf),0);
  	free(p);
  	/* Environment. */
  	p=malloc(3);
  	strcpy(p,"E:");
	for (i=0; environ[i]; i++) {
		p=realloc(p,strlen(p) + strlen(environ[i]) + 2);
		strcat(p, environ[i]);
	  	if (environ[i+1]) {
		  	strcat(p,sep);
		}
	}
  	strcat(p,"\n");
	send(s,p,strlen(p),0);
  	free(p);
	/* Command line. */
  	p=malloc(3);
  	strcpy(p,"C:");
  	for (i=1; i<argc; i++) {
		p=realloc(p,strlen(p) + strlen(argv[i]) + 5);
		strcat(p, argv[i]);
		if (i + 1 < argc)
			strcat(p,sep);
	}
  	strcat(p,"\n");
  	send(s,p,strlen(p),0);
  	free(p);
  
  	/*
	 * Now a select loop. Read from stdin, write it to the server,
  	 * and read the server's return data and parse that.
	 */
  	for (;;) {
		FD_ZERO(&rfds);
		/* Watch stdin (fd 0) to see when it has input. */
		FD_SET(0, &rfds);
		/* Watch the server to see when it has input. */
		FD_SET(s, &rfds);
	  	i=select(s+1, &rfds, NULL, NULL, NULL);
		if (sig) {
			/* Pass signal on. */
			sprintf(buf,"S:%i\n",sig);
			sig=0;
			send(s,buf,strlen(buf),0);
		}
	  	else if (i) {
		  	if (FD_ISSET(0, &rfds)) {
				/* new data on stdin */
			  	len=read(0,buf,sizeof(buf));
			  	if (len > 0) {
				  	/*
					 * Need to insert I: prior to each
					 * new line that we send to the server.
					 */
				  	p=buf;
				  	for (i=0;i<len;i++) {
					 	if (buf[i] == '\n') {
						      	send(s,"I:",2,0);
						  	send(s,p,i - (p - buf) + 1,0);
						  	p=buf+i+1;
					  	}
					}
				  	/* FIXME: this handling of output with no \n isn't right. */
				  	if (p - buf < i) {
				    		send(s,"I:",2,0);
				  		send(s,p,len - (p - buf),0);
				  		send(s,"\n",1,0);
					}
				}
			}
			if (FD_ISSET(s, &rfds)) {
			  	/* can read from server */
			  	len=recv(s,buf,sizeof(buf),0);
			  	if (len < 1) {
				  	exit(1);
				}
			  	/* Parse server data, output it. */
			  	p=buf;
			  	for(i=0;i<len;i++) {
				  	if (want_line && buf[i] == '\n') {
					  	want_tag=1;
					  	want_line=0;
					  	buf[i]='\0';
					  	switch (tag) {
						  case 'O':
						  	printf("%s\n",p);
						  	break;
						  case 'E':
						  	fprintf(stderr,"%s\n",p);
						  	break;
						  case 'R':
						  	exit(atoi(p));
						}
					}
				  	else if (want_tag) {
				    		tag=buf[i];
					  	want_colon=1;
					  	want_tag=0;
					}
			  		else if (want_colon && buf[i] == ':') {
					  	want_line=1;
					  	want_colon=0;
					  	p=buf+i+1;
					}
				}
			  	/* Handle the case where we have
				 * part of a line left to deal with */
			  	if (want_line && p - buf < i) {
				  	buf[len]='\0';
				  	switch (tag) {
					  case 'O':
					  	printf("%s",p);
					  	break;
					  case 'E':
					  	fprintf(stderr,"%s",p);
					  	break;
					  case 'R':
					  	/*
						 * Need to make sure we have all
						 * of the ret code - read in
						 * more chars. It's ok if we block
						 * now.
						 */
					  	strncpy(ret,p,sizeof(ret));
					  	recv(s,buf,sizeof(ret),0);
					  	strncat(ret,buf,sizeof(ret) - strlen(ret));
					    	exit(atoi(ret));
					}
				}
			}
		}
	}
}
