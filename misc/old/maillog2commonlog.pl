#!/usr/bin/perl
#
#Oddly, this translates a sendmail-style mail log into a httpd log file.
#
#Why? Because there are tons of programs to process stuff in that format, 
#that's why!
#
#Run it as a filter:
#	maillog2commonlog.pl </var/log/maillog >mail_log
#

#This program is quite crappy and should be rewritten, My apologies. Also,
#it has a problem with getting the year right if you run december 1996 logs
#in jan '97, say. I guess I'll fix it next year :-)

#Note that the user name is excluded from the log, only the host name is left 
#in, to ensure privacy.

# Joey Hess <jeh22@cornell.edu> 

@date=split(/ /,`date`);
$year=$date[$#date];
chomp $year;

while (<>) {
	if (/: to=/ ne '') {
		#check to see if it was sent ok
		if (/stat=Sent/i ne '') {
			print ProcLine($_);
		}
	}
}

#Sorry, I gave up on commenting this. Bet you're tearing your hair out over it
#now. :-)
#
sub ProcLine { $_=shift;
	tr/ / /s;
	s/to=(.*?),/$a=$1;$a=~s# #_#g; "to=$a,"/e;
	($mon,$day,$time,$junk,$junk,$junk,$to,$from,@junk)=split(/ /,$_);
	$from=~s/^ctladdr=//g;
	if ($from=~m/^delay=/ ne '') { $from="unknown" }
	($user,$from)=split(/\@/,$from,2);
	if (!$from) { $from=$user }
	$to=~s/^to=//g;
	$to=~s/\,$//g;
	$email=~s/^<//;
	($email)=($to=~m/.*?\<(.*?)\>/);
	if (!$email) { $email=($to=~m/.*?\((.*?)\)/) } #gotta love that pattern matching ;-)
	if ($email) { $to=$email }
	($user,$to)=split(/\@/,$to,2);
	if (!$to) { $to=$user }
	if (length($day) eq 1) { $day="0$day" }
	return "$from - - [$day/$mon/$year:$time -0400] \"GET $to HTTP/1.0\" 200 1\n";
}