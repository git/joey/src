#include "pdmenu.h"

/* Read in a pdmenu style rc file. Preserves data from any other files that 
 * might have previously been read. */
void ReadRc (char *fname) {
	Menu_Type *m=NULL;
	static char str[MAX_LINE_LEN+2];
	char rest[MAX_LINE_LEN+2];
	char fields[NUM_FIELDS][256];
	char *key,*tmp;
	char *end=NULL;
	int i,line,field;
	FILE *fp;

	line=0;

	fp=fopen(fname,"r");
	if (fp==NULL) {
		fprintf(stderr,"Unable to open %s: %s.\n",fname,strerror(errno));
		exit(-1);
	}
	while (fgets(str,MAX_LINE_LEN,fp) != NULL ) {
		line++;
		
		/* remove trailing \n */
		tmp=str;
		while((*tmp != '\0') && (*tmp != '\n')) {
			tmp++;
		}
		*tmp='\0';
		
		/* ignore comments and lines without the delimiter in them */
		/* find the location of the first delim, set rest equal to the string after it */
		if ((str[0]=='#') || (str[0]==';') || ((end=strchr(str,FIELD_DELIM))==NULL)) {
			continue;
		}
		strcpy(rest,++end);
    
		/* Make str end at the first delim now. This gives us our keyword. */
		for( i=0; str[i] != '\0'; i++) {
			if ( str[i]==FIELD_DELIM) {
				str[i] = '\0';
				break;
			}
		}
		
		/* make key be the keyword with any leading whitespace stripped off. */
		key=str;
		while (isspace(*key)) { ++key; };

		/* get all the other fields */
		field=0;
		do {
			end=strchr(rest,FIELD_DELIM);
			if (end!=NULL) {
				for(i=0; rest[i] != '\0'; i++) {
					if (rest[i]==FIELD_DELIM) {
						rest[i] = '\0';
						break;
					}
				}
			}
			strcpy(fields[field++],rest);
			if (end!=NULL)
				strcpy(rest,++end);
			if (field==NUM_FIELDS)
				break;
		} while (end!=NULL);
		/* If there is anything else left, tack it onto the end of the last field. */

		/* process the data depending on the keyword */
		if (strcasecmp(key,NEW_MENU_KEYWORD)==0) {
			if (NumMenus>MAX_NUM_MENUS) {
				fprintf(stderr,"%s:%i Too many menus (max %i).\n",fname,line,MAX_NUM_MENUS);
				exit(-1);
			}
			m=menus+NumMenus++;
			strcpy(m->name,fields[0]);
			strcpy(m->title,fields[1]);
			strcpy(m->helptext,fields[2]);
			m->num=0;
			m->selected=0;
			m->x=0;
			m->y=0;
			m->dx=0;
			m->dy=0;
			m->mustscroll=0;
			m->recalc=1;
		}
		else if ((strcasecmp(key,EXEC_KEYWORD)==0) || (strcasecmp(key,SHOW_MENU_KEYWORD)==0) || (strcasecmp(key,EXIT_KEYWORD)==0)) {
			if (m->num>MAX_ITEMS_IN_MENU) {
				fprintf(stderr,"%s:%i Too many items in menu (max %i).\n",fname,line,MAX_ITEMS_IN_MENU);
				exit(-1);
			}
			if (strcasecmp(key,EXEC_KEYWORD)==0)
				m->items[m->num].type=MENU_EXEC;
			else if (strcasecmp(key,SHOW_MENU_KEYWORD)==0)
				m->items[m->num].type=MENU_SHOW;
			else
				m->items[m->num].type=MENU_EXIT;

			m->items[m->num].hotkey=-1;
			for (i=0;i<strlen(fields[0])-1;i++) {
				if (fields[0][i]==HOTKEY_SEL_CHAR) {
					m->items[m->num].hotkey=i;
					for (;i<strlen(fields[0]);i++) {
						fields[0][i]=fields[0][i+1];
					}
					break;
				}
			}
			
			strcpy(m->items[m->num].text,fields[0]);
			strcpy(m->items[m->num].flags,fields[1]);
			strcpy(m->items[m->num].command,fields[2]);
			m->num++;
		}
		for (i=0;i<field+1;i++)
			fields[i][0]='\0';
	}
	fclose(fp);
}
