#define MAX_ITEMS_IN_MENU 63
#define MAX_NUM_MENUS 10
#define MAX_WINDOWS 20

#define MENU_ITEM_TEXTSIZE 76

typedef struct Menu_Item_Type {
	signed char hotkey;
	char text[MENU_ITEM_TEXTSIZE];
	char command[255];
	char flags[4];
	#define CLEAR_FLAG 'c'
	#define PAUSE_FLAG 'p'
	char type;
	#define MENU_EXEC 1
	#define MENU_SHOW 2
	#define MENU_EXIT 3
} Menu_Item_Type;
          
typedef struct Menu_Type {
	char name[32];
	char title[76];
	char helptext[80];
	Menu_Item_Type items[MAX_ITEMS_IN_MENU];
	int num;
	int selected;
	int x, y;
	int dx, dy;
	int recalc;
	char mustscroll;
} Menu_Type;

#define MAX_LINE_LEN 512
#define FIELD_DELIM ':'
#define NUM_FIELDS 4
#define HOTKEY_SEL_CHAR '_'

#define NEW_MENU_KEYWORD "menu"
#define SHOW_MENU_KEYWORD "show"
#define EXEC_KEYWORD "exec"
#define EXIT_KEYWORD "exit"

#define DEFAULTRC "/tmp/foremenurc" /* ~/.DEFAULTRC and /etc/DEFAULTRC are checked */
#define DEFAULTBASE "Welcome to Foremenu 0.1 by Joey Hess <jeh22@cornell.edu>"
#define DEFAULTTITLE "Foremenu"