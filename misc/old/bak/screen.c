#include "pdmenu.h"

/* Draw the desktop; the background of the screen */
void DrawDesktop () {
	int x;
	
	SLsmg_set_color(DESKTOP);
	for (x=1;x<SLtt_Screen_Rows-1;x++) {
		SLsmg_gotorc(x,0);
		SLsmg_erase_eol();	
	}
}

/* Draw the title at the top of the screen (centered) */
void DrawTitle (char *title) {
	SLsmg_gotorc(0,0);
	SLsmg_set_color(TITLE);
	SLsmg_erase_eol();
	SLsmg_gotorc(0,(SLtt_Screen_Cols-strlen(title))/2);
	SLsmg_write_string(title);
}

/* Draw the message at the base of the screen */
void DrawBase (char *base) {
	SLsmg_gotorc(SLtt_Screen_Rows-1,0);
	SLsmg_set_color(BASE);
	SLsmg_write_nstring(base,SLtt_Screen_Cols);
}

/* Draw a shadow around the given dialog */
void DrawShadow (int x,int y,int dx,int dy) {
	int c;
	char *empty="";

	SLsmg_set_color(SHADOW);
	for (c=0;c<dy-1;c++) {
		SLsmg_gotorc(c+1+y,x+dx);
		SLsmg_write_char(' ');
		SLsmg_write_char(' ');
	}
	SLsmg_gotorc(y+dy,x+2);
	SLsmg_write_nstring(empty,dx);
}

/* Draw a dialog box on the screen. Doesn't save whatever's under it; sorry.
 * Pass 1 as shadow to enable a shadow
 * Set the color beforehand */
void DrawDialog (char *title,int x,int y,int dx,int dy,int shadow) {
	int c;
	char *empty="";
	
	SLsmg_draw_box(y,x,dy,dx);
	for(c=y+1;c<y+dy-1;c++) {
		SLsmg_gotorc(c,x+1);
		SLsmg_write_nstring(empty,dx-2);
	}	
	SLsmg_gotorc(y,x+(dx-strlen(title))/2);
	SLsmg_write_string(title);

	if (shadow==1) DrawShadow(x,y,dx,dy);
}

/* Highlight the hotkey of a given menu item, if there is a hotkey. */
void HighlightHotkey (Menu_Type *m,int item,int highlight_color) {
	if (m->items[item].hotkey!=-1) {
		SLsmg_set_color(highlight_color);
		SLsmg_gotorc(m->y+item+1,m->x+2+m->items[item].hotkey);
		SLsmg_write_char(m->items[item].text[m->items[item].hotkey]);
	}
}

/* Select a particular item in a menu. 
 * Pass the object color to change it to as the second parameter. */
void SelMenuItem (Menu_Type *m,int object_color,int object_hi_color) {
		SLsmg_set_color(object_color);
		SLsmg_gotorc(m->y+1+m->selected,m->x+1);
		SLsmg_write_char(' ');
		SLsmg_write_nstring(m->items[m->selected].text,m->dx-4);
		SLsmg_write_char(' ');
		HighlightHotkey(m,m->selected,object_hi_color);
}

/* Fill in the entries in a menu. Set the color first. 
 * Calls SelMenuItem to select the menu item, as well. */
void FillMenu (Menu_Type *m) {
	int c;
	
	for(c=0;c<m->num;c++) {
		SLsmg_set_color(MENU);
		SLsmg_gotorc(m->y+c+1,m->x+2);

		SLsmg_write_nstring(m->items[c].text,m->dx-4); 
		HighlightHotkey(m,c,MENU_HI);
	}
  SelMenuItem(m,SELBAR,SELBAR_HI);
}

/* Calculate the dimentions of a menu so it fits onscreen. */
void CalcMenu (Menu_Type *m) {
	int c,dx,dy;
	
	if (m->recalc==1) {
		m->recalc=0;
		dy=m->num;
		if (dy>SLtt_Screen_Rows-6) {
			dy=SLtt_Screen_Rows-6;
			m->mustscroll=1;
		}
	                   
		dx=strlen(m->title)+2;
		for(c=0;c<m->num;c++) {
			if (dx<strlen(m->items[c].text)+4)
				dx=strlen(m->items[c].text)+4;
		}
		if (dx>SLtt_Screen_Cols) 
			dx=SLtt_Screen_Cols;
	                                   
		m->dx=dx;
		m->dy=dy+2;
		m->x=(SLtt_Screen_Cols-dx)/2;
		m->y=(SLtt_Screen_Rows-dy)/2-1;
	}
}

void DrawMenu (Menu_Type *m) {
	SLsmg_set_color(MENU);
	CalcMenu(m);
	DrawDialog(m->title,m->x,m->y,m->dx,m->dy,1); 
	FillMenu(m);
}

void Screen_Init () {
	/* init terminal */
	SLtt_get_terminfo();
	SLang_init_tty(7, 0, 0);
	SLsmg_init_smg();
	SLtt_Use_Ansi_Colors=1;
}

/* set object colors */
void Screen_Setcolors () {
	SLtt_set_color(DESKTOP,"",DESKTOP_FG,DESKTOP_BG);
	SLtt_set_color(BASE,"",BASE_FG,BASE_BG);
	SLtt_set_color(TITLE,"",TITLE_FG,TITLE_BG);
	SLtt_set_color(MENU,"",MENU_FG,MENU_BG);
  SLtt_set_color(SELBAR,"",SELBAR_FG,SELBAR_BG);
  SLtt_set_color(SHADOW,"",SHADOW_FG,SHADOW_BG);
	SLtt_set_color(MENU_HI,"",MENU_HI_FG,MENU_HI_BG);
	SLtt_set_color(SELBAR_HI,"",SELBAR_HI_FG,SELBAR_HI_BG);
}

void Screen_Reset () {	
	/* reset terminal */
	SLsmg_gotorc(SLtt_Screen_Rows - 1, 0);
	SLsmg_refresh();
	SLsmg_reset_smg();
	SLang_reset_tty();
}	
