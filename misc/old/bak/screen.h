/* SLang screen objects and their colors */
int DESKTOP; char *DESKTOP_FG; char *DESKTOP_BG;
int TITLE;   char *TITLE_FG;   char *TITLE_BG;
int BASE;    char *BASE_FG;    char *BASE_BG;
int MENU;    char *MENU_FG;    char *MENU_BG;
int SELBAR;  char *SELBAR_FG;  char *SELBAR_BG;
int SHADOW;  char *SHADOW_FG;  char *SHADOW_BG;
int MENU_HI; char *MENU_HI_FG; char *MENU_HI_BG;
int SELBAR_HI; char *SELBAR_HI_FG; char *SELBAR_HI_BG;
int DEFAULT;


extern void DrawTitle(char *);
extern void DrawDesktop(void);
extern void DrawMenu(Menu_Type *);
extern void DrawBase(char *);
extern void Screen_Reset(void);
extern void Screen_Init(void);
extern void Screen_Setcolors(void);
extern void SelMenuItem(Menu_Type *,int,int);
