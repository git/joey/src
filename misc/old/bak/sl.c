#include <slang.h>
#include <stdio.h>
#include <string.h>

/* SLang screen objects and their colors */
static int DESKTOP=1;	char *DESKTOP_FG="white";	char *DESKTOP_BG="blue";
static int TITLE=2;		char *TITLE_FG="black";		char *TITLE_BG="red";
static int BASE=3;		char *BASE_FG="black";		char *BASE_BG="red";
static int MENU=4;		char *MENU_FG="black";		char *MENU_BG="cyan";
static int SELBAR=5;	char *SELBAR_FG="cyan";		char *SELBAR_BG="black";	
static int SHADOW=6;	char *SHADOW_FG="black";	char *SHADOW_BG="black";

#define MAX_ITEMS_IN_MENU 63
#define MAX_NUM_MENUS 10
char *menu_titles[MAX_NUM_MENUS];
char *menu_items[MAX_NUM_MENUS][MAX_ITEMS_IN_MENU];
int menu_num[MAX_NUM_MENUS];
int menu_selected[MAX_NUM_MENUS];
int CurrentMenu, dx, dy, mustscroll;

/* Draw the desktop; the background of the screen */
void DrawDesktop () {
	int x;
	
	SLsmg_set_color(DESKTOP);
	for (x=1;x<SLtt_Screen_Rows-1;x++) {
		SLsmg_gotorc(x,0);
		SLsmg_erase_eol();	
	}
}

/* Draw the title at the top of the screen (centered) */
void DrawTitle (char *title) {
	SLsmg_gotorc(0,0);
	SLsmg_set_color(TITLE);
	SLsmg_erase_eol();
	SLsmg_gotorc(0,(SLtt_Screen_Cols-strlen(title))/2);
	SLsmg_write_string(title);
}

/* Draw the message at the base of the screen */
void DrawBase (char *base) {
	SLsmg_gotorc(SLtt_Screen_Rows-1,0);
	SLsmg_set_color(BASE);
	SLsmg_write_nstring(base,SLtt_Screen_Cols);
}

/* Draw a dialog box on the screen. Doesn't save whatever's under it; sorry.
 * Pass 1 as shadow to enable a shadow
 * Set the color beforehand */
void DrawDialog (char *title,int x,int y,int dx,int dy,int shadow) {
	int c;
	char *empty="";
	
	SLsmg_draw_box(y,x,dy,dx);
	for(c=y+1;c<y+dy-1;c++) {
		SLsmg_gotorc(c,x+1);
		SLsmg_write_nstring(empty,dx-2);
	}
	SLsmg_gotorc(y,x+(dx-strlen(title))/2);
	SLsmg_write_string(title);
}

/* Select a particular item in a menu. 
 * Pass the object color to change it to as the second parameter. */
void SelMenuItem (int item, int object_color) {
	  int x,y;
	  
		x=(SLtt_Screen_Cols-dx)/2;
		y=(SLtt_Screen_Rows-dy)/2;
		
		SLsmg_set_color(object_color);
		SLsmg_gotorc(y+1+menu_selected[CurrentMenu],x+1);
		SLsmg_write_char(' ');
		SLsmg_write_nstring(menu_items[CurrentMenu][menu_selected[CurrentMenu]],dx-4);
		SLsmg_write_char(' ');
}

/* Fill in the entries in a menu. Set the color first. 
 * Calls SelMenuItem to select the menu item, as well. */
void FillMenu () {
	int c,x,y;

	x=(SLtt_Screen_Cols-dx)/2;
	y=(SLtt_Screen_Rows-dy)/2;

	for(c=0;c<menu_num[CurrentMenu];c++) {
		SLsmg_gotorc(y+c+1,x+2);
		SLsmg_write_nstring(menu_items[CurrentMenu][c],dx-4);
	}
  SelMenuItem(menu_selected[CurrentMenu],SELBAR);
}

/* Draw a menu. */
void DrawMenu () {
	int c;
	
	/* Determine the dimentions of this menu. It is centered onscreen, and 
	 * everything fits in it. */
	dy=menu_num[CurrentMenu];
	if (dy>SLtt_Screen_Rows-6) {
		dy=SLtt_Screen_Rows-6;
		mustscroll=1;
	}

	dx=strlen(menu_titles[CurrentMenu]);
	for(c=0;c<menu_num[CurrentMenu]-1;c++) { 
		if (dx<strlen(menu_items[CurrentMenu][c]))
			dx=strlen(menu_items[CurrentMenu][c]);
	}
	if (dx>SLtt_Screen_Cols-6)
		dx=SLtt_Screen_Cols-6;

	dx=dx+4;
	dy=dy+2;

	SLsmg_set_color(MENU);
	DrawDialog(menu_titles[CurrentMenu],(SLtt_Screen_Cols-dx)/2,(SLtt_Screen_Rows-dy)/2,dx,dy,1);
	FillMenu();
}

int main () {
	int key, ok=1;

  /* load menurc file */
  CurrentMenu=0;
	menu_titles[CurrentMenu]="Main Menu";
	menu_selected[CurrentMenu]=2;
	menu_num[CurrentMenu]=5;
	menu_items[CurrentMenu][0]="test 1";
	menu_items[CurrentMenu][1]="test 2";
	menu_items[CurrentMenu][2]="this right here is test 3";
	menu_items[CurrentMenu][3]="test 4";
	menu_items[CurrentMenu][4]="test 5";

	/* init terminal */
	SLtt_get_terminfo ();
	SLang_init_tty(7, 0, 0);
	SLsmg_init_smg();
		
	/* set object colors */
	SLtt_Use_Ansi_Colors=1;
	SLtt_set_color(DESKTOP,"",DESKTOP_FG,DESKTOP_BG);
	SLtt_set_color(BASE,"",BASE_FG,BASE_BG);
	SLtt_set_color(TITLE,"",TITLE_FG,TITLE_BG);
	SLtt_set_color(MENU,"",MENU_FG,MENU_BG);
  SLtt_set_color(SELBAR,"",SELBAR_FG,SELBAR_BG);
  SLtt_set_color(SHADOW,"",SHADOW_FG,SHADOW_BG);
	
	/* draw the screen */
	DrawTitle("Foremenu");
	DrawBase("Welcome to Foremenu 0.1 by Joey Hess <jeh22@cornell.edu>");
	DrawDesktop();
	DrawMenu();

	SLsmg_refresh();

	/* This is a really rotten, cheap way to do this. FIX!! */
	while (ok==1) {
		key=SLang_getkey();
		switch (key) {
			case 'A':
			case '8':
				SelMenuItem(menu_selected[CurrentMenu],MENU);
				if (--menu_selected[CurrentMenu]<0)
					menu_selected[CurrentMenu]=menu_num[CurrentMenu]-1;
				SelMenuItem(menu_selected[CurrentMenu],SELBAR);
				SLsmg_refresh();
				break;
			case 'B':
			case '2':
				SelMenuItem(menu_selected[CurrentMenu],MENU);
				if (++menu_selected[CurrentMenu]>=menu_num[CurrentMenu])
					menu_selected[CurrentMenu]=0;
				SelMenuItem(menu_selected[CurrentMenu],SELBAR);
				SLsmg_refresh();
				break;
			case 'Q':
			case 'q':
				ok=0;
				break;
		}
	}	

	/* reset terminal */
	SLsmg_gotorc(SLtt_Screen_Rows - 1, 0);
	SLsmg_refresh();
	SLsmg_reset_smg();
	SLang_reset_tty();
	
	return 0;
}
