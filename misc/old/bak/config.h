/*
** Menu limits 
*/

/* How many items can be in a menu? */
#define MAX_ITEMS_IN_MENU 75

/* How many menus and other windows can be showing at once? */
#define MAX_WINDOWS 10

/* How long can a item in one of the menus be? */
#define MENU_ITEM_TEXTSIZE 76


/* 
** Rc file 
*/

/* Maximum number of menu definitions that can be loaded at a time */
#define MAX_NUM_MENUS 20

/* Largest line length allowed in a rc file */
#define MAX_LINE_LEN 512

/* Seperates fields in the file */
#define FIELD_DELIM ':'

/* Put this before the character in a menu item that is to become the hotkey */
#define HOTKEY_SEL_CHAR '_'

/* The keywords in a rc file are.. */
#define NEW_MENU_KEYWORD "menu"
#define SHOW_MENU_KEYWORD "show"
#define EXEC_KEYWORD "exec"
#define EXIT_KEYWORD "exit"


/* 
** Strings 
*/

/* ~/.DEFAULTRC and /etc/DEFAULTRC are checked */
#define DEFAULTRC "/tmp/pdmenurc"

/* The default line at the base of the screen. */
#define DEFAULTBASE "Welcome to Pdmenu 0.3 by Joey Hess <jeh22@cornell.edu>"

/* A help line, displayed if a bad key is pressed */
#define HELPBASE "Arrow keys move, 'q' exits current menu, Enter runs program"

/* The title of the screen */
#define DEFAULTTITLE "Foremenu"
