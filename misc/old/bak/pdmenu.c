#include "pdmenu.h"

/* Draw the whole screen, with menus on it */
void DrawAll () {
	int c;

	SLsmg_cls();
	DrawTitle(DEFAULTTITLE);
	DrawDesktop();
	for (c=0;c<=NumOnScreen;c++) {
		DrawMenu(OnScreen[c]);
	}
	if (strlen(OnScreen[NumOnScreen]->helptext)>0)
		DrawBase(OnScreen[NumOnScreen]->helptext);
	else 
		DrawBase(DEFAULTBASE);
 	SLsmg_refresh(); 
}

/* Add a window to the list onscreen. */
void AddWindow (int n) {
	if (NumOnScreen==MAX_WINDOWS) {
		Screen_Reset();
		fprintf(stderr,"Tried to open too many menus at once. (Max %i)\n",MAX_WINDOWS);
		exit(1);
	}
	OnScreen[++NumOnScreen]=menus+n;
}

/* Remove the topmost window. Returns the number of the menu in array. */
int RemoveWindow () {
	return --NumOnScreen;
}

int main (int argc, char **argv) {
	int key, ok=0;
	int c;
	char command[255];
	Menu_Type *m;

	NumMenus=0;
	NumOnScreen=-1;

	/* SLang screen objects and their colors */
	DESKTOP=1; DESKTOP_FG="white"; DESKTOP_BG="blue";
	TITLE=2;   TITLE_FG="black";   TITLE_BG="red";
	BASE=3;    BASE_FG="black";    BASE_BG="red";
	MENU=4;    MENU_FG="black";    MENU_BG="cyan";
	SELBAR=5;  SELBAR_FG="cyan";   SELBAR_BG="black";
	SHADOW=6;  SHADOW_FG="white";  SHADOW_BG="black";
	MENU_HI=7; MENU_HI_FG="white"; MENU_HI_BG="cyan";
	SELBAR_HI=8; SELBAR_HI_FG="white"; SELBAR_HI_BG="black";
	DEFAULT=0;
	
	/* parse parameters */
	                                                                                                                                                                                                                                                               
  /* load pdmenurc file */
	if (ok==0) {
		ReadRc(DEFAULTRC);
	}

  Screen_Init();
	Screen_Setcolors();

	m=menus+0;
	AddWindow(0);	
	DrawAll();

	/* This is a really rotten, cheap way to do this. FIX!! */
	ok=1;
	while (ok==1) {
		key=SLang_getkey();
		switch (key) {
			case 'A':
			case '8':
				SelMenuItem(m,MENU,MENU_HI);
				if (--m->selected<0)
					m->selected=m->num-1;
				SelMenuItem(m,SELBAR,SELBAR_HI);
				SLsmg_refresh();
				break;
			case 'B':
			case '2':
				SelMenuItem(m,MENU,MENU_HI);
				if (++m->selected>=m->num)
					m->selected=0;
				SelMenuItem(m,SELBAR,SELBAR_HI);
				SLsmg_refresh();
				break;
			case 'Q':
			case 'q': /* back out a window or quit */
				if (NumOnScreen>0) {
					m=menus+RemoveWindow();
					DrawAll();
					break;
				}
			case '\3': /* ctrl-c */
				ok=0;
				break;
			case '\n':
			case '\r':
				if (m->items[m->selected].type==MENU_EXEC) {
					if (strchr(m->items[m->selected].flags,CLEAR_FLAG)!=NULL) {
						SLsmg_cls();
            SLsmg_set_color(DEFAULT);
						Screen_Reset();
					}
					strcpy(command,m->items[m->selected].command);
					c=system(command); 

					if (strchr(m->items[m->selected].flags,PAUSE_FLAG)!=NULL)
						{
							printf("\nPress Enter to return to Foremenu.");
							fflush(stdout); /* make sure above is displayed. */
							SLang_getkey();
							SLang_flush_input(); /* kill any buffered input */
						}
					if (strchr(m->items[m->selected].flags,CLEAR_FLAG)!=NULL) {
  					Screen_Init();
						DrawAll();
					}
				}
				else if (m->items[m->selected].type==MENU_SHOW) {
					/* find matching menu, if any */
					for (c=0;c<NumMenus;c++) {
						if (strcasecmp(m->items[m->selected].command,menus[c].name)==0) {
							/* got match -- show it */
							m=menus+c;
							AddWindow(c);
							DrawAll();
							break;
						}
					}
				}
				else if (m->items[m->selected].type==MENU_EXIT) {
					Screen_Reset();
					return 0;
				}
			default:
				/* Check to see if they hit a hotkey, if so warp to it. */
				c=m->selected+1;
				while (c!=m->selected) {
					if ((key==m->items[c].text[m->items[c].hotkey]) || (toupper(key)==m->items[c].text[m->items[c].hotkey])) {
						SelMenuItem(m,MENU,MENU_HI);
						m->selected=c;
						SelMenuItem(m,SELBAR,SELBAR_HI);
						SLsmg_refresh();
						break;
					}
					c++;
					if (c>m->num)
						c=0;
				}
		}
	}	
	Screen_Reset();
	return 0;                  
}
