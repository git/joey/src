#include <slang.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>

#include "config.h"

/* How many fields can possibly be in a rc file? */
#define NUM_FIELDS 3

typedef struct Menu_Item_Type {
	signed char hotkey;
	char text[MENU_ITEM_TEXTSIZE];
	char command[255];
	char flags[4];
	#define CLEAR_FLAG 'c'
	#define PAUSE_FLAG 'p'
	char type;
	#define MENU_EXEC 1
	#define MENU_SHOW 2
	#define MENU_EXIT 3
} Menu_Item_Type;
          
typedef struct Menu_Type {
	char name[32];
	char title[76];
	char helptext[80];
	Menu_Item_Type items[MAX_ITEMS_IN_MENU];
	int num;
	int selected;
	int x, y;
	int dx, dy;
	int recalc;
	char mustscroll;
} Menu_Type;

#include "screen.h"
#include "rc.h"

/* Global variable */
Menu_Type *OnScreen[MAX_WINDOWS];
signed int NumOnScreen;
Menu_Type menus[MAX_NUM_MENUS];
int NumMenus;

/*
void DrawAll (void);
void AddWindow (int);
int RemoveWindow (void);
int main (int,char **);
*/
