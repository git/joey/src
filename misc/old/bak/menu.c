#include <panel.h>
#include <ncurses.h>
#include <strings.h>

#define CENTER	1
#define LEFT		2
#define RIGHT		3 /*broken*/

char *MENU_TITLE="Foremenu";
char TITLE_ALIGN=CENTER;

static void fill_box(WINDOW *mywin, int x,int y, int h, int w)
{
	int dx,dy;
	for(dy=y; dy<y+h; dy++) {
		for(dx=x;dx<x+w;dx++) {
			mvwaddch(mywin,dx,dy,' ');
		}
	}
}

/*	Draw a standard window for Foremenu. Pass it the window to draw it on, and
		the title string. 
*/
drawwindow (WINDOW *mywin, char *title, short fg_color, short bg_color) {
	char titlepos;
	short mycolor;

	init_pair(COLOR_RED,fg_color,bg_color);
	wattron(mywin,COLOR_PAIR(COLOR_RED));
	fill_box(mywin,mywin->_begx,mywin->_begy,5,5);
	
	switch (TITLE_ALIGN) { /* How to align the title */
		case CENTER:	
			titlepos=(mywin->_maxx - mywin->_begx)/2 + (mywin->_begx - strlen(title))/2;
			break;
		case LEFT:
			titlepos=2;
			break;
		case RIGHT:
			titlepos=mywin->_begx + mywin->_maxx - strlen(title) - 3;
			break;
	}

	box(mywin,0,0);
	mvwprintw(mywin,0,titlepos,title);
}

init (void) {
	initscr(); /* start up curses */
	start_color(); /* yep, we want color */
	cbreak(); /* disable keypress buffering */
	drawwindow(stdscr, MENU_TITLE, COLOR_YELLOW, COLOR_BLUE); /* draw full screen window */
}

main (void) {
	WINDOW *menuwin;
	WINDOW *w2;
	PANEL *menupanel;
	PANEL *p2;

	init();

	menuwin=newwin(LINES-4,COLS-40,2,15);
	menupanel=new_panel(menuwin);
	drawwindow(menuwin,"Test menu",COLOR_WHITE, COLOR_YELLOW);

  w2=newwin(LINES-10,COLS-30,3,12);
  p2=new_panel(w2);
  drawwindow(w2,"Test menu 2",COLOR_WHITE, COLOR_GREEN);
      
	update_panels();
	refresh();

	while (1) {
		getch();
	}
}
