VERSION 2.00
Begin Form config 
   BorderStyle     =   3  'Fixed Double
   Caption         =   "Configure AutoNet"
   ClientHeight    =   3285
   ClientLeft      =   1500
   ClientTop       =   1590
   ClientWidth     =   5415
   Height          =   3690
   Left            =   1440
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3285
   ScaleWidth      =   5415
   Top             =   1245
   Width           =   5535
   Begin CommandButton Command2 
      Caption         =   "OK"
      Height          =   375
      Left            =   4080
      TabIndex        =   2
      Top             =   60
      Width           =   1275
   End
   Begin CommandButton Command1 
      Caption         =   "Cancel"
      Default         =   -1  'True
      Height          =   375
      Left            =   4080
      TabIndex        =   1
      Top             =   480
      Width           =   1275
   End
   Begin ListBox List 
      Enabled         =   0   'False
      Height          =   1395
      Left            =   60
      TabIndex        =   0
      Top             =   60
      Width           =   3915
   End
End
