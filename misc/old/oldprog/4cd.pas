uses dos,crt,screen,FourDos;

var
	DirList                             :array [1..1000] of string[12];
	NumDirs,BoxHeight,ScrollPos,ListPos :integer;
	ThumbPos                            :real;
	NoDot,MultiPick,done,scroll         :boolean;
	QuickChange												  :string[12];

procedure ParseHelp;
begin
	NoDot:=false;
	MultiPick:=false;
	if paramcount>0 then
	begin
		for ListPos:=1 to ParamCount do
		begin
			if paramstr(ListPos)='/?' then
			begin
				writeln;
				writeln('4CD -- Written by Joey Hess');
				writeln;
				writeln('Displays a list of directories for quick and easy directory changing.');
				writeln('Has same look and feel as the 4DOS history list.');
				writeln;
				writeln('Syntax: 4CD [/?] [/D] [/C]');
				writeln;
				writeln('/? Display this help screen.');
				writeln('/D Do not include ".." directory in list. (Included by default.)');
				writeln('/C Continue changing directories until Esc pressed. (Default is to exit');
				writeln('   after changing once.)');
				halt;
			end;
			if (paramstr(ListPos)='/D') or (paramstr(ListPos)='/d') then
				NoDot:=true;
			if (paramstr(ListPos)='/C') or (paramstr(ListPos)='/c') then
				MultiPick:=true;
		end;
	end;
end;

procedure DrawQuickChange;
	{Type the quickchange variable on the bottom of the window.}
var
 x:integer;
begin
	GotoXY (51,BoxHeight+3);
	textColor(black);
	textBackGround(white);
	write (QuickChange);
	for x:=1 to 12-length(QuickChange) do
		write('�');
end;

procedure AddQuickChange (cha:char);
	{add passed variable to QuickChange, if there is a partial match, and
	 change the scroll postion the picked varaible.}
var
	x,x2:integer;

begin
	QuickChange:=QuickChange+upcase(cha);
	x2:=-1;
	for x:=1 to NumDirs-1 do
	begin
		if (pos(QuickChange,DirList[x])=1) and (x2=-1) then x2:=x;
	end;
	if x2=-1 then
	begin
		{this change didn't match anything.}
		QuickChange:=copy(QuickChange,1,length(QuickChange)-1);
	end
	else
	begin
		{move the selection bar}
		ListPos:=x2;
		ScrollPos:=x2-BoxHeight;
		while ScrollPos<0 do
			ScrollPos:=ScrollPos+1;
	end;
end;


procedure InsertSort (s: string);
	{sort the passed string into the DirList array}
	Var
		count: integer;
		s2: string;

	begin
		if NumDirs<>0 then
		begin
			{find insertation point.}
			DirList[NumDirs]:=chr(255);
			s2:='';
			for count:=1 to NumDirs do
				begin
					if DirList[count]>s then
						begin
							{Insert string at insertation point, and move all things after it down by one.
							The beauty of this is that you don't have to do anything to move them down.
							After the passed string is sorted in, the string it overwrites is assined to
							"s", and it is inserted--always in the next postion down. This continues all
							the way down the list after the initial insertation. Cool, no?}
							s2:=DirList[count];
							DirList[count]:=s;
							s:=s2;
						end;
				end;
		end
		else
		begin
			DirList[1]:=s;
		end
	end;

procedure GetSubDirs;
  {get all subdirectoris on current directory, save in array.}
	var
		DirInfo: SearchRec;
		attrib: string;
	begin
		NumDirs:=1;
		DosError:=0;
		FindFirst('*.*', Anyfile, DirInfo);
		while DosError = 0 do
		begin
			if (dirinfo.attr and 16=16) and (dirinfo.name<>'.') and ((NoDot=false) or (dirinfo.name<>'..')) then
				begin
					InsertSort (DirInfo.name);
					NumDirs:=NumDirs+1;
				end;
    	FindNext(DirInfo);
	  end;
	end;

var
  initX,initY:integer;

Procedure ScrollBox(x,y,ListPos,ScrollPos:integer;Scroll:boolean);
  {Draw the array in the box, starting at the coodinates given
	(all array elements are assumed to fit in the box.)
	Also draw the selection bar and the scroll bar "thumb".}

  var
		count,c:integer;
	begin
		if Scroll=true then
			begin;
				GotoXY(x+27,y+round(ThumbPos)+1);
				textColor(lightgray);
				textBackGround(black);
				write(chr(178));
				{draw thumb}
				ThumbPos:=(ListPos / NumDirs)*(BoxHeight-3);
				GotoXY(x+27,y+round(ThumbPos)+1);
				textColor(white);
				textBackGround(black);
				write(' ');
			end;
		{draw contents of box}
		for count:=1 to BoxHeight do
		begin
			if ListPos=count+ScrollPos then
				begin
					textColor(white);
					textBackGround(black);
				end
			else
				begin
					textColor(black);
					textBackGround(white);
				end;
			GotoXY(x,y+count-1);
			Write(DirList[count+ScrollPos]);
			for  c:=length(DirList[count+ScrollPos]) to 26 do
				write (' ');
		end;
  end;

procedure AcceptInput;
	{Move the bar in the box, accept all input here!}

	var
		cha: char;
		Typed: string;

	begin
		ThumbPos:=1;
		cha:=#0;
		while (cha<>chr(27)) and (cha<>chr(13)) do
			begin
				ScrollBox(51,3,ListPos,ScrollPos,Scroll);
				cha:=readKey;
				case cha of
				#0:
					begin {extended keys}
						QuickChange:='';
						cha:=ReadKey;
						case cha of
						'P':
							begin {scroll down}
								if ListPos+2<=NumDirs then
									begin
										ListPos:=ListPos+1;
										if ListPos>BoxHeight+ScrollPos then
											ScrollPos:=ScrollPos+1;
									end;
							end;
						'H':
							begin {scroll up}
								if ListPos-1>0 then
									begin;
										ListPos:=ListPos-1;
										if ListPos=ScrollPos then
											ScrollPos:=ScrollPos-1;
									end;
							end;
						'Q':
							begin {page down}
								ListPos:=ListPos+BoxHeight;
								while ListPos>=NumDirs do
									ListPos:=ListPos-1;
								ScrollPos:=ListPos-BoxHeight;
								while BoxHeight+ScrollPos>=NumDirs do
									ScrollPos:=ScrollPos-1;
							end;
						'I':
							begin {page up}
								ListPos:=ListPos-BoxHeight;
								while ListPos<1 do
									ListPos:=ListPos+1;
								ScrollPos:=ScrollPos-BoxHeight;
								while ScrollPos<0 do
									ScrollPos:=ScrollPos+1;
							end;
						'O':
							begin {go to end}
								ListPos:=NumDirs-1;
								ScrollPos:=Numdirs-boxheight-1;
              end;
						'G':
							begin {go to home}
								ListPos:=1;
								ScrollPos:=0;
              end;
						end;
						cha:=#0;
					end;
				chr(13): {change directory}
					begin
						chdir(DirList[ListPos]);
					end;
				chr(27): {exit for good}
					begin
						done:=true;
					end;
				#8: {delete part of QuickChange}
					begin
						QuickChange:=copy(QuickChange,0,length(QuickChange)-1);
					end;
				else  {edit QuickChange variable}
					begin
						AddQuickChange (cha);
					end;
				end;
				DrawQuickChange;
			end;
	end;

var
	OldCursor: word;

begin
	initX:=WhereX;
	initY:=WhereY;
	ParseHelp;
	done:=false;
	while not done do
	begin
		QuickChange:='';
		ListPos:=1;
		ScrollPos:=0;
		Scroll:=false;
		GetSubDirs;
		if NumDirs>1 then
		begin
			PageCopy (0,4);
			CHide(OldCursor);
			BoxHeight:=NumDirs;
			if BoxHeight>15 then
				begin
				BoxHeight:=15;
					Scroll:=true;
				end
			else
				begin
					BoxHeight:=BoxHeight-1;
				end;
			FourDosBox (50,2,78,3+BoxHeight,'Change Directory',Scroll);
			AcceptInput;
			if done=false then Done:=not MultiPick;
			if not done then GetSubDirs;
			Pagecopy(4,0);
		end
		else
		begin
			done:=true;
		end;
	end;
	GotoXY(initX,initY);
	Cshow (OldCursor);
end.

