VERSION 2.00
Begin Form Form1 
   BackColor       =   &H0000FFFF&
   BorderStyle     =   3  'Fixed Double
   Caption         =   "Pick and Run - "
   Height          =   4260
   Left            =   615
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   ScaleHeight     =   3825
   ScaleWidth      =   6675
   Top             =   1650
   Width           =   6825
   Begin TextBox combo1 
      Height          =   285
      Left            =   135
      TabIndex        =   8
      Top             =   3405
      Width           =   4725
   End
   Begin TextBox Text1 
      BackColor       =   &H0000FF00&
      ForeColor       =   &H00000000&
      Height          =   285
      Left            =   165
      TabIndex        =   4
      Text            =   "Text1"
      Top             =   2775
      Width           =   2295
   End
   Begin DriveListBox Drive1 
      BackColor       =   &H0000FF00&
      Height          =   315
      Left            =   2535
      TabIndex        =   5
      Top             =   2745
      Width           =   2310
   End
   Begin CommandButton Command2 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   1200
      Left            =   4980
      TabIndex        =   3
      Top             =   1425
      Width           =   1560
   End
   Begin CommandButton Command1 
      Caption         =   "OK"
      Height          =   1200
      Left            =   4980
      TabIndex        =   2
      Top             =   135
      Width           =   1560
   End
   Begin FileListBox File1 
      BackColor       =   &H0000FF00&
      Height          =   2370
      Left            =   135
      TabIndex        =   0
      Top             =   120
      Width           =   2310
   End
   Begin DirListBox Dir1 
      BackColor       =   &H0000FF00&
      Height          =   2505
      Left            =   2535
      TabIndex        =   1
      Top             =   135
      Width           =   2295
   End
   Begin Label Label2 
      BackStyle       =   0  'Transparent
      Caption         =   "&File Type:"
      Height          =   210
      Left            =   165
      TabIndex        =   7
      Top             =   2520
      Width           =   900
   End
   Begin Label Label1 
      BackColor       =   &H0000FFFF&
      BackStyle       =   0  'Transparent
      Caption         =   "&Command Line:"
      Height          =   195
      Left            =   165
      TabIndex        =   6
      Top             =   3150
      Width           =   1365
   End
End
Dim Shared selectedcolor As Long, defaultdir As String, progfile As String
Dim Shared seltype As Integer, params As String, TextforeColor As Long, filepos As Integer

Sub combo1_GotFocus ()
  If combo1.BackColor <> selectedcolor Then combo1.BackColor = selectedcolor
End Sub

Sub Combo1_KeyPress (keyascii As Integer)
  If keyascii = 13 Then
    command1_click
  End If
End Sub

Sub combo1_LostFocus ()
  If combo1.BackColor <> ForeColor Then combo1.BackColor = ForeColor
End Sub

Sub command1_click ()
  updatecombo
  'change to the correct directory.
  ChDir dir1.Path
  'execute the command line
  b$ = combo1.Text
  a$ = cutnext(b$, False)
  r = Shell(b$, 1)
  End
End Sub

Sub Command2_Click ()
  End
End Sub

Function cutnext (a$, message)
  If (a$ = "" Or a$ = " ") And message = True Then
    MsgBox "LASTFILE selecttype newfileisafterwhatparam forecolor backcolor selectedcolor textforecolor progfilename defaultdir filespec defaultparams...", 64, "Syntax for LastFile"
    End
  End If
  cutnext = Left(a$ + " ", InStr(a$, " ") - 1)
  a$ = Trim(Mid(a$ + " ", InStr(a$, " ") + 1, 1000)) + " "
End Function

Sub Dir1_Change ()
  file1.Path = dir1.Path
  updatecombo
End Sub

Sub Dir1_GotFocus ()
  If dir1.BackColor <> selectedcolor Then dir1.BackColor = selectedcolor
End Sub

Sub Dir1_KeyPress (keyascii As Integer)
  If keyascii = 13 Or keyascii = 32 Then
    dir1.Path = dir1.List(dir1.ListIndex)
    If keyascii = 13 Then command1_click
  End If
End Sub

Sub Dir1_LostFocus ()
  If dir1.BackColor <> ForeColor Then dir1.BackColor = ForeColor
End Sub

Sub Drive1_Change ()
  a$ = dir1.Path
  On Error GoTo trap
  dir1.Path = drive1.Drive
Exit Sub
trap:
  drive1.Drive = a$
  Resume Next
End Sub

Sub Drive1_GotFocus ()
  If drive1.BackColor <> selectedcolor Then drive1.BackColor = selectedcolor
End Sub

Sub Drive1_LostFocus ()
  If drive1.BackColor <> ForeColor Then drive1.BackColor = ForeColor
End Sub

Sub file1_click ()
  updatecombo
End Sub

Sub File1_DblClick ()
  file1_click
  command1_click
End Sub

Sub File1_GotFocus ()
  If file1.BackColor <> selectedcolor Then file1.BackColor = selectedcolor
End Sub

Sub File1_KeyPress (keyascii As Integer)
  If keyascii = 32 Or keyascii = 13 Then
    command1_click
  End If
End Sub

Sub File1_LostFocus ()
  If file1.BackColor <> ForeColor Then file1.BackColor = ForeColor
End Sub

Sub Form_Load ()
  'syntax:
  left = (screen.Width - Width) / 2
  top = (screen.Height - Height) / 2
  c$ = Trim(Command$) + " "
  seltype = cutnext(c$, True)
  filepos = Int(cutnext(c$, True))
  ForeColor = QBColor(Int(cutnext(c$, True)))
  BackColor = QBColor(Int(cutnext(c$, True)))
  selectedcolor = QBColor(Int(cutnext(c$, True)))
  TextforeColor = QBColor(Int(cutnext(c$, True)))
  progfile = cutnext(c$, True)
  defaultdir = cutnext(c$, True)
  dir1.Path = defaultdir
  a$ = cutnext(c$, True)
  text1.Text = a$
  params = c$
  file1.BackColor = ForeColor
  dir1.BackColor = ForeColor
  combo1.BackColor = ForeColor
  text1.BackColor = ForeColor
  drive1.BackColor = ForeColor
  file1.ForeColor = TextforeColor
  dir1.ForeColor = TextforeColor
  combo1.ForeColor = TextforeColor
  text1.ForeColor = TextforeColor
  drive1.ForeColor = TextforeColor
  command1.BackColor = BackColor
  command2.BackColor = BackColor
  If seltype = 1 Then 'select dir only
    file1.Enabled = False
    text1.Enabled = False
    caption = caption + "Select Directory"
  Else
    caption = caption + "Select File"
  End If
  updatecombo
End Sub

Sub Text1_Change ()
  On Error GoTo trap2
  a$ = file1.Pattern
  file1.Pattern = text1.Text
Exit Sub
trap2:
  text1.Text = a$
End Sub

Sub Text1_GotFocus ()
  If text1.BackColor <> selectedcolor Then text1.BackColor = selectedcolor
End Sub

Sub Text1_LostFocus ()
  If text1.BackColor <> ForeColor Then text1.BackColor = ForeColor
End Sub

Sub updatecombo ()
  'add the file to the programs list of params in the correct place.
  a$ = params
  If filepos = 0 Then b$ = file1.FileName + " "
  Do While a$ <> " " And a$ <> ""
    X = X + 1
    c$ = cutnext(a$, False)
    If X = filepos Then b$ = b$ + file1.FileName + " "
    b$ = b$ + c$ + " "
  Loop
  combo1.Text = Trim(dir1.Path + " " + progfile + " " + b$)
End Sub

