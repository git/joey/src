'Insult mailer.

'like insult.bas, but this one asks for name, email, and a friends email, and
'insults the friend by mail. Companion program insend.bas sends a batch of insults
'using wsbeeper.exe

'IF YOU USE THIS PROGRAM, YOU _MUST_
'include a link on your web page to <A HREF = "http://kite.resnet.cornell.edu/">here</A>
'You also must contact me by e-mail and tell me you
'are using the program.
'
'Aside from that, you may freely distribute and modify this
'program in any way.

Dim Shared filelocation$, LinkBack$
Const noone = "oh bashful one" 'in case they reload, and don't specify a name.

'Returns a shakespearean insult.
'Reads the insult.ini file, which contains the path to the 3 insult keyword files,
'then reads in 3 keywords, and puts then together
'
Function Insult$ ()
  f = FreeFile
  Randomize Timer
  For x = 1 To 3
    Open filelocation$ + "insult." + Chr$(x + 48) For Input As f
    Line Input #f, a$ 'file desc line - junk
    Input #f, a$ 'number of lines after tihs one
    For y = 1 To Int(Rnd * Val(a$)) + 1
      Input #f, b$
      t = DoEvents()
    Next y
    i$ = i$ + b$ + " "
    Close #f
  Next x
  Insult$ = Trim$(i$)
End Function

Sub LoadInsultINI ()
  f = FreeFile
  Open AddTrailingSlash((app.Path)) + "insult.ini" For Input As f
  Input #f, filelocation$
  filelocation$ = AddTrailingSlash(filelocation$)
  Line Input #f, LinkBack$
  Close #f
End Sub

Function LoadLastUser$ ()
  f = FreeFile
  Open "c:\httpd\cgi-win\lastuser.dat" For Input As f
  Input #f, a$
  LoadLastUser$ = a$
  Close #f
End Function

Sub Main ()
  LoadInsultINI
  Call InitCGI(MimeType$)                         'init CGI
  n$ = Trim$(ReadInput("name", noone))            'read name typed in.
  n2$ = Trim$(ReadInput("name2", noone))          'friends name
  e$ = LCase$(Trim$(ReadInput("email", "")))      'their email address.
  e2$ = LCase$(Trim$(ReadInput("email2", "")))    'address to send to.
  If e$ <> "jeh22@cornell.edu" And e2$ <> "jeh22@cornell.edu" And n$ <> "" And n2$ <> "" And e$ <> e2$ And InStr(e$, "@") > 0 And InStr(e2$, "@") > 0 Then
    q$ = Chr$(34)
    sentance$ = n$ + " said, " + q$ + "Thou art a " + Insult() + ", " + n2$ + q$ + "!"'create insult.
    Call SaveLastUser(n$)
    Call PrintHTMLHeader("Insults by Mail", sentance$)'print header and insult
    Print #cgioutputfile, "Will appear in your friend's mailbox within the next 24 hours.<P>"
    Print #cgioutputfile, "<HR>"                    'insert horizontal line
    Print #cgioutputfile, LinkBack$                 'link back to your homepage.
    Print #cgioutputfile, "<HTML>"                  'end of document
    QueMail sentance$, n$, n2$, e$, e2$
  Else
    Call PrintHTMLHeader("Bad data", "You left out something, or tried to fool me. No insult was sent.")
  End If
  EndCGI                                          'close file and end CGI
  End
End Sub

'Save mail in the queue to be handled by Insend
'
Sub QueMail (s$, n$, n2$, e$, e2$)
  f = FreeFile
  Open "c:\httpd\cgi-win\insult.que" For Append As #f
    Print #f, s$
    Print #f, n$
    Print #f, n2$
    Print #f, e$
    Print #f, e2$
  Close #f
End Sub

Sub SaveLastUser (n$)
  n$ = Trim(n$)
  If n$ <> "" And n$ <> noone Then
    f = FreeFile
    Open "c:\httpd\cgi-win\lastuser.dat" For Output As f
    Print #f, CapitalizeFirstChar(n$)
    Close #f
    Open "c:\httpd\cgi-win\insulted.dat" For Append As #f
    Print #f, n$
    Close #f
  End If
End Sub

