'Insult generator for HTTPD.
'This is a demo of the CGIWIN.bas file plus a nice insult
'generator.

'IF YOU USE THIS PROGRAM, YOU _MUST_
'include a link on your web page to <A HREF = "http://kite.resnet.cornell.edu/">here</A>
'You also must contact me by e-mail and tell me you
'are using the program.
'
'Aside from that, you may freely distribute and modify this
'program in any way.

Dim Shared filelocation$, LinkBack$
Const noone = "oh bashful one" 'in case they reload, and don't specify a name.

'Returns a shakespearean insult.
'Reads the insult.ini file, which contains the path to the 3 insult keyword files,
'then reads in 3 keywords, and puts then together
'
Function Insult$ ()
  f = FreeFile
  Randomize Timer
  For x = 1 To 3
    Open filelocation$ + "insult." + Chr$(x + 48) For Input As f
    Line Input #f, a$ 'file desc line - junk
    Input #f, a$ 'number of lines after tihs one
    For y = 1 To Int(Rnd * Val(a$)) + 1
      Input #f, b$
      t = DoEvents()
    Next y
    i$ = i$ + b$ + " "
    Close #f
  Next x
  Insult$ = Trim$(i$)
End Function

Sub LoadInsultINI ()
  f = FreeFile
  Open AddTrailingSlash((app.Path)) + "insult.ini" For Input As f
  Input #f, filelocation$
  filelocation$ = AddTrailingSlash(filelocation$)
  Line Input #f, LinkBack$
  Close #f
End Sub

Function LoadLastUser$ ()
  f = FreeFile
  Open "c:\httpd\cgi-win\lastuser.dat" For Input As f
  Input #f, a$
  LoadLastUser$ = a$
  Close #f
End Function

Sub Main ()
  LoadInsultINI
  lastuser = LoadLastUser()
  Call InitCGI(MimeType$)                         'init CGI
  n$ = ReadInput("name", noone)                   'read name typed in.
  q$ = Chr$(34)
  sentance$ = lastuser + " just said, " + q$ + "Thou art a " + Insult() + ", " + Trim(n$) + q$ + "!" 'create insult.
  Call SaveLastUser(n$)
  Call PrintHTMLHeader("Did you hear?", sentance$)'print header and insult
  Print #cgioutputfile, "<HR>"                    'insert horizontal line
  Print #cgioutputfile, LinkBack$                 'link back to your homepage.
  Print #cgioutputfile, "<HTML>"                  'end of document
  EndCGI                                          'close file and end CGI
  End
End Sub

Sub SaveLastUser (n$)
  n$ = Trim(n$)
  If n$ <> "" And n$ <> noone Then
    f = FreeFile
    Open "c:\httpd\cgi-win\lastuser.dat" For Output As f
    Print #f, CapitalizeFirstChar(n$)
    Close #f
    Open "c:\httpd\cgi-win\insulted.dat" For Append As #f
    Print #f, n$
    Close #f
  End If
End Sub

