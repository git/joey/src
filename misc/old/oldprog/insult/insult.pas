uses strstuff;
var
	i:text;
	s:string;
	y,x,rand:integer;
	insult:string;
begin
	if (paramcount=0) or (paramstr(1)='/h') or (paramstr(1)='H') then
		begin
			Write('Syntax: INSULT insult.1 insult.2 insult.3 outfile'+
							'This is a custom .sig file creator, it reads data from the three files,'+
							'insult.1, insult.2, and insult.3, (containing shacespearean insults),');
			writeln(
							'creates a new insult (prefaced with "Thou"), and writes it to outfile'+
							'So, compile it, thou mangy wormeaten codpeice!');
			halt;
		end;
	write('Creating the daily insult...');
  insult:='Thou ';
  randomize;
	for x:=1 to 3 do
		begin
			assign(i,paramstr(x));
			reset(i);
			{read desc line.}
			readln(i,s);
			{read num of lines.}
			readln(i,s);
			rand:=random(cint(s)-1)+1;
			for y:=1 to rand do
				begin
					readln(i,s);
				end;
			insult:=insult+s+' ';
		end;
	insult:=trim(insult)+'.';
	writeln(insult);
  close(i);
  assign(i,paramstr(4));
  rewrite(i);
  writeln(i,insult);
  close(i);
end.