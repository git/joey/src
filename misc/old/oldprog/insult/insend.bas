'INSEND - Insult batch mailer program. Reads from
'c:\httpd\cgi-win\insult.que, and uses wsbeeper to
'send mail.

DefInt A-Z
Declare Function GetModuleUsage% Lib "Kernel" (ByVal hModule%)

Sub Main ()
  f = FreeFile
  On Error GoTo he
  f = FreeFile
  FileCopy "c:\httpd\cgi-win\insult.que", "c:\tmp\insult.que"
  Kill "c:\httpd\cgi-win\insult.que"
  Open "c:\tmp\insult.que" For Input As #f
  Do Until EOF(f)
    Line Input #f, i$
    Line Input #f, n$
    Line Input #f, n2$
    Line Input #f, e$
    Line Input #f, e2$
    f2 = FreeFile
    Open "c:\tmp\~insult.txt" For Output As #f2
    Print #f2, i$
    Print #f2, ""
    Print #f2, "(" + n$ + "'s email address is " + e$ + ". PLEASE SEND"
    Print #f2, "REPLIES TO THAT ADDRESS, DON'T JUST REPLY TO THIS MESSAGE.)"
    Print #f2, ""
    Print #f2, "-- This notice is a service of KITE. On the world wide"
    Print #f2, "   web at http://kite.resnet.cornell.edu/index.htm     --"
    Close #f2
    a = Shell("c:\httpd\cgi-win\wsbeeper.exe insult " + e2$, 6)
    While GetModuleUsage(a) > 0    ' Has Shelled program finished?
      j = DoEvents()              ' If not, yield to Windows.
    Wend
  Loop
  Close #f2
q:
  End
he:
  Resume q
End Sub

