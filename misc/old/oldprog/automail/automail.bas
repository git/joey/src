'Automail ver 1.0
'By Joey Hess

'Uses the abra library

DefInt A-Z

Declare Function GetModuleUsage% Lib "Kernel" (ByVal hModule%)
Declare Function GetPrivateProfileString Lib "Kernel" (ByVal lpApplicationName As String, ByVal lpKeyName As Any, ByVal lpDefault As String, ByVal lpReturnedString As String, ByVal nSize As Integer, ByVal lpFilename As String) As Integer
Declare Function writePrivateProfileString Lib "Kernel" (ByVal lpApplicationName As String, lpKeyName As Any, lpString As Any, ByVal lplFileName As String) As Integer

Global Const backslash = "\"

Global appath$, FileToRead$, MessageInitFlag$, beeperloc$, Logfile$, ErrorReporting
Global currentmessage$(), NumLines, LastMessageprocessed$


Global from$

Dim Shared MailRulesheader$(), MailRulesvalue$(), MailRulescommand$(), NumRules

'Adds the backslash in a filname, if its not present.
'
Function AddTrailingSlash$ (a$)
  If Right$(a$, 1) <> backslash Then
    AddTrailingSlash$ = a$ + backslash
  Else
    AddTrailingSlash$ = a$
  End If
End Function

'Compare 2 dates
'
'return true if a$ is later than b$
'
Function DateCompare (a$, b$)
  If b$ = "" Then
    DateCompare = True
  Else
    at = TimeValue(a$)
    ad = DateValue(CVDate((Mid$(a$, 18, 7) + Mid$(a$, 18 + 7 + 16, 4))))
    bt = TimeValue(a$)
    bd = DateValue(CVDate((Mid$(b$, 18, 7) + Mid$(b$, 18 + 7 + 16, 4))))
  End If
End Function

'Display an error and exit program.
'
Sub endwitherror (a$)
  LogError a$ + " - Fatal Error - Aborting."
  MsgBox a$, 48
  n = writePrivateProfileString("automail", "LastMessageProcessed", LastMessageprocessed$, appath$ + "automail.ini")
  End
End Sub

'Read INI File and .CFG file
'
Sub Init ()
  appath$ = AddTrailingSlash((app.Path))
  f = FreeFile
  'On Error GoTo no_cfg
  Open appath$ + "automail.cfg" For Input As #f
    Do Until EOF(f)
      ReDim Preserve MailRulesheader$(1 To NumRules + 1)
      ReDim Preserve MailRulesvalue$(1 To NumRules + 1)
      ReDim Preserve MailRulescommand$(1 To NumRules + 1)
      If Not EOF(f) Then Line Input #f, a$
      MailRulesheader$(NumRules + 1) = LCase$(a$)
      If Not EOF(f) Then Line Input #f, a$
      MailRulesvalue$(NumRules + 1) = LCase$(a$)
      If Not EOF(f) Then
        Line Input #f, a$
        NumRules = NumRules + 1
      End If
      MailRulescommand$(NumRules) = a$
    Loop
  Close #f
  If NumRules = 0 Then endwitherror "No rules found. Exiting."
  LastMessageprocessed$ = LoadINI(appath$ + "automail.ini", "automail", "LastProcessed", "")
  ErrorReporting = Val(LoadINI(appath$ + "automail.ini", "automail", "ErrorReporting", ""))
  Logfile$ = LCase$(LoadINI(appath$ + "automail.ini", "automail", "LogFile", ""))
  beeperloc$ = LCase$(LoadINI(appath$ + "automail.ini", "automail", "WSBeeperLocation", "."))
  MessageInitFlag$ = LCase$(LoadINI(appath$ + "automail.ini", "automail", "MessageInitFlag", ""))
  FileToRead$ = LCase$(LoadINI(appath$ + "automail.ini", "automail", "FileToRead", ""))
  If FileToRead$ = "" Then endwitherror "No FileToRead specified in INI file. Exiting."
  If Right$(FileToRead$, 3) <> ".mbx" And InStr(FileToRead$, ".") = 0 Then FileToRead$ = FileToRead$ + ".mbx"
exit_init:
  Exit Sub
no_cfg:
  endwitherror "No automail.cfg file was found, or the file has an invalid format!"
  Resume exit_init
End Sub

'Load a line from the given INI file.
'
Function LoadINI$ (datafile$, header$, linename$, default$)
  Dim a As String * 254
  b = GetPrivateProfileString(header$, linename$, default$, a, 254, datafile$)
  m$ = Trim(a)
  LoadINI$ = Mid$(m$, 1, InStr(m$ + Chr$(0), Chr$(0)) - 1)
End Function

'Log an error to the logfile, if a logfile is specified and logging
'is on
'
Sub LogError (a$)
  If ErrorReporting = 1 And Trim$(Logfile$) <> "" Then
    f = FreeFile
    Open Logfile$ For Append As #f
      Print #f, Now + " " + a$
    Close #f
  End If
End Sub

Sub main ()
  Init
  'On Error GoTo CantOpenFile
  f = FreeFile
  NumLines = 0
  MaxNumLines = 300
  LastMessgeFound = (LastMessageprocessed = "")
  ReDim Preserve currentmessage$(MaxNumLines)
  Open FileToRead$ For Input As #f
    Do Until EOF(f)
      Line Input #f, a$
      NumLines = NumLines + 1
      If NumLines <= MaxNumLines Then
        currentmessage$(NumLines) = a$
      End If
      a$ = LCase$(a$)
      i = InStr(a$, "from:")
      If i > 0 Then
        from$ = Trim$(Mid$(a$, i + 5))
      End If
      i = DoEvents()
      If InStr(a$, MessageInitFlag$) > 0 Then 'end of message - process it all
        If Not LastMessageFound Then
          LastMessageFound = DateCompare(a$, LastMessageprocessed$)
        End If
        LastMessageprocessed$ = a$
        If LastMessageFound Then
          For y = 1 To NumLines
            If y <= MaxNumLines Then b$ = currentmessage$(y)
            a$ = LCase$(b$)
            For x = 1 To NumRules
              ii = DoEvents()
              If InStr(a$, MailRulesheader$(x)) > 0 Then
                'found a matching header - does a matching keyword follow? (wildcards allowed)
                If Mid$(a$, InStr(a$, MailRulesheader$(x)) + Len(MailRulesheader$(x)) + 1) Like MailRulesvalue$(x) Then
                  'found it - act.
                  ProcessCommand MailRulescommand$(x)
                End If
              End If
            Next x
          Next y
        End If
        NumLines = 0 'new message
        from$ = ""
        c = c + 1
        form1.Caption = form1.Tag + CStr(c)
      End If
      NumChars = NumChars + Len(a$) + 2
    Loop
  Close #f
done:
  n = writePrivateProfileString("automail", "LastMessageProcessed", LastMessageprocessed$, appath$ + "automail.ini")
  End
CantOpenFile:
  endwitherror "Can't read file " + FileToRead$ + "!"
  Resume done
End Sub

'Display an error, or log it.
'
Sub MiniError (a$)
  If ErrorReporting = 2 Then
    MsgBox a$, 48
  Else
    LogError a$
  End If
End Sub

'f$ is the alias to use in wsbeeper
't$ is who we're sending to.
'file$ is the file we're sending
'
Sub SendMail (f$, t$, file$)
  a = Shell(AddTrailingSlash(beeperloc$) + "wsbeeper.exe " + f$ + " " + t$ + " " + file$, 6)
  While GetModuleUsage(a) > 0   ' Has Shelled program finished?
    j = DoEvents()              ' If not, yield to Windows.
  Wend
End Sub

