
'Expands "environment variables"
'Currently handles:
'%from% -the email address the triggering message is from
'
Function Expand$ (a$)
  Expand$ = change(a$, "%from%", from$)
End Function

Function HandleError (a$)
  MiniError a$
End Function

'Process command c$
'
Function ProcessAppSpecificCommands (c$, p$)
  ProcessAppSpecificCommands = True
  k$ = c$
  If k$ <> "forward" Then file$ = nextword$(p$) Else file$ = appath$ + "~temp.tmp"
  t$ = nextword$(p$) '"TO" cut out here
  addressee$ = nextword$(p$) 'who to forward it to
  a$ = nextword$(p$) '"AS" cut out here
  ak$ = Trim$(p$)
  If file$ = "" Or t$ = "" Or addressee$ = "" Or a$ = "" Or ak$ = "" Then
    MiniError "Error parsing command:" + k$
  Else
    Select Case c$
    Case "forward"
      'forward the current message to someone.
      f = FreeFile
      Open file$ For Output As #f
        Print #f, "--FORWARDED MESSAGE--"
        For x = 1 To NumLines - 1
          Print #f, CurrentMessage$(x)
        Next x
      Close #f
      SendMail Expand$(ak$), Expand$(addressee$), file$
    Case "send"
      'send mail normally.
      SendMail Expand$(ak$), Expand$(addressee$), file$
    Case "smartsend"
      'send mail, expanding the variables as we go -this _requires_ that there be a file as specified.
      On Error Resume Next
      Kill appath$ + "~temp.tmp"
      On Error GoTo 0
      FileCopy file$, appath$ + "~temp.tmp"
      f = FreeFile
      Open appath$ + "~temp.tmp" For Input As #f
        f2 = FreeFile
        Open file$ For Output As #f2
          Do Until EOF(f)
            Line Input #f, a$
            Print #f2, Expand$(a$)
            ii = DoEvents()
          Loop
        Close #f2
      Close #f
      SendMail Expand$(ak$), Expand$(addressee$), file$
      FileCopy appath$ + "~temp.tmp", file$
      Kill appath$ + "~temp.tmp"
    End Select
  End If
End Function

'Run the passed winscript file.
'
Sub RunScript (a$)
  If waiting Then Exit Sub
  If Not Scriptfile(a$) Then a$ = a$ + ScriptExt 'make sure it ends with the correct .ext
  f = FreeFile
  Open a$ For Input As #f
  Do Until EOF(f)
    Line Input #f, b$
    ProcessCommand b$
  Loop
  Close #f
End Sub

