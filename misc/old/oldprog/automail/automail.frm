VERSION 2.00
Begin Form Form1 
   BackColor       =   &H0080FFFF&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "AutoMail - Init"
   ClientHeight    =   1920
   ClientLeft      =   4020
   ClientTop       =   1950
   ClientWidth     =   3225
   ControlBox      =   0   'False
   Height          =   2325
   Icon            =   AUTOMAIL.FRX:0000
   Left            =   3960
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   ScaleHeight     =   1920
   ScaleWidth      =   3225
   Tag             =   "AutoMail - Processing #"
   Top             =   1605
   Width           =   3345
   WindowState     =   1  'Minimized
   Begin Label Label3 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "Written By Joey Hess (jeh22@cornell.edu)"
      Height          =   435
      Left            =   180
      TabIndex        =   2
      Top             =   1140
      Width           =   2895
   End
   Begin Label Label2 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "Automatic Mail Reply and Forwarding"
      Height          =   255
      Left            =   0
      TabIndex        =   1
      Top             =   540
      Width           =   3195
   End
   Begin Label Label1 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "AutoMail"
      FontBold        =   0   'False
      FontItalic      =   0   'False
      FontName        =   "Arial"
      FontSize        =   18
      FontStrikethru  =   0   'False
      FontUnderline   =   0   'False
      ForeColor       =   &H00FF0000&
      Height          =   375
      Left            =   60
      TabIndex        =   0
      Top             =   60
      Width           =   3075
   End
End

Sub Form_Load ()
  Show
  main
End Sub

