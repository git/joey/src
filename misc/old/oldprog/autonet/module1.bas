Const NumItems = 4
Global cdto$(1 To NumItems), run$(1 To NumItems), param$(1 To NumItems)
Global url$

Function expandparam$ (ByVal param$)
  'url$ is of format xxx://aaa.aaaa.aaa/fil/fil/fil[:port]/
  'or xxx://aaa.aaa.aaa[:port]/
  b$ = url$
  
  'get address
  b$ = Mid$(Find("://", b$), 4)'aaa.aaa.aaa/a/a/a:port
  If InStr(b$, "/") > 0 Then
    adr$ = Mid$(b$, 1, InStr(b$, "/") - 1) 'aaa.aaa.aaa[:port]
  Else
    adr$ = b$ 'aaa.aaa.aaa[:port]
  End If
  If InStr(adr$, ":") > 0 Then adr$ = Mid$(adr$, 1, InStr(adr$, ":") - 1) 'aaa.aaa.aaa
  
  'get filename
  If InStr(b$, "/") > 0 Then
    fn$ = Mid$(b$, InStr(b$, "/")) '/a/a/a[:port]
    If InStr(fn$, ":") > 0 Then fn$ = Mid$(fn$, 1, InStr(fn$, ":") - 1) '/a/a/a
    If Right$(fn$, 1) = "/" Then fn$ = Mid$(fn$, 1, Len(fn$) - 1)
  Else
    fn$ = "" 'no filename
  End If
  

  'get port
  If InStr(b$, ":") > 0 Then
    port$ = Mid$(b$, InStr(b$, ":") + 1)
    If Right$(port$, 1) = "/" Then port$ = Mid$(port$, 1, Len(port$) - 1)
  Else
    port$ = ""'no port
  End If

  'do replacements
  'change:
  '~ to complete url,
  '@ to address
  '\ to filename
  '# to port
  n$ = Replace$(param$, "~", url$)
  n$ = Replace$(n$, "@", adr$)
  n$ = Replace$(n$, "\", fn$)
  n$ = Replace$(n$, "#", port$)
  expandparam$ = Trim(n$)
End Function

'find f$ in in$, return in$ after f$
'
Function Find$ (in$, f$)
  If InStr(f$, in$) > 0 Then
    Find$ = Mid$(f$, InStr(f$, in$))
  End If
End Function

Static Function INIFile$ ()
  If a$ = "" Then
    a$ = app.Path
    If Right$(a$, 1) <> "\" Then a$ = a$ + "\"
    a$ = a$ + "autonet.ini"
  End If
  INIFile$ = a$
End Function

Sub LoadINI ()
  Open INIFile$() For Input As #1
    autonet.List.Clear
    Do Until EOF(1)
      x = x + 1
      Line Input #1, a$
      Line Input #1, b$
      Line Input #1, c$
      Line Input #1, d$
      autonet.List.AddItem a$
      cdto$(x) = b$
      run$(x) = c$
      param$(x) = d$
    Loop
  Close #1
End Sub

'replace all occurences of b$ in a$ with c$
'
Function Replace$ (ByVal a$, b$, c$)
  i = InStr(a$, b$)
  Do Until i = 0
    If i > 1 Then f$ = f$ + Mid$(a$, 1, i - 1)
    a$ = Mid$(a$, i + Len(b$))
    f$ = f$ + c$
    i = InStr(a$, b$)
  Loop
  Replace$ = f$ + a$
End Function

Sub SaveINI ()
  Open INIFile$() For Output As #1
    For x = 1 To NumItems
      Print #1, autonet.List.List(x - 1)
      Print #1, cdto$(x)
      Print #1, run$(x)
      Print #1, param$(x)
    Next x
  Close #1
End Sub

