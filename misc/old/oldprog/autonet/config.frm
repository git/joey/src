VERSION 2.00
Begin Form config 
   BorderStyle     =   3  'Fixed Double
   Caption         =   "Configure AutoNet"
   ClientHeight    =   3870
   ClientLeft      =   1500
   ClientTop       =   1590
   ClientWidth     =   5415
   Height          =   4275
   Left            =   1440
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3870
   ScaleWidth      =   5415
   Top             =   1245
   Width           =   5535
   Begin TextBox Text3 
      Enabled         =   0   'False
      Height          =   285
      Left            =   60
      TabIndex        =   4
      Top             =   3420
      Width           =   5295
   End
   Begin TextBox Text2 
      Enabled         =   0   'False
      Height          =   285
      Left            =   60
      TabIndex        =   7
      Top             =   2400
      Width           =   5295
   End
   Begin TextBox Text1 
      Enabled         =   0   'False
      Height          =   285
      Left            =   60
      TabIndex        =   5
      Top             =   1800
      Width           =   5295
   End
   Begin CommandButton Command2 
      Caption         =   "OK"
      Height          =   375
      Left            =   4080
      TabIndex        =   2
      Top             =   60
      Width           =   1275
   End
   Begin CommandButton Command1 
      Caption         =   "Cancel"
      Default         =   -1  'True
      Height          =   375
      Left            =   4080
      TabIndex        =   1
      Top             =   480
      Width           =   1275
   End
   Begin ListBox List 
      Height          =   1395
      Left            =   60
      TabIndex        =   0
      Top             =   60
      Width           =   3915
   End
   Begin Label Label3 
      Caption         =   "&Parameters: (""~"" replaced by complete URL, ""@"" replaced by address, ""\"" replaced by filename, ""#"" replaced by port number.)"
      Height          =   675
      Left            =   60
      TabIndex        =   8
      Top             =   2820
      Width           =   5295
      WordWrap        =   -1  'True
   End
   Begin Label Label2 
      AutoSize        =   -1  'True
      Caption         =   "&Launch program:"
      Height          =   195
      Left            =   60
      TabIndex        =   6
      Top             =   2160
      Width           =   1440
   End
   Begin Label Label1 
      AutoSize        =   -1  'True
      Caption         =   "&Change to directory:"
      Height          =   195
      Left            =   60
      TabIndex        =   3
      Top             =   1560
      Width           =   1740
   End
End
Sub Command1_Click ()
  LoadINI
  Unload config
End Sub

Sub Command2_Click ()
  List_Click
  SaveINI
  Unload config
End Sub

Sub Form_Load ()
  left = (screen.Width - width) / 2
  top = (screen.Height - height) / 2
  If config.List.ListCount = 0 Then
    For x = 0 To autonet.List.ListCount - 1
      List.AddItem (autonet.List.List(x))
    Next x
  End If
End Sub

Static Sub List_Click ()
  i = List.ListIndex + 1
  If i > 0 Then
    If text1.Enabled = False Then
      text1.Enabled = True
      text2.Enabled = True
      text3.Enabled = True
    Else
      cdto$(oldi) = text1.Text
      run$(oldi) = text2.Text
      param$(oldi) = text3.Text
    End If
    text1.Text = cdto$(i)
    text2.Text = run$(i)
    text3.Text = param$(i)
    oldi = i
  End If
End Sub

