'''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'HTMBlast 1.11 - HTML signing, version control, and whatever
'else software.

'FREEWARE - distribute with this source code, and with
'the documentation, please.

'Report any modifications you make to this source code to
'jeh22@cornell.edu

'Syntax: HTMBlast filespec {/any}
' /any any file processed, even if archive bit not set.
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''

Option Compare Text
DefInt A-Z

Dim Shared tag$(1 To 200), replace$(1 To 200)
Dim Shared NumPairs

Dim Shared FileDate$, appath$, FileOpen$, filepath$
Dim Shared counter

'tag syntax: <!tag "tagname">...<!tag>
Dim Shared StartTag1$, StartTag2$, EOL$
Global Const StartTag = "<!tag ", EndTag = "<!endtag>", gt = ">"

'Global Const sep = "--", lt = "<", gt = ">", slash = "/", datetag = "date ", sp = " "
Global Const SlashAny = " /any", tempfile$ = "~temp.xxx"

Declare Function SetTime Lib "SETUPKIT.DLL" (ByVal a$, ByVal b$) As Integer

'Close the passed file, copy it to the file that originated it and restore the date.
'
Sub Closefile (f)
  Close #f
  'copy now.
  a = SetTime(FileOpen$, appath$ + tempfile$) 'how odd - set date of one file to another one - oh, well, it works...
  FileCopy appath$ + tempfile$, FileOpen$
  SetAttr FileOpen$, 32
End Sub

'f is the file handle of the file we are going to convert
'from, and n is the file to write to
Sub DoConversion (f, n)
  Const LookingForStartTag = 0, LookingForEndTag = 1 'current state.
  
  On Error GoTo DoConversion_error
  condition = LookingForStartTag
  Line Input #f, a$
  a$ = a$ + EOL$
  Do Until (EOF(f) And a$ = "" And condition = LookingForStartTag)
    If a$ = "" Then 'need a new line
      Print #n, newline$;
      newline$ = ""
      If Not EOF(f) Then Line Input #f, a$
      a$ = a$ + EOL$
    End If
    Select Case condition
    Case LookingForStartTag
      If InStr(a$, StartTag1) > 0 Then
	newline$ = newline$ + Mid$(a$, 1, InStr(a$, StartTag1) - 1) 'save the part of the line before the starttag.
	a$ = Mid$(a$, InStr(a$, StartTag1) + Len(StartTag1)) 'cut a$ to tag name.
	t$ = LCase$(Mid$(a$, 1, InStr(a$, StartTag2) - 1)) 'save the actal tag name.
	a$ = Mid$(a$, InStr(a$, StartTag2) + Len(StartTag2))
	condition = LookingForEndTag
      Else
	newline$ = newline$ + a$
	a$ = ""
      End If
    Case LookingForEndTag
      EndTagPos = InStr(a$, EndTag)
      If EndTagPos > 0 And (InStr(a$, StartTag1) = 0 Or EndTagPos < InStr(a$, StartTag1)) Then
	'found matching endtag
	l$ = LookUp$(t$)
	If l$ <> Chr$(3) Then
	  newline$ = newline$ + StartTag1$ + t$ + StartTag2 + l$ + EndTag
	Else
	  newline$ = newline$ + StartTag1$ + t$ + StartTag2 + between$ + Mid$(a$, 1, EndTagPos - 1) + EndTag
	End If
	a$ = Mid$(a$, EndTagPos + Len(EndTag))
	between$ = ""
	condition = LookingForStartTag
      ElseIf EndTagPos = 0 And EOF(f) Then
	ShowError "In " + UCase$(FileOpen) + " there is no matching end tag for " + Chr$(34) + t$ + Chr$(34), True
      ElseIf InStr(a$, StartTag1) > 0 Then
	'there is another start tag before an end tag - error.
	ShowError UCase$(FileOpen) + " has an imbalanced tag after " + Chr$(34) + t$ + Chr$(34), True
      Else
	between$ = between$ + a$
	a$ = ""
      End If
    End Select
    do_events = DoEvents()
  Loop
  Print #n, newline$
Exit Sub
DoConversion_error:
  ShowError UCase$(FileOpen) + " has invalid syntax, possibly around tag " + Chr$(34) + t$ + Chr$(34) + " or around " + Chr$(34) + a$ + Chr$(34), True
End Sub

Function GetRandomLine$ (fil$)
  'grab a random line from a file. The file must start with a line that
  'is a number and tells how many other lines are in the file.
  On Error GoTo RandomError
  f = FreeFile
  Open fil$ For Input As #f
  Input #f, a$
  n = Val(a$)
  If n > 0 Then
    num = Int(Rnd * n) + 1
    For x = 1 To num
      Input #f, a$
    Next x
    GetRandomLine$ = a$
  End If
  Exit Function
RandomError:
  Call ShowError("Error reading random file:" + fil$, True)
End Function

Sub KillTempFile ()
  If counter > 0 Then Kill appath$ + tempfile$
End Sub

Function LoadFileToString$ (f$)
  On Error GoTo NotFound
  i = FreeFile
  Open f$ For Input As #i
  Do Until EOF(i)
    Line Input #i, a$
    If Not EOF(i) Then a$ = a$ + EOL$
    ret$ = ret$ + a$
  Loop
  LoadFileToString$ = ret$
  Exit Function
NotFound:
  ShowError "Unable to load file, " + f$ + ".", True
End Function

'NOTES: A Tag cannot contain the characters "=" or "~"!!!
'There may be up to 200 definitions in the file.
Sub LoadTags ()
  On Error GoTo LoadTags_trap
  f = FreeFile
  Open appath$ + app.EXEName + ".tag" For Input As #f
  Do While Not EOF(f)
    Line Input #f, a$
    If InStr(a$, "=") > 0 And Left$(a$, 1) <> "#" And Left$(a$, 1) <> ";" Then
      NumPairs = NumPairs + 1
      tag$(NumPairs) = LCase$(Mid$(a$, 1, InStr(a$, "=") - 1))
      replace$(NumPairs) = Mid$(a$, InStr(a$, "=") + 1)
    End If
  Loop
  Close #f
Exit Sub
LoadTags_trap:
  MsgBox "Error loading tag file, " + app.EXEName + ".tag! Halting program."
  End
End Sub

'Look up the given tag t$, handle date specially
Function LookUp$ (t$)
  t$ = LCase$(t$)
  If Left$(t$, 7) = "random " Then
    LookUp$ = GetRandomLine$(Mid$(t$, 8))
  ElseIf Left$(t$, 4) = "date" Then
    'put in the date - if date is all, then in mm-dd-yyyy format, otherwise,
    'do a format$ to create your own format.
    If Len(t$) = 4 Then
      LookUp$ = FileDate$
    Else
      LookUp$ = Format$(FileDate$, Mid$(t$, 6))
    End If
  ElseIf Left$(t$, 5) = "file " Then
    'include the file
    '***the file _can't_ be bigger than max. string size - 64KB***
    LookUp$ = LoadFileToString(Mid$(t$, 6))
  Else
    LookUp$ = Chr$(3)
    done = False
    x = 1
    Do Until done Or x > NumPairs
      If t$ = tag$(x) Then
	LookUp$ = replace$(x)
	done = True
      End If
      x = x + 1
      do_events = DoEvents()
    Loop
  End If
End Function

Sub Main ()
  Randomize Timer
  worthless.Show
  appath$ = app.Path
  EOL$ = Chr$(13) + Chr$(10)
  StartTag1$ = StartTag + Chr$(34)
  StartTag2$ = Chr$(34) + gt
  If Right$(appath$, 1) <> "\" Then appath$ = appath$ + "\"
  LoadTags
  'process the specified file or wildcard.
  c$ = LCase$(Command$)
  If InStr(c$, SlashAny) Then
    anyok = True
    c$ = Mid$(c$, 1, InStr(c$, SlashAny) - 1) + Mid$(c$, InStr(c$, SlashAny) + Len(SlashAny))
  End If
  If InStr(c$, ":") = 2 Then
    filepath$ = Left$(c$, 2)
    c$ = Mid$(c$, 3)
  End If
  Do While InStr(c$, "\") > 0
    filepath$ = filepath$ + Left$(c$, InStr(c$, "\"))
    c$ = Mid$(c$, InStr(c$, "\") + 1)
  Loop
  If Right$(filepath$, 1) <> "\" Then filepath$ = filepath$ + "\"
  Select Case c$
  Case ""
    a$ = Dir$(filepath$ + "*.htm", 32)
  Case Else
    a$ = Dir$(filepath$ + c$, 0)
  End Select
  Do While a$ <> ""
    worthless.l2.Caption = a$
    If a$ <> tempfile$ And (anyok Or (GetAttr(filepath$ + a$) And 32) <> 0) Then
      Process a$
      counter = counter + 1
    End If
    a$ = Dir$
  Loop
  KillTempFile
  Unload worthless
End Sub

'Open the file, and save the date in FileDate$
'
Function OpenFile (File$)
  f = FreeFile
  FileOpen = filepath$ + File$
  FileDate$ = FileDateTime(FileOpen)
  Open FileOpen$ For Input As #f
  OpenFile = f
End Function

'convert f$
Sub Process (f$)
  o = OpenFile(f$)
  n = FreeFile
  Open appath + tempfile$ For Output As #n
  DoConversion o, n
  Close #n
  Closefile o
End Sub

Sub ShowError (text$, end_now)
  MsgBox text$, 16, "Error"
  If end_now Then
    Reset
    KillTempFile
    End
  End If
End Sub

