md temp
copy *.* temp
cd temp
del *.exe
del *.doc
del *.tag
del *.zip
pkzip htmcode.zip
cd ..
md distribute
del distribute\*.* /y
copy *.exe distribute
copy *.doc distribute
copy htmblast.doc c:\pub\prog\htmblast.txt
copy *.tag distribute
copy temp\htmcode.zip distribute
copy \os\win\system\setupkit.dll distribute
cd distribute
pkzip htmblast.zip
del ..\temp /y
except (htmblast.zip) del *.* /y
rd ..\temp
move *.zip \pub\prog\
cd ..
rd distribute