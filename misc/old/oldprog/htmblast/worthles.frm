VERSION 2.00
Begin Form Worthless 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "HMTBlast Processing..."
   ClientHeight    =   1860
   ClientLeft      =   2115
   ClientTop       =   2490
   ClientWidth     =   3795
   ControlBox      =   0   'False
   Height          =   2265
   Icon            =   WORTHLES.FRX:0000
   Left            =   2055
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   ScaleHeight     =   1860
   ScaleWidth      =   3795
   Top             =   2145
   Width           =   3915
   WindowState     =   1  'Minimized
   Begin Label l2 
      Height          =   195
      Left            =   30
      TabIndex        =   4
      Top             =   1620
      Width           =   3705
   End
   Begin Line Line1 
      Index           =   2
      X1              =   -15
      X2              =   3810
      Y1              =   1575
      Y2              =   1575
   End
   Begin Line Line1 
      Index           =   1
      X1              =   -30
      X2              =   3795
      Y1              =   600
      Y2              =   600
   End
   Begin Line Line1 
      Index           =   0
      X1              =   0
      X2              =   3825
      Y1              =   885
      Y2              =   885
   End
   Begin Label l 
      Alignment       =   2  'Center
      Caption         =   "Thanks to Randolph Chung, Bug Hunter extrordinair!"
      Height          =   465
      Index           =   3
      Left            =   135
      TabIndex        =   3
      Top             =   960
      Width           =   3600
   End
   Begin Label l 
      Alignment       =   2  'Center
      Caption         =   "*Freeware*"
      FontBold        =   -1  'True
      FontItalic      =   0   'False
      FontName        =   "MS Sans Serif"
      FontSize        =   9.75
      FontStrikethru  =   0   'False
      FontUnderline   =   0   'False
      ForeColor       =   &H000000FF&
      Height          =   270
      Index           =   2
      Left            =   120
      TabIndex        =   2
      Top             =   630
      Width           =   3600
   End
   Begin Label l 
      Alignment       =   2  'Center
      Caption         =   "By Joey Hess - Copyright 1994"
      Height          =   225
      Index           =   1
      Left            =   120
      TabIndex        =   1
      Top             =   375
      Width           =   3600
   End
   Begin Label l 
      Alignment       =   2  'Center
      Caption         =   "HTMBlast 1.11"
      FontBold        =   -1  'True
      FontItalic      =   0   'False
      FontName        =   "MS Sans Serif"
      FontSize        =   9.75
      FontStrikethru  =   0   'False
      FontUnderline   =   0   'False
      Height          =   225
      Index           =   0
      Left            =   135
      TabIndex        =   0
      Top             =   90
      Width           =   3600
   End
End

Sub Form_Load ()
  top = (screen.Height - Height) / 2
  left = (screen.Width - Width) / 2
End Sub

