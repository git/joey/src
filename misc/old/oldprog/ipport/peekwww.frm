VERSION 2.00
Begin Form Form1 
   BackColor       =   &H00C0C0C0&
   Caption         =   "WWW Peek..."
   ClientHeight    =   6090
   ClientLeft      =   1470
   ClientTop       =   2160
   ClientWidth     =   6405
   Height          =   6495
   Left            =   1410
   LinkTopic       =   "Form1"
   ScaleHeight     =   6090
   ScaleWidth      =   6405
   Top             =   1815
   Width           =   6525
   Begin TextBox tResponse 
      Height          =   3975
      Left            =   120
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   5
      Top             =   2040
      Width           =   6135
   End
   Begin CommandButton Command1 
      Caption         =   "Send HTTP Request..."
      Height          =   375
      Left            =   3240
      TabIndex        =   4
      Top             =   1560
      Width           =   2295
   End
   Begin TextBox tURLPath 
      Height          =   285
      Left            =   1440
      TabIndex        =   3
      Text            =   "/"
      Top             =   1080
      Width           =   4095
   End
   Begin TextBox tHostAddress 
      Height          =   285
      Left            =   2280
      TabIndex        =   2
      Top             =   600
      Width           =   2535
   End
   Begin TextBox tHostName 
      Height          =   285
      Left            =   2280
      TabIndex        =   1
      Text            =   "www.ncsu.edu"
      Top             =   240
      Width           =   2535
   End
   Begin IPPORT IPPort1 
      EOL             =   ""
      InBufferSize    =   2048
      Left            =   5040
      LocalPort       =   0
      OutBufferSize   =   2048
      Port            =   0
      Top             =   240
   End
   Begin Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "Server Response:"
      Height          =   255
      Index           =   3
      Left            =   120
      TabIndex        =   8
      Top             =   1680
      Width           =   2415
   End
   Begin Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "URL Path:"
      Height          =   255
      Index           =   2
      Left            =   120
      TabIndex        =   7
      Top             =   1110
      Width           =   1095
   End
   Begin Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "Remote Host Address:"
      Height          =   255
      Index           =   1
      Left            =   120
      TabIndex        =   6
      Top             =   630
      Width           =   2175
   End
   Begin Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "Remote Host Name:"
      Height          =   255
      Index           =   0
      Left            =   120
      TabIndex        =   0
      Top             =   270
      Width           =   1815
   End
End
Dim rConnected

Sub Command1_Click ()

On Error GoTo ErrorHandling:

Me.MousePointer = 11

'close old connection - if any
IPPort1.Connected = False

If IPPort1.HostAddress = "0.0.0.0" Then
    If tHostName <> "" Then
        IPPort1.HostName = tHostName.Text
        tHostAddress.Text = IPPort1.HostAddress
    ElseIf IPPort1.HostAddress <> "" Then
        IPPort1.HostAddress = tHostAddress.Text
        tHostName.Text = IPPort1.HostName
    Else
        MsgBox "Please specify a host."
        Exit Sub
    End If
End If

IPPort1.Port = 80

'ask for connection
IPPort1.Connected = True

'wait until it is achieved
Do Until rConnected: DoEvents: Loop

'send data
IPPort1.DataToSend = "GET " & tURLPath.Text & Chr$(10)

Exit Sub

ErrorHandling:
    Me.MousePointer = 0
    MsgBox "Error!! " & Error$
    IPPort1.Connected = False
    Exit Sub

End Sub

Sub Form_Resize ()

margin = tResponse.Left

tResponse.Height = Me.ScaleHeight - tResponse.Top - margin
tResponse.Width = Me.ScaleWidth - 2 * margin

End Sub

Sub IPPort1_Connected (StatusCode As Integer, Description As String)

tResponse = "": rConnected = True

If Description <> "OK" Then
    MsgBox "Connection error: " & Description
    Me.MousePointer = 0
End If

End Sub

Sub IPPort1_DataIn (Text As String, EOL As Integer)

tResponse.SelStart = Len(tResponse.Text)
tResponse.SelText = Text

End Sub

Sub IPPort1_Disconnected (StatusCode As Integer, Description As String)

Me.MousePointer = 0

If Description <> "OK" Then
    MsgBox "Connection broken: " & Description
End If

rConnected = False

End Sub

Sub tHostAddress_KeyPress (KeyAscii As Integer)

On Error GoTo DAR_Failed:

If KeyAscii = 13 Then
    KeyAscii = 0
    IPPort1.HostAddress = tHostAddress.Text
    tHostName.Text = IPPort1.HostName
End If

Exit Sub

DAR_Failed:
    MsgBox Error$
    Exit Sub

End Sub

Sub tHostName_KeyPress (KeyAscii As Integer)

On Error GoTo DNR_Failed:

If KeyAscii = 13 Then
    KeyAscii = 0
    IPPort1.HostName = tHostName.Text
    tHostAddress.Text = IPPort1.HostAddress
End If

Exit Sub

DNR_Failed:
    MsgBox Error$
    Exit Sub

End Sub

