
***************************************************************
IPPort Control Version 0.9
***************************************************************

Please read this file before using any of the included software.

IPPORT.VBX is ABSOLUTELY FREE, but certain terms and conditions
apply. Please refer to the COPYRIGHT NOTICE below for more
information.

PACKAGE CONTENTS

  ipport.vbx      The IPPort Custom Control.
  ipport.hlp      Accompanying Windows Help file.
  
  peekwww.mak     Demo VB application demostrating communication
  peekwww.frm     - with a World Wide Web server

  readme.txt      This file.


REGISTRATION

If you would like to register your copy, please send a mailing 
address to any of the addresses provided below. If you provide an 
email address, you will be notified about future versions and/or 
bug fixes. (If you don't have an email address, you can send me a SASE
(self-addressed-stamped-envelope) and I will send you any information
whenever it is available). 

Contact information follows. Please use the same address for bug reports,
suggestions, comments, questions. I will try to answer as soon as I can.

GENT  HITO                 COMPUSERVE:    Gent Hito 73744,2070	
642 Chappell Drive         INTERNET:      ghito@bvcd.csc.ncsu.edu 	
Raleigh, NC 27606
U.S.A. 



COPYRIGHT NOTICE

IPPORT.VBX is Copyrighted (1994) by Gent Hito, referred to below 
as 'the author'. By using the IPort Control you implicitly indicate 
that you have read and agreed to the following terms:

You may:

- Use IPPORT.VBX in your development environment.
- Freely copy and distribute it as long as this Copyright Notice is
  distributed with it and the copy is provided to the third party 
  FREE OF CHARGE.
- Freely distribute it as a RUNTIME component of  your applications. 
  The author asks for NO royalties or run-time fees. Also, this help 
  file and accompanying demos do not have to be included in your 
  distribution package.

You may NOT:

- Modify IPPORT.VBX, and this help file in any way.
- Remove any proprietary notices, labels, or marks on the program 
  and accompanying documentation (help file).
- Sell or resell IPPORT.VBX and/or any of its accompanying products 
  and documentation, by itself or as part of another package, except 
  as a RUNTIME component of another application whose primary purpose 
  is NOT illustration of the functionality of IPPORT.VBX.


LIMITATION OF LIABILITY

The author of IPPORT.VBX makes no warranties, expressed or implied, 
in relation to this product. In no event shall the author be liable to 
you for any damages, including any loss of profits, loss of data, 
including but not limited to special, incidental, consequential, or 
indirect damages arising from the use of this software. 

