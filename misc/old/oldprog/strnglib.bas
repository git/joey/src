'Visual Basic String Library
'See individual functions for descriptions

Const backslash = "\"
Declare Function GetPrivateProfileString Lib "Kernel" (ByVal lpApplicationName As String, ByVal lpKeyName As Any, ByVal lpDefault As String, ByVal lpReturnedString As String, ByVal nSize As Integer, ByVal lpFilename As String) As Integer

'Adds the backslash in a filname, if its not present.
'
Function AddTrailingSlash (a)
  If Right$(a, 1) <> backslash Then
    AddTrailingSlash = a + backslash
  Else
    AddTrailingSlash = a
  End If
End Function

'Handy for making words start sentences, capping names, etc.
'
Function CapitalizeFirstChar (n$)
  CapitalizeFirstChar = UCase$(Left$(n$, 1)) + Mid$(n$, 2)
End Function

'Change any occurences of changefrom$ in in$ to changeto$
'
Function change$ (ByVal in$, changefrom$, changeto$)
  Do While in$ <> ""
    p = InStr(in$, changefrom$)
    If p > 0 Then
      out$ = out$ + Mid$(in$, 1, p - 1) + changeto$
      change$ = out$
      in$ = Mid$(in$, p + Len(changefrom$))
    Else
      out$ = out$ + in$
      change$ = out$
      in$ = ""
    End If
  Loop
End Function

'Returns the last word in a$,
'and removes that word from a$  - if you don't pass a$ by
'value, then the parameter you pass will be cut (recommended)
'
Function Lastword$ (a$)
  l = Len(a$)
  For x = l To 1 Step -1
    If Right$(a$, 1) = " " Then Exit For
    b$ = Right$(a$, 1) + b$
    a$ = Mid$(a$, 1, Len(a$) - 1)
  Next x
  a$ = Trim(a$)
  Lastword$ = b$
End Function

'Load a line from the given INI file.
'
Function LoadINI$ (datafile$, header$, linename$, default$)
  Dim a As String * 254
  b = GetPrivateProfileString(header$, linename$, default$, a, 254, datafile$)
  m$ = Trim(a)
  LoadINI$ = Mid$(m$, 1, InStr(m$ + Chr$(0), Chr$(0)) - 1)
End Function

'Returns the first word in a$,
'and removes that word from a$  - if you don't pass a$ by
'value, then the parameter you pass will be cut
'
Function Nextword$ (a$)
  a$ = a$ + " "
  Nextword$ = Left$(a$, InStr(a$, " ") - 1)
  a$ = Trim(a$)
  If InStr(a$, " ") > 0 Then a$ = Trim(Mid$(a$, InStr(a$, " ") + 1, Len(a$) - 2)) Else a$ = ""
End Function

'Pads s$ to length characters with spaces
'
Function pad (s$, length)
  pad = Left$(s$ + Space$(length), length)
End Function

Function stripNull (txt$) As String
  If InStr(txt$, Chr$(0)) > 0 Then stripNull = Left$(txt$, InStr(txt$, Chr$(0)) - 1) Else stripNull = txt$
End Function

