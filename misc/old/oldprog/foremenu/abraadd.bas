

'Abra extentions file.
'Modify to customise Abra for your programain.

'Includes code for timers and schedules.

'Modified for ForeMenu.

'Added Commands:
'ShowSchedules
'ShowTimers
'Command
'MenuTitle
'Reload
'LoadMenus
'SaveMenus

Global Const on_$ = "on"
Global Const onnow$ = "onnow"
Global Const NumTimers = 10, TimerInterval = 100
Global TimerName$(1 To 2, 1 To NumTimers), TimerIntervals(1 To NumTimers) As Long, ScheduleTime$(1 To NumTimers), TimerCommand$(1 To 2, 1 To NumTimers), TimerVal(1 To NumTimers)  As Long
Global CommandLineUp As Integer 'is the command line visible?
Global NextScheduleTest

'Initialization.
'
Sub AbraAddInit ()
  NextScheduleTest = Int((Val(Mid$(Time$, 4, 2)) + 14) / 15) * 15
  If NextScheduleTest = 60 Then NextScheduleTest = 0
  main.Timer1.Interval = TimerInterval
End Sub

'Check to see if any of the schedules should be ran now.
'
'min hour date month day
'
Sub CheckSchedules ()
  Const DaysOfWeek$ = "sunmontuewedthufrisat"
  Const MonthsOfYear$ = "janfebmaraprmayjunjulaugsepnovdec"
  n$ = Now
  For y = 1 To NumTimers
    If ScheduleTime$(y) <> "" Then
      'do it now?
      a$ = ScheduleTime$(y)
      Still_Ok = True
      For x = 1 To 5
        b$ = Nextword$(a$)
        If b$ <> "*" Then
          b$ = b$ + ","
          Select Case x 'get the current min/hour/...
          Case 1
            current = (Int((Minute(n$) + 7) / 15) * 15) Mod 60
          Case 2
            current = Hour(n$)
          Case 3
            current = Day(n$)
          Case 4
            current2$ = LCase$(Format(n$, "mmm"))
          Case 5
            current2$ = LCase$(Format(n$, "ddd"))
          End Select
          Part_Ok = False
          OldSepType = 2
          SubPart_Ok = True
          Do Until Len(b$) = 0 Or Part_Ok
            d = DoEvents()
            dashpos = InStr(b$, "-") 'allows for things like:
            commapos = InStr(b$, ",")'0,30 6,18 1-15,20 Jan-Mar *
            If dashpos = 0 Then dashpos = 1000
            If commapos = 0 Then commapos = 1000
            If dashpos < commapos Then
              SepType = 1
              c$ = Mid$(b$, 1, dashpos - 1)
              b$ = Mid$(b$, dashpos + 1)
            ElseIf commapos < dashpos Then
              SepType = 2
              c$ = Mid$(b$, 1, commapos - 1)
              b$ = Mid$(b$, commapos + 1)
            End If
            v = Val(c$)
            'the tricky part..
            Select Case SepType
            Case 1 'dash: a range must be ok
              'get the next part, then check the range
              OldV = v
              OldC$ = c$
            Case 2 'comma:
              If OldSepType = 2 Then 'unless ok, keep looking for an ok item
                Select Case x
                Case 1 To 3 'min/hour/date
                  If v = current Then Part_Ok = True
                Case 4, 5 'month/day
                  If c$ = current2$ Then Part_Ok = True
                End Select
              Else 'test a range - we're processing the second item of a dashed pair
                Select Case x
                Case 1 To 3
                  If OldV <= current And v >= current Then Part_Ok = True
                Case 4
                  'convert from day of week string to a number, so we can do a range comparason
                  i = InStr(current2$, DaysOfWeek$)
                  If InStr(OldC$, DaysOfWeek$) <= i And InStr(c$, DaysOfWeek$) >= i Then Part_Ok = True
                Case 5
                  'convert from month to number for range compairason
                  i = InStr(current2$, MonthsOfYear$)
                  If InStr(OldC$, MonthsOfYear$) <= i And InStr(c$, MonthsOfYear$) >= i Then Part_Ok = True
                End Select
              End If
            End Select
            OldSepType = SepType
          Loop
          If Part_Ok = False Then
            Still_Ok = False
            Exit For
          End If
        End If
      Next x
      If Still_Ok Then ProcessSchedule (y)
    End If
  Next y
End Sub

'Looks up the number of the timer (or schedule) named n$.
'If none, returns the next free timer, and sets that timers name to n$.
'If no free timers, returns 0
'
'if t=1, looks up timers, if t=2, looks up schedules.
'
'Case-insensitive
'
Function GetTimerNum (T, n$)
  'look up timer name
  n$ = LCase$(n$)
  For x = 1 To NumTimers
    If TimerName$(T, x) = n$ Then
      GetTimerNum = x
      Exit Function
    End If
  Next x
  'find a free spot to make a new timer
  For x = 1 To NumTimers
    If TimerName$(T, x) = "" Then
      GetTimerNum = x
      TimerName$(T, x) = n$
      Exit Function
    End If
  Next x
End Function

'"Global" Error handler
'
'Returns:
'3: quit processing now.     (abort)
'4: rerun the command that caused the error     (retry)
'5: resume next   (ignore)
'
Function HandleError (e)
  HandleError = MsgBox("Error: " + Error$(e), 2 Or 48, "Error")
End Function

'Processes timers and schedules - called by timer1.timer
'
Sub ProcessAbraTimers ()
  For x = 1 To 10
    If TimerIntervals(x) > 0 Then
      TimerVal(x) = TimerVal(x) + 1
      If TimerVal(x) >= TimerIntervals(x) Then
        ProcessTimer x
        TimerVal(x) = 0
      End If
    End If
  Next x
  i = Val(Mid$(Now, InStr(Now, ":") + 1, 2))
  If i >= NextScheduleTest And i - NextScheduleTest <= 15 Then
    NextScheduleTest = (NextScheduleTest + 15) Mod 60
    CheckSchedules
  End If
End Sub

'Commands specific to your program can be added to Abra Here.
'
Function ProcessAppSpecificCommands (c$, p$)
  ProcessAppSpecificCommands = True
  Select Case c$
  'these commands handle schedules and timers:
  '(remove or comment out if you aren't using these.)
  Case "loadMenus"
    LoadMenus p$
  Case "saveMenus"
    SaveMenus p$
  Case "schedule"
    SetSchedule p$
  Case "timer"
    SetTimer p$
  'foremenu addtions
  Case "command"
    CommandLine.Show
    CommandLineUp = True
  Case "showtimers"
    ShowTimers
  Case "showschedules"
    ShowSchedules
  Case "menutitle"
    SetTitle p$
  Case "reload"
    DeleteAllMenus
    LoadMenus ""
  Case Else
    ProcessAppSpecificCommands = False
  End Select
End Function

'Process a schedule command
'
Sub ProcessSchedule (num)
  ProcessCommand (TimerCommand$(2, num))
End Sub

'Process the timer command
'
Sub ProcessTimer (num)
  ProcessCommand (TimerCommand$(1, num))
End Sub

'Run the passed winscript file.
'
Sub RunScript (a$)
  If waiting Then Exit Sub
  If Not Scriptfile(a$) Then a$ = a$ + ScriptExt 'make sure it ends with the correct .ext
  f = FreeFile
  Open a$ For Input As #f
  Do Until EOF(f)
    Line Input #f, b$
    ProcessCommand b$
  Loop
  Close #f
End Sub

'schedule name on[now] min hour date month day command
'schedule name off
'
'Borrows from Unix cron command. (TNX to Randolph Chung)
'
Sub SetSchedule (ByVal a$)
  num = GetTimerNum(2, Nextword$(a$))
  onoff$ = LCase$(Nextword$(a$))
  If LCase$(onoff$) = on_$ Or LCase$(onoff$) = onnow$ Then
    If num = 0 Then
      MsgBox "All schedules are in use! Turn off a schedule, and try again."
    Else
      For x = 1 To 5
        when$ = when$ + LCase$(Nextword$(a$)) + " "
      Next x
      ScheduleTime$(num) = when$
      TimerCommand$(2, num) = a$
      If LCase$(onoff$) <> on_$ Then ProcessSchedule num 'do it immediately if its onnow
    End If
  ElseIf onoff$ = "off" Then
    If num > 0 Then
      TimerName$(2, num) = ""
      TimerCommand$(2, num) = ""
      ScheduleTime$(num) = ""
    End If
  End If
End Sub

'timer name on[now] interval[sec|min|hr] command
'timer name off
'
'interval formats:
'
'plain number - in 10ths of sec
'7sec
'8min
'3hr
'
Sub SetTimer (ByVal a$)
  Dim Interval As Long, mult As Long
  num = GetTimerNum(1, Nextword$(a$))
  onoff$ = LCase$(Nextword$(a$))
  If LCase$(onoff$) = on_$ Or LCase$(onoff$) = onnow$ Then
    If num = 0 Then
      MsgBox "All timers are currently in use! Turn off a timer, and try again."
    Else
      i$ = LCase$(Nextword$(a$))
      mult = 1
      If Right$(i$, 3) = "sec" Then mult = 10
      If Right$(i$, 3) = "min" Then mult = 600
      If Right$(i$, 2) = "hr" Then mult = 36000
      Interval = Val(i$) * mult
      TimerIntervals(num) = Interval
      TimerVal(num) = 0
      TimerCommand$(1, num) = a$
      If LCase$(onoff$) <> on_$ Then ProcessTimer num 'do it immediately if its onnow
    End If
  ElseIf onoff$ = "off" Then
    If num > 0 Then
      TimerName$(1, num) = ""
      TimerIntervals(num) = 0
    End If
  End If
End Sub

'Just disply all the schedules.
'
Sub ShowSchedules ()
  For x = 1 To NumTimers
    a$ = a$ + Chr$(13) + Chr$(10) + TimerName$(2, x) + "....."
    If (x) > 0 Then a$ = a$ + TimerCommand$(2, x)
  Next x
  MsgBox Mid$(a$, 3), 0 Or 64, "Schedules"
End Sub

'Just display all the timers currently in effect.
'
'
Sub ShowTimers ()
  For x = 1 To NumTimers
    a$ = a$ + Chr$(13) + Chr$(10) + TimerName$(1, x) + "....."
    If TimerIntervals(x) > 0 Then a$ = a$ + TimerCommand$(1, x)
  Next x
  MsgBox Mid$(a$, 3), 0 Or 64, "Timers"
End Sub

