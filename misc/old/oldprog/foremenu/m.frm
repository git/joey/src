VERSION 2.00
Begin Form main 
   BorderStyle     =   0  'None
   Caption         =   "ForeMenu"
   ClientHeight    =   0
   ClientLeft      =   285
   ClientTop       =   990
   ClientWidth     =   6405
   ControlBox      =   0   'False
   Height          =   690
   Icon            =   M.FRX:0000
   KeyPreview      =   -1  'True
   Left            =   225
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   0
   ScaleWidth      =   6405
   Top             =   360
   Width           =   6525
   Begin Timer Timer1 
      Interval        =   2000
      Left            =   2520
      Top             =   -120
   End
   Begin Menu t 
      Caption         =   "&Control"
      Index           =   1
      Begin Menu EditMenus 
         Caption         =   "&Edit Items.."
      End
      Begin Menu EditTitles 
         Caption         =   "&Edit Titles.."
      End
      Begin Menu i1 
         Index           =   1
      End
   End
   Begin Menu t 
      Index           =   2
      Visible         =   0   'False
      Begin Menu i2 
         Index           =   1
      End
   End
   Begin Menu t 
      Index           =   3
      Visible         =   0   'False
      Begin Menu i3 
         Index           =   1
      End
   End
   Begin Menu t 
      Index           =   4
      Visible         =   0   'False
      Begin Menu i4 
         Index           =   1
      End
   End
   Begin Menu t 
      Index           =   5
      Visible         =   0   'False
      Begin Menu i5 
         Index           =   1
      End
   End
   Begin Menu t 
      Index           =   6
      Visible         =   0   'False
      Begin Menu i6 
         Index           =   1
      End
   End
   Begin Menu t 
      Index           =   7
      Visible         =   0   'False
      Begin Menu i7 
         Index           =   1
      End
   End
   Begin Menu t 
      Index           =   8
      Visible         =   0   'False
      Begin Menu i8 
         Index           =   1
      End
   End
   Begin Menu t 
      Index           =   9
      Visible         =   0   'False
      Begin Menu i9 
         Index           =   1
      End
   End
   Begin Menu t 
      Index           =   10
      Visible         =   0   'False
      Begin Menu i10 
         Index           =   1
      End
   End
End
DefInt A-Z

Sub EditMenus_Click ()
  If EditMenus.Checked = False Then
    EditMenus 1, 1
    EditMenus.Checked = True
  Else
    EditMenus.Checked = False
    SaveItem
    Unload edititem
  End If
End Sub

Sub EditTitles_Click ()
  If EditTitles.Checked = True Then
    EditTitles.Checked = False
    SaveTitle
    Unload Edtitles
  Else
    EditTitles.Checked = True
    Edtitles.Show
  End If
End Sub

Sub Form_KeyDown (KeyCode As Integer, shift As Integer)
  If shift = 6 Then TimerPause
End Sub

Sub Form_Load ()
  AbraAddInit
  NumItems(1) = 1
  NumTitles = 1
  MakeOnTop main.hWnd 'make this the topmost window.
  Appath$ = app.Path
  If Right$(Appath$, 1) <> "\" Then Appath$ = Appath$ + "\"
  MenuFile$ = Appath$ + "menu.dat"
  LoadMenus ""
  Show
  'the file doesn't have to exist
  On Error GoTo OK_error
  RunScript Appath$ + "autoload.scr"
Load_done:
  Exit Sub
OK_error:
  Resume Load_done
End Sub

Sub i1_Click (index As Integer)
  HandleMenu 1, index
End Sub

Sub i10_Click (index As Integer)
  HandleMenu 10, index
End Sub

Sub i2_Click (index As Integer)
  HandleMenu 2, index
End Sub

Sub i3_Click (index As Integer)
  HandleMenu 3, index
End Sub

Sub i4_Click (index As Integer)
  HandleMenu 4, index
End Sub

Sub i5_Click (index As Integer)
  HandleMenu 5, index
End Sub

Sub i6_Click (index As Integer)
  HandleMenu 6, index
End Sub

Sub i7_Click (index As Integer)
  HandleMenu 7, index
End Sub

Sub i8_Click (index As Integer)
  HandleMenu 8, index
End Sub

Sub i9_Click (index As Integer)
  HandleMenu 9, index
End Sub

'Process timers and schedules
'
Sub Timer1_Timer ()
  ProcessAbraTimers
End Sub

