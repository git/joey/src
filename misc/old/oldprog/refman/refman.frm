VERSION 2.00
Begin Form Form1 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Refman Import Utility"
   ClientHeight    =   3735
   ClientLeft      =   435
   ClientTop       =   1605
   ClientWidth     =   8895
   Height          =   4140
   Icon            =   REFMAN.FRX:0000
   Left            =   375
   LinkTopic       =   "Form1"
   ScaleHeight     =   3735
   ScaleWidth      =   8895
   Top             =   1260
   Width           =   9015
   Begin ListBox List 
      FontBold        =   0   'False
      FontItalic      =   0   'False
      FontName        =   "Terminal"
      FontSize        =   9
      FontStrikethru  =   0   'False
      FontUnderline   =   0   'False
      Height          =   2730
      Left            =   -15
      MultiSelect     =   1  'Simple
      TabIndex        =   1
      Top             =   300
      Visible         =   0   'False
      Width           =   8925
   End
   Begin CommonDialog CMDialog1 
      DialogTitle     =   "Select Capture File"
      Filename        =   "*.*"
      Filter          =   "*.*"
      Left            =   4380
      Top             =   3240
   End
   Begin CommandButton Command2 
      BackColor       =   &H00000000&
      Caption         =   "&Unselect All"
      Enabled         =   0   'False
      Height          =   315
      Left            =   6300
      TabIndex        =   3
      Top             =   0
      Width           =   1275
   End
   Begin CommandButton Command1 
      BackColor       =   &H00000000&
      Caption         =   "&Select All"
      Enabled         =   0   'False
      Height          =   315
      Left            =   4980
      TabIndex        =   2
      Top             =   0
      Width           =   1275
   End
   Begin CommandButton Command3 
      BackColor       =   &H00000000&
      Caption         =   "Sa&ve"
      Enabled         =   0   'False
      Height          =   315
      Left            =   7620
      TabIndex        =   4
      Top             =   0
      Width           =   1275
   End
   Begin Label Progress 
      Caption         =   "Starting up..."
      ForeColor       =   &H000000FF&
      Height          =   255
      Left            =   60
      TabIndex        =   0
      Top             =   60
      Width           =   4875
   End
End
DefInt A-Z

Const NumUsedTags = 7

Dim Shared NumItems, appath$

Dim Shared TagArray$(1 To 100, 1 To 2), TagArraySize
Dim Shared TagData$(1 To NumUsedTags)
Dim Shared TagHeader$(1 To NumUsedTags), NumTagHeaders

Dim Shared doctype, itemfile

'Erase old tag data
'
Sub ClearTags ()
  For X = 1 To NumTagHeaders
    TagData$(X) = ""
  Next X
End Sub

Sub command1_click ()
  'select all items in the listbox
  list.Visible = False
  For X = 0 To list.ListCount - 1
    list.Selected(X) = True
    a = DoEvents()
  Next X
  list.Visible = True
End Sub

Sub Command2_Click ()
  'un select all items in the listbox
  For X = 0 To list.ListCount - 1
    list.Selected(X) = False
    a = DoEvents()
  Next X

End Sub

Sub command3_click ()
  itemfile = FreeFile
  Open "c:\~tmp.tmp" For Input As #itemfile
  
  cmdialog1.DialogTitle = "Save As"
  cmdialog1.Action = 2
  If CancelError = True Then Exit Sub
  On Error GoTo cant_save
  f = FreeFile
  Open cmdialog1.Filetitle For Output As #f
  For X = 0 To NumItems - 1
    If list.Selected(X) = True Then
      Print #f, Chr$(13) + Chr$(10)
      progress.Caption = "Saving #" + CStr(X + 1)
      a$ = ""
      Do Until Trim$(a$) = "* * *" Or EOF(itemfile)
        ii = DoEvents()
        Line Input #itemfile, a$
        If a$ <> "" And Trim$(a$) <> "* * *" Then
          Print #f, a$
        End If
      Loop
      Print #f, "ER  - "
    Else
      a$ = ""
      Do Until Trim$(a$) = "* * *" Or EOF(itemfile)
        ii = DoEvents()
        Line Input #itemfile, a$
      Loop
    End If
  Next X
  Close #f
  End
eq:
  Exit Sub
cant_save:
  MsgBox "Unable to save. Try a different filename or drive."
  Resume eq
End Sub

Sub Form_Load ()
  appath$ = app.Path
  If Right$(appath$, 1) <> "\" Then appath$ = appath$ + "\"
  LoadTagInfo
  top = (screen.Height - Height) / 2
  left = (screen.Width - Width) / 2
  Show
  cmdialog1.Action = 1
  If cmdialog1.CancelError = True Then Error 10
  On Error GoTo Bad_File
  f = FreeFile
  Open cmdialog1.Filetitle For Input As #f
  itemfile = FreeFile
  Open "c:\~tmp.tmp" For Output As #itemfile
  HistoricalAbstracts f
  On Error GoTo 0
  progress.Caption = "Select Titles to Import"
  list.Visible = True
  list.Height = 3450
  command1.Enabled = True
  command2.Enabled = True
  command3.Enabled = True
  Close #f
  Close #itemfile
  If Command$ = "auto" Then
    'auto-select everything, and call the save proc.
    command1_click
    command3_click
  End If
  Exit Sub
quit:
  End
Bad_File:
  MsgBox "Invalid file selected or no file selected. Exiting."
  Resume quit
End Sub

Sub Form_Unload (Cancel As Integer)
  On Error GoTo ke
  Kill "c:\~tmp.tmp"
es:
  Exit Sub
ke:
  Resume es
End Sub

'Import from historical abstracts ascii format.
'
Sub HistoricalAbstracts (f)
  i$ = "Importing file...reference #"
  Do Until EOF(f)
    iii = iii + 1
    progress.Caption = i$ + CStr(iii)
    eor = False
    last = 0
    ClearTags
    Do Until eor Or EOF(f)
      n = DoEvents()
      Line Input #f, c$
      a$ = Trim$(c$)
      b$ = LCase$(a$)
      ok = False
      For X = 1 To TagArraySize
        If InStr(b$, TagArray(X, 1)) = 1 Then
          ProcessTag Trim$(Mid$(a$, Len(TagArray(X, 1)) + 1)), X, False
          last = X
          
          ok = True
          Exit For
        End If
        ii = DoEvents()
      Next X
      
      If Mid$(Trim$(b$), 1, 20) = String$(20, "-") Then
        'end of article
        ok = True
        eor = True
        'save this article, after processing it.
        ProcessTags
        list.AddItem Mid$(TagData$(3), 6)
        SaveItem
      End If
      If Not ok And last > 0 And Mid$(c$, 1, 5) = "     " Then ProcessTag a$, last, True'append to a tag.
      If Not ok And Mid$(c$, 1, 5) <> "     " Then
        n = MsgBox(nextword$(b$), 1, "Unknown Tag")
        If n = 2 Then End
      End If
    Loop
  Loop
End Sub

'load tags into the array
'
Sub LoadTagInfo ()
  f = FreeFile
  Open appath$ + "tag.dat" For Input As #f
  Input #f, TagArraySize
  For y = 1 To TagArraySize
    Line Input #f, a$
    TagArray$(y, 1) = LCase$(a$)
    Line Input #f, b$
    TagArray$(y, 2) = b$
  Next y
  Input #f, NumTagHeaders
  For X = 1 To NumTagHeaders
    Input #f, a$
    TagHeader(X) = UCase$(a$) + "  - "
  Next X
  Close #f
End Sub

'trim authors off, one at a time, and return a string in RIS format.
'
Function ProcessAuthor$ (a$)
  a$ = LCase$(a$)
  Do Until Trim$(a$) = ""
    'text up till 1st comma is the last name
    c = InStr(a$ + ",", ",")
    n$ = Mid$(a$, 1, c - 1)
    a$ = Trim$(Mid$(a$, c + 1))
    'next letter is initial
    init$ = Mid$(a$, 1, 1)
    'is there an " and " in here? If so, there are 2 authors.
    i = InStr(LCase$(a$), " and ")
    If i > 0 Then
      a$ = Trim$(Mid$(a$, i + 6))
    Else
      a$ = ""
    End If
    If pa$ <> "" Then pa$ = pa$ + Chr$(13) + Chr$(10) + "AU  - "
    pa$ = pa$ + WordCap$(n$) + "," + UCase$(init$) + "."
  Loop
  ProcessAuthor$ = pa$
End Function

Function ProcessBookData$ (a$)
  'a book
  'format:
  'Location: Publisher, pub_year num_pages pp.
  n$ = "CY  - " + Mid$(a$, 1, InStr(a$, ":") - 1) + Chr$(13) + Chr$(10)
  a$ = Trim$(Mid$(a$, InStr(a$, ":") + 1))
  n$ = n$ + "PB  - " + Mid$(a$, 1, InStr(a$, ",") - 1) + Chr$(13) + Chr$(10)
  a$ = Trim$(Mid$(a$, InStr(a$, ",") + 1))
  a$ = Mid$(a$, 1, InStr(a$, ".") - 1)
  n$ = n$ + "PY  - " + CStr(Val(a$))
  ProcessBookData$ = n$
End Function

'Pick out the various doc types, and make the TY tag
'
Function ProcessDocType$ (t$)
  t$ = LCase$(t$)
  Select Case nextword$(LCase$(t$))
  Case "article"
    t$ = "JOUR"
    doctype = 1
  Case "book"
    t$ = "BOOK"
    doctype = 2
  Case "abstract"
    t$ = "ABST"
    doctype = 3
  Case Else
    t$ = "UNPB"
    doctype = 4
  End Select
  ProcessDocType$ = t$
End Function

'Pull out the publisher, etc.
'
Function ProcessJournalData$ (a$)
  a$ = LCase$(a$)
  'a journal
  'format:
  'jornal_name [location] pub_year volume_number: start_page-end_page
  'or:
  'journal_name pub_year volume_number: start_page-end_page
  p$ = Lastword$(a$)
  i = InStr(p$, "-")
  If i > 0 Then
    n$ = "SP  - " + CStr(Val(Mid$(p$, 1, i - 1))) + Chr$(13) + Chr$(10) + "EP  - " + CStr(Val(Mid$(p$, i + 1))) + Chr$(13) + Chr$(10)
  Else
    n$ = "SP  - " + p$ + Chr$(13) + Chr$(10)
  End If
  v$ = Lastword$(a$)
  i = InStr(v$, ":")
  If i > 0 Then v$ = Mid$(v$, 1, i - 1)
  n$ = n$ + "VL  - " + Trim$(v$) + Chr$(13) + Chr$(10)

  y$ = Lastword$(a$)
  n$ = n$ + "PY  - " + CStr(Val(y$)) + Chr$(13) + Chr$(10)
  
  i = InStr(a$, "[")
  If i > 0 Then a$ = Mid$(a$, 1, i - 1)
  n$ = n$ + "JO  - " + WordCap$(a$)
  
  ProcessJournalData$ = n$
End Function

'Add tag to array of processed tags
'
Sub ProcessTag (value$, num, appnd)
  t = Val(TagArray$(num, 2))
  If t > 0 Then
    If appnd = False Then
      TagData$(t) = value$
    Else
      TagData$(t) = Trim$(TagData$(t)) + "  " + Trim$(value$)
    End If
  End If
End Sub

'Process the whole tag array, adding tag headers and making everything spiffy
'
Sub ProcessTags ()
  For X = 1 To NumTagHeaders
    t$ = TagData$(X)
    If t$ <> "" Then
      Select Case X
      Case 1
        'document type
        t$ = TagHeader$(X) + ProcessDocType$(t$)
      Case 2
        'author
        t$ = TagHeader$(X) + ProcessAuthor$(t$)
      Case 3
        'title
        t$ = WordCap$(LCase$(t$))
        Select Case doctype
        Case 1, 3
          t$ = TagHeader$(X) + t$
        Case 2
          t$ = "BT  - " + t$
        Case Else
          t$ = "BT  - " + t$
          
        End Select
      Case 5
        t$ = ProcessJournalData$(t$)
      Case 6
        t$ = ProcessBookData$(t$)
      Case Else
        t$ = TagHeader$(X) + t$
      End Select
      TagData$(X) = t$
    End If
  Next X
End Sub

Sub SaveItem ()
  NumItems = NumItems + 1
  'ReDim Preserve Items$(1 To NumUsedTags, 1 To numitems)
  For X = 1 To NumUsedTags
    Print #itemfile, TagData$(X)
  Next X
  Print #itemfile, "* * *"
End Sub

'Cap 1st letter of each word
'
Function WordCap$ (ByVal n$)
  n$ = LCase$(n$)
  Do Until n$ = ""
    i = InStr(n$ + " ", " ")
    b$ = Mid$(n$, 1, i - 1)
    n$ = Mid$(n$, i + 1)
    w$ = w$ + UCase$(Mid$(b$, 1, 1)) + Mid$(b$, 2) + " "
  Loop
  WordCap$ = Trim$(w$)
End Function

