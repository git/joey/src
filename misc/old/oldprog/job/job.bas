'Job search program
'Written by joey hess

DefInt A-Z
Dim Shared appath$, header$
Global INIFile As String
Global Const SearchHeader = "search"

Dim Shared ExcludedItem$(), NumExcluded

'Add this to the list of excluded items.
'
Sub Exclude (a$)
  NumExcluded = NumExcluded + 1
  ReDim Preserve ExcludedItem$(1 To NumExcluded)
  ExcludedItem$(NumExcluded) = a$
End Sub

'Is a$ in the list of excluded items?
'
Function Excluded (a$)
  Exculded = False
  For x = 1 To NumExcluded
    If ExcludedItem$(x) = a$ Then
      Excluded = True
      Exit Function
    End If
    ii = DoEvents()
  Next x
End Function

'Load a string from an INIfile.
'
Function GetINIStr (ByVal File$, ByVal Section$, ByVal Key$, ByVal Default$) As String
  Dim Temp As String * 200
  Dim dummy%

  dummy% = GetPrivateProfileString(Section$, Key$, Default$, Temp$, 200, File$)
  GetINIStr = stripNull(Temp$)
End Function

'Handle form posts - two cases; search and send are handled.
'
'In addition, send:n is handled, where n is the number of search feilds to
'look at.
'
Sub Main ()
  'Dim tim As Long
  'tim = Timer
  INIFile$ = app.EXEName + ".ini"
  appath$ = addTrailingSlash((app.Path))
  header$ = GetINIStr$(appath$ + INIFile, "general", "header", "Job Center")
  InitCGI mimetype$
  On Error GoTo ErrorTrap
  Select Case LCase$(Mid$(urlarg$, 1, InStr(urlarg$ + ":", ":") - 1))
  Case "send"
    SendFile
  Case "search"
    SearchDB
  Case Else
    PrintHTMLHeader header$, ""
    CGIError "Bad parameters: specify either send or search.<P>"
  End Select
done:
  EndCgi
  
  'MsgBox CStr(Timer - tim) + " " + CStr(tim) + " " + CStr(Timer)

  Exit Sub
ErrorTrap:
  PrintHTMLHeader header$, ""
  CGIError "There was an error executing the script. The error code was " + CStr(Err) + ". Please report this error to the webmaster."
  Resume done 'not really - the above command ends the program.
End Sub

'Returns true if:
'How=1 : text$ starts with tofind$
'How=2 : text$ contains with tofind$
'How=3 : text$ in with tofind$, exactly
'How=4 : text$ is like tofind$
'
Function Match (text$, tofind$, how)
  
  Match = False
  Select Case how
  Case 1
    If InStr(text$, tofind$) = 1 Then Match = True
  Case 2
    If InStr(text$, tofind$) > 0 Then Match = True
  Case 3
    If text$ = tofind$ Then Match = True
  Case 4
    If text$ Like tofind$ Then Match = True
  End Select
End Function

'Doesn't actually redirect the browser - instead, it
'reads in the while file, and returns it. This is because
'many browsers don't to support redirection...
'
'U$ should be the DOS path the the file to send.
'
Sub RedirectBrowserTo (u$)
  f = FreeFile
  Open u$ For Input As #f
  Do Until EOF(f)
    Line Input #f, a$
    Print #cgioutputfile, a$
    i = DoEvents()
  Loop
  Close #f
End Sub

'Search the database file, looking for a string, and
'return all entries that match that string to the browser
'
Sub SearchDB ()
  
  If Val(Mid$(urlarg$, InStr(urlarg$ + ":", ":") + 1)) > 1 Then
    numfields = Val(Mid$(urlarg$, InStr(urlarg$ + ":", ":") + 1))
  Else
    numfields = 1
  End If
  
  ReDim how(1 To numfields) As Integer
  ReDim what(1 To numfields) As Integer
  ReDim ValueToFind$(1 To numfields)

  PrintHTMLHeader header$, ""
  On Error GoTo SearchError

  If LCase$(GetINIStr$(appath$ + INIFile, SearchHeader, "fast", "true")) = "true" Then fastmode = True Else fastmode = False
  
  UrlPrefix$ = GetINIStr$(appath$ + INIFile, SearchHeader, "URLPrefix", "/")
  
  dbfile$ = GetINIStr$(appath$ + INIFile, SearchHeader, "dbfile", "")
  If dbfile$ = "" Then CGIError "Error in INI File: No DBfile specified. Inform your webmaster.<P>"
    
  'load in data from the form, for each search field.
  'this could be done slightly faster, but at the expense of memory,
  'by loading the jobsearch.dat file into memory 1st.
  
  For cf = 1 To numfields
    If numfields > 1 Then
      fnum$ = CStr(cf)
      fe$ = "(In part " + fnum$ + ")"
    Else
      fnum$ = ""
      fe$ = ""
    End If
    
    'load in the data for this field from the form, save in arrays
    HowToSearch$ = LCase$(Trim$(ReadInput$("how" + fnum$, "")))
    Select Case HowToSearch$
    Case LCase$(GetINIStr$(appath$ + INIFile, SearchHeader, "StartsWith", ""))
      how(cf) = 1
    Case LCase$(GetINIStr$(appath$ + INIFile, SearchHeader, "Contains", ""))
      how(cf) = 2
    Case LCase$(GetINIStr$(appath$ + INIFile, SearchHeader, "IsExactly", ""))
      how(cf) = 3
    Case LCase$(GetINIStr$(appath$ + INIFile, SearchHeader, "PatternMatches", ""))
      how(cf) = 4
    Case Else
      CGIError "You did not specify a valid way to search the database. There may be a problem with the form. Inform your webmaster.<P>" + fe$
    End Select
    
    WhatToSearch$ = LCase$(Trim$(ReadInput$("what" + fnum$, "")))
    f = FreeFile
    done = False
    Open appath$ + app.EXEName + "search.dat" For Input As #f
    Do Until EOF(f) Or done
      Line Input #f, a$
      Input #f, b
      ii = DoEvents()
      If LCase$(a$) = WhatToSearch$ Then
        what(cf) = b
        done = True
      End If
    Loop
    Close #f
    If what(cf) = 0 Then CGIError "You did not specify what to search for. There may be a form problem - inform your webmaster.<P>" + fe$
    
    ValueToFind$(cf) = LCase$(Trim$(ReadInput$("value" + fnum$, "")))
    If ValueToFind$(cf) = "" Then CGIError "You did not specify any text to search for.<P>" + fe$
  Next cf

  ResultText$ = GetINIStr$(appath$ + INIFile, SearchHeader, "ResultText", "<h4>Results of Your Search</h4>")
  
  LinkSuffix$ = GetINIStr$(appath$ + INIFile, SearchHeader, "LinkSuffix", "")
  
  ListTrailer$ = GetINIStr$(appath$ + INIFile, SearchHeader, "ListTrailer", "")

  coltodisp = Val(GetINIStr$(appath$ + INIFile, SearchHeader, "ColumnToDisplay", "1"))

  f = FreeFile
  Open dbfile$ For Input As #f
  Do Until EOF(f)
    Line Input #f, a2$
    'iterate througfh each search field in turn, and make sure the data
    'from all matchs. Then, print the line.
    isgood = 0
    For cf = 1 To numfields
      a$ = a2$
      If Trim$(a$) <> "" Then
        a$ = change$(a$, " ", "~")
        a$ = change$(a$, """,", " ")
        a$ = Trim$(change$(a$, """", " "))
        x = 0
        Do Until a$ = ""
          x = x + 1
          w$ = nextword$(a$)
          If what(cf) = x Then category$ = Trim$(LCase$(change(w$, "~", " ")))
          If x = coltodisp Then disp$ = Trim$(change(w$, "~", " "))
          ii = DoEvents()
        Loop
        filename$ = w$ 'the filename is always the last word
        If Match(category$, ValueToFind$(cf), how(cf)) Then
          l$ = "<LI><A HREF = """ + UrlPrefix$ + filename$ + """>" + Trim$(disp$ + " " + LinkSuffix$) + "</a>"
          If fastmode Then
            isgood = isgood + 1
          ElseIf Not Excluded(l$) Then
            isgood = isgood + 1
          End If
        End If
      End If
      If isgood < cf Then Exit For 'if we arn't keeping up, we'll never catch
      'back up :-) Just quit now, and save us the anguish :-)
    Next cf
    If isgood = numfields Then 'matched all fields
      'add url to file
      num = num + 1
      If num = 1 Then
        Print #cgioutputfile, ResultText$
        Print #cgioutputfile, "<ul>"
      End If
      Print #cgioutputfile, l$
      'exclude this from being printed again
      If Not fastmode Then Exclude l$
    End If
  Loop
  Close #f
  If num = 0 Then
    Print #cgioutputfile, "No matches were found. Try broadening your search criteria.<P>"
  Else
    Print #cgioutputfile, "</ul>"
    Print #cgioutputfile, ListTrailer$
  End If
  
searchdone:
  Exit Sub
SearchError:
  CGIError "There was an error searching the database. The error code was " + CStr(Err) + ". Please report this error to the webmaster."
End Sub

'send the file. Keywords and the matching files are listed in
'jobsend.dat.
'
Sub SendFile ()
  tosend$ = LCase$(ReadInput("category", ""))
  f = FreeFile
  Open appath$ + app.EXEName + "send.dat" For Input As #f
  done = False
  Do Until EOF(f) Or done
    Line Input #f, a$
    a$ = Trim$(LCase$(a$))
    Line Input #f, b$
    If a$ = tosend$ Then
      RedirectBrowserTo b$
      done = True
    End If
  Loop
  Close #f
  If done = False Then
    PrintHTMLHeader header$, ""
    CGIError "Unable to find match to selected option. No matching entry in " + app.EXEName + "send.dat."
  End If
End Sub

