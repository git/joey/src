Job Center CGI Script 1.0

By Joey Hess (jeh22@cornell.edu)

------
Getting Started
------
This section delas with setting the script up for the situation it was
designed for: the Job Center form.

The essential files for the script are job.exe, job.ini, jobsearc.dat, 
and jobsend.dat. Copy all of these to your cgi-win subdirectory. The 
other files are source code and documentation.

There are two parts of the script to set up - the send and search options.

To set up the send option, you will probably need to edit the jobsend.dat 
file to reflect where you keep your pages. (If all the pages are in 
c:\httpd\htdocs\, then you won't need to do this.) See the next section for 
more information.

To set up the search option, you will need to edit job.ini, and you may need
to edit jobsearc.dat. The ini file is self-documenting, and jobsearc.dat is
explained in the next section.

------
Advanced Configuration
------
This script can easily be adapted to other situations. 

You can add different forms to be returned by the send option by editing the
jobsend.dat file.

The file is of this format:
description 1
file 1
description 2
file 2
.
.
.

Where the descriptions are identical to the values of the select box in the
calling form (the "blah" in "<option value="blah">"), and the files are the
complete filenames of the files to send.

You can teach the program to use a new database by editing the jobsearc.dat
file.

The file is of the format:
description 1
column number
description 2
column number
.
.
.

Where the descriptions are identical to the values of the "what" select box in 
the calling form, and the column numbers are the location of that item in the
database. So, if you had a database that consisted of 4 entries -- name,
favorite color, gender, address, and phone number -- then it would look like:
name
1
favorite color
2
gender
3
address
4
phone number
5

-----
Multiple Search Fields
-----
Multiple search fileds are now supported! The fields are logically ANDed
together (an item in the database must match all of them to be listed in the
output).

To add multiple search fields, you have only to modify your HTML form. No
changes in the datafiles are neccessary.

Here's a sample HTML form with multiple search fields:
---------Begin-----------
<FORM METHOD="POST" ACTION="/cgi-win/job.exe?search:2">
<hr>
<h4>Search for  job offerings by job category, job title, city or state using key words</h4>

Search for <SELECT NAME="what1">
<OPTION SELECTED>Job  category
<OPTION>Job title
</select><p>
which <SELECT NAME="how1">

<OPTION SELECTED>starts with
<OPTION>contains
<OPTION>is exactly
<OPTION>pattern matches
</SELECT><P>
the following: <INPUT TYPE="text" NAME="value1"><P>

and geographically by: <SELECT NAME="what2">
<OPTION SELECTED>State
<OPTION>City
</SELECT><P>
<p>
which <SELECT NAME="how2">
<OPTION SELECTED>starts with
<OPTION>contains
<OPTION>is exactly
<OPTION>pattern matches
</SELECT><P>
the following: <INPUT TYPE="text" NAME="value2"><P>
<INPUT TYPE="submit" VALUE="Start Search">
</FORM>
-----------End--------------
Note that the program is passed the parameters "search:2" this lets it know
that there are 2 fields in the form.

Also notice the way the controls in the search fields are named - each field
consists of 3 controls named "what","how",and "value". In a normal search
(just 1 field, and the parameter to the program is "search"), no number is
added after the control name. In a multiple fields search form, add a number
after the name of each control to specify what field its in.

-----
Design Issues
-----
In the send case of the script, I've decided to actually read in the whole 
document instead of redirecting the client to an URL. Redirection is faster,
but it is not supported by all the clients.

Some badly designed browsers (such as Winweb) do not read the form correctly.
To make script will still work with them, you will need to add extra lines to
jobsend.dat. Add a new line for each item in the select box, where the
description is the description of the item in the calling form (the 
"blah blah blah" in "<option> value="something">blah blah blah"). The default
file is configured in this way.

In the search case, I've had the program filter out duplicate names, so only
one is returned to the user. 

The search case supports matching by wildcards. This works just like DOS
wildcards -- if you want to search for all occurances of "bank" followed by
"company", just use *bank*company*. Question-mark wildcards are also 
supported.

-----
Note:
Job.exe is now filename aware - if you rename it to xxx.exe, it will try to 
read xxx.ini, xxxsend.dat, and xxxsearc.dat (of course, xxx should be no 
longer than 3 letters. If the name is 4 letters, it'll look for xxxx.ini,
xxxxsend.dat, and xxxxsear.dat, and so on.)
-----
Send questions, bug reports, etc. to jeh22@cornell.edu