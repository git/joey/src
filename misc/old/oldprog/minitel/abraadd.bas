'Abra extentions file.
'Modify to customise Abra for your program.

DefInt A-Z

'Initialization.
'
Sub AbraAddInit ()
End Sub

'"Global" Error handler
'
'Returns:
'3: quit processing now.
'4: rerun the command that caused the error
'5: resume next
'
Function HandleError (a$)
  HandleError = MsgBox(a$, 2 Or 48, "Error")
End Function

'Commands specific to your program can be added to Abra Here.
'
Function ProcessAppSpecificCommands (c$, p$)
  ProcessAppSpecifiCommands = True
  Select Case c$
  End Select
End Function

'Run the passed winscript file.
'
Sub RunScript (a$)
  If waiting Then Exit Sub
  If Not Scriptfile(a$) Then a$ = a$ + ScriptExt 'make sure it ends with the correct .ext
  f = FreeFile
  Open a$ For Input As #f
  Do Until EOF(f)
    Line Input #f, b$
    ProcessCommand b$
  Loop
  Close #f
End Sub

