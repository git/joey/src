VERSION 2.00
Begin Form Form1 
   Caption         =   "Minitel"
   ClientHeight    =   930
   ClientLeft      =   2805
   ClientTop       =   2385
   ClientWidth     =   1515
   Height          =   1335
   Left            =   2745
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   ScaleHeight     =   930
   ScaleWidth      =   1515
   Top             =   2040
   Width           =   1635
   WindowState     =   1  'Minimized
   Begin VBWsk VBWsk1 
      Blocking        =   0   'False
      EOL             =   ""
      Left            =   1080
      Port            =   ""
      Top             =   480
   End
   Begin Label Label1 
      Alignment       =   2  'Center
      Height          =   795
      Left            =   120
      TabIndex        =   0
      Top             =   60
      Width           =   1275
      WordWrap        =   -1  'True
   End
End
'Watches a specified port, and if a password comes it, runs
'a command.

'new in this version - reads a .cfg file, for password/command pairs.

Declare Function GetPrivateProfileString Lib "Kernel" (ByVal lpApplicationName As String, ByVal lpKeyName As Any, ByVal lpDefault As String, ByVal lpReturnedString As String, ByVal nSize As Integer, ByVal lpFilename As String) As Integer

Dim Shared pwsofar$, Com$(), Password$(), NumCommands, portnum

Sub Form_Load ()
  HeaderName$ = "minitel"
  IniName$ = app.Path
  If Right$(IniName$, 1) <> "\" Then IniName$ = IniName$ + "\"
  CfgName$ = IniName$ + HeaderName$ + ".cfg"
  IniName$ = IniName$ + HeaderName$ + ".ini"
  'load ini file
  portnum = CStr(LoadINI$(IniName$, HeaderName$, "port", "25"))
  VBWsk1.Port = portnum
  VBWsk1.Host = "" 'server mode
  f = FreeFile
  Open CfgName$ For Input As #f
  Do Until EOF(f)
    Line Input #f, a$
    If Trim$(a$) <> "" Then Line Input #f, b$
    If Trim$(a$) <> "" And Trim$(b$) <> "" Then
      NumCommands = NumCommands + 1
      ReDim Preserve Com$(1 To NumCommands)
      ReDim Preserve Password$(1 To NumCommands)
      Password$(NumCommands) = a$
      Com$(NumCommands) = b$
    End If
  Loop
  label1.Caption = "Loaded and listening to port" + Str$(portnum) + " -" + Str$(NumCommands) + " commands loaded."
End Sub

Function LoadINI$ (datafile$, header$, linename$, default$)
  Dim a As String * 254
  b = GetPrivateProfileString(header$, linename$, default$, a, 254, datafile$)
  m$ = Trim(a)
  LoadINI$ = Mid$(m$, 1, InStr(m$ + Chr$(0), Chr$(0)) - 1)
End Function

Sub VBWsk1_Accept ()
  VBWsk1.Socket = VBWsk1.Socket
  VBWsk1.Data = "Hello. Enter your password at the beep...<beep!>"
End Sub

Sub VBWsk1_Close ()
  pwsofar$ = ""
  VBWsk1.Socket = -1
  VBWsk1.Port = portnum
  VBWsk1.Host = ""
End Sub

Sub VBWsk1_Connect ()
' vbwsk1.Data = "Hello. Enter your password at the beep...<beep!>"
End Sub

Sub VBWsk1_Read ()
  pwsofar$ = pwsofar$ + VBWsk1.Data
  For x = 1 To NumCommands
    If pwsofar$ = Password$(x) Then
      VBWsk1.Data = Chr$(13) + Chr$(10) + "Here goes:" + Com$(x) + "!"
      ProcessCommand (Com$(x))
      pwsofar$ = ""
      VBWsk1.Data = Chr$(13) + Chr$(10) + "Done-closing connection."
      VBWsk1.Socket = -1
      VBWsk1.Port = portnum
      VBWsk1.Host = ""
    End If
  Next x
End Sub

