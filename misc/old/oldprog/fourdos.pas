Unit FourDos;

Interface

uses crt,dos,screen;

Procedure FourDosBox(left,top,right,bottom:integer;title:string;ScrollBar:boolean);

Implementation

Procedure FourDosBox(left,top,right,bottom:integer;title:string;ScrollBar:boolean);
  {Draw a box, with title centered}
  var
    x,y,center: Integer;

	begin
    if length(title)>right-left-2 then
      title:=copy(title,1,right-left-2);
    textColor(black);
    textBackGround(white);
    GotoXY (left,top);
    write('�');
    for x:=left+1 to right-1 do
      write('�');
    write('�');
    center:=(right-left-2-length(title)) div 2;
    GotoXY (left+center+1,top);
    writeln(title);
    for x:=top+1 to bottom-1 do
      begin
        gotoXY (left,x);
        write('�');
        for y:=left+1 to right-1 do
          write(' ');
        if ScrollBar=false then
          begin
            write('�');
          end
        else
          begin
            textBackGround(black);
            textColor(LightGray);
            if x=top+1 then write(chr(24));
            if x=bottom-1 then write(chr(25));
						if (x<>top+1) and (x<>bottom-1) then write(chr(178));
            textColor(black);
            textBackGround(white);
          end;
      end;
      GotoXY (left,bottom);
    write('�');
    for x:=left+1 to right-1 do
      write('�');
    write('�');
    Shadow (left+1,bottom+1,right+1,top+1,0);
  end;
end.
