VERSION 2.00
Begin Form Hotel 
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   1125
   ClientLeft      =   2775
   ClientTop       =   2295
   ClientWidth     =   2535
   Height          =   1530
   Icon            =   HOTEL.FRX:0000
   Left            =   2715
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   ScaleHeight     =   1125
   ScaleWidth      =   2535
   Top             =   1950
   Width           =   2655
   WindowState     =   1  'Minimized
   Begin IPDAEMON IPDaemon1 
      EOL             =   ""
      InBufferSize    =   2048
      Left            =   2100
      Linger          =   -1  'True
      OutBufferSize   =   2048
      Port            =   0
      Top             =   60
   End
   Begin Label Label2 
      Alignment       =   2  'Center
      Caption         =   "Hotel 1.01a"
      FontBold        =   -1  'True
      FontItalic      =   0   'False
      FontName        =   "MS Sans Serif"
      FontSize        =   13.5
      FontStrikethru  =   0   'False
      FontUnderline   =   0   'False
      ForeColor       =   &H000000FF&
      Height          =   315
      Left            =   120
      TabIndex        =   1
      Top             =   60
      Width           =   2235
   End
   Begin Label Label1 
      Alignment       =   2  'Center
      Caption         =   "Written By Joey Hess <jeh22@cornell.edu>"
      FontBold        =   -1  'True
      FontItalic      =   0   'False
      FontName        =   "MS Sans Serif"
      FontSize        =   9.75
      FontStrikethru  =   0   'False
      FontUnderline   =   0   'False
      Height          =   555
      Left            =   60
      TabIndex        =   0
      Top             =   480
      Width           =   2415
      WordWrap        =   -1  'True
   End
End
DefInt A-Z
Dim Shared lf

Sub Form_Load ()
  lf = FreeFile
  Open "hotel.log" For Output As #lf
End Sub

Sub Form_Unload (Cancel As Integer)
  hotel.IPDaemon1.Listening = False
End Sub

Sub IPDaemon1_Connected (ConnectionID As Integer, StatusCode As Integer, Description As String)
  NumActive = NumActive + 1
  UpdateCaption
  CurrentUser = GetFreeTerm()
  ClearTerm (CurrentUser)
  Term(CurrentUser).active = True
  Term(CurrentUser).ConnectionID = ConnectionID
  ClearUser (CurrentUser)
  NegotiateIAC will, echo
  Runscript "logon.scr"
  Poppin
End Sub

'get data from a term, and act on it by popping the first command off the stack and running it.
'
Sub IPDaemon1_DataIn (ConnectionID As Integer, text As String, EOL As Integer)
  'first, cook the data - filter out all the IAC codes and lf's.
  
  CurrentUser = LookUpTerm(ConnectionID)
  
  If CurrentUser > -1 Then
    a$ = text$
    i = InStr(a$, Chr$(10))
    Do While i > 0 'keep looking for line feeds
      a$ = Mid$(a$, 1, i - 1) + Mid$(a$, i + 1)
      i = InStr(a$, Chr$(10))
    Loop
    'CBL = Len(CookedBuffer$(CurrentUser))
    CookedBuffer$(CurrentUser) = CookedBuffer$(CurrentUser) + a$
    l = Len(text) - 2
    i = InStr(CookedBuffer$(CurrentUser), Chr$(IAC))
    Do While i <= l And i > 0 'keep looking for the IAC char.
      ProcessIAC Mid$(CookedBuffer$(CurrentUser), i + 1, 2)
      CookedBuffer$(CurrentUser) = Mid$(CookedBuffer$(CurrentUser), 1, i - 1) + Mid$(CookedBuffer$(CurrentUser), i + 3)
      i = InStr(CookedBuffer$(CurrentUser), Chr$(IAC))
    Loop
    'If CBL <= Len(CookedBuffer(CurrentUser)) And Len(CookedBuffer(CurrentUser)) > 0 Then
    'there is still text left after cooking? if so, process commands
    If Len(CookedBuffer(CurrentUser)) > 0 Then Poppin
    'End If
  End If
End Sub

Sub IPDaemon1_Disconnected (ConnectionID As Integer, StatusCode As Integer, Description As String)
  t = LookUpTerm(ConnectionID)
  If t > -1 Then
    ClearTerm (t)
  End If
  NumActive = NumActive - 1
  UpdateCaption
End Sub

Sub wsk_Read (Index As Integer)
  'a$ = wsk(Index).Data
  'Print #lf, a$
  'UserInput$(Index) = UserInput$(Index) + change$(a$, Chr$(10), "")
End Sub

