VERSION 2.00
Begin Form Form1 
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   4410
   ClientLeft      =   1545
   ClientTop       =   1830
   ClientWidth     =   7110
   Height          =   5100
   Left            =   1485
   LinkTopic       =   "Form1"
   ScaleHeight     =   4410
   ScaleWidth      =   7110
   Tag             =   "Hotel Editor - "
   Top             =   1200
   Width           =   7230
   Begin PictureBox b 
      BorderStyle     =   0  'None
      Height          =   4500
      Index           =   1
      Left            =   0
      ScaleHeight     =   4500
      ScaleWidth      =   7200
      TabIndex        =   17
      TabStop         =   0   'False
      Tag             =   "Menus"
      Top             =   0
      Visible         =   0   'False
      Width           =   7200
      Begin CheckBox Check2 
         Caption         =   "&Return to menu after command"
         Enabled         =   0   'False
         Height          =   195
         Left            =   3360
         TabIndex        =   35
         Top             =   3360
         Width           =   3555
      End
      Begin TextBox Text5 
         Enabled         =   0   'False
         Height          =   285
         Left            =   3360
         TabIndex        =   34
         Top             =   2940
         Width           =   3675
      End
      Begin CommandButton Command7 
         Caption         =   "&Remove"
         Enabled         =   0   'False
         Height          =   315
         Left            =   1080
         TabIndex        =   24
         Top             =   4020
         Width           =   1155
      End
      Begin CommandButton Command6 
         Caption         =   "&Add"
         Height          =   315
         Left            =   60
         TabIndex        =   23
         Top             =   4020
         Width           =   975
      End
      Begin TextBox Text4 
         Enabled         =   0   'False
         Height          =   285
         Left            =   3360
         TabIndex        =   32
         Top             =   1800
         Width           =   915
      End
      Begin TextBox Text3 
         Enabled         =   0   'False
         Height          =   285
         Left            =   3360
         TabIndex        =   33
         Top             =   2340
         Width           =   3675
      End
      Begin CommandButton Command5 
         Caption         =   "&Remove"
         Enabled         =   0   'False
         Height          =   315
         Left            =   2400
         TabIndex        =   31
         Top             =   4020
         Width           =   855
      End
      Begin CommandButton Command4 
         Caption         =   "&Add"
         Enabled         =   0   'False
         Height          =   315
         Left            =   2400
         TabIndex        =   30
         Top             =   3660
         Width           =   855
      End
      Begin ListBox List1 
         Enabled         =   0   'False
         Height          =   1980
         Left            =   2400
         TabIndex        =   29
         Top             =   1620
         Width           =   855
      End
      Begin TextBox Text2 
         Enabled         =   0   'False
         Height          =   285
         Left            =   3960
         TabIndex        =   27
         Top             =   540
         Width           =   3075
      End
      Begin CheckBox Check1 
         Caption         =   "&Redisplay menu if you return to it."
         Enabled         =   0   'False
         Height          =   255
         Left            =   2400
         TabIndex        =   28
         Top             =   900
         Width           =   3675
      End
      Begin CommandButton Command3 
         Caption         =   "&Edit..."
         Enabled         =   0   'False
         Height          =   280
         Left            =   5880
         TabIndex        =   26
         Top             =   180
         Width           =   1155
      End
      Begin TextBox Text1 
         Enabled         =   0   'False
         Height          =   285
         Left            =   3660
         TabIndex        =   25
         Top             =   180
         Width           =   2175
      End
      Begin DirListBox Dir1 
         Height          =   1605
         Left            =   60
         TabIndex        =   20
         Top             =   1980
         Width           =   2175
      End
      Begin FileListBox File1 
         Height          =   1590
         Left            =   60
         Pattern         =   "*.mnu"
         TabIndex        =   19
         Top             =   300
         Width           =   2175
      End
      Begin DriveListBox Drive1 
         Height          =   315
         Left            =   60
         TabIndex        =   22
         Top             =   3660
         Width           =   2175
      End
      Begin Line Line2 
         X1              =   2300
         X2              =   8060
         Y1              =   1260
         Y2              =   1260
      End
      Begin Label Label12 
         AutoSize        =   -1  'True
         Caption         =   "&Command:"
         Height          =   195
         Left            =   3360
         TabIndex        =   40
         Top             =   2700
         Width           =   885
      End
      Begin Line Line1 
         Index           =   0
         X1              =   2300
         X2              =   2300
         Y1              =   0
         Y2              =   5000
      End
      Begin Label Label11 
         Caption         =   "&Hotkey: (^x is control-x)"
         Height          =   195
         Left            =   3360
         TabIndex        =   39
         Top             =   1560
         Width           =   2355
      End
      Begin Label Label10 
         AutoSize        =   -1  'True
         Caption         =   "Required &Flags:"
         Height          =   195
         Left            =   3360
         TabIndex        =   38
         Top             =   2100
         Width           =   1350
      End
      Begin Label Label9 
         Caption         =   "&Commands:"
         Height          =   195
         Left            =   2340
         TabIndex        =   37
         Top             =   1320
         Width           =   975
      End
      Begin Label Label8 
         AutoSize        =   -1  'True
         Caption         =   "&Prompt to display:"
         Height          =   195
         Left            =   2400
         TabIndex        =   36
         Top             =   600
         Width           =   1530
      End
      Begin Label Label7 
         Caption         =   "&File to display:"
         Height          =   195
         Left            =   2400
         TabIndex        =   21
         Top             =   240
         Width           =   1275
      End
      Begin Label Label6 
         Caption         =   "&Menu Filename:"
         Height          =   195
         Left            =   60
         TabIndex        =   18
         Top             =   60
         Width           =   1515
      End
   End
   Begin PictureBox b 
      BorderStyle     =   0  'None
      Height          =   4455
      Index           =   2
      Left            =   0
      ScaleHeight     =   4455
      ScaleWidth      =   7095
      TabIndex        =   41
      TabStop         =   0   'False
      Tag             =   "Setup"
      Top             =   0
      Width           =   7095
      Begin TextBox Text6 
         Enabled         =   0   'False
         Height          =   315
         Left            =   2340
         TabIndex        =   1
         Top             =   1140
         Width           =   4695
      End
      Begin ListBox List2 
         Height          =   3540
         Left            =   60
         TabIndex        =   0
         Top             =   300
         Width           =   2175
      End
      Begin CommandButton Command9 
         Caption         =   "&Add"
         Height          =   315
         Left            =   60
         TabIndex        =   2
         Top             =   4020
         Width           =   975
      End
      Begin CommandButton Command8 
         Caption         =   "&Remove"
         Enabled         =   0   'False
         Height          =   315
         Left            =   1080
         TabIndex        =   3
         Top             =   4020
         Width           =   1155
      End
      Begin Label Label14 
         Caption         =   "WARNING: This program is still being written, and has knows bugs. Use at your own risk :-)                                  - Joey hess"
         Height          =   2175
         Left            =   2460
         TabIndex        =   44
         Top             =   1800
         Width           =   4335
         WordWrap        =   -1  'True
      End
      Begin Label Label13 
         BorderStyle     =   1  'Fixed Single
         Height          =   675
         Left            =   2340
         TabIndex        =   42
         Top             =   300
         Width           =   4695
         WordWrap        =   -1  'True
      End
      Begin Label Label17 
         AutoSize        =   -1  'True
         Caption         =   "&Server Setup Options:"
         Height          =   195
         Left            =   60
         TabIndex        =   43
         Top             =   60
         Width           =   1890
      End
   End
   Begin PictureBox b 
      BorderStyle     =   0  'None
      Height          =   4455
      Index           =   0
      Left            =   0
      ScaleHeight     =   4455
      ScaleWidth      =   7095
      TabIndex        =   13
      TabStop         =   0   'False
      Tag             =   "Users"
      Top             =   0
      Width           =   7095
      Begin ListBox users 
         Height          =   3540
         Left            =   60
         TabIndex        =   4
         Top             =   300
         Width           =   2175
      End
      Begin CommandButton Command2 
         Caption         =   "&Remove"
         Enabled         =   0   'False
         Height          =   315
         Left            =   1080
         TabIndex        =   10
         Top             =   4020
         Width           =   1155
      End
      Begin TextBox Flags 
         Enabled         =   0   'False
         Height          =   285
         Left            =   3000
         TabIndex        =   8
         Top             =   1320
         Width           =   3975
      End
      Begin TextBox Level 
         Enabled         =   0   'False
         Height          =   285
         Left            =   3000
         TabIndex        =   7
         Top             =   960
         Width           =   855
      End
      Begin TextBox Pw 
         Enabled         =   0   'False
         Height          =   285
         Left            =   3300
         TabIndex        =   6
         Top             =   600
         Width           =   3675
      End
      Begin TextBox UserName 
         Enabled         =   0   'False
         Height          =   285
         Left            =   3000
         TabIndex        =   5
         Top             =   240
         Width           =   3975
      End
      Begin CommandButton Command1 
         Caption         =   "&Add"
         Height          =   315
         Left            =   60
         TabIndex        =   9
         Top             =   4020
         Width           =   975
      End
      Begin Label Label5 
         AutoSize        =   -1  'True
         Caption         =   "&Flags:"
         Height          =   195
         Left            =   2400
         TabIndex        =   11
         Top             =   1380
         Width           =   525
      End
      Begin Label Label4 
         AutoSize        =   -1  'True
         Caption         =   "&Level:"
         Height          =   195
         Left            =   2400
         TabIndex        =   12
         Top             =   1020
         Width           =   540
      End
      Begin Label Label3 
         AutoSize        =   -1  'True
         Caption         =   "&Password:"
         Height          =   195
         Left            =   2400
         TabIndex        =   16
         Top             =   660
         Width           =   885
      End
      Begin Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "&Name:"
         Height          =   195
         Left            =   2400
         TabIndex        =   15
         Top             =   300
         Width           =   555
      End
      Begin Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "&Users:"
         Height          =   195
         Left            =   60
         TabIndex        =   14
         Top             =   60
         Width           =   555
      End
   End
   Begin Menu Exit 
      Caption         =   "Exit"
   End
   Begin Menu m 
      Caption         =   "Users"
      Index           =   0
   End
   Begin Menu m 
      Caption         =   "Menus"
      Index           =   1
   End
   Begin Menu m 
      Caption         =   "Setup"
      Index           =   2
   End
End
'Hotel setup editor. V 1.0a, (C) 1995 by Joey Hess.

DefInt A-Z
Dim Shared appath$
Dim Shared Setup_MainINI$(1 To 1000), Setup_Main(1 To 1000), Setup_Main_TotalItems, Numlevels
Dim Shared Userlevel(1 To 1000), UserFlag$(1 To 1000), UserPw$(1 To 1000), NumUsers
Dim Shared Menu_Flag$(500), Menu_Command$(500), Menu_Linger(500), CurrentKey

Sub Command1_Click ()
  If NumUsers <= 1000 Then
    NumUsers = NumUsers + 1
    users.AddItem ""
    users.ListIndex = users.ListCount - 1
    users_click
  Else
    Beep
  End If
End Sub

Sub Command3_Click ()
  a = Shell("notepad " + text1.Text, 1)
End Sub

Sub Command4_Click ()
  List1.AddItem ""
  List1.ListIndex = List1.ListCount - 1


End Sub

'Add a new menu file
'
Sub Command6_Click ()
  a$ = Mid$(LCase$(InputBox$("New Menu", "Enter the filename of the new menu.")), 1, 8)
  If Right$(a$, 4) <> ".mnu" Then a$ = a$ + ".mnu"
  f = FreeFile
  Open a$ For Output As #f
    Print #f,
    Print #f,
    Print #f,
  Close #f
  file1.FileName = a$
  file1_click
  file1.Pattern = "*.mnu"
  file1.Refresh
  
End Sub

'Add item to hotel.ini
'
'In practice, this adds a security level to the file.
'
Sub Command9_Click ()
  Setup_Main_TotalItems = Setup_Main_TotalItems + 1
  If Numlevels < 900 Then
    Numlevels = Numlevels + 1
    list2.AddItem CStr(Numlevels)
    Setup_MainINI$(Setup_Main_TotalItems) = CStr(Numlevels) + "="
    list2.ListIndex = list2.ListCount - 1
    list2_click
  Else
    Beep
  End If
End Sub

Sub Dir1_Change ()
  file1.Path = dir1.Path
End Sub

Static Sub Drive1_Change ()
  On Error GoTo bad_drive
  dir1.Path = drive1.Drive
  olddrive$ = drive1.Drive
drive_change_done:
  Exit Sub
bad_drive:
  drive1.Drive = olddrive$
  Resume drive_change_done
End Sub

Sub Exit_Click ()
  'save all modifications
  'save main ini file:
  list2_click
  f = FreeFile
  Open appath$ + "hotel.ini" For Output As #f
    For x = 1 To Setup_Main_TotalItems
      If x = 2 Then
        'print the number of user levels. ** Sorta screwey...maybe this will be fixed later..
        Print #f, "numlevels=" + CStr(Numlevels)
      End If
      Select Case Setup_Main(x)
      Case 0, 2
        Print #f, Setup_MainINI$(x)
      Case 1
        Print #f, ";" + Setup_MainINI$(x)
      End Select
    Next x
  Close #f
  'save user ini file
  users_click
  f = FreeFile
  Open appath$ + "user.ini" For Output As #f
    For x = 1 To NumUsers
      Print #f, "[" + users.List(x - 1) + "]"
      Print #1, "pw=" + UserPw$(x)
      Print #1, "flags=" + UserFlag$(x)
      Print #1, "level=" + CStr(Userlevel(x))
    Next x
  Close #f
  'save menu file
  list1_click
  file1_click
  End
End Sub

Static Sub file1_click ()
  ChDir file1.Path
  If OldFile$ <> "" Then
    'save last menu file.
    f = FreeFile
    Open OldFile$ For Output As #f
      Print #f, text1.Text
      Print #f, text2.Text
      Print #f, CStr(check1.Value)
      For x = 1 To List1.ListCount
        Print #1, List1.List(x - 1)
        Print #1, Menu_Command$(x)
        Print #1, Menu_Flag$(x)
        Print #1, CStr(Menu_Linger(x))
      Next x
    Close #f
  Else
    List1.Enabled = True
    command4.Enabled = True
    command3.Enabled = True
    text2.Enabled = True
    text1.Enabled = True
    check1.Enabled = True
    text4.Enabled = True
    text3.Enabled = True
    text5.Enabled = True
    check2.Enabled = True
  End If
  'load in a menu file, and let them edit it.
  List1.Clear
  f = FreeFile
  If file1.FileName <> "" Then
    OldFile$ = file1.Path
    If Right$(OldFile$, 1) <> "\" Then OldFile$ = OldFile$ + "\"
    OldFile$ = OldFile$ + file1.FileName
    Open OldFile$ For Input As #f
      Line Input #f, mf$
      text1.Text = mf$
      Line Input #f, mp$
      text2.Text = mp$
      Input #f, redisplay
      check1.Value = redisplay
      Do Until EOF(f)
        Line Input #f, a$
        List1.AddItem (a$)
        Line Input #f, a$
        Menu_Command$(List1.ListCount) = a$
        Line Input #f, a$
        Menu_Flag$(List1.ListCount) = a$
        Input #f, l
        Menu_Linger(List1.ListCount) = l
      Loop
    Close #f
    CurrentKey = -1
    If List1.ListCount > 0 Then List1.ListIndex = 0
    list1_click
  End If
End Sub

Sub Form_Load ()
  top = (screen.Height - Height) / 2
  left = (screen.Width - Width) / 2
  m_click (2)
  If Command$ = "" Then appath = app.Path Else appath = Command$
  If Right$(appath$, 1) <> "\" Then appath$ = appath$ + "\"
  ChDir Mid$(appath$, 1, Len(appath$) - 1)
  On Error GoTo Cant_Read_File
  'init setup values
  f = FreeFile
  fi$ = appath$ + "hotel.ini"
  Open fi$ For Input As #f
    Do Until EOF(f)
      Line Input #f, a$
      x = x + 1
      If LCase$(Left$(a$, 9)) <> "numlevels" Then
        Select Case Left$(a$, 1)
        Case "["
          Setup_Main(x) = 0
          list2.AddItem (a$)
          Setup_MainINI$(x) = a$
        Case ";"
          Setup_Main(x) = 1
          Setup_MainINI$(x) = Mid$(a$, 2)
        Case Else
          If InStr(a$, "=") > 0 Then
            Setup_Main(x) = 2
            list2.AddItem Mid$(a$, 1, InStr(a$, "=") - 1)
            Setup_MainINI$(x) = a$
          End If
        End Select
      Else
        Numlevels = Val(Mid$(a$, 11))
      End If
    Loop
  Close #f
  Setup_Main_TotalItems = x
  'init user values
  f = FreeFile
  fi$ = appath$ + "user.ini"
  Open fi$ For Input As #f
    Do Until EOF(f)
      Line Input #f, a$
      If Mid$(a$, 1, 1) = "[" Then
        users.AddItem (Mid$(a$, 2, Len(a$) - 2))
        NumUsers = NumUsers + 1
      End If
      la$ = LCase$(a$)
      If InStr(la$, "level=") = 1 Then
        If NumUsers > 0 Then Userlevel(NumUsers) = Val(Mid$(a$, 7))
      End If
      If InStr(la$, "pw=") = 1 Then
        If NumUsers > 0 Then UserPw$(NumUsers) = Mid$(a$, 4)
      End If
      If InStr(la$, "flags=") = 1 Then
        If NumUsers > 0 Then UserFlag$(NumUsers) = Mid$(a$, 7)
      End If
    Loop
  Close #f
  'init menu editor
  CurrentKey = -1
  Exit Sub
Cant_Read_File:
  MsgBox "Unable to read file " + fi$ + ". Aborting!"
  Resume quit
quit:
  End
End Sub

Sub list1_click ()
  If CurrentKey <> -1 Then
    Menu_Linger(CurrentKey) = check2.Value
    List1.List(CurrentKey - 1) = text4.Text
    Menu_Flag$(CurrentKey) = text3.Text
    Menu_Command$(CurrentKey) = text5.Text
  End If
  CurrentKey = List1.ListIndex + 1
  text4.Text = List1.List(CurrentKey - 1)
  check2.Value = Menu_Linger(CurrentKey)
  text3.Text = Menu_Flag$(CurrentKey)
  text5.Text = Menu_Command$(CurrentKey)
End Sub

Static Sub list2_click ()
  If oli > 0 Then
    Setup_MainINI$(oli) = Mid$(Setup_MainINI$(oli), 1, InStr(Setup_MainINI$(oli), "=")) + text6.Text
  End If
  a$ = list2.List(list2.ListIndex)
  If Left$(a$, 1) = "[" Then
    label13.Caption = ""
    text6.Text = ""
    text6.Enabled = False
  Else
    For x = 1 To Setup_Main_TotalItems
      If InStr(Setup_MainINI$(x), "=") > 0 Then
        If a$ = Mid$(Setup_MainINI$(x), 1, InStr(Setup_MainINI$(x), "=") - 1) Then
          oli = x
          Exit For
        End If
      End If
      If Setup_Main(x) = 1 Then
        c$ = Setup_MainINI$(x)
      End If
    Next x
    label13.Caption = c$
    text6.Text = Mid$(Setup_MainINI$(x), InStr(Setup_MainINI$(x), "=") + 1)
    text6.Enabled = True
  End If
End Sub

'Change boxes
'
Static Sub m_click (Index As Integer)
  If Index <> old_m Then
    b(old_m).Visible = False
    b(Index).Visible = True
    old_m = Index
    Caption = Tag + b(Index).Tag
  End If
End Sub

Static Sub users_click ()
  If CurrentUser > 0 Then
    Userlevel(CurrentUser) = Val(level.Text)
    UserFlag$(CurrentUser) = flags.Text
    UserPw$(CurrentUser) = pw.Text
    users.List(CurrentUser - 1) = username.Text
  End If
  CurrentUser = users.ListIndex + 1
  If CurrentUser > 0 Then
    a$ = users.List(CurrentUser - 1)
    level.Enabled = True
    flags.Enabled = True
    pw.Enabled = True
    username.Enabled = True
    level.Text = CStr(Userlevel(CurrentUser))
    flags.Text = UserFlag$(CurrentUser)
    pw.Text = UserPw$(CurrentUser)
    username.Text = users.List(CurrentUser - 1)
  End If
End Sub

