'
'Hotel 1.01a, (C) 1995 by Joey Hess
'
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
DefInt A-Z

'data abstraction:

Type UserType
  name As String * 200
  pw As String * 200
  level As Integer
  flags As String * 200
End Type

Type TermType
  active As Integer
  height As Integer
  width As Integer
  LinesUsed As Integer
  echo As Integer
  emulation As Integer
  negotiating As String * 255
  ConnectionID As Integer
End Type

Declare Function GetPrivateProfileString Lib "Kernel" (ByVal lpApplicationName As String, ByVal lpKeyName As Any, ByVal lpDefault As String, ByVal lpReturnedString As String, ByVal nSize As Integer, ByVal lpFilename As String) As Integer

Global Const ServerSection = "server", TextSection = "text", SecuritySection = "security", UserINI = "user.ini", MainINI = "hotel.ini"
Global Const title = "Hotel [", active = "active:", Idle = "idle]"
Global MaxUsers, portnum, NumLevels
Global user() As UserType
Global term() As TermType
Global NumActive, CurrentUser
Global LevelFlag$()
Global FileLoc$, LineEnd$, bs$
Global NamePrompt$, PasswordPrompt$, BadName$, BadPassword$, GoodPassword$

Global stack$() 'the stacks are grown as needed, as are lot of these things.
Global stack_state(), sstate
Global StackTop(), HighestStackVal

Global CookedBuffer$() 'not just a buffer, this holds lines we've just been editing, characters we just hit, etc, in _processed_ form for use by prodecures.
Global LastStringGot$() 'the last string getstring$() returned, for each user

Global Cont()

'Set up a default term.
'
Sub ClearTerm (t)
  term(t).active = False

  term(t).height = 23
  term(t).width = 80
  term(t).LinesUsed = 0
  term(t).echo = False
  term(t).emulation = 0
  term(t).negotiating = Space$(255)
  term(t).ConnectionID = 0
  StackTop(t) = 0
  CookedBuffer$(t) = ""
End Sub

'Set up a blank user.
'
Sub ClearUser (u)
  user(u).name = "guest"
  user(u).pw = ""
  user(u).level = 0
  user(u).flags = Space$(200)
End Sub

'Remove backapces from a$, as well as the character
'immediately before them, moving left-to-right
'
Function FilterBackSpace$ (ByVal a$)
  i = InStr(a$, bs$)
  Do Until i = 0
    If i > 1 Then
      a$ = Mid$(a$, 1, i - 2) + Mid$(a$, i + 1)
    Else
      a$ = Mid$(a$, 2)
    End If
    i = InStr(a$, bs$)
  Loop
  FilterBackSpace$ = a$
End Function

'Find a terminal that is free, or return -1
'Set the free terminal to active
'
Function GetFreeTerm ()
  For x = 1 To MaxUsers
    If Not term(x).active Then
      GetFreeTerm = x
      term(x).active = True
      Exit Function
    End If
  Next x
  GetFreeTerm = -1
End Function

'Swiped from Randolph Chung <rc42@cornell.edu> (modified)
'
Function GetINIStr (ByVal File$, ByVal Section$, ByVal Key$, ByVal Default$) As String
  Dim Temp As String * 200
  Dim dummy%

  dummy% = GetPrivateProfileString(Section$, Key$, Default$, Temp$, 200, File$)
  GetINIStr = stripNull(Temp$)
End Function

'Read num characters from the term.
'If blocking=true, we'll loop here until that many chars
'are inputted, or someone disconnects.
'
'Otherwise, we'll return num chars, or the number of chars we have
'in the buffer right now, if less than n
'
Function in$ (num, blocking)
'  l = Len(UserInput$(CurrentUser))
'  If l >= num Or Not blocking Then 'return first n chars in buffer
'    Do Until x = num Or NumActive = 0
'      If Left$(UserInput$(CurrentUser), 1) = Chr$(IAC) Then
'        If Len(UserInput$(CurrentUser)) >= 3 Then 'IAC+xxx+xxx all here.
'          b$ = Mid$(UserInput$(CurrentUser), 2, 2)
'          UserInput$(CurrentUser) = Mid$(UserInput$(CurrentUser), 4)
'          ProcessIAC b$
'        ElseIf blocking Then 'not all here - wait for it
'          b$ = Mid$(UserInput$(CurrentUser), 2, 2) + in$(3 - Len(UserInput$(CurrentUser)), blocking)
'          UserInput$(CurrentUser) = Mid$(UserInput$(CurrentUser), 4)
'          ProcessIAC b$
'        Else 'not all here, leave till later
'          x = num
'        End If
'      ElseIf Len(UserInput$(CurrentUser)) >= 1 Or Not blocking Then
'        x = x + 1
'        a$ = a$ + Left$(UserInput$(CurrentUser), 1)
'        UserInput$(CurrentUser) = Mid$(UserInput$(CurrentUser), 2)
'      End If
'      t = DoEvents()
'    Loop
'    in$ = a$
'  ElseIf blocking Then  'wait for n chars to build up.
'    Do Until NumActive = 0 Or l >= num
'      l = Len(UserInput$(CurrentUser))
'      t = DoEvents()
'    Loop
'    'ok - now return it by recursive call.
'    If NumActive > 0 Then in$ = in$(num, blocking)
'  End If
End Function

'Load User Data From INI File
'Return False if cannot find user
'
'If it does, it fills the user record with the data.
'
Function LoadUser (n$)
  pw$ = GetINIStr(FileLoc$ + UserINI, n$, "pw", "")
  If pw$ <> "" Then
    LoadUser = True
    user(CurrentUser).name = n$
    user(CurrentUser).pw = pw$
    user(CurrentUser).level = Val(GetINIStr(FileLoc$ + UserINI, n$, "level", "1"))
    user(CurrentUser).flags = GetINIStr(FileLoc$ + UserINI, n$, "flags", Space$(200))
  Else
    LoadUser = False
  End If
End Function

'Given a connection ID, returns an index into the terminal array.
'
Function LookUpTerm (ConnectionID)
  LookUpTerm = -1
  For x = 1 To MaxUsers
    If term(x).ConnectionID = ConnectionID Then
      LookUpTerm = x
      Exit Function
    End If
  Next x
End Function

'You may specify the file location in command$
'
Sub Main ()
  hotel.Caption = title + "startup]"
  hotel.Show
  If Command$ <> "" Then FileLoc$ = Command$ Else FileLoc$ = app.Path
  If Right$(FileLoc$, 1) = "\" Then FileLoc$ = Mid$(FileLoc$, 1, Len(FileLoc$) - 1)
  ChDir FileLoc$
  FileLoc$ = FileLoc$ + "\"
  MaxUsers = Val(GetINIStr(FileLoc$ + MainINI, ServerSection, "maxusers", "1"))
  ReDim user(1 To MaxUsers)
  ReDim term(1 To MaxUsers)
  ReDim CookedBuffer$(1 To MaxUsers)
  ReDim stack$(1 To MaxUsers, 1 To 1)
  ReDim stack_state(1 To MaxUsers, 1 To 1)
  ReDim StackTop(1 To MaxUsers)
  ReDim Cont(1 To MaxUsers)
  ReDim LastStringGot(1 To MaxUsers)
  NumLevels = Val(GetINIStr(FileLoc$ + MainINI, ServerSection, "numlevels", "10"))
  ReDim LevelFlag$(0 To NumLevels)
  For x = 0 To NumLevels
    LevelFlag$(x) = GetINIStr(FileLoc$ + MainINI, SecuritySection, CStr(x), "")
  Next x
  portnum = Val(GetINIStr(FileLoc$ + MainINI, ServerSection, "PortNum", "23"))
  NamePrompt$ = GetINIStr(FileLoc$ + MainINI, TextSection, "NamePrompt", "")
  PasswordPrompt$ = GetINIStr(FileLoc$ + MainINI, TextSection, "PasswordPrompt", "")
  BadName$ = GetINIStr(FileLoc$ + MainINI, TextSection, "BadName", "")
  BadPassword$ = GetINIStr(FileLoc$ + MainINI, TextSection, "BadPassword", "")
  GoodPassword$ = GetINIStr(FileLoc$ + MainINI, TextSection, "GoodPassword", "")
  'Set up defaults on all users and terms
  For x = 1 To MaxUsers
    ClearUser (x)
    ClearTerm (x)
  Next x
  NumActive = 0
  LineEnd$ = Chr$(13) + Chr$(10)
  bs$ = Chr$(8)
  'listen for connects
  hotel.IPDaemon1.Port = portnum
  hotel.IPDaemon1.Listening = True
  UpdateCaption
End Sub

'Send data to user.
'
Sub out (a$)
  If NumActive > 0 Then hotel.IPDaemon1.DataToSend(term(CurrentUser).ConnectionID) = a$
End Sub

'Pop a command off the stack belonging to the current user, and return it.
'
'also set the state global variable to the state of the command
'
Function PopStack$ ()
  'get the value
  PopStack$ = stack$(CurrentUser, StackTop(CurrentUser))
  sstate = stack_state(CurrentUser, StackTop(CurrentUser))
  'shrink the stack - maybe we can shrink the actual array here.
  If StackTop(CurrentUser) <= HighestStackVal - 5 Then
    'are we using a bigger stack than anyone else?
    ok = True
    For x = 1 To MaxUsers
      If x <> CurrentUser And StackTop(x) > HighestStackVal - 5 Then ok = False
    Next x
    If ok And StackTop(CurrentUser) - 1 > 0 Then
      'shrink everyone's stack.
      HighestStackVal = StackTop(CurrentUser) - 1
      ReDim stack$(1 To MaxUsers, 1 To HighestStackVal)
      ReDim stack_state(1 To MaxUsers, 1 To HighestStackVal)
    End If
  End If
  StackTop(CurrentUser) = StackTop(CurrentUser) - 1
End Function

'Push a command onto the stack beloning to the current user, along with the
'state that command is in (how far it already processed).
'
Sub PushStack (a$, n)
  StackTop(CurrentUser) = StackTop(CurrentUser) + 1
  If StackTop(CurrentUser) > HighestStackVal Then
    'grow the stack, in increments of 5
    HighestStackVal = StackTop(CurrentUser) + 5
    ReDim Preserve stack$(1 To MaxUsers, 1 To HighestStackVal)
    ReDim Preserve stack_state(1 To MaxUsers, 1 To HighestStackVal)
  End If
  stack$(CurrentUser, StackTop(CurrentUser)) = a$
  stack_state(CurrentUser, StackTop(CurrentUser)) = n
End Sub

'Swiped from Randolph Chung <rc42@cornell.edu>
'
Function stripNull (txt$) As String
  If InStr(txt$, Chr$(0)) > 0 Then stripNull = Left$(txt$, InStr(txt$, Chr$(0)) - 1) Else stripNull = txt$
End Function

'Make the caption reflect the current #of users.
'
Sub UpdateCaption ()
  If NumActive = 0 Then c$ = title + Idle Else c$ = title + active + CStr(NumActive) + "]"
  If c$ <> hotel.Caption Then hotel.Caption = c$
End Sub

'Does user n$ exist in the userbase?
'
Function UserExists (n$)
  UserExists = (UserPassword$(n$) <> "")
End Function

'does the user own all flags in f$?
'
Function UserMatchesFlags (ByVal f$)
  uf$ = LevelFlag$(user(CurrentUser).level) + " " + Trim(user(CurrentUser).flags) + " "
  f$ = Trim$(f$)
  UserMatchesFlags = False
  Do While f$ <> ""
    a$ = Nextword$(f$)
    If Left$(a$, 1) <> "-" Then 'not dealing with a - flag.
      i = InStr(uf$, a$ + " ")
      If i = 0 Then Exit Function
      If i > 1 Then If Mid$(uf$, i - 1, 1) = "-" Then Exit Function
    Else 'They shouldn't have this flag.
      If InStr(uf$, Mid$(a$, 2) + " ") > 0 Then Exit Function
    End If
  Loop
  UserMatchesFlags = True
End Function

'Return the password of user n$
'
Function UserPassword$ (n$)
  UserPassword$ = GetINIStr(FileLoc$ + UserINI, n$, "pw", "")
End Function

'Waits for a keypress from user, and returns it
'
Function WaitForKey$ ()
  WaitForKey$ = in$(1, True)
End Function

'display 1 line of text for user ~ is changed to CRLF.
'
Sub WriteLine (ByVal a$)
  out change$(a$, "~", LineEnd$)
End Sub

