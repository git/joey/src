'Telnet negotianion codes module.
DefInt A-Z
Global Const IAC = 255, will = 251, wont = 252, do_ = 253, dont = 254
Global Const echo = 1

'Mostly, just answer "won't" to do's.
'
'IAK! Phfft! :-)
'
Sub ProcessIAC (a$)
  i = Asc(a$)
  j = Asc(Mid$(a$, 2))
  neg = Asc(Mid$(term(CurrentUser).negotiating, j, 1))
  If neg = 0 Or neg = 32 Then 'new request
    Select Case i
    Case will '"I will do j,ok?"
      Select Case j
      Case echo
        term(CurrentUser).echo = False
        r$ = Chr$(do_) + Chr$(j)
      Case Else
        r$ = Chr$(dont) + Chr$(j)
      End Select
    Case do_ '"You please do j,ok?"
      Select Case j
      Case echo
        term(CurrentUser).echo = True
        r$ = Chr$(will) + Chr$(j)
      Case Else
        r$ = Chr$(wont) + Chr$(j)
      End Select
    End Select
  ElseIf neg = will Then 'we told them we would do something.
    Select Case i
    Case dont '"please, don't do that"
      Select Case j
      Case echo
        term(CurrentUser).echo = False
      End Select
    Case do_ '"please do!"
      Select Case j
      Case echo
        term(CurrentUser).echo = True
      End Select
    End Select
  ElseIf neg = do_ Then 'we asked them to do something
    Select Case i
    Case will '"Sure, I'll do that."
      Select Case j
      Case echo
        term(CurrentUser).echo = False
      End Select
    Case wont '"Sorry, I won't do that."
      Select Case j
      Case echo
        term(CurrentUser).echo = True
      End Select
    End Select
  End If
  If r$ <> "" Then out Chr$(IAC) + r$ 'reply
End Sub

'Tell them will/do option
'
Sub NegotiateIAC (how, num)
  out Chr$(IAC) + Chr$(how) + Chr$(num) 'send request
  Mid$(term(CurrentUser).negotiating, num, 1) = Chr$(how) 'remember we're negotiating this option right now.
End Sub

