'Abra extentions file.
'Modified for TelnetServer

'Added Commands:
'DisplayFile
'Logout
'Prompt (or WriteLine)
'Pause
'ProcessMenu - this is the big one :-)
'Logon

DefInt A-Z


'display the file to user, pausing every 23 lines
'
Sub DisplayFile (f$)
  n = FreeFile
  Open f$ For Input As #n
    Do Until EOF(n) Or NumActive = 0
      Line Input #n, a$
      WriteLine a$ + LineEnd$
    Loop
  Close #n
End Sub

'Inputs a string over the network, with interactive editing.
'
'States: 0 - just starting
'        1 - continuing an edit
'
'If the first char of p$=chr$(1) then we are in password edit mode.
'
'Returns the string as entered so far.
'
Sub GetString (p$)
  'read data from cookedbuffer, until we hit an enter.
  'process backspace characters out.
  'echo it to the screen if echo is on.
  
  If sstate = 0 Then LastStringGot$(CurrentUser) = ""
  If Left$(p$, 1) = Chr$(1) Then password = True
  i = InStr(CookedBuffer$(CurrentUser), Chr$(13))
  If i > 0 Then
    done = True
    c$ = Mid$(CookedBuffer$(CurrentUser), 1, i - 1)
    CookedBuffer$(CurrentUser) = Mid$(CookedBuffer$(CurrentUser), i + 1)
  Else
    done = False
    c$ = CookedBuffer$(CurrentUser)
    CookedBuffer$(CurrentUser) = ""
  End If
  LastStringGot$(CurrentUser) = FilterBackSpace$(LastStringGot$(CurrentUser) + c$)
  If term(CurrentUser).echo = True Then
    If password = True Then out String$(Len(c$), "*") Else out c$
  End If
  If done Then
    cont(CurrentUser) = True
    WriteLine "~"
  Else
    If password = True Then PushStack "_gs " + Chr$(1), 1 Else PushStack "_gs ", 1
    cont(CurrentUser) = False
  End If
End Sub

'"Global" Error handler
'
'Returns:
'3: quit processing now.
'4: rerun the command that caused the error
'5: resume next
'
Function HandleError (e)
  HandleError = MsgBox("Error: " + Error$(e) + a$, 2 Or 48, "Error")
End Function

'Ask for name, if in database, ask for password.
'
'states: 0 - beginng login process
'        1 - just got name
'        2 - just got password
'
Sub Login (n$)
  Select Case sstate
  Case 0
    WriteLine NamePrompt$
    PushStack "_li", 1
    PushStack "_gs", 0 'get name
    cont(CurrentUser) = False
  Case 1
    If UserExists(LastStringGot$(CurrentUser)) = True Then
      WriteLine PasswordPrompt$
      PushStack "_li " + LastStringGot$(CurrentUser), 2
      PushStack "_gs " + Chr$(1), 0 'get password
      cont(CurrentUser) = False
    Else
      WriteLine BadName$ + "~"
      cont(CurrentUser) = True
    End If
  Case 2
    If UserPassword(n$) = LastStringGot$(CurrentUser) Then
      i = LoadUser(n$)
      WriteLine GoodPassword$ + "~"
    Else
      WriteLine BadPassword$ + "~"
    End If
    cont(CurrentUser) = True
  End Select
End Sub

'Logs user off - not really, just clear all their data.
'
Sub Logout ()
  'ClearTerm (CurrentUser)
End Sub

'Display press any key to continue prompt
'
'states: 0 - display prompt
'        1 - got the keypress - continue with next command
'
Sub Pause ()
  If sstate <> 1 Then
    out (LineEnd$ + "Press any key to continue..")
    'wait for a keypress.
    cont(CurrentUser) = False
    PushStack "_p", 1
  Else
    'trim off the keypress from thebuffer
    out LineEnd$
    CookedBuffer$(CurrentUser) = Mid$(CookedBuffer$(CurrentUser), 2)
  End If
End Sub

'keep popping commands off the stack until end of stack or
'until one of the commands sets cont to false
'
Sub Poppin ()
  done = False
  Do Until done
    If stacktop(CurrentUser) > 0 Then
      cont(CurrentUser) = True
      ProcessCommand PopStack()
      a = DoEvents()
    End If
    'change to the next active user
    n = 1
    x = CurrentUser + 1
    If x > MaxUsers Then x = 1
    Do Until n > MaxUsers Or (term(x).active = True And stacktop(x) > 0 And (cont(x) = True Or CookedBuffer$(x) <> ""))
      n = n + 1
      x = x + 1
      If x > MaxUsers Then x = 1
    Loop
    If n > MaxUsers Then done = True Else CurrentUser = x
  Loop
End Sub
    

'Commands specific to your program can be added to Abra Here.
'
Function ProcessAppSpecificCommands (c$, p$)
  ProcessAppSpecificCommands = True
  If NumActive > 0 Then
    Select Case c$
    Case "displayfile", "_df"
      DisplayFile p$
    Case "logout"
      Logout
    Case "pause", "_p"
      Pause
    Case "processmenu", "_pm"
      ProcessMenu p$, sstate
    Case "writeline", "prompt", "_wl"
      WriteLine p$
    Case "login", "_li"
      Login p$
    Case "_gs"
      GetString p$
    Case Else
      ProcessAppSpecificCommands = False
    End Select
  End If
End Function

'Display a menu, get a keypress, and run a command based on it.
'
'm$ - the menu definition file to use.
'
'Menu file format:
'
'text file to load
'prompt
'redisplay menu after running commands on it? 1=true, 0=false
'prompt to display for menu
'menu item composed of 3 lines:
'   key - (^x is control x)
'   command
'   return to menu after porcessing command? 1=true, 0=false
'menu item...
'
'states:0=start by displaying menu
'       1=process keypress
'
Sub ProcessMenu (m$, state)
  f = FreeFile
  If sstate <> 1 Then
    'Display the text file and prompt associated with the menu.
    Open m$ For Input As #f
      Input #f, mf$
      Input #f, mp$
      If mf$ <> "" Then DisplayFile mf$
      If mp$ <> "" Then WriteLine mp$
    Close #f
    'go get me a keypress
    PushStack "_pm " + m$, 1
    cont(CurrentUser) = False
  Else
    '** putting this here isn't good - we need a menu cache, or something.
    'as it is, this is loaded time after time untill they hit something right.
    ReDim Key$(200), Run$(200), linger(200), Flag$(200)
    Open m$ For Input As #f
      Line Input #f, mf$
      Line Input #f, mp$
      Input #f, ReDisplay
      Do Until EOF(f)
        c = c + 1
        Input #f, a$
        If Len(a$) = 2 And Left$(a$, 1) = "^" Then a$ = Chr$(Asc(LCase$(Mid$(a$, 2))) - 96)
        Key$(c) = a$
        Input #f, a$
        Run$(c) = a$
        Input #f, a$
        Flag$(c) = a$
        Input #f, l
        linger(c) = l
      Loop
    Close #f
    
    'act on a keypress
    done = False
    a$ = Mid$(CookedBuffer$(CurrentUser), 1, 1)
    CookedBuffer$(CurrentUser) = Mid$(CookedBuffer$(CurrentUser), 2)
    For x = 1 To c
      If Key$(x) = a$ Then
        If UserMatchesFlags(Flag$(x)) Then
          'ok, they can do this..
          done = True
          If mp$ <> "" Then
            If term(CurrentUser).echo = True Then WriteLine a$
            WriteLine "~"
          End If
          'shall we re-display later?
          If linger(x) = 1 Then
            PushStack "_pm " + m$, 0 'just push running all this again onto the stack.
          End If
          PushStack (Run$(x)), 0
        End If
      End If
    Next x
    If done = False Then
      'oh, no - we couldn't use their keystroke. Well, try again..
      PushStack "_pm " + m$, 1
      cont(CurrentUser) = False
    End If
  End If
End Sub

'Push all commands in the file onto the stack,
'in reverse order, of course..
'
Sub Runscript (a$)
  ReDim temp$(1 To 100) '<--limit to # of lines in a script
  If Not Scriptfile(a$) Then a$ = a$ + ScriptExt 'make sure it ends with the correct .ext
  f = FreeFile
  Open a$ For Input As #f
  Do Until EOF(f)
    Line Input #f, b$
    i = i + 1
    temp$(i) = b$
  Loop
  Close #f
  For x = i To 1 Step -1
    PushStack temp$(x), 0
  Next x
End Sub

