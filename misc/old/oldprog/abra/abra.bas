'Abra Windows Scripting Library. Written By Joey Hess.
'
'Imprortant:
'o To use the timers and schedules, you must have a form named "main",
'   which has a timer control named timer1 on it.
'   If you won't be using this, remove the definitions for
'   timers and schedules from the AbraAdd file for your program.
'o If you want to extend Abra for your specific application,
'   create a ProcessAppSpecificCommands function that
'   takes a command and its parameters (c$,p$) and returns
'   true if it can process the command.
'   Otherwise, comment the if statement that calls this procedure in
'   ProcessCommand
'o You will need to create an error handling function
'   named HandleError that takes an error number, and returns:
'   3: quit processing now.
'   4: rerun the command that caused the error
'   5: resume next
'o You will also need a runscript sub. You can modify the one in abradd however you like.
'o The file AbraAdd.bas contains basic definitions for the 3 procedures
'   above.
'
'Commands accepted (*=future)
'run
'cd
'exitwin
'message
'exit
'sendkeys
'winmove
'winsize
'winmovesize
'timer
'schedule
'ontop
'winactivate
'wait
'winclose
'*playsound (why not :-)
'copy
'ren
'del
'md
'rd
'
DefInt A-Z

Type rect
  left As Integer
  Top As Integer
  right As Integer
  bottom As Integer
End Type

Declare Sub SetWindowPos Lib "User" (ByVal hwnd As Integer, ByVal hWndInsertAfter As Integer, ByVal x As Integer, ByVal y As Integer, ByVal cx As Integer, ByVal cy As Integer, ByVal wFlags As Integer)
Global Const swp_nomove = 2, swp_nosize = 1, hwnd_topmost = -1, hwnd_notopmost = -2
Declare Function ExitWindows Lib "User" (ByVal dwReturnCode As Long, ByVal wReserved As Integer) As Integer
Declare Function ExitWindowsExec Lib "User" (ByVal lpszExe As String, ByVal lpszParams As String) As Integer
Declare Function WinExec Lib "Kernel" (ByVal lpCmdLine As String, ByVal nCmdShow As Integer) As Integer
Declare Function FindWindow Lib "User" (ByVal lpClassName As Any, ByVal lpWindowName As Any) As Integer
Declare Sub GetWindowRect Lib "User" (ByVal hwnd As Integer, lpRect As rect)
Declare Sub MoveWindow Lib "User" (ByVal hwnd As Integer, ByVal x As Integer, ByVal y As Integer, ByVal nWidth As Integer, ByVal nHeight As Integer, ByVal bRepaint As Integer)
Declare Function GetFreeSpace Lib "Kernel" (ByVal wFlags As Integer) As Long
Declare Function GetFreeSystemResources Lib "User" (ByVal fuSysResource As Integer) As Integer
Declare Function SetActiveWindow Lib "User" (ByVal hwnd As Integer) As Integer
Declare Function SendMessage Lib "User" (ByVal hwnd As Integer, ByVal wMsg As Integer, ByVal wParam As Integer, lParam As Any) As Long
Global Const WM_SYSCOMMAND = &H112, SC_CLOSE = &HF060

Global Const ScriptExt = ".scr"
Global Const sys_date = "%date%", sys_time = "%time%", sys_mem = "%mem%", sys_sys = "%sys%", sys_gdi = "%gdi%", sys_user = "%user%"
Global waiting

'Change to the given dir.
'
'Lets you specify either "c:\windows\"
'or "c:\windows".
'
Sub Cd (ByVal a$)
  If Right$(a$, 1) = "\" Then a$ = Mid$(a$, 1, Len(a$) - 1)
  If a$ = "" Then a$ = "\"
  If Right$(a$, 1) = ":" Then a$ = a$ + "\"
  ChDir a$
End Sub

'Change any occurences of changefrom$ in in$ to changeto$
'
Function change$ (ByVal in$, changefrom$, changeto$)
  Do While in$ <> ""
    p = InStr(in$, changefrom$)
    If p > 0 Then
      out$ = out$ + Mid$(in$, 1, p - 1) + changeto$
      change$ = out$
      in$ = Mid$(in$, p + Len(changefrom$))
    Else
      out$ = out$ + in$
      change$ = out$
      in$ = ""
    End If
  Loop
End Function

'Like it says...
'
Sub Copy (p$)
  a$ = nextword$(p$)
  FileCopy a$, p$
End Sub

'delete file
'
Sub Del (p$)
  Kill p$
End Sub

'Exit windows (and run a file, reboot, or restart windows)
'
'Syntax:
'exitwin [restart|reboot|run program]
'
'Warning: _does_not_ prompt.
'
Sub ExitWin (ByVal a$)
  Dim prog As String * 127, param As String * 126
  n$ = nextword$(a$)
  Select Case LCase$(n$)
  Case "restart"
    x = ExitWindows(66, 0)
  Case "reboot"
    x = ExitWindows(67, 0)
  Case "run"
    prog = nextword$(a$)
    param = a$
    x = ExitWindowsExec(prog, param)
  Case Else 'default is just exit
    x = ExitWindows(0, 0)
  End Select
End Sub

Function FreeMemory$ ()
  FreeMemory$ = CStr(GetFreeSpace(0) \ 1024)
End Function

'Return free system resources as a %
'
Function FreeSys$ (t)
  FreeSys$ = CStr(GetFreeSystemResources(t))
End Function

Function GetHWND (ByVal winname$)
  GetHWND = FindWindow(0&, winname$)
End Function

'Make the window with the given handle not always on top.
'
Sub MakeOffTop (hwnd)
  Call SetWindowPos(hwnd, hwnd_notopmost, 0, 0, 0, 0, swp_nomove Or swp_nosize)
End Sub

'Make the window with the given handle on-top
'
Sub MakeOnTop (ByVal hwnd)
  Call SetWindowPos(hwnd, hwnd_topmost, 0, 0, 0, 0, swp_nomove Or swp_nosize)
End Sub

'make directory
'
Sub Md (p$)
  MkDir p$
End Sub

'Display the message on the screen, after processing any system variables.
'
Sub message (a$)
  MsgBox ProcessSystemVariable$(a$)
End Sub

'1=move, 2=size, 3=move and size
'
'
'winmove xpos ypos name
'
'winsize width height name
'
'winmovesize xpos ypos width height name
'
Sub MoveSize (ByVal a$, todo)
  Dim r As rect
  x = Val(nextword$(a$))
  y = Val(nextword$(a$))
  If todo <> 3 Then
    h = GetHWND(a$)
    If h > 0 Then
      GetWindowRect h, r
      Select Case todo
      Case 1 'move
	MoveWindow h, x, y, r.right - r.left, r.bottom - r.Top, True
      Case 2 'size
	MoveWindow h, r.left, r.Top, x, y, True
      End Select
    End If
  Else
    x2 = Val(nextword$(a$))
    y2 = Val(nextword$(a$))
    h = GetHWND(a$)
    If h > 0 Then MoveWindow h, x, y, x2, y2, True
  End If
End Sub

Function nextword$ (a$)
  If Right$(a$, 1) <> " " Then a$ = a$ + " "
  nextword$ = Mid$(a$, 1, InStr(a$, " ") - 1)
  a$ = Trim$(Mid$(a$, InStr(a$, " ") + 1))
End Function

'
'ontop [no] winname
'
Sub OnTop (ByVal a$)
  b$ = nextword$(a$)
  If b$ = "no" Then
    h = GetHWND(a$)
    If h > 0 Then MakeOffTop (h)
  Else
    h = GetHWND(b$ + " " + a$)
    If h > 0 Then MakeOnTop (h)
  End If
End Sub

'Look at the command, and decide what to do.
'
Sub ProcessCommand (ByVal c$)
  If waiting Then Exit Sub
  If Left$(c$, 1) = ";" Then Exit Sub
  n$ = c$
  Do Until n$ = ""
    i = InStr(n$ + "^", "^")
    If i > 0 Then
      c$ = Trim$(Mid$(n$, 1, i - 1))
      n$ = Mid$(n$, i + 1)
    End If
    If Trim$(c$) = "" Then Exit Sub
Top:
    a$ = LCase$(nextword$(c$))
    'On Error GoTo Handler
    Select Case a$
    Case "cd"
      Cd c$
    Case "run"
      RunProg c$
    Case "exitwin"
      ExitWin c$
    Case "message"
      message c$
    Case "sendkeys"
      SendKey c$
    Case "winmove"
      MoveSize c$, 1
    Case "winsize"
      MoveSize c$, 2
    Case "winactivate"
      WinActivate c$
    Case "winmovesize"
      MoveSize c$, 3
    Case "winclose"
      WinClose c$
    Case "exit"
      End
    Case "wait"
      Wait_ c$
    Case "ontop"
      OnTop c$
    Case "copy"
      Copy c$
    Case "ren"
      Ren c$
    Case "del"
      Del c$
    Case "md"
      Md c$
    Case "rd"
      RD c$
    Case Else
      'run the function ProcessAppSpecificCommands
      'if there is no suche procedure, you should comment out
      'the if and end if statements.
      If Not ProcessAppSpecificCommands(a$, c$) Then
	'default is to try to run it.
	RunProg a$ + " " + c$
      End If
    End Select
  Loop
PC_done:
  Exit Sub
Handler:
  ii = HandleError("Error number " + CStr(Err) + " in command " + n$)
  Resume PC_done
End Sub

'Changes system variables %xxx% to their current values.
'
'Do this as fast as possible - it might be run hundreds of times per min!
'
Function ProcessSystemVariable$ (ByVal a$)
  If InStr(a$, "%") > 0 Then
    If InStr(a$, sys_date) > 0 Then a$ = change(a$, sys_date, Format$(Date$, "short date"))
    If InStr(a$, sys_time) > 0 Then a$ = change(a$, sys_time, Format$(Time$, "h:mmA/P"))
    If InStr(a$, sys_mem) > 0 Then a$ = change(a$, sys_mem, FreeMemory$())
    If InStr(a$, sys_sys) > 0 Then a$ = change(a$, sys_sys, FreeSys$(1))
    If InStr(a$, sys_gdi) > 0 Then a$ = change(a$, sys_gdi, FreeSys$(2))
    If InStr(a$, sys_user) > 0 Then a$ = change(a$, sys_user, FreeSys$(3))
  End If
  ProcessSystemVariable$ = a$
End Function

'remove directory
'
Sub RD (p$)
  RmDir p$
End Sub

'rename file or directory
'
Sub Ren (p$)
  a$ = nextword$(p$)
  Name a$ As p$
End Sub

'Runs the passed program.
'If a path is specified (c:\windows\progman.exe),
'it will change dirs to that, first.
'
'If its not a program, but a winscript (.scr), we'll run
'that, instead.
'
'if preceeded by a @dir, it will change to dir.
'
'complete syntax:
'[+|-|*] [@dir] [path]program[.exe|.scr] [params]
'
'+ = full screen  (3)
'- = icon         (6) (without focus)
'* = hidden       (0)
'
Sub RunProg (ByVal dat$)
  If waiting Then Exit Sub
  a$ = nextword$(dat$)
  If a$ = "+" Then
    a$ = nextword$(dat$)
    winstyle = 3
  ElseIf a$ = "-" Then
    a$ = nextword$(dat$)
    winstyle = 6
  ElseIf a$ = "*" Then
    a$ = nextword$(dat$)
    winstyle = 0
  Else
    winstyle = 1
  End If
  
  If Left$(a$, 1) = "@" Then
    ChD$ = Mid$(a$, 2)
    a$ = nextword$(dat$)
  End If

  n = InStr(a$, "\")
  Do While n > 0
    p$ = p$ + Mid$(a$, 1, n)
    a$ = Mid$(a$, n + 1)
    n = InStr(a$, "\")
  Loop
  
  If ChD$ <> "" Then 'change dir here
    Cd ChD$
  Else
    If p$ <> "" Then Cd p$
  End If
  
  If Scriptfile(a$) Then
    'run the script
    RunScript p$ + a$
  Else
    'run the program here
    success = WinExec(p$ + a$ + " " + dat$, winstyle)
    If success < 32 Then
      Error 53
    End If
  End If
End Sub

'Tests to see if this file ends in .scr
'
Function Scriptfile (a$)
  If InStr(LCase$(a$), ScriptExt) > 0 Then Scriptfile = True Else Scriptfile = False
End Function

'Passes keystrokes to an application, using VB sendkeys.
'
'*Not implemented: activate a specified application to
'send keys to.
'
'Currrently sends to the active application.
'
'Syntax:
'Sendkeys [wait] keys
'
Sub SendKey (ByVal a$)
  b$ = a$
  Wait$ = nextword$(a$)
  If LCase$(Wait$) = "wait" Then SendKeys a$, True Else SendKeys b$, False
End Sub

'Wait a number of seconds.
'
Sub Wait_ (p$)
  Dim t As Long
  num = Val(p$)
  t = Timer
  waiting = True
  Do Until num < 1
    If Int(t) <> Int(Timer) Then
      t = Timer
      num = num - 1
    End If
    a = DoEvents()
  Loop
  waiting = False
End Sub

'Make the window active
'
Sub WinActivate (ByVal a$)
  hwnd = GetHWND(a$)
  If hwnd > 0 Then n = SetActiveWindow(hwnd)
End Sub

'Close the given window
'
Sub WinClose (ByVal a$)
  hwnd = GetHWND(a$)
  If hwnd > 0 Then x = SendMessage(hwnd, WM_SYSCOMMAND, SC_CLOSE, 0)
End Sub

