md tmp
copy *.* tmp
copy c:\joey\prog\strnglib.bas tmp
copy c:\joey\prog\cgiwin.bas tmp
copy c:\pub\ped*.* tmp
copy c:\httpd\cgi-win\ped*.* tmp
copy c:\httpd\cgi-win\dog*.* tmp
copy c:\httpd\cgi-win\names.dat tmp
copy c:\httpd\cgi-win\p*.htm tmp
cd tmp
del create.bat
del c:\tmp\dog.zip
pkzip c:\tmp\dog
cd..
del tmp /y
rd tmp
