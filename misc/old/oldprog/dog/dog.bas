Global appath$

'Expand all occurences of "~name~" to dog$, in the file,
'and return that as the script's output.
'
Sub ExpandReturn (file$, dog$, human$)
  On Error GoTo CantLoad
  f = FreeFile
  Open file$ For Input As #f
  MsgBox dog$
  Do Until EOF(f)
    Line Input #f, a$
    b$ = change$(a$, "~dogname~", dog$)
    c$ = change$(b$, "~humanname~", human$)
    Print #CGIOutputFile, c$
  Loop
  Close #f
DoneExpandReturn:
  Exit Sub
CantLoad:
  CGIError "Unable to read " + file$ + "."
  Resume DoneExpandReturn
End Sub

Function GetName$ (n$)
  On Error GoTo NoNameFile
  f = FreeFile
  Open appath$ + "names.dat" For Input As #f
  done = False
  Do Until done Or EOF(f)
    Line Input #f, a$
    Line Input #f, b$
    If n$ Like a$ Then
      done = True
      doggie$ = b$
    End If
  Loop
  Close #f
  If doggie$ = "" Then
    'no matches - increment counter, and read from dogextra.dat
    On Error GoTo NoCounter
    Open appath$ + "dogcount.dat" For Input As #f
    Input #f, max
    Input #f, current
    Close #f
    current = current + 1
    If current > max Then current = 1
    Open appath$ + "dogcount.dat" For Output As #f
    Print #f, max
    Print #f, current
    Close #f
    On Error GoTo NoDogExtra
    Open appath$ + "dogextra.dat" For Input As #f
    For x = 1 To current
      Line Input #f, doggie$
    Next x
    Close #f
  End If
DoneGetName:
  GetName$ = doggie$
  Exit Function
NoNameFile:
  CGIError "Unable to read the file names.dat or error in the file's format."
  Resume DoneGetName
NoCounter:
  CGIError "Unable to read/write to the counter file, dogcount.dat."
  Resume DoneGetName
NoDogExtra:
  CGIError "Error reading dogextra.dat."
  Resume DoneGetName
End Function

Sub Main ()
  appath$ = app.Path
  If Right$(appath$, 1) <> "\" Then appath$ = appath$ + "\"
  
  InitCGI (Mimetype$)
  Select Case LCase$(urlarg$)
  Case "dog"
    'get name, and match against patterns in names.dat
    human$ = ReadInput("fname", "")
    If Len(human$) > 0 Then human$ = UCase$(Left$(human$, 1)) + LCase$(Mid$(human$, 2))
    dog$ = GetName(LCase$(human$))
    'return p2.htm, with ~humanname~ and ~dogname~ expanded
    ExpandReturn appath$ + "p2.htm", dog$, human$
  Case "dog2"
    'return p3.htm.
    ExpandReturn appath$ + "p3.htm", "", ""
  End Select
  EndCgi
End Sub

