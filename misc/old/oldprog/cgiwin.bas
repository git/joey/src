'Windows CGI interface file for httpd/win.
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'This include file requires that the file stringlib.bas is
'also loaded. (I want winpascal :-(  )

'Include with a program, and use these functions:
'  InitCGI - calls ReadCommandLine, opens files, etc.
'  ReadCommandLine - fill command line parameter values.
'  EndCGI - closes output file, preforms shutdown stuff.
'  ReadInput - read an item from the input file.
'  PrintHTMLHeader - write generic HTML header.
'  and to write more to output, use: Print #cgioutputfile, "text_to_print"
'  CGIError - display an error, and terminate
'mods:

'Dec 2, 1994: If no mime string is specified in the InitCGI, no header at
'all is written.

'Feb 14, 1995: Added CGIError subroutin

'Copyright (c) 1994, 1995 by Joey Hess (jeh22@cornell.edu)

DefInt A-Z
Declare Function GetPrivateProfileString Lib "Kernel" (ByVal lpApplicationName As String, ByVal lpKeyName As String, ByVal lpDefault As String, ByVal lpReturnedString As String, ByVal nSize As Integer, ByVal lpFileName As String) As Integer

Global Const Mimetype$ = "Content-type: text/html"'mime header to specify text.

'these are passed on the command line.
Global datafile$, contentfile$, outputfile$
Global urlarg$

Global cgioutputfile 'this is the number of the file opened by initCGI

'Disply error, and exit immediately
'
Sub CGIError (e$)
  Print #cgioutputfile, "<P><HR><H1>Error!</h1><P>"
  Print #cgioutputfile, e$
  EndCgi
  End
End Sub

Sub EndCgi ()
  Close cgioutputfile
End Sub

'Init the CGI interface.
'Fills variables, assigns a number to the input file, and opens it,
' and writes mime header.
'
'Param: mimetext - string specifying mime type of the output file.
'                   Just use the constant mimetype$ normally.
'If "" is specified, no header is written.
'
Sub InitCGI (mimetext As String)
  ReadCommandLine
  cgioutputfile = FreeFile
  Open outputfile$ For Output As #cgioutputfile

  If mimetext$ <> "" Then
    Print #cgioutputfile, mimetext
    Print #cgioutputfile, ""
  End If
End Sub

'Writes, to CGIOutputFile, a HTML formatted header consisting of title and
'<H1> header.
'
'Params: if h1$ is ommitted, title$ is used in its place.
'
Sub PrintHTMLHeader (title$, h1$)
  Print #cgioutputfile, "<html>"
  Print #cgioutputfile, "<title>" + title$ + "</title>"
  If h1$ = "" Then h1$ = title$
  Print #cgioutputfile, "<h1>" + h1$ + "</h1><P>"
End Sub

'Read the commandlive passed by httpd, fill the global vars.
'
Sub ReadCommandLine ()
  urlarg$ = Command$

  datafile$ = Nextword$(urlarg$)

  contentfile$ = Nextword$(urlarg$)

  outputfile$ = Nextword$(urlarg$)

End Sub

'Returns an item from the input .INI file in the [form literal] section.
'Normally this is limited to 254 chars by win-CGI specification -if somthing is bigger,
'this looks in the [form external] section, reads in the tempfile, and returns it.
'(No support for the [form huge] section yet!)
'
'Params:
'linename$ - name of line. Don't include = sign.
'default$ - default value to return.
'
Function ReadInput$ (linename$, default$)
  Dim a As String * 254
  On Error GoTo NoInput
  'try the [form literal] section
  b = GetPrivateProfileString("Form Literal", linename$, default$, a, 254, datafile$)
  If b = 0 Then
    'try the [form external] section.
    b = GetPrivateProfileString("Form External", linename$, default$, a, 254, datafile$)
    If b <> 0 Then
      'ok..now open the file and read the whole thing into one string.
      'Since the form external method don't use bigger than 64kb,
      'this should work fine.
      f = FreeFile
      Open Trim(a) For Input As #f
      m$ = Input$(LOF(f), f)
    End If
  Else
    m$ = Trim(a)
  End If
  ReadInput$ = Mid$(m$, 1, InStr(m$ + Chr$(0), Chr$(0)) - 1)
ReadDone:
  Exit Function
NoInput:
  ReadInput$ = default$
  Resume ReadDone
End Function

