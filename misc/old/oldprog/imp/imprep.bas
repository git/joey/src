'IMPressions - program #1 - IMPRep - generates a reply
'form for a given page and item address.

'command line should include the form this comes from,
'and the address of the comment this will be in reply to,
'seperated by a *

DefInt A-Z

Global Const INI = "imp.ini", header = "Imp"

'Return an error to the user
'
Sub DispError (a$)
  Print #cgioutputfile, "<h1>" + a$ + "</h1>"
  EndCgi
  End
End Sub

Function GetINIStr (ByVal File$, ByVal Section$, ByVal Key$, ByVal Default$) As String
  Dim Temp As String * 200
  Dim dummy%

  dummy% = GetPrivateProfileString(Section$, Key$, Default$, Temp$, 200, File$)
  GetINIStr = stripNull(Temp$)
End Function

Sub Main ()
  INITCgi mimetype$
  appath$ = app.Path
  If Right$(appath$, 1) <> "\" Then appath$ = appath$ + "\"
  'load INI file
  title$ = GetINIStr$(appath$ + INI, header, "title", "Record Your Impressions")
  intro$ = GetINIStr$(appath$ + INI, header, "intro", "")
  e$ = GetINIStr$(appath$ + INI, header, "end", "")
  i = InStr(urlarg$, "*")
  If i = 0 Or i = Len(urlarg$) Then
    DispError "Incorrect use of program!"
  Else
    fn$ = Trim$(Mid$(urlarg$, i - 1))
    addr$ = Trim$(Mid$(urlarg$, i))
    Print #cgioutputfile, "<title>" + title$ + "</title>"
    Print #cgioutputfile, "<h1>" + title$ + "</h1>"
    Print #cgioutputfile, "<FORM method=""post"" action =""" + apppath$ + "imp.exe?" + urlarg$ + """>"
    Print #cgioutputfile, intro$
    Print #cgioutputfile, GetINIStr$(appath$ + INI, header, "YourNameHere", "Enter your name here:"); " <INPUT TYPE=""text"" NAME=""from""><P>"
    Print #cgioutputfile, GetINIStr$(appath$ + INI, header, "CommentHere", "Enter your comment here:"); " <TEXTAREA NAME=""message"" ROWS=9 COLS=70></TEXTAREA><P>"
    Print #cgioutputfile, e$ + " <INPUT TYPE=""submit"" VALUE=""" + GetINIStr$(appath$ + INI, header, "SubmitComments", "Submit Comments") + """ <P>"
    Print #cgioutputfile, "</FORM>"
  End If
  EndCgi
End Sub

