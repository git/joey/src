'IMPressions - program #2 - IMP - processes a comment
'and adds it to the list of comments.

'command line should include the form this comes from (full dos path!),
'and the address of the comment this will be in reply to,
'seperated by a *

DefInt A-Z

Global Const INI = "imp.ini", header = "Imp"
Global appath$, CGILoc$
Global reply$

'Return an error to the user
'
Sub DispError (a$)
  Print #cgioutputfile, "<h1>" + a$ + "</h1>"
  EndCgi
  End
End Sub

Function GetINIStr (ByVal File$, ByVal Section$, ByVal Key$, ByVal Default$) As String
  Dim Temp As String * 200
  Dim dummy%

  dummy% = GetPrivateProfileString(Section$, Key$, Default$, Temp$, 200, File$)
  GetINIStr = stripNull(Temp$)
End Function

'general format of the file messages are written to:
'
'<!counter=xxx>
'
'<UL>
'<LI>blah svip voip vivipoivoifoisfogisofgi sofigosdifgoidoigf
'-- name <a href="imprep.exe?xxx*yyy">Reply</a>
'<ul>
'<LI>blah? blah blah blah...<P>
'-- name <a href=...
'
'The <ul>'s must be on lines by themselves, as must <!counter=xxx>!
'
Sub Main ()
  INITCgi mimetype$
  appath$ = app.Path
  If Right$(appath$, 1) <> "\" Then appath$ = appath$ + "\"
  I = InStr(urlarg$, "*")
  If I = 0 Or I = Len(urlarg$) Then
    DispError "Incorrect use of program!"
  Else
    reply$ = GetINIStr(appath$ + INI, header, "ReplyLink", "[Reply]")
    CGILoc$ = GetINIStr(appath$ + INI, header, "ImpRepLoc", "/cgi-win/")
    message$ = Trim$(ReadInput$("message", ""))
    If Trim$(message$) = "" Then DispError "Nothing to say? Or did something go wrong?"
    n$ = Trim$(ReadInput$("from", ""))
    If Trim$(n$) = "" Then
      If LCase$(GetINIStr(appath$ + INI, header, "anonymousok", "false")) = "true" Then
        n$ = "unknown"
      Else
        DispError "You have to give your name."
      End If
    End If
    fn$ = Trim$(Mid$(urlarg$, 1, I - 1))
    addr$ = Trim$(Mid$(urlarg$, I + 1))
    On Error GoTo BadSetup
    f = FreeFile
    Open fn$ For Input As #f
    f2 = FreeFile
    Open appath$ + "imp.new" For Output As #f2
    state = -1 'look for the counter
    hunting$ = "<!counter="
    Do Until EOF(f)
      Line Input #f, a$
      I = DoEvents()
      If state < 50 Then I = InStr(LCase$(a$), hunting$)
      If I > 0 And state < 50 Then
        Select Case state
        Case -1 'found the counter
          counter = Val(Mid$(a$, I + Len(hunting$))) + 1
          a$ = Mid$(a$, 1, I - 1) + hunting$ + CStr(counter) + ">"
          state = 0
          hunting$ = "*" + LCase$(addr$)
        Case 0
          state = 2
          'we've found the place in the file where the cgi-script is called.
          'if the next line is <UL>, then there are already a list of replies. Otherwise, we can add it right here.
          hunting$ = "ul>"
        Case 2
          'MsgBox a$
          If Mid$(a$, I - 1, 1) = "<" Then uls = uls + 1 Else enduls = enduls + 1
          'MsgBox ("<UL>:" + CStr(uls) + "  </UL>:" + CStr(enduls))
          If enduls = uls Then
            'ok - we can add it here.
            SaveComment f2, n$, message$, fn$, CStr(counter)
            state = 50
          End If
        End Select
      End If
      Print #f2, a$
    Loop
    If state <> 50 Then
      'exit with error
      DispError "Bad HTML file format! Inform the webmaster!"
    Else
      'replace the source html file with the new one.
      Close #f
      Close #f2
      FileCopy appath$ + "imp.new", fn$
      'give them a link back to the page, and say it was created ok.
      saved$ = GetINIStr(appath$ + INI, header, "CommentsSaved", "Your Comments Have Been Saved")
      Print #cgioutputfile, "<title>" + saved$ + "</title>"
      Print #cgioutputfile, "<h1>" + saved$ + "</h1>"
      'log it?
      a$ = GetINIStr(appath$ + INI, header, "logfile", "")
      If a$ <> "" Then
        f = FreeFile
        Open a$ For Append As #f
        Print #f, Now + fn$ + "*" + addr$ + " " + n$
        Close #f
      End If
    End If
  End If
quit:
  EndCgi
  End
BadSetup:
  Print #cgioutputfile, "<h1>Error saving comment!</h1>"
  Resume quit
End Sub

'Write the comment in the correct format.
'
Sub SaveComment (f, n$, c$, fn$, addr$)
  Print #f, "<LI>" + c$ + "<P>"
  If Trim$(n$) <> "" Then Print #f, "--- " + n$ + " ";
  Print #f, "<A HREF = """ + CGILoc$ + "imprep.exe?" + fn$ + "*" + addr$ + """>" + " " + reply$ + "</a><P>"
  Print #f, "<ul>"
  Print #f, "</ul>"
End Sub

