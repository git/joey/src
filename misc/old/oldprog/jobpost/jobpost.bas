'Jobpost cgi-win script.

DefInt A-Z
Option Compare Text

Global appath$, Item$(), ItemDesc$()
Global NumItems, ItemRequired(), ItemInDB()

Global header$, DBFile$, HTMLLoc$, TZ$, template$, url$, HTMLFileHeader$

'Just like change in strnglib, but is not case-sensative
'
Function CaseIgnoreChange$ (ByVal in$, changefrom$, changeto$)
  Do While in$ <> ""
    P = InStr(in$, changefrom$)
    If P > 0 Then
      out$ = out$ + Mid$(in$, 1, P - 1) + changeto$
      CaseIgnoreChange$ = out$
      in$ = Mid$(in$, P + Len(changefrom$))
    Else
      out$ = out$ + in$
      CaseIgnoreChange$ = out$
      in$ = ""
    End If
  Loop

End Function

'Load ini file and .cfg file
'
Sub Init ()
  appath$ = AddTrailingSlash((app.Path))
  iniPath$ = appath$ + app.EXEName + ".ini"
  header$ = LoadINI$(iniPath$, (app.EXEName), "header", "")
  DBFile$ = Trim$(LoadINI$(iniPath$, (app.EXEName), "DBFile", ""))
  HTMLLoc$ = AddTrailingSlash(LoadINI$(iniPath$, (app.EXEName), "HTMLLoc", ""))
  HTMLFileHeader$ = LoadINI$(iniPath$, (app.EXEName), "HTMLFileHeader", "j")
  TZ$ = LoadINI$(iniPath$, (app.EXEName), "TZ", "")
  url$ = LoadINI$(iniPath$, (app.EXEName), "HTMLUrl", "")
  template$ = LoadINI$(iniPath$, (app.EXEName), "template", "")
  f = FreeFile
  Open appath$ + app.EXEName + ".cfg" For Input As #f
  Do Until EOF(f)
    'i=doevents()
    Line Input #f, a$
    If Trim$(a$) <> "" And Mid$(a$, 1) <> ";" Then
      NumItems = NumItems + 1
      ReDim Preserve ItemDesc$(1 To NumItems)
      ReDim Preserve ItemRequired(1 To NumItems)
      ReDim Preserve ItemInDB(1 To NumItems)
      'preface an item name with a ! to make it a required item.
      If Left$(a$, 1) = "!" Then
        ItemRequired(NumItems) = True
        a$ = Mid$(a$, 2)
      Else
        ItemRequired(NumItems) = False
      End If
      'append on a + to make it be a database element.
      If Right$(a$, 1) = "+" Then
        ItemInDB(NumItems) = True
        a$ = Mid$(a$, 1, Len(a$) - 1)
      Else
        ItemInDB(NumItems) = False
      End If
      ItemDesc$(NumItems) = LCase$(a$)
    End If
  Loop
  If NumItems = 0 Then CGIError "No items listed in " + app.EXEName + ".cfg! Inform your webmaster."
  ReDim Preserve Item$(1 To NumItems)
  Close #f
End Sub

Sub Main ()
  On Error GoTo error_trap
  part = 1
  InitCGI mimetype$
  part = 2
  Init
  PrintHTMLHeader header$, ""
  'load in all the data.
  part = 3
  For x = 1 To NumItems
    Item$(x) = ReadInput$(ItemDesc$(x), "")
    If ItemRequired(x) = True And Trim$(Item$(x)) = "" Then
      'required item not listed.
      i = DoEvents()
      If Not RequiredNotFound Then Print #cgioutputfile, "<h1>Required Items not Found:</h1><ul>"
      RequiredNotFound = True
      Print #cgioutputfile, "<LI>" + ItemDesc$(x)
    End If
  Next x
  If RequiredNotFound Then
    Print #cgioutputfile, "</ul><P>Please enter these items onto the form and try again."
  Else
    'assign it a unique number, and add it to the database file
    part = 4
    On Error GoTo skip_error 'if the DB file doesn't exist, I'll gladly create a new one :-)
    f = FreeFile
    Open DBFile$ For Input As #f
      Do Until EOF(f)
        i = DoEvents()
        Line Input #f, a$
        Do Until InStr(a$, """,""") = 0
          a$ = Mid$(a$, InStr(a$, """,""") + 3)
        Loop
        t = Val(Mid$(Trim$(a$), 2))
        If t > HighestNum Then HighestNum = t
      Loop
    Close #f
skip:
    On Error GoTo error_trap
    HighestNum = HighestNum + 1
    'save in database
    part = 5
    f = FreeFile
    num = 0
    Open DBFile$ For Append As #f
      Print #f, """";
      For x = 1 To NumItems
        i = DoEvents()
        If ItemInDB(x) Then
          If num > 1 Then Print #f, """,""";
          num = num + 1
          Print #f, Item$(x);
        End If
      Next x
    'add the date and then filename on the end, where job.exe likes it (***Change in this version**)
    Print #f, """,""" + Trim$(Date$ + " " + TZ$) + """,""" + HTMLFileHeader$ + CStr(HighestNum) + ".htm"""
    Close #f
    
    'now, create the output HTML file.
    part = 6
    f = FreeFile
    Open template$ For Input As #f
    part = 7
    f2 = FreeFile
    Open HTMLLoc$ + HTMLFileHeader$ + CStr(HighestNum) + ".htm" For Output As #f2
    Do Until EOF(f)
      Line Input #f, a$
      i = DoEvents()
      ok = True
      '?...? <=if this vairable exists, process this line
      Do While Left$(a$, 1) = "?" And InStr(Mid$(a$, 2), "?") > 0 And ok
        If Trim$(VarLookup(Mid$(a$, 2, InStr(Mid$(a$, 2), "?") - 1))) = "" Then ok = False
        a$ = Mid$(a$, InStr(a$, "?") + 1)
        a$ = Mid$(a$, InStr(a$, "?") + 1)
      Loop
      If ok Then 'expand variables, and save it.
        a$ = CaseIgnoreChange(a$, "%date%", Trim$(Now + " " + TZ$))
        For x = 1 To NumItems
          a$ = CaseIgnoreChange(a$, "%" + ItemDesc$(x) + "%", Item$(x))
        Next x
        Print #f2, a$
      End If
    Loop
    Close #f
    Close #f2
    'print a link to this page, and let them know everything went ok.
    part = 8
    Print #cgioutputfile, "Your data has been processed. A page has been created <a href = """ + url$ + "j" + CStr(HighestNum) + ".htm"""">here</a> containing the information.<P>"
  End If
  EndCGI
ex:
  End
error_trap:
  CGIError "Error #" + CStr(Err) + " occurred in part " + CStr(part) + " of the script. Inform your webmaster."
  Resume ex
skip_error:
  Resume skip
End Sub

'look for a varible named a$, return its value
'
Function VarLookup$ (a$)
  a$ = LCase$(a$)
  For x = 1 To NumItems
    If ItemDesc$(x) = a$ Then VarLookup$ = Item$(x)
  Next x
End Function

