#!/usr/bin/perl
#
#X-windows ytalk frontend.
#

use Tk;
$main = MainWindow->new;
$main->title('Reach out and touch someone');
$main->iconname('Talk');

my $message=$main->Label(-text => 'Talk to who? (Enter name or email address)');
$message->pack(-side=>'top');

my $talkto=$main->Entry(-relief=>'sunken',-textvariable=>\$who);
$talkto->pack(-side=>'top',-expand=>'y',-fill=>'x',-pady=>'2m');

my $d=$main->Checkbutton(-text=>'special X display',-variable=>\$display,-relief =>'flat');
$d->pack(-side=>'left',-expand=>'y',-pady=>'2m');

my $ok=$main->Button(
	-text=>'Ok',
	-command=>[sub {
		if (!defined $display) 
			{ system "xterm -e ytalk -x $who &" }
		else 
			{system "ytalk $who"}
		exit}
	]);
my $cancel=$main->Button(-text=>'Cancel',-command=>[sub {exit}]);
$ok->pack(-side=>'left',-expand=>'y',-pady=>'2m');
$cancel->pack(-side=>'left',-expand=>'y',-pady=>'2m');

MainLoop;
