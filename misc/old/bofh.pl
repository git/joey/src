#!/usr/bin/perl
#
# BOFH filter, in the tradition of a.s.r
#
# version 1.0
#
# Copyright (C) 1996 by Joey Hess <jeh22@cornell.edu>
# May be distributed or modified as dictated by the terms of the GPL.
#

$maxwidth=70;

# Save a footnote, returning a pointer to it.
# Also separates out any child footnotes.
sub SaveFootNote { my $footnote=shift;
	print "$footnote\n";
	$footnote=~s/^\(//;
	$footnote=~s/\)$//;
	$footnote=~s/(\([^\)]+\))/SaveFootNote($1)/egs;
	$footnotes[$#footnotes+1]=$footnote;
	return '['.$footnote_c++.']';
}

# Initialize footnote counter, to either 0 or 1, randomly.
srand($$ ^ time); #seed random number generator.
if (rand() >0.5) { $footnote_c_init=1 } else { $footnote_c_init=0 }
$footnote_c=$footnote_c_init;

while (<>) {
	last if /^-{1,5}\s+$/ ne undef; # Halt processing at .signature.
	$message.=$_;
}

# Process body of message.
$_=$message;
s/user/luser/g;
s/User/Luser/g;
s/([C|c])o(\s?|\s?-\s?)worker/$1ow-orker/g;
s/(\(.*\))/SaveFootNote($1)/egs;
print;

# Display the footnotes, wrapping to fit nicely.
$footnote_i=$footnote_c_init;
foreach (@footnotes) {
  chomp;

  my $footnote='['.$footnote_i++.'] ';
	$footnoth_size=length($footnote);
	print $footnote;	
	@words=split /\s+/;
	
	$width=$footnote_size;
	foreach (@words) {
		if ($width+length > $maxwidth) {
			print "\n" . ' ' x $footnote_size . $_ . ' ';
			$width=$footnote_size+length($_)+1;
		}
		else {
			print $_.' ';
		}
	}
	print "\n";
}

# Display the signature, if it fits McQ limits.
@sig=<>;
foreach (@sig) { if (length >81) { $non_mcq=1; last } }

if (!$non_mcq && $#sig < 4) {
	print "-- \n";
	map print,@sig;
}
else {
	$wanker=1;
	print "-- \nJoe Wanker\n";
}
