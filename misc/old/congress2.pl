#!/usr/bin/perl
#
#A saveform addon, this needs a ~data~ element, and any number of control-*
#elements, and runs a saveform command for every single one of the 
#control-*'s.
#
#It builds a list, ~allto~ of each address.
#

#where is startup.pl located?
require '/httpd/htdocs/cgi/startup.pl';

$path='/httpd/htdocs/cgi/actnow/';   #The path to this program, and our INI file
&INIproc($path.'congress.ini');      #Read our INI

if ($in{"control-all"} ne '') {
  $a=$in{"control-all"};
  @a=split(/ /,$a);
}
else
{
  foreach $a (@in) {
  	($_,$v)=split(/=/,$a,2);
  	if ((/^control-/i ne '') && ($v ne '')) { #send to this one.
  	  @a[$#a+1]=$v;
  	}
  }
}
$n=$#in+1;
$allto='';
foreach $a (@a) { #actually send to each here.
  $in[$n]=$saveformvar.'='.$a;
  $allto.="$a ";
  &DoRunFile($saveformcommand);
}
$in[$#in+1]="allto=$allto";
#print "Content-type:text/html\n\n";
#print $allto;