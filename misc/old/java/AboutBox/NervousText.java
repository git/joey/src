/*  Daniel Wyszynski 
    Center for Applied Large-Scale Computing (CALC) 
    04-12-95 

    Test of text animation.

    kwalrath: Changed string; added thread suspension. 5-9-95

		Joey Hess: added fgcolor and bgcolor attributes to let you change 
			background 21-Feb-96
*/

import java.awt.*;
import java.lang.Integer;
import java.net.*;
import java.io.*;

public class NervousText extends java.applet.Applet implements Runnable {

	char separated[];
  String s=null;
	Thread killme = null;
	int i;
	int x_coord = 0, y_coord = 0;
	String num;
	int speed=35;
	int counter =0;
	boolean threadSuspended = false; //added by kwalrath

public void init() {
	//load colors -- mod by Joey
	s = getParameter(
"bgcolor" //#bgcolor=Name of parameter that sets background color.
);
	setBackground(
		(s == null) ? getBackground() : 
			new Color(Integer.valueOf(s,16).intValue()));
	s = getParameter(
"fgcolor" //#fgcolor=Name of parameter that sets foreground color.
);
	setForeground(
		(s == null) ? getForeground() :
			new Color(Integer.valueOf(s,16).intValue()));

	setFont(new Font("TimesRoman",Font.BOLD,36));
	s = getParameter("text");
	if (s == null) {
	    s = 
"Mainsail - Java!" //#NervousString=Default string displayed by nervousText.
;
	}
	separated =  new char [s.length()];
	s.getChars(0,s.length(),separated,0);

	//Use a new flow layout that right-aligns.
	setLayout(new FlowLayout(FlowLayout.RIGHT));

	//Add the AboutButton.

	String aboutBoxText="";

	try {
		URL texturl = new URL(
"http://mainsail.com/java/joey/about/about.txt" //#texturl=Absolute url to the location of text in the AboutBox's textbox.
);

		URLConnection textConnection = texturl.openConnection();
		DataInputStream dis;
		String inputLine;

		dis = new DataInputStream(textConnection.getInputStream());
		while ((inputLine = dis.readLine()) != null) {
			aboutBoxText=aboutBoxText+inputLine+"\n";
		}
		dis.close();

	} catch (MalformedURLException e) {     // generated from new URL()
	} catch (IOException e) {               // generated from openConnection()
	}
	
  DataInputStream dis;
	String inputLine;



	TextArea ta=new TextArea(aboutBoxText,
10 //#numlines=The number of lines in the about box's textarea.
,
50 //#numcols=The number of columns in the about box's textarea.
);
  ta.setEditable(false); //only used for display.

	AboutButton abutton=new AboutButton(
		getImage(getCodeBase(), 
"aboutbutton.gif" //#filename=The name of the .gif file that is loaded for the AboutButton.
),
		getImage(getCodeBase(),
"aboutbutton_pressed.gif" //#filename_pressed=The name of the .gif file for a pressed AboutButton.
),
		getImage(getCodeBase(),
"aboutbutton_mouseover.gif" //#filename_mouseover=The name of the .gif file for am AboutButton when the mouse is over it.
),
		new Dimension(
150 //#button_sizex=Width of button (in pixels)
,
200 //#button_sizey=Height of button (in pixels)
),
		new AboutBox(
"About Mainsail's NervousText" //#abouttitle=Title of the AboutBox.
,
"This is a Mainsail Applet." //#desctext=Text that appears above the TextArea on the AboutBox.
,
ta,
"Mainsail" //#linktext=The text displayed on the button that links to a page on the WWW.
,
"http://mainsail.com/" //#url=The url to load if they click on the above button.
,null,this));

	add(abutton);
	validate();
}

public void start() {
	if(killme == null) 
	{
        killme = new Thread(this);
        killme.start();
	}
 }

public void stop() {
	killme = null;
 }

public void run() {
	while (killme != null) {
	try {Thread.sleep(200);} catch (InterruptedException e){}
	repaint();
	}
	killme = null;
 }

public void paint(Graphics g) {
	for(i=0;i<s.length();i++)
	{
	x_coord = (int) (Math.random()*10+15*i);
	y_coord = (int) (Math.random()*10+36);
	g.drawChars(separated, i,1,x_coord,y_coord);
	}
}
 
public boolean mouseDown(java.awt.Event evt, int x, int y) {
        if (threadSuspended) {
            killme.resume();
        }
        else {
            killme.suspend();
        }
        threadSuspended = !threadSuspended;

	return true;
}

}
