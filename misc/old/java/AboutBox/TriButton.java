/*
 * By Joey Hess <jeh22@cornell.edu>
 *
 * A tri-state button
 * Has 3 states, normal, pressed, and when the mouse is over it. 
 *
 * Makes use of a media tracker to pre-load images for better interactive use.
 *
 * Override the click() method to add functionality when the button is clicked.
 *
 */

import java.awt.*;

public class TriButton extends Canvas {
  private MediaTracker tracker;

	public Image image=null;
  public Image image_norm=null;
  public Image image_pressed=null;
  public Image image_mouseover=null;
	public Dimension preferredSize=null;
  public boolean buttonDown=false;

	TriButton(Image img_norm, Image img_pressed, Image img_mouseover, 
							Dimension prefSize) {

		tracker = new MediaTracker(this);

		image_norm = img_norm;
		tracker.addImage(image_norm, 0);
		try {
			tracker.waitForAll();
		} catch (InterruptedException e) {}

		image_pressed=img_pressed;
		tracker.addImage(image_pressed, 0);
		try {
			tracker.waitForAll();
		} catch (InterruptedException e) {}

		image_mouseover=img_mouseover;
		tracker.addImage(image_mouseover, 0);
		try {
			tracker.waitForAll();
		} catch (InterruptedException e) {}

		preferredSize = prefSize;
		image=img_norm;
	}

  public void click() { //Override this!
	}

	public Dimension minimumSize() {
		return preferredSize;
	}

	public Dimension preferredSize() {
		return preferredSize;
	}

	//Override update to eliminate flicker.
  public void update(Graphics g) {
		g.drawImage(image, 0, 0, this);
	}

	public void paint(Graphics g) {
		update(g);
	}

  public boolean mouseDown(java.awt.Event evt, int x, int y) {
		//make the button appear pressed down until the mouse is released.		
		buttonDown=true;
		image=image_pressed;
		repaint();

		return true;
	}

	public boolean mouseUp(java.awt.Event evt, int x, int y) {
		//make button raise back up when mouse is released. (display mouseover)
		buttonDown=false;
		image=image_mouseover;
		repaint();

		click();

		return true;
	}

  public boolean mouseEnter(Event e, int x, int y) {
		//make button display special image or be pressed when cursor is in it.
		if (buttonDown==true) {
			image=image_pressed;
			repaint();
		}
		else {
			image=image_mouseover;
			repaint();
		}
		
		return true;
  }

  public boolean mouseExit(Event e, int x, int y) {
		//display default image again..
		image=image_norm;
		repaint();
		
		return true;
  }

}




