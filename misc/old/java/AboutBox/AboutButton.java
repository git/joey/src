/*
 * By Joey Hess <jeh22@cornell.edu>
 *
 * A button that brings up an AboutBox.
 *
 */

import java.awt.*;

public class AboutButton extends TriButton {
	public AboutBox aboutBox=null;

	AboutButton(Image img_norm, Image img_pressed, Image img_mouseover, 
							Dimension prefSize, AboutBox abox) {
		super(img_norm,img_pressed,img_mouseover,prefSize);
		aboutBox=abox;
	}

  public void click() {
		//display the about window now, or bring it to the front.
		aboutBox.pack();
		aboutBox.show();
	}
}




