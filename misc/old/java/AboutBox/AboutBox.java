/*
 * AboutBox: Display info about a java applet.
 *
 * By Joey Hess: jeh22@cornell.edu
 *
 */

import java.awt.*;
import java.net.URL;
import java.applet.*;

public class AboutBox extends Frame {
	private Button closeButton;
	private Button pageButton;
	private String PageButtonResultUrl;
	private java.applet.Applet CallingApp;

	public AboutBox(String windowTitle, String topLabelString, 
									Component mainComponent, String pageButtonCaption, 
									String ResultUrl, String closeButtonCaption, 
									java.applet.Applet App) {
		setTitle((windowTitle==null) ? "About" : windowTitle);
		CallingApp=App;

		//use a GridBag to layout everything.
		GridBagLayout gridBag = new GridBagLayout();
		GridBagConstraints c = new GridBagConstraints();
		setLayout(gridBag);

		//A label at the top of the window.
		Label topLabel=new Label((topLabelString==null) ? "This app was created by" : topLabelString, Label.CENTER);
		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridwidth = GridBagConstraints.REMAINDER;
		gridBag.setConstraints(topLabel, c);
		add(topLabel);

		//Add the main component, whatever it may be.
		c.fill = GridBagConstraints.BOTH;
		c.weightx = 1.0;
		c.weighty = 1.0;
		gridBag.setConstraints(mainComponent, c);
		add(mainComponent);

		if (ResultUrl!=null) {			
			//A button to load a page.
			pageButton=new Button((pageButtonCaption==null) ? "Mainsail" : pageButtonCaption);
			c.fill = GridBagConstraints.NONE;
			c.gridwidth = 1;
			c.insets = new Insets(3,0,3,0);
	    c.weightx = 0.5;
	    c.weighty = 0.0;
	    gridBag.setConstraints(pageButton, c);
	    add(pageButton);
			PageButtonResultUrl=ResultUrl;
		}

		//A close button.
		closeButton=new Button((closeButtonCaption==null) ? "Close" : closeButtonCaption);
    gridBag.setConstraints(closeButton, c);
    add(closeButton);

		validate();
	}

	public boolean handleEvent(Event event) {
		if ((event.id == Event.WINDOW_DESTROY) ||
			  ((event.id == Event.ACTION_EVENT) &&
          (event.target == closeButton))) { //handle cases that close window.
				dispose();
		}
		else if ((event.id == Event.ACTION_EVENT) && (event.target == pageButton)) {
			//load up a new page.
			try {
				URL test=new URL(PageButtonResultUrl);
				java.applet.AppletContext context=CallingApp.getAppletContext();
				context.showDocument(test);
			} 
			catch(Exception e) {
				CallingApp.showStatus("Unable to load "+PageButtonResultUrl+".");
			}
		}
		return super.handleEvent(event);
	}
}
