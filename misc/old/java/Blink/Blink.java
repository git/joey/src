/*
 * Copyright (c) 1994 Sun Microsystems, Inc. All Rights Reserved.
 */

import java.awt.*;
import java.util.StringTokenizer;

/**
 * I love blinking things.
 *
 * @author Arthur van Hoff
 * 
 * Mod by Joey Hess -- added fgcolor,bgcolor, and textcolor attribute handling.
 */
public class Blink extends java.applet.Applet implements Runnable {
	boolean threadSuspended = false; //added by Joey
    Thread blinker;
    String lbl,s;
    Font font;
    int speed;
		Color textcolor;

    public void init() {

		//load colors -- mod by Joey
		s = getParameter("bgcolor");
		setBackground(
			(s == null) ? getBackground() : 
				new Color(Integer.valueOf(s,16).intValue()));
		s = getParameter("fgcolor");
		setForeground(
			(s == null) ? getForeground() :
				new Color(Integer.valueOf(s,16).intValue()));
    s = getParameter("textcolor");
    textcolor=(s == null) ? getBackground() :
			new Color(Integer.valueOf(s,16).intValue());

	font = new java.awt.Font("TimesRoman", Font.PLAIN, 24);
	String att = getParameter("speed");
	speed = (att == null) ? 400 : (1000 / Integer.valueOf(att).intValue());
	att = getParameter("lbl");
	lbl = (att == null) ? "Blink" : att;
    }
    
    public void paint(Graphics g) {
	int x = 0, y = font.getSize(), space;
	int red = (int)(Math.random() * 50);
	int green = (int)(Math.random() * 50);
	int blue = (int)(Math.random() * 256);
	Dimension d = size();

	g.setColor(Color.black);
	g.setFont(font);
	FontMetrics fm = g.getFontMetrics();
	space = fm.stringWidth(" ");
	for (StringTokenizer t = new StringTokenizer(lbl) ; t.hasMoreTokens() ; ) {
	    String word = t.nextToken();
	    int w = fm.stringWidth(word) + space;
	    if (x + w > d.width) {
		x = 0;
		y += font.getSize();
	    }
	    if (Math.random() < 0.5) {
		g.setColor(new java.awt.Color((red + y * 30) % 256, (green + x / 3) % 256, blue));
	    } else {
		g.setColor(textcolor);
	    }
	    g.drawString(word, x, y);
	    x += w;
	}
    }

    public void start() {
	blinker = new Thread(this);
	blinker.start();
    }
    public void stop() {
	blinker.stop();
    }
    public void run() {
	while (true) {
	try {Thread.currentThread().sleep(speed);} catch (InterruptedException e){}
	    repaint();
	}
    }

	/* Added by Joey. */
	public boolean mouseDown(java.awt.Event evt, int x, int y) {
        if (threadSuspended) {
            blinker.resume();
        }
        else {
            blinker.suspend();
        }
        threadSuspended = !threadSuspended;
    return true;
  }
}
