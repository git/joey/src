import java.awt.*;
import java.net.*;
import java.io.*;

class AboutTarget extends DragItem {
	AboutBox abox=null;
	
	AboutTarget(Image defImg, Dimension prefSize, Point p, 
							DragDropField reg_field, boolean canDrag, AboutBox box) {
		super(defImg,prefSize,p,reg_field,canDrag);
		abox=box;
	}

	public void dropInto(DragItem item) {
		//display the about window now, or bring it to the front.
		abox.pack();
		abox.show();
	}
}

public class TestApp extends java.applet.Applet implements Runnable {
	
	public void init() {

		String s;
		
		//Set the colors.
		s = getParameter(
"bgcolor" //#bgcolor=Name of parameter that sets background color.
);
		setBackground(
									(s == null) ? getBackground() : 
									new Color(Integer.valueOf(s,16).intValue()));
		s = getParameter(
"fgcolor" //#fgcolor=Name of parameter that sets foreground color.
);
        setForeground(
											(s == null) ? getForeground() :
											new Color(Integer.valueOf(s,16).intValue()));
				
		//make the DragDropField;
		DragDropField df=new DragDropField(size());
		df.setBackground(getBackground());
		add(df);
		validate();	

		//Create the aboutbox.
		String aboutBoxText="";

		try {

			URL texturl = new URL(
"http://mainsail.com/java/joey/DragDrop/about.txt" //#texturl=Absolute url to the location of text in the AboutBox's textbox.
);
			URLConnection textConnection = texturl.openConnection();
			DataInputStream dis;
			String inputLine;
			
			dis = new DataInputStream(textConnection.getInputStream());
			while ((inputLine = dis.readLine()) != null) {
				aboutBoxText=aboutBoxText+inputLine+"\n";
			}
			dis.close();
			
		} catch (MalformedURLException e) {     // generated from new URL()
		} catch (IOException e) {               // generated from openConnection()
		}
		
		TextArea ta=new TextArea(aboutBoxText,
10 //#numlines=The number of lines in the about box's textarea.
,
50 //#numcols=The number of columns in the about box's textarea.
);
		ta.setEditable(false); //only used for display.

		AboutBox abox=new AboutBox(
"About Mainsail's Drag and Drop" //#abouttitle=Title of the AboutBox.
,
"This is a Mainsail Applet." //#desctext=Text that appears above the TextArea on the AboutBox.
,ta,
"Mainsail" //#linktext=The text displayed on the button that links to a page on the WWW.
,
"http://mainsail.com/" //#url=The url to load if they click on the above button.
,null,this);

		//add a few DragItems to it.
		AboutTarget di1=new AboutTarget(getImage(getCodeBase(),
"target.gif" //#target_gif=Gif that's the target of the drop
),
																		new Dimension(
144 //#target_width=Width of the target gif, in pixels.
,
72 //#target_height=Height of the target gif, in pixels.
),
															new Point(
144 //#target_x=position of the target gif (x coordinate)
,
72 //#target_y=position of the target gif (y coordinate)
),
															df,false,abox);
		
		DragItem di2=new DragItem(getImage(getCodeBase(),
"drag.gif" //drag_gif=Gif that's the drag and drop icon
),
															new Dimension(
53 //#drag_width=Width of the icon, in pixels.
,
37 //#drag_height=Height od the icon, in pixels.
),
															new Point(
50 //#drag_x=Initial location of the icon (x coordinate).
,
50 //#drag_y=Initial location of the icon (y coordinate).
),
															df,true);

	}

	public void run() {
	}	

}
