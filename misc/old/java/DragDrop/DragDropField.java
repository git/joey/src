/* 
 * By Joey Hess <jeh22@cornel.edu>
 *
 * A panel within which you can drag and drop objects.
 * All items that are dragged and droped must be members of the DragItem class
 * and they must register themselves with the DragDropField via its 
 * AddDragItem method. Drop targets (also DragItems) may register themselves 
 * via the AddDropItem method.
 *
 */

import java.awt.*;

public class DragDropField extends Panel {

	boolean drag=false;
  public int nitems=0; //total number of DragItems
  public int citem=-1; //currently selected DragItem
  public Point cOffset=null; //cursor offset in currently selected DragItem.

  public DragItem items[] = new DragItem[
50 //#maxDragItems=Maximum number of Dragable items that can be contained in the DragDropField
];

	public int ndrops=0;
	public DragItem drops[]= new DragItem[
50 //#maxDropItems=Maximum number of Drop points that can be inside the DragDropField
];
	
	public Dimension preferredSize=new Dimension(100,100);

	Dimension offDimension;
	Image offImage;
	Graphics offGraphics;

	DragDropField(Dimension psize) {
		preferredSize=psize;
	}

	DragDropField() {
	}

  public Dimension minimumSize() {
		return preferredSize;
	}

  public Dimension preferredSize() {
		return preferredSize;
	}

	//Call to register a drag item with me.
	public void AddDragItem(DragItem item) {
		items[nitems++]=item;
		repaint();
	}

	//Call to register a drop item with me.
  public void AddDropItem(DragItem drop) {
		drops[ndrops++]=drop;
	}

	//Override to prevent flicker.
  public void update(Graphics g) {
		//may need to initialize our double buffer the 1st time through.
		if ( (offGraphics == null)
				 || (preferredSize.width != offDimension.width)
				 || (preferredSize.height != offDimension.height) ) {
			offDimension = preferredSize;
			offImage = createImage(preferredSize.width, preferredSize.height);
			offGraphics = offImage.getGraphics();
		}

		//Erase previous image
		offGraphics.setColor(getBackground());
		offGraphics.fillRect(0, 0, preferredSize.width, preferredSize.height);
		offGraphics.setColor(
Color.black //#Bordercolor=The color of a border around an icon. Color.red, for instance.
);

		//Draw all DropItems
		for (int i=0; i<ndrops; i++) {
			offGraphics.drawImage(drops[i].image, drops[i].pos.x,
														drops[i].pos.y, this);
		}

		//Draw all DragItems
		for (int i=0; i<nitems; i++) {
			offGraphics.drawImage(items[i].image, items[i].pos.x, 
														items[i].pos.y, this);
		}

		if (citem>-1) {
			//Draw selected DragItem
			offGraphics.drawRect(items[citem].pos.x-1,items[citem].pos.y-1,
													 items[citem].size.width+1,
													 items[citem].size.height+1);
		}

		//Paint buffer to screen.
		g.drawImage(offImage,0,0,this);
	}
		
  public void paint(Graphics g) {
		update(g);
	}

	//Move the selected DragItem around.
  public synchronized boolean mouseDrag(Event evt, int x, int y) {
		if ((!drag) && (citem<0)) { //try to select an item now since we just started dragging.
			selectDragItem(x,y);
			selectDragItem(x,y);
		}
		drag=true;
		if (citem>-1) { //do the move or do nothing.
			items[citem].pos.x=x-cOffset.x;
			items[citem].pos.y=y-cOffset.y;
			repaint();
		}
		return true;
	}
	
	//Unselect any selected DragItems
  public synchronized boolean mouseUp(Event evt, int x, int y) {
		//See if we need to notify a DropItem that it's been dropped on.
		for (int i=0; i<ndrops; i++) {
 			if ((drops[i].pos.x-1 <x-cOffset.x+items[citem].size.width) && 
					(drops[i].pos.y-1<y-cOffset.y+items[citem].size.height) &&
					(drops[i].pos.x+drops[i].size.width+1>x-cOffset.x) && 
					(drops[i].pos.y+drops[i].size.height+1>y-cOffset.y)) {
				//Found one.
				drops[i].dropInto(items[citem]);
			}
		}

		if ((citem>-1) && (drag)) {
			items[citem].drop(); //pass the drop event along to the DragItem
			citem=-1;
			drag=false;
			repaint();
		}
			
		return true;
  }

	//Clicking on a dragitem causes it to become selected.
	public synchronized boolean mouseDown(Event evt, int x, int y) {
		selectDragItem(x,y);
		selectDragItem(x,y);
		return true;
	}

	//See if we can select an item at the given cooredinates.
	//Currently this has to be called twice to work, I dunno why. FIX!
  public synchronized void selectDragItem(int x, int y) {
		citem=-1;
		for (int i=0; i<nitems; i++) {
			if ((items[i].pos.x-1 <x) && (items[i].pos.y-1<y) && 
					(items[i].pos.x+items[i].size.width+1>x) && 
					(items[i].pos.y+items[i].size.height+1>y)) {
				//found it.
				citem=i;
				cOffset=new Point(x-items[citem].pos.x,y-items[citem].pos.y);
				
				//now shuffle it to the bottom of the list so it is drawn on top.
				DragItem swap=items[citem];
				items[citem]=items[nitems-1];
				items[nitems-1]=swap;
					
				i=nitems;
				repaint();
			}
		}
	}

}






