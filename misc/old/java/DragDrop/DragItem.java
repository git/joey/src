/*
 * By Joey Hess <jeh22@cornell.edu>
 *
 * A class that may be dragged and dropped on a DragDropField
 * or may be the reciepient of a drop of another item.
 * 
 * Auto-registers itself with the DragDropField when constructed.
 *
 * Extend its drop method to add functionality when it is dropped, and 
 * its dropInto method to add functionality when it is dragged.  
 *
 * In the constructor, pass true as the canDrag parameter to make it a
 * draggeable item, otherwise it'll be a drop reciepient. 
 */

import java.awt.*;

public class DragItem {
	public Image image=null;
	public Dimension size=null;
	public Point pos=null;

	DragItem (Image defImg, Dimension prefSize, Point p, 
						DragDropField reg_field, boolean canDrag) {

		MediaTracker tracker = new MediaTracker(reg_field);

		//Wait for the image to load.
		image=defImg;
		tracker.addImage(image, 0);
		try {
			tracker.waitForAll();
		} catch (InterruptedException e) {}

		size=prefSize;
		pos=p;
		if (canDrag) {
			reg_field.AddDragItem(this); //Register myself as a DragItem.
		}
		else {
			reg_field.AddDropItem(this); //Register myself as a DropItem.
		}
	}        

	public void drop() {}

  public void dropInto(DragItem item) {} //Passed the item that is dropped into it.
}




