#!/bin/sh
#
#A login shell that shows system logs.

LOGFILES="/var/log/maillog /var/log/messages /home/www/logs/access_log /home/www/cgi/storage/insult-by-mail.log /home/www/logs/error_log /box/var/log/maillog /box/var/log/messages"
OK=1
while let $OK==1;
	do logmon-tail -f $LOGFILES && 
	if -e /tmp/.logview-restart; then 
		OK=0;
	else
		sleep 3;
	fi
done
