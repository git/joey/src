#!/usr/bin/perl
#
#LUG database access functions.
#
#By Joey Hess <jeh22@cornell.edu>

#Notes:
#
#All these call a error handler function, Err, which you can write
#specially for your program. (Or, just use "sub Err { die shift }" as the
#function, if you don't feel like coding one.)
#
#The flock() and symlink() system calls must be supported, or this library 
#won't work.


#This file contains information about where files in the LUG 
#pages are.
require 'lug-def.pl';

#Load in the LUG database, and store it in the array @lug_db.
#So, to get info on a user, just look at $lug_db{id,field}, 
#where id is their userid, and field is whatever field you want.
#
#Also sets: 
#@db_fields: the names of the database fields.
#
sub lug_getdb {
	open (LUG_DB,"<$lug_db_file") or &Err("Read: $lug_db_file");
	lock_db();
	#top line of the database specifies the fields.
	my $fields=<LUG_DB>;
	chomp $fields;
	@db_fields=split(/$db_delim/,$fields);

	#Load all the records of the db.
	while (<LUG_DB>) {
		chomp;
		my ($id,@a)=split(/$db_delim/,$_);
		foreach $x (0..$#a) {
			$lug_db{$id}{$db_fields[$x+1]}=$a[$x];
		}
	}
	unlock_db();
	close LUG_DB;
}

#Saves the LUG database.
#
sub lug_savedb {
	open (LUG_DB,">$lug_db_file") or &Err("Write: $lug_db_file");
	lock_db();
	$"=$db_delim; #Set the list seperator to our database delimeter
	print LUG_DB "@db_fields\n"; #print the fields of the db on the 1st line.
  $"=' '; #Reset the list seperator back to default.
  	
	#Print out all the records. My @lug_db structure is easy to use, but a pain  
	#to print out..
	foreach $key (keys(%lug_db)) { #for each record
		my $a=$key;	#add id to output string
		foreach $key2 (@db_fields) {	#for each field in the record
			$a.=$lug_db{$key}{$key2}.$db_delim;	#add field to output string
		}
		$a=~s/$db_delim$//; #remove a trailing delimeter
		print LUG_DB "$a\n";	#output the record
	}
	unlock_db();
	close LUG_DB;
}

#Lock the LUG database. This is a blocking function.
#
sub lock_db {
	flock(LUG_DB,$LOCK_EX);
}

#Unlock the database.
#
sub unlock_db {
	flock(LUG_DB,$LOCK_UN);
}

#Verifies a user. Pass this the user id and the password, in that order.
#
#This handles symlink creation and expiration. It returns a false value if
#the password and id are invalid, or the name of the symlink that is 
#created if they are valid.
#
#Case is important in the password, but sohld not be important in the user id.
#
#Before calling this function, you must have called lug_getdb 
#
sub verify_user { my $id=shift; my $password=shift;
	if ($lug_db{$id}{password} eq $password) {
		#password and id are good. 
		$link_name=cool_string()."_$id"; #generate a random symlink name that contains the userid.
		while (-e $lug_public_dir.$link_name) {
			print "a";
			$link_name=cool_string()."_$id"; #try a different one.
		}
		symlink $lug_private_dir,$lug_public_dir.$link_name;	#create the symlink.
		return $link_name;
	}
	else {
		return '';
	}
}

#Return a random string. There should be several thousand possible return 
#values, for security.
#
#These stings are used for filenames, so no odd character, please.
#Also, the "_" character should not be in the string.
#
sub cool_string {
	return int(rand(99999999999)); #a big random number isn't very cool. This needs rewriting..
}

#Locking constants.
$LOCK_SH = 1;
$LOCK_EX = 2;
$LOCK_NB = 4;
$LOCK_UN = 8;

srand($$ ^ time); #seed random number generator.
