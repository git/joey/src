/* Simple Program To Set Your Terminal Translation
 *
 * Great if you want to view ANSI Images, etc...
 *
 * <mode>
 *            0  =  Graphic Translation
 *            B  =  Normal Translation
 *            U  =  No Translation (Good For ANSI, etc.)
 *            K  =  User Defined Translation
 *            1  =  Default Translation
 *
 * In linux these translations are configured in drivers/char/console.c
 *
 * You may distribute this source code freely.
 * Chris Haos <chris@ideal.ios.net> August 1994
 *
 * Modified 1995 by Joey Hess -- display the above list if there are no params
 * Maybe I'll learn C someday :-)
 */

#include <stdio.h>

void main(int argc, char *argv[])
{
  if (argc!=2) {
    printf("usage: settrans <mode>\n");
		printf("	Where <mode> is one of:\n");
		printf("	0  =  Graphic Translation\n");
		printf("	B  =  Normal Translation\n");
		printf("	U  =  No Translation (Good For ANSI, etc.)\n");
		printf("	K  =  User Defined Translation\n");
		printf("	1  =  Default Translation\n");
		    
    exit(0);
  }
  printf("\033(%c",argv[1][0]);
}
