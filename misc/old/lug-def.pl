#!/usr/bin/perl
#
#Global variables for LUG scripts.

#Location of the lug database:
$lug_db_file='/home/joey/lug.db';

#Our database delimeter is currently:
$db_delim=','; #comma

#The location of our private lug directory.
$lug_private_dir='/tmp/';

#The location of our public lug directory.
$lug_public_dir='/tmp/';

1 #return a true value. Leave alone.

