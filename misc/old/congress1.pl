#!/usr/bin/perl
#
#A saveform addon, this accepts a fields "congress", which is the state field
#of a database, and looks up congresspeople in that state. The names
#are formatted and returned in ~list~
#

#where is startup.pl located?
require '/httpd/htdocs/cgi/startup.pl';

$path='/httpd/htdocs/cgi/actnow/';   #The path to this program, and our INI file
&INIproc($path.'congress.ini');      #Read our INI

$state=$in{congress};

$list='';
$allvalue='';

$n=0;

open (DB,'<'.$congressdb);
while (<DB>) {
  s/\n$//;
  ($_,$name,$email)=split(/:/,$_,3);
  if (/$state/io ne '') { #match
    $n=$n+1;
    $_=$listformat;
    s/~name~/$name/gi;
    s/~email~/$email/gi;
    s/~control~/control-$n/gi;
    $list.=$_."\n";
    $allvalue.=$email.' ';
  }
}
close DB;

$allvalue=~s/ $//;
$allformat=~s/~value~/$allvalue/;
$list.=$allformat."\n";

$in[$#in+1]='list='.$list; #return value.
