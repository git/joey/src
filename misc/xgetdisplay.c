/* by Wakko */


#include <unistd.h>
#include <string.h>
#include <netinet/in.h>
#include <stdio.h>
#include <errno.h>
#include <sys/socket.h>

/* well, not what you'd think, but it's a start! */
int get_first_x_display()
{
 int sock, port;
 struct sockaddr_in sin;

 for (port = 6000;port < 8000;port++)
 {
  /* Create a port to listen for the host. */
  sock = socket(AF_INET, SOCK_STREAM, 0);
  if (sock < 0)
  {
   perror("Can't connect socket");
   exit(0);
  }
  /* Initialize socket address. */
  memset(&sin, 0, sizeof(sin));
  sin.sin_family = AF_INET;
  sin.sin_addr.s_addr = INADDR_ANY;
  sin.sin_port = htons(port);
  /* Bind the socket to the address. */
  if (bind(sock, (struct sockaddr *)&sin, sizeof(sin)) >= 0)
  {
   close(sock);
   return port - 6000;
  }
  close(sock);
 }
 return -1;
}  

void main()
{
 printf(":%d\n", get_first_x_display());
}
