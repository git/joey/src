#!/usr/bin/perl
# An implementation of RUBE (Rather Unique Befungoid Engine) in perl,
# by Joey Hess <joey@kitenet.net>. GPL copyright 1999.
# 
# RUBE's homepage is http://www.cats-eye.com/cet/soft/lang/rube/
# The RUBE spec (such as it is) is 

# Advantages over reference implmentation:
# - can handle any size input program.
# - dynamic memory allocation
# - object oriented implmentation.
# - added exit condition (stop when 2 crates fall onto an 'X', return 
#   value of crates as return code)
# - fixes some bugs in the original (which means it's not necessarily
#   compatabile with programs written for the original).

package Rube;
use vars qw($AUTOLOAD);

# This maps the characters that make up a rube program into
# symbolic names.
my %char_names=(
	'F' => 'furnace',
	'C' => 'crate_killer',
	'D' => 'dozer_killer',
	'(' => 'right_dozer',
	')' => 'left_dozer',
	'*' => 'crumble_wall',
	'>' => 'right_conveyer',
	'<' => 'left_conveyer',
	'O' => 'output',
	'X' => 'exit',
	' ' => 'empty',
);
# Tack on all the types of crates.
for (0..9, a..f) {
	$char_names{$_}='crate';
}
# Generate the reverse hash.
my %name_chars;
foreach (keys %char_names) {
	$name_chars{$char_names{$_}}=$_;
}
$char_names{''}='empty';

sub new {
	my $proto=shift;
	my $class = ref($proto) || $proto;
	my @this={};
	$this->{obuffer}='';
	$this->{program}=\();
	$this->{stopped}=0;
	$this->{retcode}=0;
	bless($this, $class);
	return $this;
}

# Pass it a character, returns true only if the character is influenced
# by gravity.
sub can_fall {
	return $_[1]=~m/[a-f0-9()]/;
}

# Pass it a character, returns true only if that character is a valid
# surface that a crate can rest on.
sub is_surface {
	return $_[1]=~m/[-+a-f0-9()=\/><:;.,*OKWMVACDX]/;
}

# Pass it a character, returns true only if that character is a crate.
sub is_crate {
	return $_[1]=~m/[a-f0-9]/;
}

# Pass it a character, returns true only if it is a dozer.
sub is_dozer {
	return $_[1]=~m/[()]/;
}

# Pass it a character, returns true only if dozers reverse when they hit those.
sub is_dozer_reverser {
	return $_[1]=~m/[=*]/;
}

# Load up a rube source file.
sub load { 
	my $this=shift;
	my $fn=shift;
	
	open (RUBE_IN,"<$fn") || die "Unable to load \"$fn\": $!\n";
	while (<RUBE_IN>) {
		chomp;
		push @p, $_;
	}
	close RUBE_IN;
	
	$this->{program}=\@p;
}

# Output the current program state.
sub debug { 
	my $this=shift;
	
	my @p=@{$this->{program}};
	foreach (@{$this->{program}}) {
		print "$_\n";
	}
	print "-" x 79 ."\n";
}

# Pass it x, y coordinates and it returns the character at those coordinates
# in the program.
sub get {
	my ($this, $x, $y)=@_;
	return substr(${$this->{program}}[$y], $x, 1);
}

# Pass it x, y coordinates and a value; the buffer at those coordinates
# is set to that value.
sub set {
	my ($this, $x, $y, $value)=@_;
	substr(${$this->{buffer}}[$y], $x, 1) = $value;
}

# This next set of functions handle performing one operation on various
# objects in the program. They are passed the coordinates of the object,
# and should make that object do whatever it does. Note that any changes
# to the program this causes should be written to the buffer array, but the
# program array should be examined to see the nearby environment.
# (set and get handle this, for the most part)

sub furnace {
}

sub crate_killer {
}

sub dozer_killer {
}

sub right_dozer {
}

sub left_dozer {
}

sub crumble_wall {
}

# Move items on top to the right. (TODO: what items exactly should move?)
sub right_conveyer {
}

sub left_conveyer {
}

# If two crates stack up above an output block, and a "c" or a ?? are below it,
# it converts them to ASCII or binary and returns them. TODO: check that is 
# right. The block order may be wrong too. This whole thing needs looking at
# with the spec in the other hand.
sub output {
	my ($this, $x, $y)=@_;
	if ($this->is_crate($this->get($x, $y-1)) &&
	    $this->is_crate($this->get($x, $y-2))) {
		if ($this->get($x, $y+1) eq 'c') {
			$this->{obuffer}.=
				chr hex $this->get($x, $y-2).$this->get($x, $y-1);
		}
		$this->set($x, $y-1, ' ');
		$this->set($x, $y-2, ' ');
	}
}

sub exit {
}

# Empty space pulls whatever is above it downward. A wacky
# way to implement gravity, I do admit. Note that if more than
# one item above the empty space can fall, they all do.
sub empty {
	my ($this, $x, $y)=@_;
	while ($this->can_fall($this->get($x, --$y))) {
		$this->set($x, $y+1, $this->get($x, $y));
	}
	$this->set($x, $y+1, $name_chars{empty});
}

# Step the program once.
sub step {
	my $this=shift;
	
	my @program=@{$this->{program}};
	
	# Initialize the buffer to be a copy of the program.
	$this->{buffer}=[@program];
	
	# Figure out the longest line in the program. All other lines
	# will be assumed to be this long, if they arn't the remainder
	# of them is just treated as empty space.
	my $max=0;
	foreach (my $y=0; $y <= $#program ; $y++) {
		$max = length($program[$y]) if $max < length($program[$y]);
	}

	for (my $y=0; $y <= $#program ; $y++) {
		for (my $x=0; $x < $max; $x++) {
			# Call the function corresponding to this
			# character in the program.
			my $f=$char_names{$this->get($x,$y)};
			$this->$f($x,$y) if $f;
		}
	}
	
	# Turn the buffer into the program.
	$this->{program}=$this->{buffer};
}

# Output the contents of the output buffer.
sub write {
	my $this=shift;
	
	if ($this->{obuffer} ne '') {
		print "==> ".$this->{obuffer}."\n";
		$this->{obuffer}='';
	}
}

sub AUTOLOAD {
	my $this=shift;
	
	my $name = $AUTOLOAD;
	$name =~ s/.*://; # strip fully-qualified portion
	
	if (@_) {
		return $this->{$name} = shift;
	}
	else {
		return $this->{$name};
	}       
}

#######################################################################

my $rube=Rube->new;
$rube->load(shift);
$rube->debug;
until ($rube->stopped) {
	$rube->step;
	$rube->debug;
	$rube->write;
}

exit $rube->retcode;
