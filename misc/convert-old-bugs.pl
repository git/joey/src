#!/usr/bin/perl

# This monster takes a file or files in html format as output by the bts in
# 1997, and outputs a bts db files as used by the bts in 2001.

use Date::Parse;
use HTML::Entities;

my $archivedir=shift;
mkdir $archivedir;

my %state = (
	autocheck => chr(1)."\n",
	recips => chr(2)."\n",
	end => chr(3)."\n",
	sep => chr(4),
	go => chr(5)."\n",
	html => chr(6)."\n",
	incoming => chr(7)."\n",
);
sub convert_log {
	my @lines=split(/\n/, shift());
	my $state='skip';
	my $message='';
	my $html='';
	my @chunks=();
	foreach (@lines) {
#		print "$state: $_\n";
		if ($state eq 'message' and m!^</pre>!) {
			$state='skip';
			$message=decode_entities($message);
			if ($message=~m/^Received: \(at (\S+)\) by (\S+)\;/) {
				push @chunks, $state{incoming}.$message.$state{end};
			}
			else {
				# This autocheck stuff is EVIL, and the
				# html is missing some necessary data, so
				# make it up.
				my @l=map { "X$_" } split(/\n/, $message);
				push @chunks, $state{autocheck}.
					"X-Debian-Bugs: This is an autoforward from debian-bugs\n\n".
					join("\n", @l)."\n\n".$state{end};
			}
			next;
		}
		elsif ($state eq 'skip' and /^<pre>/) {
			$state='message';
			$message='';
			next;
		}
		elsif ($state eq 'skip' and /^<strong>/i) {
			$state='html';
			$html='';
		}
		elsif ($state eq 'html' and m#Full text</A> available.#i) {
			$state='skip';
			my ($emailtext)=$html=~m#<code>(.*?)</code>#;
			my @emails=split(', ', $emailtext);
			push @chunks, $state{html}.$html.$state{end}.
			              $state{recips}.join($state{sep}, @emails)."\n".
				      $state{go}."Sorry, this message was lost when this bug report was restored from a backup.\n".
				      $state{end};
			next;
		}
		
		if ($state eq 'message') {
			$message.="$_\n";
		}
		elsif ($state eq 'html') {
			$html.="$_\n";
		}
	}
	return join("",reverse @chunks);
}

foreach my $file (@ARGV) {
	my %data=();
	open IN, "<$file" or die "$file: $!";
	$/=undef;
	$_=<IN>;
	close IN;
	
	my @lines=split(/\n/, $_);

	my ($num)=$file=~m/(\d+)\.html/;
	my ($dir)=$num=~m/(\d\d)$/;
	$dir="$archivedir/$dir";
	mkdir $dir;
	print "$file is $num, save to $dir\n";
	
	open OUT, ">$dir/$num.log" or die "$dir/$num.log: $!";
	print OUT convert_log($_);
	close OUT;
	
	if (m!<strong>Done:</strong> (.*?)[;.]\s!s) {
		$data{done}=decode_entities($1);
	}
	else {
		$data{done}='unknown';
	}
	if (/Maintainer for ([^ ]*) is/) {
		$data{package}=decode_entities($1);
	}
	elsif (/Package: <strong>\(?(.*?)\)?<\/strong>/) {
		$data{package}=decode_entities($1);
	}
	else {
		# unknown package
		if (m!</h1>Reported by:!) {
			$data{package}='unknown';
		}
	}
	
	if (/Reported by: ([^\n]*?);\s/s) {
		$data{originator}=decode_entities($1);
	}
	elsif (/Reported by: (.*?)\.\n/s) {
		$data{originator}=decode_entities($1);
	}
	if (m!<strong>Forwarded</strong> to (.*?);\s!) {
		$data{forwarded}=decode_entities($1);
	}
	else {
		$data{forwarded}='';
	}
	if (/merged with (.*?);/s) {
		my $raw=$1;
		my @nums=();
		foreach my $i (split(/\n/, $raw)) {
			if (m/>#(\d+)</) {
				push @nums, $1;
			}
		}
		$data{mergedwith}=join(' ', @nums);
	}
	else {
		$data{mergedwith}='';
	}
	if (/dated (.*?);\s/s) {
		$data{date}=str2time(decode_entities($1));
	}
	else {
                # Hard way.
                foreach my $line (reverse @lines) {
                        if ($line =~ /Date:\s*(.*)/i) {
                                $data{date}=str2time(decode_entities($1));
                                last;
                        }
                        last if $line =~ /<h2>Message received/;
                }
	}
	if ($lines[3] =~ m!(.*?)</h1>Package:!) {
		$data{subject}=decode_entities($1);
	}
	else {
		# Hard way.
		foreach my $line (reverse @lines) {
			if ($line =~ /Subject:\s*(.*)/i) {
				$data{subject}=decode_entities($1);
				last;
			}
			last if $line =~ /<h2>Message received/;
		}
	}
	foreach my $line (reverse @lines) {
		if ($line =~ /Message-Id:\s*(.*)/i) {
			$data{msgid}=decode_entities($1);
			last;
		}
		last if $line =~ /<h2>Message received/;
	}
	$data{keywords}=''; # don't think we had this back then.
	$data{severity}='normal'; # or this

	open (OUT, ">$dir/$num.status") or die "$dir/$num.status: $!";
	foreach my $field (qw{originator date subject msgid package keywords
			      done forwarded mergedwith severity}) {
		if (! exists $data{$field}) {
			print "!$file $field was not found\n";
		}
		print OUT "$data{$field}\n";
		print "\t$field = $data{$field}\n";
	}
	close OUT;
	
	my @report=();
	my $take=0;
	foreach my $line (reverse @lines) {
		if ($line eq '</pre>') {
			$take=1;
		}
		elsif ($line eq '<pre>') {
			last;
		}
		elsif ($take) {
			unshift @report, $line;
		}
	}
	if (! @report) {
		print "!$file report not found!\n";
	}
	open (OUT, ">$dir/$num.report") or die "$dir/$num.report: $!";
	print OUT join("\n", @report);
	close OUT;
}
