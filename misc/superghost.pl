#!/usr/bin/perl -w

use strict;
use Text::Wrap;
use Term::ReadLine;
my $term=Term::ReadLine->new('superghost');
my %words;
my %wordsleft;
my $computerword;

# Pass in a prompt, a default value, a validation function, and a
# help test. The user will be prompted, their input validated, if the
# function returns false, the help text will be displayed and they will be
# prompted again.
sub prompt {
	my ($prompt, $default, $validate, $help)=@_;
	my $ret=lc($term->readline($prompt, $default));
	return $ret if &$validate($ret);
	$help=~y/ \t\n/ /s;
	print "\n".wrap("","", $help."\n")."\n";
	$ret=lc($term->readline($prompt, $default)) until &$validate($ret);
	return $ret;
}

sub computer_play {
	my $word=shift;

	# First, eliminate all words that the word fragment is not
	# a substring of, or that would cause us to lose immediatly.
	my @losers;
	foreach (keys %wordsleft) {
		if (length($_) == length($word) + 1 && $_ =~ m/$word/) {
			push @losers, $_;
			delete $wordsleft{$_};
		}
		elsif ($_ !~ m/$word/) {
			delete $wordsleft{$_};
		}
	}
	
	# Are any words actually left, or did the computer lose?
	if (! %wordsleft) {
		print "You win! ";
		if (@losers) {
			print "The only word I can think of is $losers[0].\n";
		}
		else {
			print "I don't know any words that contain \"$word\".\n";
			prompt("What is the complete word? ", '', sub { 1 }, '');
		}
		return "";
	}
	else {
# TODO: this sometimes fails and generates something that is a full word.
# Ugh.
	
		# Pick one of the remaining words at random.
		my @wordsleft=keys %wordsleft;
		$computerword=$wordsleft[rand(@wordsleft)];
		# Make a list of letters ajoining the word fragment.
		# It may have more than 2 elements, if the complete 
		# word contains the fragment twice.
		my @letterlist;
		push @letterlist, $computerword=~m/(.)$word/g;
		push @letterlist, $computerword=~m/$word(.)/g;
		# Make a complimentary list of new word fragments.
		my @wordlist;
		push @wordlist, $computerword=~m/(.$word)/g;
		push @wordlist, $computerword=~m/($word.)/g;
	
		# Now pick the list item to use at random.
		my $item=rand(@letterlist);
		$word=$wordlist[$item];		
		print "I'll add a '$letterlist[$item]', giving \"$word\".\n";
		return $word;
	}
}

print "Welcome to SuperGhost!\n\n";

# Read wordlist.
open (WORDS, "/usr/share/dict/words") || die "/usr/share/dict/words: $!";
while (<WORDS>) {
	chomp;
	# Upper-cased words in my wordlist are proper nouns, which 
	# are not legal in SuperGhost.
	$words{$_}=1 unless $_ eq ucfirst $_;
}
close WORDS;

do {
	@wordsleft{keys %words}=values %words;

	my $currentplayer=int(rand(2));
	my $word='';
	if ($currentplayer == 0) { # computer starts
		# Just pick a random letter of the alphabet.
		$word=chr(rand(26) + 97);
		print "We will start with the letter '$word'.\n";
	}
	else { # human starts
		$word=prompt(
			"You go first. Pick a letter to start with: ", '', 
			sub { shift =~ m/^[a-z]$/ },
			"The game begins with one of the players picking a
			letter to start with. Any letter in the alphabet will
			do.",
		);
	}

	for (;;) {
		$currentplayer = !$currentplayer;
		if ($currentplayer == 0) {
			$word=computer_play($word) || last;
		}
		else {
			$word=prompt(
				"Enter word fragment: ", $word,
				sub {
					my $newword=shift;
					
					return 1 if $newword eq 'q';
					$newword =~ m/^[a-z]*$/ &&
					length($newword) == length($word)+1 &&
					(substr($newword, 0, length $word) eq $word ||
					 substr($newword, 1, length $word) eq $word)
				},
				"The word fragment so far is \"$word\".
				You must think of a letter that can be added
				to that, which will make it still be a
				fragment of a word, without itself being a 
				word. If you cannot, just type in 'q' to give
				up.",
			);
			if ($word eq 'q') {
				print "I was thinking of the word \"$computerword\".\n";
				last;
			}
			if ($words{$word}) {
				print "You entered a full word; you lose\n";
				last
			}
		}
	}
} until prompt(
	"Would you like to play again? ", "y",
	sub { shift =~ m/^(y|n|yes|no)$/ },
	"Just answer \"yes\" or \"no\".",
	) =~ m/^n/;

print "\nBye!\n";
