#!/usr/bin/perl
# This is an sirc script to automate sucking files down from those 
# servers out there. And eventually, it will get them all..
# To use, start sirc, join the channel, download lists and then
# /load this file and /suck
#use warnings;
#use strict;
use Data::Dumper;

# config
my $listpooldir="lists";
my $skiplist="lists/done/skip";
my $downloaddir=".";
my $db="$ENV{HOME}/ircsuck.dat";
my $debuglog="$ENV{HOME}/ircsuck.log";
my $start_download_timeout=60 * 60; # should be long; servers have queues
my $stall_time=60;
my $stall_giveup_time=60 * 2;
my $checkpoint_time=60 * 60 * 2;
# a bit larger than the average queue size is ok, it will fill up the queue
# and then avoid trying to overfill it (much) as long as
# start_download_timeout is nice and long
my $queuemax=10;

my $channel="#bookz";

{
	package WantedItem;

	sub new {
		my WantedItem $this = shift;
		unless (ref $this) {
			$this = bless({}, $this);
		}
		$this->{creationtime}=time;
		$this->{modtime}=time;
		$this->{filename}=shift;
		$this->{_sources}=[];
		$this->{done}=0;
		$this->{queued}=0;
		return $this;
	}

	sub sources {
		my $this=shift;
		return @{$this->{_sources}};
	}

	sub add_source {
		my $this=shift;
		my $source=shift;
		$this->{modtime}=time;
		foreach my $item ($this->sources) {
			if ($item->{trigger} eq $source->{trigger}) {
				return;
			}
		}
		push @{$this->{_sources}}, $source;
	}

	sub remove_source {
		my $this=shift;
		my $source=shift;
		my @sources;
		$this->{modtime}=time;
		foreach my $item ($this->sources) {
			push @sources, $item unless $item->{trigger} eq $source->{trigger};
		}
		$this->{_sources}=\@sources;
	}
}

{
	package Source;

	sub new {
		my Source $this = shift;
		unless (ref $this) {
			$this = bless({}, $this);
		}
		$this->{creationtime}=time;
		$this->{trigger}=shift;
		$this->{servername}=shift;
		return $this;
	}

	sub trigger {
		my $this=shift;

		&main::say($this->{trigger});
	}
}

{
	package Download;

	sub new {
		my Download $this = shift;
		unless (ref $this) {
			$this = bless({}, $this);
		}
		$this->{creationtime}=time;
		$this->{filename}=shift;
		$this->{server}=shift;
		$this->{item}=shift;
		$this->{lastchangetime}=0;
		$this->{lastsize}=0;
		return $this;
	}

	sub updated {
		my $this=shift;
		my $size=(-s $this->{filename});
		if ($size > $this->{lastsize}) {
			$this->{lastsize}=$size;
			$this->{lastchangetime}=time;
			return 1;
		}
		else {
			return 0;
		}
	}

	sub done {
		my $this=shift;
		$this->stop;
		$this->{item}->{done}=1;
	}
	
	sub stop {
		my $this=shift;
		$this->{server}->dequeue($this->{item}) if ref $this->{server};
	}
}

{
	package Server;
	
	sub new {
		my Server $this = shift;
		unless (ref $this) {
			$this = bless({}, $this);
		}
		$this->{creationtime}=time;
		$this->{seentime}=time;
		$this->{name}=shift;
		$this->{_queue}=[];
		$this->{queuemax}=$queuemax;
		return $this;
	}

	sub queue {
		my $this=shift;
		return @{$this->{_queue}};
	}

	sub starved {
		my $this=shift;
		return ($#{$this->{_queue}} + 1) < $this->{queuemax};
	}

	sub enqueue {
		my $this=shift;
		my $item=shift;
		return if $item->{queued} || $item->{done};
		foreach my $source ($item->sources) {
			if ($source->{servername} ne $this->{name}) {
				next;
			}
			if ($this->starved) {
				$this->dequeue($item);
				push @{$this->{_queue}}, $item;
				$item->{queued}=1;
				$source->trigger;
				return $item;
			}
			else {
				return undef;
			}
		}
	}

	sub dequeue {
		my $this=shift;
		my $item=shift;
		$item->{queued}=0;
		my @queue;
		foreach my $i ($this->queue) {
			push @queue, $i unless $i == $item;
		}
		$this->{_queue}=\@queue;
	}
	
	sub killqueue {
		my $this=shift;
		$this->{_queue}=[];
	}
}

our %wanteditems;
our %servers;
our %downloads;
our %skiplist;

open (DEBUGLOG, ">>$debuglog");
sub debug {
	&print("ircsuck: @_");
	CORE::print DEBUGLOG localtime(time).": @_\n";
}

my %lasttimes;
sub runat {
	my $action=shift;
	my $freq=shift;
	if (! exists $lasttimes{$action} || time - $lasttimes{$action} >= $freq) {
		$lasttimes{$action}=time;
		eval $action;
		die $@ if $@;
	}
}

sub savedb {
	open (OUT, ">$db") || warn "$db: $!";
	my $dumper=Data::Dumper->new([\%wanteditems, \%servers, \%downloads],
	                             [qw(*wanteditems *servers *downloads)]);
	$dumper->Indent(1);
	print OUT $dumper->Dump;
	$dumper->Reset;
	print OUT "\n1";
	close OUT;
}

sub loaddb {
	if (-e $db) {
		debug("loading db");
		my ($wanteditems, $servers, $downloads);
		require $db;
	}
}

sub sirc_munge {
	my $fn=shift;
	$fn=~s/\s/_/g; # sirc does space munging
	return $fn;
}

sub parselist {
	my $list=shift;

	my $addcount=shift;
	my $totalcount=shift;
	
	open (LIST, $list) || warn "open $list: $!\n";
	while (<LIST>) {
		s/\r/ /g;
		if (/^\s*\!([^\s]+)(?:\s+|\t)(.*)/) {
			my $servername=$1;
			my $filename=$2;
			$filename=~s/\s\s.*//;
			my $trigger="!$servername $filename";
			
			$totalcount++;
			
			if (-e "$downloaddir/done/".sirc_munge($filename)) {
				#debug("ignoring done item $filename");
				next;
			}
			next if exists $skiplist{sirc_munge($filename)};
			
			$addcount++;
			
			my $server;
			if (exists $servers{$servername}) {
				$server=$servers{$servername};
			}
			else {
				$server=Server->new($servername);
				$servers{$servername}=$server;
			}
			$server->{seentime}=time;

			my $wanteditem;
			if (exists $wanteditems{$filename}) {
				$wanteditem=$wanteditems{$filename};
			}
			else {
				$wanteditem=WantedItem->new($filename);
				$wanteditems{$filename}=$wanteditem;
			}
			
			my $source=Source->new($trigger, $servername);
			$wanteditem->add_source($source);
		}
	}
	close LIST;

	debug("for $list, have ".($totalcount - $addcount)."/$totalcount items");
}

sub loadskiplist {
	open(IN, $skiplist) || debug("no skiplist");
	while (<IN>) {
		chomp;
		$skiplist{$_}=1;
	}
	close IN;
}

sub loadlists {
	my $loadtime=time;
	
	foreach my $list (glob("$listpooldir/*")) {
		next unless -f $list;
		parselist($list);
	}

	foreach my $server (keys %servers) {
		if ($servers{$server}->{seentime} < $loadtime) {
			debug("removing server $server: no files");
			delete $servers{$server};
		}
	}
	foreach my $item (keys %wanteditems) {
		if ($wanteditems{$item}->{modtime} < $loadtime) {
			debug("removing item $item: no sources");
			delete $wanteditems{$item};
		}
	}
	# TODO: remove sources that are no longer listed
}

sub scandownloads {
	foreach my $filename (glob("$downloaddir/*")) {
		next if -d $filename;
		my ($basename)=$filename=~m!.*/(.*)!;
		if (exists $downloads{$basename}) {
			my $dl=$downloads{$basename};
			if (!$dl->updated && $dl->{lastchangetime} && time - $dl->{lastchangetime} > $stall_time) {
				debug("download $basename is idle for ".(time - $dl->{lastchangetime})." seconds");
				if (time - $dl->{lastchangetime} > $stall_giveup_time) {
					debug("done download $basename");
					$downloads{$basename}->done;
					delete $downloads{$basename};
					system("mv", "-f", $filename, "$downloaddir/done/");
				}
					
			}
		}
		else {
			debug("ignoring spurious downloaded file $basename");
		}
	}
}

sub checkstarted {
	foreach my $dl (values %downloads) {
		if ($dl->{lastchangetime} == 0 &&
		    time - $dl->{creationtime} > $start_download_timeout) {
		    	debug("never started download of ".$dl->{filename});
			$dl->stop;
			# Note: not removed from list, so if it starts up
			# anyway, the download will still be processed.
		}
	}
}

sub queuedownloads {
	my $count=0;
	foreach my $server (keys %servers) {
		my $add=0;
		if ($servers{$server}->starved && $count < 3) {
			foreach my $item (values %wanteditems) {
				my $fn=sirc_munge($item->{filename});
				next if -e "$downloaddir/done/$fn"; # double check for done items
				if ($servers{$server}->enqueue($item)) {
					$count++;
					$downloads{$fn}=Download->new("$downloaddir/$fn", $servers{$server}, $item);
					debug("enqueued $fn on $server");
					$added++;
					last;
				}
			}
		}
		if ($servers{$server}->starved && ! $added) {
			debug("$server is starving");
		}
	}
}

sub process {
	runat(scandownloads => $stall_time / 10);
	runat(checkstarted => $start_download_timeout);
	runat(queuedownloads => $stall_time / 10);
	#runat(savedb => $checkpoint_time);
	timer(10, "process", 1);

	# Really doesn't belong here, robustness measure.
	if ($channel) {
		&sl("JOIN $channel");
	}
}

my $channel;
sub cmd_suck {
	$channel=$talkchannel;

	loadskiplist();
	loadlists();
	timer(10, "process", 1);
	debug("sucking enabled on $channel..");
}

sub cmd_update {
	loadlists();
}

sub hook_adcc_get {
	return unless $_[0] eq 'SEND';
	my $dfile = $_[3];

	&docommand("dcc get $who $dfile");
}

sub hook_disc {
	debug("disconncted from server");
	sleep 5;
	&docommand("server 1");
}

sub hook_reconnect {
	if ($channel) {
		debug("reconnecting");
		&sl("JOIN $channel");
	}
}

my $exit_save_done=0;
sub END {
	if (! $exit_save_done) {
		debug("exiting..");
		#savedb;
		$exit_save_done=1;

		# remove partial downloads
		foreach my $file (keys %downloads) {
			unlink("$downloaddir/$file");
		}
	}
};

system("mkdir -p $downloaddir/done");
#loaddb;
&addcmd("suck");
&addcmd("update");
&addhook("dcc_request", "adcc_get");
&addhook("disconnect", "disc");
&addhook("376", "reconnect");

#while (1) {
#	process;
#	sleep 10;
#}
