#!/usr/bin/perl

# This script takes a tokenized ATARI BASIC Binary and converts it to either ANSI or ATASCII
# In ANSI Mode, inverse characters are reverse video, control characters are [a-z] in red,
# and inverse control characters are [a-z] in black on red.

# THEN could be:

# THEN <LINE #> 
# THEN STATEMENT

# Chris Martin <atariguy1@attbi.com>
# 2003.06.27

$debug = 0;
$ascii = 1;

&process_args();

@statement_text = ( "REM ", 
                    "DATA ", 
                    "INPUT ",
                    "COLOR ",
                    "LIST ",
                    "ENTER ",
                    "LET ",
                    "IF ",
                    "FOR ",
                    "NEXT ",
                    "GOTO ",
                    "GO TO ",
                    "GOSUB ",
                    "TRAP ",
                    "BYE ",
                    "CONT ",
                    "COM ",
                    "CLOSE ",
                    "CLR ",
                    "DEG ",
                    "DIM ",
                    "END ",
                    "NEW ",
                    "OPEN ",
                    "LOAD ",
                    "SAVE ",
                    "STATUS ",
                    "NOTE ",
                    "POINT ",
                    "XIO ",
                    "ON ",
                    "POKE ",
                    "PRINT ",
                    "RAD ",
                    "READ ",
                    "RESTORE ",
                    "RETURN ",
                    "RUN ",
                    "STOP ",
                    "POP ",
                    "? ",
                    "GET ",
                    "PUT ",
                    "GRAPHICS ",
                    "PLOT ",
                    "POSITION ",
                    "DOS ",
                    "DRAWTO ",
                    "SETCOLOR ",
                    "LOCATE ",
                    "SOUND ",
                    "LPRINT ",
                    "CSAVE ",
                    "CLOAD ",
                    "",    # Implied let!
                    "ERROR - "
                  );

%operation_text = (0x11, " THEN ",
              0x12, ",",
              0x13, "\$",
              0x14, ":",
              0x15, ";",
              0x17, " GOTO ",
              0x18, " GOSUB ",
              0x19, " TO ",
              0x1a, " STEP ",
              0x1b, " THEN ",
              0x1c, "#",
              0x1d, "<=",
              0x1e, "<>",
              0x1f, ">=",
              0x20, "<",
              0x21, ">",
              0x22, "=",
              0x23, " ",
              0x24, "*",
              0x25, "+",
              0x26, "-",
              0x27, "/",
              0x28, " NOT ",
              0x29, " OR ",
              0x2a, " AND ",
              0x2b, "(",
              0x2c, ")",
              0x2d, "=",
              0x2e, "=",
              0x2f, "<=",
              0x30, "<>",
              0x31, ">=",
              0x32, "<",
              0x33, ">",
              0x34, "=",
              0x35, "+",
              0x36, "-",
              0x37, "(",
              0x38, "", #ARRAY LEFT PAREN
              0x39, "", # DIM ARRAY LEFT PAREN
              0x3a, "(",
              0x3b, "(",
              0x3c, ","
             ); 
%function_text = (0x3d, "STR\$",
                  0x3e, "CHR\$",
                  0x3f, "USR",
                  0x40, "ASC",
                  0x41, "VAL",
                  0x42, "LEN",
                  0x43, "ADR",
                  0x44, "ATN",
                  0x45, "COS",
                  0x46, "PEEK",
                  0x47, "SIN",
                  0x48, "RND",
                  0x49, "FRE ",
                  0x4a, "EXP",
                  0x4b, "LOG",
                  0x4c, "CLOG",
                  0x4d, "SQR",
                  0x4e, "SGN",
                  0x4f, "ABS",
                  0x50, "INT",
                  0x51, "PADDLE",
                  0x52, "STICK",
                  0x53, "PTRIG",
                  0x54, "STRIG"
                 ); 
             #\x1b[0;40;36m\\\x1b[0m
%ascii = (
          0x00, "\x1b[0;40;31m0\x1b[0m",
          0x01, "\x1b[0;40;31ma\x1b[0m",
          0x02, "\x1b[0;40;31mb\x1b[0m",
          0x03, "\x1b[0;40;31mc\x1b[0m",
          0x04, "\x1b[0;40;31md\x1b[0m",
          0x05, "\x1b[0;40;31me\x1b[0m",
          0x06, "\x1b[0;40;31mf\x1b[0m",
          0x07, "\x1b[0;40;31mg\x1b[0m",
          0x08, "\x1b[0;40;31mh\x1b[0m",
          0x09, "\x1b[0;40;31mi\x1b[0m",
          0x0a, "\x1b[0;40;31mj\x1b[0m",
          0x0b, "\x1b[0;40;31mk\x1b[0m",
          0x0c, "\x1b[0;40;31ml\x1b[0m",
          0x0d, "\x1b[0;40;31mm\x1b[0m",
          0x0e, "\x1b[0;40;31mn\x1b[0m",
          0x0f, "\x1b[0;40;31mo\x1b[0m",
          0x10, "\x1b[0;40;31mp\x1b[0m",
          0x11, "\x1b[0;40;31mq\x1b[0m",
          0x12, "\x1b[0;40;31mr\x1b[0m",
          0x13, "\x1b[0;40;31ms\x1b[0m",
          0x14, "\x1b[0;40;31mt\x1b[0m",
          0x15, "\x1b[0;40;31mu\x1b[0m",
          0x16, "\x1b[0;40;31mv\x1b[0m",
          0x17, "\x1b[0;40;31mw\x1b[0m",
          0x18, "\x1b[0;40;31mx\x1b[0m",
          0x19, "\x1b[0;40;31my\x1b[0m",
          0x1a, "\x1b[0;40;31mz\x1b[0m",

          0x1b, "\x1b[0;44;37mE\x1b[0m",
          0x1c, "\x1b[0;44;37m^\x1b[0m",
          0x1d, "\x1b[0;44;37mV\x1b[0m",
          0x1e, "\x1b[0;44;37m<\x1b[0m",
          0x1f, "\x1b[0;44;37m>\x1b[0m",
          0x20, " ",
          0x21, "!",
          0x22, "\"",
          0x23, "#",
          0x24, "\$",
          0x25, "%",
          0x26, "&",
          0x27, "'",
          0x28, "(",
          0x29, ")",
          0x2a, "*",
          0x2b, "+",
          0x2c, ",",
          0x2d, "-",
          0x2e, ".",
          0x2f, "/",
          0x30, "0",
          0x31, "1",
          0x32, "2",
          0x33, "3",
          0x34, "4",
          0x35, "5",
          0x36, "6",
          0x37, "7",
          0x38, "8",
          0x39, "9",
          0x3a, ":",
          0x3b, ";",
          0x3c, "<",
          0x3d, "=",
          0x3e, ">",
          0x3f, "?",
          0x40, "@",
          0x41, "A",
          0x42, "B",
          0x43, "C",
          0x44, "D",
          0x45, "E",
          0x46, "F",
          0x47, "G",
          0x48, "H",
          0x49, "I",
          0x4a, "J",
          0x4b, "K",
          0x4c, "L",
          0x4d, "M",
          0x4e, "N",
          0x4f, "O",
          0x50, "P",
          0x51, "Q",
          0x52, "R",
          0x53, "S",
          0x54, "T",
          0x55, "U",
          0x56, "V",
          0x57, "W",
          0x58, "X",
          0x59, "Y",
          0x5a, "Z",
          0x5b, "[",
          0x5c, "\\",
          0x5d, "]",
          0x5e, "^",
          0x5f, "_",
          0x60, "`",
          0x61, "a",
          0x62, "b",
          0x63, "c",
          0x64, "d",
          0x65, "e",
          0x66, "f",
          0x67, "g",
          0x68, "h",
          0x69, "i",
          0x6a, "j",
          0x6b, "k",
          0x6c, "l",
          0x6d, "m",
          0x6e, "n",
          0x6f, "o",
          0x70, "p",
          0x71, "q",
          0x72, "r",
          0x73, "s",
          0x74, "t",
          0x75, "u",
          0x76, "v",
          0x77, "w",
          0x78, "x",
          0x79, "y",
          0x7a, "z",
          0x7b, "{",
          0x7c, "|",
          0x7d, "}",
          0x7e, "~",
          0x7f, "\x1b[0;44;37mT\x1b[0m", #DEL(ASCII); TAB(ATASCII)

          0x80, "\x1b[0;41;30m0\x1b[0m",
          0x81, "\x1b[0;41;30ma\x1b[0m",
          0x82, "\x1b[0;41;30mb\x1b[0m",
          0x83, "\x1b[0;41;30mc\x1b[0m",
          0x84, "\x1b[0;41;30md\x1b[0m",
          0x85, "\x1b[0;41;30me\x1b[0m",
          0x86, "\x1b[0;41;30mf\x1b[0m",
          0x87, "\x1b[0;41;30mg\x1b[0m",
          0x88, "\x1b[0;41;30mh\x1b[0m",
          0x89, "\x1b[0;41;30mi\x1b[0m",
          0x8a, "\x1b[0;41;30mj\x1b[0m",
          0x8b, "\x1b[0;41;30mk\x1b[0m",
          0x8c, "\x1b[0;41;30ml\x1b[0m",
          0x8d, "\x1b[0;41;30mm\x1b[0m",
          0x8e, "\x1b[0;41;30mn\x1b[0m",
          0x8f, "\x1b[0;41;30mo\x1b[0m",
          0x90, "\x1b[0;41;30mp\x1b[0m",
          0x91, "\x1b[0;41;30mq\x1b[0m",
          0x92, "\x1b[0;41;30mr\x1b[0m",
          0x93, "\x1b[0;41;30ms\x1b[0m",
          0x94, "\x1b[0;41;30mt\x1b[0m",
          0x95, "\x1b[0;41;30mu\x1b[0m",
          0x96, "\x1b[0;41;30mv\x1b[0m",
          0x97, "\x1b[0;41;30mw\x1b[0m",
          0x98, "\x1b[0;41;30mx\x1b[0m",
          0x99, "\x1b[0;41;30my\x1b[0m",
          0x9a, "\x1b[0;41;30mz\x1b[0m",

          0x9b, "\x1b[0;44;37mR\x1b[0m",
          0x9c, "\x1b[0;44;37mD\x1b[0m",
          0x9d, "\x1b[0;44;37mI\x1b[0m",
          0x9e, "\x1b[0;45;37mT\x1b[0m",
          0x9f, "\x1b[0;46;37mT\x1b[0m",
          0xa0, " ",
          0xa1, "!",
          0xa2, "\"",
          0xa3, "#",
          0xa4, "\$",
          0xa5, "%",
          0xa6, "&",
          0xa7, "'",
          0xa8, "(",
          0xa9, ")",
          0xaa, "*",
          0xab, "+",
          0xac, ",",
          0xad, "-",
          0xae, ".",
          0xaf, "/",
          0xb0, "0",
          0xb1, "1",
          0xb2, "2",
          0xb3, "3",
          0xb4, "4",
          0xb5, "5",
          0xb6, "6",
          0xb7, "7",
          0xb8, "8",
          0xb9, "9",
          0xba, ":",
          0xbb, ";",
          0xbc, "<",
          0xbd, "=",
          0xbe, ">",
          0xbf, "?",
          0xc0, "@",
          0xc1, "A",
          0xc2, "B",
          0xc3, "C",
          0xc4, "D",
          0xc5, "E",
          0xc6, "F",
          0xc7, "G",
          0xc8, "H",
          0xc9, "I",
          0xca, "J",
          0xcb, "K",
          0xcc, "L",
          0xcd, "M",
          0xce, "N",
          0xcf, "O",
          0xd0, "P",
          0xd1, "Q",
          0xd2, "R",
          0xd3, "S",
          0xd4, "T",
          0xd5, "U",
          0xd6, "V",
          0xd7, "W",
          0xd8, "X",
          0xd9, "Y",
          0xda, "Z",
          0xdb, "[",
          0xdc, "\\",
          0xdd, "]",
          0xde, "^",
          0xdf, "_",
          0xe0, "`",
          0xe1, "a",
          0xe2, "b",
          0xe3, "c",
          0xe4, "d",
          0xe5, "e",
          0xe6, "f",
          0xe7, "g",
          0xe8, "h",
          0xe9, "i",
          0xea, "j",
          0xeb, "k",
          0xec, "l",
          0xed, "m",
          0xee, "n",
          0xef, "o",
          0xf0, "p",
          0xf1, "q",
          0xf2, "r",
          0xf3, "s",
          0xf4, "t",
          0xf5, "u",
          0xf6, "v",
          0xf7, "w",
          0xf8, "x",
          0xf9, "y",
          0xfa, "z",
          0xfb, "{",
          0xfc, "|",
          0xfd, "}",
          0xfe, "~",
          0xff, "\x1b[0;47;30mI\x1b[0m" #DEL (ASCII) , Insert Char (ATASCII)
         );
#$bas_file = $ARGV[0];
if($bas_file !~ /\w+/) {
  die "ERROR: Cannot open BASIC file '$bas_file'...\n\n";
}

print STDERR "Reading file '$bas_file'...\n";

open(BASFILE, "$bas_file");
$num = read(BASFILE, $bas_string, 0x1000000);
close(BASFILE);

$offset = 0;
#$lomem = unpack('S*', pack('a*', substr($bas_string,$offset,2)));
$lomem = ord(substr($bas_string,$offset,1)) + (ord(substr($bas_string,$offset+1,1))<<8);

$offset = 2;
#$vntp = unpack('S*', pack('a*', substr($bas_string,$offset,2)));
$vntp = ord(substr($bas_string,$offset,1)) + (ord(substr($bas_string,$offset+1,1))<<8);

$offset = 4;
#$vntd = unpack('S*', pack('a*', substr($bas_string,$offset,2)));
$vntd = ord(substr($bas_string,$offset,1)) + (ord(substr($bas_string,$offset+1,1))<<8);

$offset = 6;
#$vvtp = unpack('S*', pack('a*', substr($bas_string,$offset,2)));
$vvtp = ord(substr($bas_string,$offset,1)) + (ord(substr($bas_string,$offset+1,1))<<8);

$offset = 8;
#$stmtab = unpack('S*', pack('a*', substr($bas_string,$offset,2)));
$stmtab = ord(substr($bas_string,$offset,1)) + (ord(substr($bas_string,$offset+1,1))<<8);

$offset = 10;
#$stmcur = unpack('S*', pack('a*', substr($bas_string,$offset,2)));
$stmcur = ord(substr($bas_string,$offset,1)) + (ord(substr($bas_string,$offset+1,1))<<8);

$offset = 12;
#$starp = unpack('S*', pack('a*', substr($bas_string,$offset,2)));
$starp = ord(substr($bas_string,$offset,1)) + (ord(substr($bas_string,$offset+1,1))<<8);

$offset = 14;
#$runstk = unpack('S*', pack('a*', substr($bas_string,$offset,2)));
$runstk = ord(substr($bas_string,$offset,1)) + (ord(substr($bas_string,$offset+1,1))<<8);

$offset = 16;
#$memtop = unpack('S*', pack('a*', substr($bas_string,$offset,2)));
$memtop = ord(substr($bas_string,$offset,1)) + (ord(substr($bas_string,$offset+1,1))<<8);


printf(STDERR "MEMLO  = %d.\n", $lomem);
printf(STDERR "VNTP   = %d.\n", $vntp);
printf(STDERR "VNTD   = %d.\n", $vntd);
printf(STDERR "STMTAB = %d.\n", $stmtab);
#<STDIN>;

#build variable names
$j = 0;
for($k = 128; $k < 256; $k++) { #line 451 - supposed to be < 256, but can lock up.
  $var_char = 0;
  #while(($var_char < 128) && (($vntp-256+14+$j)<$stmtab)) {
  while(($var_char < 128) && (($vntp-256+14+$j)<=$vntd)) {
    $var_char = ord(substr($bas_string,($vntp-256+14+$j),1));
    if($var_char < 128) {
      $varname[$k] .= substr($bas_string,($vntp-256+14+$j),1);
    } else {
      $varname[$k] .= pack('s*', ord(substr($bas_string,($vntp-256+14+$j),1)) - 128);
      $varname[$k] =~ s/\x00//g;
    }

    $j++;
    #printf STDERR "J=  %d (%d).\n", $j, $var_char;
  }  
  if($debug) {printf(STDERR "V$k = %s.\n", $varname[$k]);}
}

#$ln_offset = 0;
$ln_offset = $stmtab - 256 + 14 ;
$last_line_num = 0;
while($ln_offset < $num) {

  $offset = 0;
  $line_num = ord(substr($bas_string,$ln_offset+$offset,1)) + (ord(substr($bas_string,1+$ln_offset+$offset,1))<<8);

  $offset = 2;
  $line_offset = ord(substr($bas_string,$ln_offset+$offset,1));

  $offset = 3;
  $statement_offset = ord(substr($bas_string,$ln_offset+$offset,1));

  #get line
  $line = substr($bas_string,$ln_offset,$line_offset);
  #$i = 0;
  #while($i < $line_offset) {
  #  printf("%02x ",  ord(substr($line,$i,1)));
  #  $i++
  #}
  #print "\n";

  if($debug) {
    printf(STDERR "line byte = %d, line_lo = %d, line_hi = %d, line offset = %d, state offset = %d.\n", $ln_offset, 
    ord(substr($bas_string,$ln_offset,1)), 
    ord(substr($bas_string,1+$ln_offset,1)),
    $line_offset, $statement_offset);
  }

  if(($line_num > $last_line_num) && ($line_offset >= $statement_offset)) {

    $last_line_num = $line_num;
    print("$line_num ");
    
    $i = 3;
    while($i < $line_offset) {
      $token = 0;
      $statement_offset = ord(substr($line,$i++,1));
  
      $statement = ord(substr($line,$i++,1));
      printf("%s", $statement_text[$statement]);
      
      $first = 1;
      $last_token = 0;
      while(($token != 0x14) && ($token != 0x16) && ($token != 0x1b) && ($i < $line_offset)) {
        $token = ord(substr($line,$i++,1));
  
        if($statement == 0x00) { #REM
          if($token != 0x9b) { #only print up to the atari newline char
            if($ascii) {


              if( ( ($token < 155) || ($token > 159) ) && ($token < 253)) {
                if(($last_token < 128) && ($token >= 128)) {
                  printf("\x1b[7m");
                } elsif(($last_token >= 128) && ($token < 128)) {
                  printf("\x1b[0m");
                }
              }


            
              $last_token = $token;
              printf("%s", $ascii{$token});
            } else {
              printf("%c", $token);
            }
  
          } else {
            if($ascii) {
              printf("\x1b[0m");
            }
          }
        } elsif($statement == 0x01) { #DATA
          if($token != 0x9b) { #only print up to the atari newline char
            #if($first) {
            #  $first = 0;
            #  printf("%d", $token);
            #} else {
            #  printf(",%d", $token);
            #}  
            printf("%c", $token);
          }
        } else {
  
          if($token < 0x80) {
            if($token == 0x0e) { #CONSTANT
              $numc0 = ord(substr($line,$i++,1));
              $bcd[0] = ord(substr($line,$i++,1));
              $bcd[1] = ord(substr($line,$i++,1));
              $bcd[2] = ord(substr($line,$i++,1));
              $bcd[3] = ord(substr($line,$i++,1));
              $bcd[4] = ord(substr($line,$i++,1));
              $j = $numc0 - 0x39;
              if($numc0 == 0) {
                print("0");
              }
              for($j = 0; $j <= $numc0 - 0x40; $j++) {
                if($j==0) {
                  printf("%x", $bcd[$j]);
                } else {
                  printf("%02x", $bcd[$j]);
                }
              }
              #printf("%02x %02x %02x %02x %02x %02x", $numc0,$numc1,$numc2,$numc3,$numc4,$numc5);
            } elsif($token == 0x0f) { #STRING CONSTANT 
              printf("\"");
              #while(($token != 0x14) && ($token != 0x16) && ($i < $line_offset)) {
              $string_end = $i + ord(substr($line,$i++,1));
              $last_token = 0;
              while($i <  $string_end) {
                $token = ord(substr($line,$i++,1));
                if($ascii) {

                  if( ( ($token < 155) || ($token > 159) ) && ($token < 253)) {
                    if(($last_token < 128) && ($token >= 128)) {
                      printf("\x1b[7m");
                    } elsif(($last_token >= 128) && ($token < 128)) {
                      printf("\x1b[0m");
                    }
                  }
                  $last_token = $token;

                
                  printf("%s", $ascii{$token});
                } else {
                  printf("%c", $token);
                }

              }
              if($ascii) {
                printf("\x1b[0m");
              }
              printf("\"");
    
            } elsif($token < 0x3d) {
              printf("%s", $operation_text{$token});
              #need to double check for BCD numbers after THEN
              if(($token == 0x1b) && (ord(substr($line,$i,1)) == 0x0e)  && (ord(substr($line,$i+1,1)) >= 0x40)) { #then
                $token = ord(substr($line,$i++,1));
                if($token == 0x0e) { #CONSTANT
                  $numc0 = ord(substr($line,$i++,1));
                  $bcd[0] = ord(substr($line,$i++,1));
                  $bcd[1] = ord(substr($line,$i++,1));
                  $bcd[2] = ord(substr($line,$i++,1));
                  $bcd[3] = ord(substr($line,$i++,1));
                  $bcd[4] = ord(substr($line,$i++,1));
                  $j = $numc0 - 0x39;
                  if($numc0 == 0) {
                    print("0");
                  }
                  for($j = 0; $j <= $numc0 - 0x40; $j++) {
                    if($j==0) {
                      printf("%x", $bcd[$j]);
                    } else {
                      printf("%02x", $bcd[$j]);
                    }
                  }
                } 
              }
    
            } else {
              printf("%s", $function_text{$token});
            }
          } else {
            # PRINT VARIABLE NAME
            #printf("V%d", $token);
            printf("%s", $varname[$token]);
          }
        }
      }  
    }
    if($ascii) {
      print("\n");
    } else {
      print("\x9b");
    }     
  
    $ln_offset = $ln_offset + $line_offset;
    if($debug) {<STDIN>; }
  } else { 
    while((ord(substr($bas_string,$ln_offset++,1))) != 0x16 && ($ln_offset <= $num)) {

    }
  } 
}



#----------------------------------------------------------------------------
# Sub-procedures
#----------------------------------------------------------------------------
sub process_args {
  @argv_save = @ARGV;
  while(@ARGV) {
    $_ = shift(@ARGV);
    if(/^-a/) { # ATASCII output
      $ascii = 0;
    
    } elsif(/^-d/) { # machine
      $debug = 1;
    } elsif(/^-f/) { # machine
      $bas_file = shift(@ARGV);
    } elsif(/^[^-]/) {
      $bas_file = $_;
    } else {
      &print_usage;
    }
  }

}

sub print_usage {
  $script = $0;
  $script =~ s/^.*\///g;
  print "\n  USAGE:\n";
  print "    $script [-a(tascii)] [-d(ebug)] [-f(ile) <BAS FILENAME>]\n";
  print "      -a : Print output in ATASCII\n";
  print "      -d : Print some debug information\n";
  print "\n  Example:\n";
  print "    $script -debug -atascii -file MYPRG.BAS\n\n";
  die "\n";
}

