This directory holds Atari disks from my 130XE. The BASIC programs on these
were written sometime between 1986 and the very early 90's.

The .atr.gz files are the raw disk images. I have also extracted each
disk's files into a subdirectory. I've used the labels of the disks as the
filenames.

Notes on individual disks:

finished_programs_disk_1.atr.gz
	The "NAME.PRO" menu program expects to find a formatted RAM disk at
	drive 8. If it's not there, line 10 fails. This can be worked
	around when it fails by replacing line 10, and re-running it:

	10 
	RUN

	Each of the programs on the menu is supposed to load NAME.PRO from
	D8 when it's time to return to the menu. This will also fail.

finished_programs_disk_2.atr.gz
	Much like the first, but has some new programs and some updated
	versions of the ones on disk 1.

	Has a functional copy of my tetris program on it!

finished_programs_disk_3_bak.atr.gz
	This boots into one of the better video games I did for the Atari,
	Fatso's Adventure.

	This seems to be an older version, without the opening banner
	screen, and with a less refined menu and graphics. However, it
	plays ok.

finished_programs_disk_3.atr.gz
	This is a more complete version of Fatso's Adventure.

	This disk may have been dying before I copied it.

notice.atr.gz
	Full label:

        -------------------------
	NOTICE.BAS (C)
	Master disk

	Property of Joey Hess
	-------------------------

	This is the last program I wrote for the Atari. It was used in
	Mary's office as a display program.

	There also seem to be some random BASIC games on here, like a
	version of "Trouble!"

atari_1.atr.gz
	This boots into a little choose your own adventure game.

	This also seems to be the disk that I used to develop NOTICE.BAS,
	as it contains several MARY files that are different versions of
	that.

atari_1_(creater-interpreter).atr.gz
	This doesn't seem to work, but it has several programs on it,
	including some sort of adventure game. Several of the programs seem
	to be ones that I typed in from manuals and books.

unlabeled1.atr.gz
	The label of this disk has been written over so many times that
	nothing is legible.

	It contains a bunch of misc programs and data files. I probably used
	this as a scratch disk while programming, fairly early on.

unlabeled2.atr.gz
	This disk had no label and contains misc files and programs.

cre.atr.gz
	This disk wasn't labeled, but it runs a "CRE" program on boot.
	This seems to be some kind of adventure game creator that I wrote.

atari_basic_prog_1_BAK.atr.gz
	A bunch of misc programs, probably another scratch disk.

unfinished_programs.atr.gz
	More misc basic programs, no menu.

data_files.atr.gz
	Contains some ATARIWriter files. Mostly seems to be letters to
	Atari Corp.

atari_disk_4.atr.gz
	The label says "data files only", and I find few programs on here.
	Seems to be mostly ".SCE" files, a format I don't know.

atari_tetris.atr.gz
	Contains a (broken) version of tetris and a few other programs and
	data files.
