#!/usr/bin/perl
use Net::HTTP;

my $s;
my $maxconn=100;
my $counter=$maxconn;
$|=1;

sub newconn {
	$s=undef;
	$s=Net::HTTP->new(Host => "pages.prodigy.net")
		until defined $s;
	$s->keep_alive(1);
	$counter=0;
	print ".";
}

import IO::Handle;
open (OUT, ">>hotlist.$$");
OUT->autoflush();
open (RETRY, ">>retry.$$");
RETRY->autoflush();

while ($page=<>) {
	chomp $page;
	$page=~s/ //g;
	
	if ($counter++ >= $maxconn) {
		newconn();
	}

	eval {
		$s->write_request(HEAD => "/$page/",
			"User-Agent" => "Mozilla/5.0 (X11; Linux x86_64; rv:2.0b10) Gecko/20100101 Firefox/4.0b10",
			"Cookie" => "pwpsunsettgShYEFd1IEQcVtgbNtwog=exists",
		);
		my @ret=$s->read_response_headers;
		my $buf;
		1 while $s->read_entity_body($buf, 1024) > 1;
		if (grep { $_ eq 404 } @ret) {
			#		print "$page no\n";
		}
		elsif (grep { $_ eq 403 } @ret) {
			print "$page 403 forbidden; logging for retry\n";
			print RETRY "$page\n";
		}
		else {
			print "$page yes!\n";
			print OUT $page."\n";
		}
	};
	if ($@) {
		print STDERR "crashed! $@ (on $page; connection reuse $counter) .. skipping\n";
		print RETRY "$page\n";
		newconn();
	}
}

print "clean shutdown!\n";
