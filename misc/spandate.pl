# Converts dates to dd/mm/yyyy form from long-from spanish.
my %months = (
	enero => 1,
	febrero => 2,
	marzo => 3,
	abril => 4,
	mayo => 5,
	junio => 6,
	julio => 7,
	agosto => 8,
	
	augusto => 8,
	septiembre => 9,
	octubre => 10,
	noviembre => 11,
	diciembre => 12
);

sub convertdate {
	my $input=shift;
	$input=~s/no tiene mes/0/;
	my ($mday, $month, $year) = $input =~ 
		/(\d+)\.?\s*(?:de)?\s*(\w+)\s*(?:de)?\s*([0-9, ]+)/;
	$year=~s/[, ]//g;
	$mday=~s/^0//;
	$month=~s/^0//;
	if (int($year) ne $year) {
		print STDERR "bad year: $year ($input)\n";
		return;
	}
	if (int($mday) ne $mday) {
		print STDERR "bad mday: $mday\n";
		return;
	}
	if (int($month) ne $month) {
		if (exists ($months{lc($month)})) {
			$month = $months{lc($month)};
		}
		else {
			print STDERR "bad month: $month\n";
			return;
		}
	}
	return "$mday/$month/$year";
}

while (<>) {
	chomp;
	print convertdate($_)."\n";
}
